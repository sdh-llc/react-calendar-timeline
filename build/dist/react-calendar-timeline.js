(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("react"), require("moment"), require("interact.js"));
	else if(typeof define === 'function' && define.amd)
		define("ReactCalendarTimeline", ["react", "moment", "interact.js"], factory);
	else if(typeof exports === 'object')
		exports["ReactCalendarTimeline"] = factory(require("react"), require("moment"), require("interact.js"));
	else
		root["ReactCalendarTimeline"] = factory(root["React"], root["moment"], root["interact"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_2__, __WEBPACK_EXTERNAL_MODULE_3__, __WEBPACK_EXTERNAL_MODULE_10__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "build/dist/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/*!**********************!*\
  !*** ./src/index.ts ***!
  \**********************/
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var timeline_1 = __webpack_require__(/*! ./lib/timeline */ 1);
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.default = timeline_1.default;
	//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEseUJBQXFCLGdCQUVyQixDQUFDLENBRm9DO0FBRXJDO2tCQUFlLGtCQUFRLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgVGltZWxpbmUgZnJvbSAnLi9saWIvdGltZWxpbmUnXG5cbmV4cG9ydCBkZWZhdWx0IFRpbWVsaW5lXG4iXX0=

/***/ },
/* 1 */
/*!******************************!*\
  !*** ./src/lib/timeline.tsx ***!
  \******************************/
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var React = __webpack_require__(/*! react */ 2);
	var moment = __webpack_require__(/*! moment */ 3);
	__webpack_require__(/*! ./timeline.scss */ 4);
	var items_1 = __webpack_require__(/*! ./items/items */ 8);
	var info_label_1 = __webpack_require__(/*! ./layout/info-label */ 12);
	var sidebar_1 = __webpack_require__(/*! ./layout/sidebar */ 13);
	var header_1 = __webpack_require__(/*! ./layout/header */ 14);
	var vertical_lines_1 = __webpack_require__(/*! ./lines/vertical-lines */ 15);
	var horizontal_lines_1 = __webpack_require__(/*! ./lines/horizontal-lines */ 16);
	var today_line_1 = __webpack_require__(/*! ./lines/today-line */ 17);
	var utils_ts_1 = __webpack_require__(/*! ./utils.ts */ 11);
	var defaultKeys = {
	    groupIdKey: 'id',
	    groupTitleKey: 'title',
	    itemIdKey: 'id',
	    itemTitleKey: 'title',
	    itemDivTitleKey: 'title',
	    itemGroupKey: 'group',
	    itemTimeStartKey: 'start_time',
	    itemTimeEndKey: 'end_time',
	    itemTitleHTMLKey: 'titleHTML',
	};
	var defaultTimeSteps = {
	    second: 1,
	    minute: 1,
	    hour: 1,
	    day: 1,
	    month: 1,
	    year: 1
	};
	var ReactCalendarTimeline = (function (_super) {
	    __extends(ReactCalendarTimeline, _super);
	    function ReactCalendarTimeline(props) {
	        var _this = this;
	        _super.call(this, props);
	        this.touchStart = function (e) {
	            if (e.touches.length === 2) {
	                e.preventDefault();
	                _this.lastTouchDistance = Math.abs(e.touches[0].screenX - e.touches[1].screenX);
	                _this.singleTouchStart = null;
	                _this.lastSingleTouch = null;
	            }
	            else if (e.touches.length === 1 && _this.props.fixedHeader === 'fixed') {
	                e.preventDefault();
	                var x = e.touches[0].clientX;
	                var y = e.touches[0].clientY;
	                _this.lastTouchDistance = null;
	                _this.singleTouchStart = { x: x, y: y, screenY: window.pageYOffset };
	                _this.lastSingleTouch = { x: x, y: y, screenY: window.pageYOffset };
	            }
	        };
	        this.touchMove = function (e) {
	            if (_this.state.dragTime || _this.state.resizeEnd) {
	                e.preventDefault();
	                return;
	            }
	            if (_this.lastTouchDistance && e.touches.length === 2) {
	                e.preventDefault();
	                var touchDistance = Math.abs(e.touches[0].screenX - e.touches[1].screenX);
	                var parentPosition = utils_ts_1.getParentPosition(e.currentTarget);
	                var xPosition = (e.touches[0].screenX + e.touches[1].screenX) / 2 - parentPosition.x;
	                if (touchDistance !== 0 && _this.lastTouchDistance !== 0) {
	                    _this.changeZoom(_this.lastTouchDistance / touchDistance, xPosition / _this.state.width);
	                    _this.lastTouchDistance = touchDistance;
	                }
	            }
	            else if (_this.lastSingleTouch && e.touches.length === 1 && _this.props.fixedHeader === 'fixed') {
	                e.preventDefault();
	                var x = e.touches[0].clientX;
	                var y = e.touches[0].clientY;
	                var deltaX = x - _this.lastSingleTouch.x;
	                // let deltaY = y - this.lastSingleTouch.y
	                var deltaX0 = x - _this.singleTouchStart.x;
	                var deltaY0 = y - _this.singleTouchStart.y;
	                _this.lastSingleTouch = { x: x, y: y };
	                var moveX = Math.abs(deltaX0) * 3 > Math.abs(deltaY0);
	                var moveY = Math.abs(deltaY0) * 3 > Math.abs(deltaX0);
	                if (deltaX !== 0 && moveX) {
	                    _this.refs.scrollComponent.scrollLeft -= deltaX;
	                }
	                if (moveY) {
	                    window.scrollTo(window.pageXOffset, _this.singleTouchStart.screenY - deltaY0);
	                }
	            }
	        };
	        this.touchEnd = function (e) {
	            if (_this.lastTouchDistance) {
	                e.preventDefault();
	                _this.lastTouchDistance = null;
	            }
	            if (_this.lastSingleTouch) {
	                e.preventDefault();
	                _this.lastSingleTouch = null;
	                _this.singleTouchStart = null;
	            }
	        };
	        this.onScroll = function () {
	            var scrollComponent = _this.refs.scrollComponent;
	            var canvasTimeStart = _this.state.canvasTimeStart;
	            var scrollX = scrollComponent.scrollLeft;
	            var zoom = _this.state.visibleTimeEnd - _this.state.visibleTimeStart;
	            var width = _this.state.width;
	            var visibleTimeStart = canvasTimeStart + (zoom * scrollX / width);
	            // move the virtual canvas if needed
	            if (scrollX < _this.state.width * 0.5) {
	                _this.setState({
	                    canvasTimeStart: _this.state.canvasTimeStart - zoom
	                });
	                scrollComponent.scrollLeft += _this.state.width;
	            }
	            if (scrollX > _this.state.width * 1.5) {
	                _this.setState({
	                    canvasTimeStart: _this.state.canvasTimeStart + zoom
	                });
	                scrollComponent.scrollLeft -= _this.state.width;
	            }
	            if (_this.state.visibleTimeStart !== visibleTimeStart || _this.state.visibleTimeEnd !== visibleTimeStart + zoom) {
	                _this.props.onTimeChange.bind(_this)(visibleTimeStart, visibleTimeStart + zoom);
	            }
	        };
	        this.onWheel = function (e) {
	            var traditionalZoom = _this.props.traditionalZoom;
	            if (e.ctrlKey) {
	                e.preventDefault();
	                var parentPosition = utils_ts_1.getParentPosition(e.currentTarget);
	                var xPosition = e.clientX - parentPosition.x;
	                _this.changeZoom(1.0 + e.deltaY / 50, xPosition / _this.state.width);
	            }
	            else if (e.shiftKey) {
	                e.preventDefault();
	                var scrollComponent = _this.refs.scrollComponent;
	                scrollComponent.scrollLeft += e.deltaY;
	            }
	            else if (e.altKey) {
	                var parentPosition = utils_ts_1.getParentPosition(e.currentTarget);
	                var xPosition = e.clientX - parentPosition.x;
	                _this.changeZoom(1.0 + e.deltaY / 500, xPosition / _this.state.width);
	            }
	            else {
	                if (_this.props.fixedHeader === 'fixed') {
	                    e.preventDefault();
	                    if (e.deltaX !== 0) {
	                        if (!traditionalZoom) {
	                            _this.refs.scrollComponent.scrollLeft += e.deltaX;
	                        }
	                    }
	                    if (e.deltaY !== 0) {
	                        window.scrollTo(window.pageXOffset, window.pageYOffset + e.deltaY);
	                        if (traditionalZoom) {
	                            var parentPosition = utils_ts_1.getParentPosition(e.currentTarget);
	                            var xPosition = e.clientX - parentPosition.x;
	                            _this.changeZoom(1.0 + e.deltaY / 50, xPosition / _this.state.width);
	                        }
	                    }
	                }
	            }
	        };
	        this.showPeriod = function (from, unit) {
	            var visibleTimeStart = from.valueOf();
	            var visibleTimeEnd = moment(from).add(1, unit).valueOf();
	            var zoom = visibleTimeEnd - visibleTimeStart;
	            // can't zoom in more than to show one hour
	            if (zoom < 360000) {
	                return;
	            }
	            // clicked on the big header and already focused here, zoom out
	            if (unit !== 'year' && _this.state.visibleTimeStart === visibleTimeStart && _this.state.visibleTimeEnd === visibleTimeEnd) {
	                var nextUnit = utils_ts_1.getNextUnit(unit);
	                visibleTimeStart = from.startOf(nextUnit).valueOf();
	                visibleTimeEnd = moment(visibleTimeStart).add(1, nextUnit).valueOf(); // TODO: check type of visibleTimeEnd
	                zoom = visibleTimeEnd - visibleTimeStart;
	            }
	            _this.props.onTimeChange.bind(_this)(visibleTimeStart, visibleTimeStart + zoom);
	        };
	        this.selectItem = function (item, clickType, e) {
	            if (_this.state.selectedItem === item || (_this.props.itemTouchSendsClick && clickType === 'touch')) {
	                if (item && _this.props.onItemClick) {
	                    _this.props.onItemClick(item, e);
	                }
	            }
	            else {
	                _this.setState({ selectedItem: item });
	                if (item && _this.props.onItemSelect) {
	                    _this.props.onItemSelect(item, e);
	                }
	            }
	        };
	        this.scrollAreaClick = function (e) {
	            // if not clicking on an item
	            if (!utils_ts_1.hasSomeParentTheClass(e.target, 'rct-item')) {
	                if (_this.state.selectedItem) {
	                    _this.selectItem(null);
	                }
	                else if (_this.props.onCanvasClick) {
	                    var _a = _this.rowAndTimeFromEvent(e), row = _a[0], time = _a[1];
	                    if (row >= 0 && row < _this.props.groups.length) {
	                        var groupId = utils_ts_1._get(_this.props.groups[row], _this.props.keys.groupIdKey);
	                        _this.props.onCanvasClick(groupId, time, e);
	                    }
	                }
	            }
	        };
	        this.dragItem = function (item, dragTime, newGroupOrder) {
	            var newGroup = _this.props.groups[newGroupOrder];
	            var keys = _this.props.keys;
	            _this.setState({
	                draggingItem: item,
	                dragTime: dragTime,
	                newGroupOrder: newGroupOrder,
	                dragGroupTitle: newGroup ? utils_ts_1._get(newGroup, keys.groupTitleKey) : ''
	            });
	        };
	        this.dropItem = function (item, dragTime, newGroupOrder) {
	            _this.setState({ draggingItem: null, dragTime: null, dragGroupTitle: null });
	            if (_this.props.onItemMove) {
	                _this.props.onItemMove(item, dragTime, newGroupOrder);
	            }
	        };
	        this.resizingItem = function (item, newResizeEnd) {
	            _this.setState({
	                resizingItem: item,
	                resizeEnd: newResizeEnd
	            });
	        };
	        this.resizedItem = function (item, newResizeEnd) {
	            _this.setState({ resizingItem: null, resizeEnd: null });
	            if (_this.props.onItemResize) {
	                _this.props.onItemResize(item, newResizeEnd);
	            }
	        };
	        this.handleMouseDown = function (e) {
	            var topOffset = _this.state.topOffset;
	            var pageY = e.pageY;
	            var _a = _this.props, headerLabelGroupHeight = _a.headerLabelGroupHeight, headerLabelHeight = _a.headerLabelHeight;
	            var headerHeight = headerLabelGroupHeight + headerLabelHeight;
	            if (pageY - topOffset > headerHeight) {
	                _this.setState({ isDragging: true, dragStartPosition: e.pageX, dragLastPosition: e.pageX });
	            }
	        };
	        this.handleMouseMove = function (e) {
	            if (e.buttons === 0 && _this.state.isDragging) {
	                // disable dragging if mouse buttons are not pressed
	                _this.setState({ isDragging: false, dragStartPosition: null, dragLastPosition: null });
	            }
	            else if (_this.state.isDragging && !_this.state.draggingItem && !_this.state.resizingItem) {
	                _this.refs.scrollComponent.scrollLeft += _this.state.dragLastPosition - e.pageX;
	                _this.setState({ dragLastPosition: e.pageX });
	            }
	        };
	        this.handleMouseUp = function (e) {
	            var dragStartPosition = _this.state.dragStartPosition;
	            if (Math.abs(dragStartPosition - e.pageX) <= _this.props.clickTolerance) {
	                _this.scrollAreaClick(e);
	            }
	            _this.setState({ isDragging: false, dragStartPosition: null, dragLastPosition: null });
	        };
	        this.handleDoubleClick = function (e) {
	            var _a = _this.state, canvasTimeStart = _a.canvasTimeStart, width = _a.width, visibleTimeStart = _a.visibleTimeStart, visibleTimeEnd = _a.visibleTimeEnd, groupTops = _a.groupTops, topOffset = _a.topOffset;
	            var zoom = visibleTimeEnd - visibleTimeStart;
	            var canvasTimeEnd = canvasTimeStart + zoom * 3;
	            var canvasWidth = width * 3;
	            var pageX = e.pageX, pageY = e.pageY;
	            var ratio = (canvasTimeEnd - canvasTimeStart) / canvasWidth;
	            var boundingRect = _this.refs.scrollComponent.getBoundingClientRect();
	            var timePosition = visibleTimeStart + ratio * (pageX - boundingRect.left);
	            if (_this.props.dragSnap) {
	                timePosition = Math.round(timePosition / _this.props.dragSnap) * _this.props.dragSnap;
	            }
	            var groupIndex = 0;
	            for (var _i = 0, _b = Object.keys(groupTops); _i < _b.length; _i++) {
	                var key = _b[_i];
	                var item = groupTops[key];
	                if (pageY - topOffset > item) {
	                    groupIndex = parseInt(key, 10);
	                }
	                else {
	                    break;
	                }
	            }
	            if (_this.props.onCanvasDoubleClick) {
	                _this.props.onCanvasDoubleClick(timePosition, _this.props.groups[groupIndex]);
	            }
	        };
	        var visibleTimeStart = null;
	        var visibleTimeEnd = null;
	        if (this.props.defaultTimeStart && this.props.defaultTimeEnd) {
	            visibleTimeStart = this.props.defaultTimeStart.valueOf();
	            visibleTimeEnd = this.props.defaultTimeEnd.valueOf();
	        }
	        else if (this.props.visibleTimeStart && this.props.visibleTimeEnd) {
	            visibleTimeStart = this.props.visibleTimeStart;
	            visibleTimeEnd = this.props.visibleTimeEnd;
	        }
	        else {
	            visibleTimeStart = Math.min.apply(Math, this.props.items.map(function (item) { return utils_ts_1._get(item, 'start').getTime(); }));
	            visibleTimeEnd = Math.max.apply(Math, this.props.items.map(function (item) { return utils_ts_1._get(item, 'end').getTime(); }));
	            if (!visibleTimeStart || !visibleTimeEnd) {
	                visibleTimeStart = new Date().getTime() - 86400 * 7 * 1000;
	                visibleTimeEnd = new Date().getTime() + 86400 * 7 * 1000;
	            }
	            if (this.props.onTimeInit) {
	                this.props.onTimeInit(visibleTimeStart, visibleTimeEnd);
	            }
	        }
	        this.state = {
	            width: 1000,
	            visibleTimeStart: visibleTimeStart,
	            visibleTimeEnd: visibleTimeEnd,
	            canvasTimeStart: visibleTimeStart - (visibleTimeEnd - visibleTimeStart),
	            selectedItem: null,
	            dragTime: null,
	            dragGroupTitle: null,
	            resizeEnd: null,
	            isDragging: false,
	            topOffset: 0,
	            resizingItem: null
	        };
	        var _a = this.stackItems(props.items, props.groups, this.state.canvasTimeStart, this.state.visibleTimeStart, this.state.visibleTimeEnd, this.state.width), dimensionItems = _a.dimensionItems, height = _a.height, groupHeights = _a.groupHeights, groupTops = _a.groupTops;
	        this.state.dimensionItems = dimensionItems;
	        this.state.height = height;
	        this.state.groupHeights = groupHeights;
	        this.state.groupTops = groupTops;
	    }
	    ReactCalendarTimeline.prototype.componentDidMount = function () {
	        var _this = this;
	        this.resize();
	        this.resizeEventListener = {
	            handleEvent: function (event) {
	                _this.resize();
	            }
	        };
	        window.addEventListener('resize', this.resizeEventListener);
	        this.lastTouchDistance = null;
	        this.refs['scrollComponent']['addEventListener']('touchstart', this.touchStart);
	        this.refs['scrollComponent']['addEventListener']('touchmove', this.touchMove);
	        this.refs['scrollComponent']['addEventListener']('touchend', this.touchEnd);
	    };
	    ReactCalendarTimeline.prototype.componentWillUnmount = function () {
	        window.removeEventListener('resize', this.resizeEventListener);
	        this.refs['scrollComponent']['removeEventListener']('touchstart', this.touchStart);
	        this.refs['scrollComponent']['removeEventListener']('touchmove', this.touchMove);
	        this.refs['scrollComponent']['removeEventListener']('touchend', this.touchEnd);
	    };
	    ReactCalendarTimeline.prototype.resize = function () {
	        // FIXME currently when the component creates a scroll the scrollbar is not used in the initial width calculation, resizing fixes this
	        var _a = this.refs.container.getBoundingClientRect(), containerWidth = _a.width, containerTop = _a.top;
	        var width = containerWidth - this.props.sidebarWidth;
	        var _b = this.stackItems(this.props.items, this.props.groups, this.state.canvasTimeStart, this.state.visibleTimeStart, this.state.visibleTimeEnd, width), dimensionItems = _b.dimensionItems, height = _b.height, groupHeights = _b.groupHeights, groupTops = _b.groupTops;
	        this.setState({
	            width: width,
	            topOffset: containerTop + window.pageYOffset,
	            dimensionItems: dimensionItems,
	            height: height,
	            groupHeights: groupHeights,
	            groupTops: groupTops
	        });
	        this.refs.scrollComponent.scrollLeft = width;
	    };
	    ReactCalendarTimeline.prototype.componentWillReceiveProps = function (nextProps) {
	        var visibleTimeStart = nextProps.visibleTimeStart, visibleTimeEnd = nextProps.visibleTimeEnd, items = nextProps.items, groups = nextProps.groups;
	        if (visibleTimeStart && visibleTimeEnd) {
	            this.updateScrollCanvas(visibleTimeStart, visibleTimeEnd, items !== this.props.items || groups !== this.props.groups, items, groups);
	        }
	        if (items !== this.props.items || groups !== this.props.groups) {
	            this.updateDimensions(items, groups);
	        }
	    };
	    ReactCalendarTimeline.prototype.updateDimensions = function (items, groups) {
	        var _a = this.state, canvasTimeStart = _a.canvasTimeStart, visibleTimeStart = _a.visibleTimeStart, visibleTimeEnd = _a.visibleTimeEnd, width = _a.width;
	        var _b = this.stackItems(items, groups, canvasTimeStart, visibleTimeStart, visibleTimeEnd, width), dimensionItems = _b.dimensionItems, height = _b.height, groupHeights = _b.groupHeights, groupTops = _b.groupTops;
	        this.setState({ dimensionItems: dimensionItems, height: height, groupHeights: groupHeights, groupTops: groupTops });
	    };
	    ReactCalendarTimeline.prototype.updateScrollCanvas = function (visibleTimeStart, visibleTimeEnd, forceUpdateDimensions, updatedItems, updatedGroups) {
	        var oldCanvasTimeStart = this.state.canvasTimeStart;
	        var oldZoom = this.state.visibleTimeEnd - this.state.visibleTimeStart;
	        var newZoom = visibleTimeEnd - visibleTimeStart;
	        var items = updatedItems || this.props.items;
	        var groups = updatedGroups || this.props.groups;
	        var newState = {
	            visibleTimeStart: visibleTimeStart,
	            visibleTimeEnd: visibleTimeEnd
	        };
	        var resetCanvas = false;
	        var canKeepCanvas = visibleTimeStart >= oldCanvasTimeStart + oldZoom * 0.5 &&
	            visibleTimeStart <= oldCanvasTimeStart + oldZoom * 1.5 &&
	            visibleTimeEnd >= oldCanvasTimeStart + oldZoom * 1.5 &&
	            visibleTimeEnd <= oldCanvasTimeStart + oldZoom * 2.5;
	        // if new visible time is in the right canvas area
	        if (canKeepCanvas) {
	            // but we need to update the scroll
	            var newScrollLeft = Math.round(this.state.width * (visibleTimeStart - oldCanvasTimeStart) / newZoom);
	            if (this.refs.scrollComponent.scrollLeft !== newScrollLeft) {
	                resetCanvas = true;
	            }
	        }
	        else {
	            resetCanvas = true;
	        }
	        if (resetCanvas) {
	            // Todo: need to calculate new dimensions
	            newState.canvasTimeStart = visibleTimeStart - newZoom;
	            this.refs.scrollComponent.scrollLeft = this.state.width;
	            if (this.props.onBoundsChange) {
	                this.props.onBoundsChange(newState.canvasTimeStart, newState.canvasTimeStart + newZoom * 3);
	            }
	        }
	        if (resetCanvas || forceUpdateDimensions) {
	            var canvasTimeStart = newState.canvasTimeStart ? newState.canvasTimeStart : oldCanvasTimeStart;
	            var _a = this.stackItems(items, groups, canvasTimeStart, visibleTimeStart, visibleTimeEnd, this.state.width), dimensionItems = _a.dimensionItems, height = _a.height, groupHeights = _a.groupHeights, groupTops = _a.groupTops;
	            newState.dimensionItems = dimensionItems;
	            newState.height = height;
	            newState.groupHeights = groupHeights;
	            newState.groupTops = groupTops;
	        }
	        this.setState(newState);
	    };
	    ReactCalendarTimeline.prototype.zoomIn = function (e) {
	        e.preventDefault();
	        this.changeZoom(0.75);
	    };
	    ReactCalendarTimeline.prototype.zoomOut = function (e) {
	        e.preventDefault();
	        this.changeZoom(1.25);
	    };
	    ReactCalendarTimeline.prototype.changeZoom = function (scale, offset) {
	        if (offset === void 0) { offset = 0.5; }
	        var _a = this.props, minZoom = _a.minZoom, maxZoom = _a.maxZoom;
	        var oldZoom = this.state.visibleTimeEnd - this.state.visibleTimeStart;
	        var newZoom = Math.min(Math.max(Math.round(oldZoom * scale), minZoom), maxZoom); // min 1 min, max 20 years
	        var newVisibleTimeStart = Math.round(this.state.visibleTimeStart + (oldZoom - newZoom) * offset);
	        this.props.onTimeChange.bind(this)(newVisibleTimeStart, newVisibleTimeStart + newZoom);
	    };
	    ReactCalendarTimeline.prototype.rowAndTimeFromEvent = function (e) {
	        var _a = this.props, lineHeight = _a.lineHeight, dragSnap = _a.dragSnap;
	        var _b = this.state, width = _b.width, visibleTimeStart = _b.visibleTimeStart, visibleTimeEnd = _b.visibleTimeEnd;
	        var parentPosition = utils_ts_1.getParentPosition(e.currentTarget);
	        var x = e.clientX - parentPosition.x;
	        var y = e.clientY - parentPosition.y;
	        var row = Math.floor((y - (lineHeight * 2)) / lineHeight);
	        var time = Math.round(visibleTimeStart + x / width * (visibleTimeEnd - visibleTimeStart));
	        time = Math.floor(time / dragSnap) * dragSnap;
	        return [row, time];
	    };
	    ReactCalendarTimeline.prototype.todayLine = function (canvasTimeStart, zoom, canvasTimeEnd, canvasWidth, minUnit, height, headerHeight) {
	        return (React.createElement(today_line_1.default, {canvasTimeStart: canvasTimeStart, canvasTimeEnd: canvasTimeEnd, canvasWidth: canvasWidth, lineHeight: this.props.lineHeight, lineCount: utils_ts_1._length(this.props.groups), height: height, headerHeight: headerHeight}));
	    };
	    ReactCalendarTimeline.prototype.verticalLines = function (canvasTimeStart, zoom, canvasTimeEnd, canvasWidth, minUnit, timeSteps, height, headerHeight) {
	        return (React.createElement(vertical_lines_1.default, {canvasTimeStart: canvasTimeStart, canvasTimeEnd: canvasTimeEnd, canvasWidth: canvasWidth, lineHeight: this.props.lineHeight, lineCount: utils_ts_1._length(this.props.groups), minUnit: minUnit, timeSteps: timeSteps, fixedHeader: this.props.fixedHeader, height: height, headerHeight: headerHeight}));
	    };
	    ReactCalendarTimeline.prototype.horizontalLines = function (canvasTimeStart, zoom, canvasTimeEnd, canvasWidth, groupHeights, headerHeight) {
	        return (React.createElement(horizontal_lines_1.default, {canvasWidth: canvasWidth, lineHeight: this.props.lineHeight, lineCount: utils_ts_1._length(this.props.groups), groups: this.props.groups, groupHeights: groupHeights, headerHeight: headerHeight}));
	    };
	    ReactCalendarTimeline.prototype.items = function (canvasTimeStart, zoom, canvasTimeEnd, canvasWidth, minUnit, dimensionItems, groupHeights, groupTops) {
	        return (React.createElement(items_1.default, {canvasTimeStart: canvasTimeStart, canvasTimeEnd: canvasTimeEnd, canvasWidth: canvasWidth, lineHeight: this.props.lineHeight, lineCount: utils_ts_1._length(this.props.groups), dimensionItems: dimensionItems, minUnit: minUnit, groupHeights: groupHeights, groupTops: groupTops, items: this.props.items, groups: this.props.groups, keys: this.props.keys, selectedItem: this.state.selectedItem, dragSnap: this.props.dragSnap, minResizeWidth: this.props.minResizeWidth, canChangeGroup: this.props.canChangeGroup, canMove: this.props.canMove, canResize: this.props.canResize, useResizeHandle: this.props.useResizeHandle, moveResizeValidator: this.props.moveResizeValidator, topOffset: this.state.topOffset, itemSelect: this.selectItem, itemDrag: this.dragItem, itemDrop: this.dropItem, onItemDoubleClick: this.props.onItemDoubleClick, onItemContextMenu: this.props.onItemContextMenu, itemResizing: this.resizingItem, itemResized: this.resizedItem, hideClippedTitle: this.props.hideClippedTitle}));
	    };
	    ReactCalendarTimeline.prototype.infoLabel = function () {
	        var label = null;
	        if (this.state.dragTime) {
	            label = moment(this.state.dragTime).format('LLL') + ", " + this.state.dragGroupTitle;
	        }
	        else if (this.state.resizeEnd) {
	            label = moment(this.state.resizeEnd).format('LLL');
	        }
	        return label ? React.createElement(info_label_1.default, {label: label}) : '';
	    };
	    ReactCalendarTimeline.prototype.header = function (canvasTimeStart, zoom, canvasTimeEnd, canvasWidth, minUnit, timeSteps, headerLabelGroupHeight, headerLabelHeight) {
	        return (React.createElement(header_1.default, {canvasTimeStart: canvasTimeStart, canvasTimeEnd: canvasTimeEnd, canvasWidth: canvasWidth, lineHeight: this.props.lineHeight, minUnit: minUnit, timeSteps: timeSteps, headerLabelGroupHeight: headerLabelGroupHeight, headerLabelHeight: headerLabelHeight, width: this.state.width, zoom: zoom, visibleTimeStart: this.state.visibleTimeStart, visibleTimeEnd: this.state.visibleTimeEnd, fixedHeader: this.props.fixedHeader, zIndex: this.props.zIndexStart + 1, showPeriod: this.showPeriod}));
	    };
	    ReactCalendarTimeline.prototype.sidebar = function (height, groupHeights, headerHeight) {
	        return (React.createElement(sidebar_1.default, {groups: this.props.groups, keys: this.props.keys, width: this.props.sidebarWidth, lineHeight: this.props.lineHeight, groupHeights: groupHeights, height: height, headerHeight: headerHeight, fixedHeader: this.props.fixedHeader, zIndex: this.props.zIndexStart + 2}, this.props.children));
	    };
	    ReactCalendarTimeline.prototype.stackItems = function (items, groups, canvasTimeStart, visibleTimeStart, visibleTimeEnd, width) {
	        var _a = this.props, keys = _a.keys, dragSnap = _a.dragSnap, lineHeight = _a.lineHeight, headerLabelGroupHeight = _a.headerLabelGroupHeight, headerLabelHeight = _a.headerLabelHeight, stackItems = _a.stackItems, itemHeightRatio = _a.itemHeightRatio;
	        var _b = this.state, draggingItem = _b.draggingItem, dragTime = _b.dragTime, resizingItem = _b.resizingItem, resizeEnd = _b.resizeEnd, newGroupOrder = _b.newGroupOrder;
	        var zoom = visibleTimeEnd - visibleTimeStart;
	        var canvasTimeEnd = canvasTimeStart + zoom * 3;
	        var canvasWidth = width * 3;
	        var headerHeight = headerLabelGroupHeight + headerLabelHeight;
	        var visibleItems = utils_ts_1.getVisibleItems(items, canvasTimeStart, canvasTimeEnd, keys);
	        var groupOrders = utils_ts_1.getGroupOrders(groups, keys);
	        var dimensionItems = visibleItems.map(function (item) {
	            return {
	                id: utils_ts_1._get(item, keys.itemIdKey),
	                dimensions: utils_ts_1.calculateDimensions(item, groupOrders[utils_ts_1._get(item, keys.itemGroupKey)], keys, canvasTimeStart, canvasTimeEnd, canvasWidth, dragSnap, lineHeight, draggingItem, dragTime, resizingItem, resizeEnd, newGroupOrder, itemHeightRatio)
	            };
	        });
	        var stackingMethod;
	        if (stackItems) {
	            stackingMethod = utils_ts_1.stack;
	        }
	        else {
	            stackingMethod = utils_ts_1.nostack;
	        }
	        var _c = stackingMethod(dimensionItems, groupOrders, lineHeight, headerHeight), height = _c.height, groupHeights = _c.groupHeights, groupTops = _c.groupTops;
	        return { dimensionItems: dimensionItems, height: height, groupHeights: groupHeights, groupTops: groupTops };
	    };
	    ReactCalendarTimeline.prototype.render = function () {
	        var _a = this.props, items = _a.items, groups = _a.groups, headerLabelGroupHeight = _a.headerLabelGroupHeight, headerLabelHeight = _a.headerLabelHeight, sidebarWidth = _a.sidebarWidth, timeSteps = _a.timeSteps;
	        var _b = this.state, draggingItem = _b.draggingItem, resizingItem = _b.resizingItem, isDragging = _b.isDragging, width = _b.width, visibleTimeStart = _b.visibleTimeStart, visibleTimeEnd = _b.visibleTimeEnd, canvasTimeStart = _b.canvasTimeStart;
	        var _c = this.state, dimensionItems = _c.dimensionItems, height = _c.height, groupHeights = _c.groupHeights, groupTops = _c.groupTops;
	        var zoom = visibleTimeEnd - visibleTimeStart;
	        var canvasTimeEnd = canvasTimeStart + zoom * 3;
	        var canvasWidth = width * 3;
	        var minUnit = utils_ts_1.getMinUnit(zoom, width, timeSteps);
	        var headerHeight = headerLabelGroupHeight + headerLabelHeight;
	        if (draggingItem || resizingItem) {
	            var stackResults = this.stackItems(items, groups, canvasTimeStart, visibleTimeStart, visibleTimeEnd, width);
	            dimensionItems = stackResults.dimensionItems;
	            height = stackResults.height;
	            groupHeights = stackResults.groupHeights;
	            groupTops = stackResults.groupTops;
	        }
	        var outerComponentStyle = {
	            height: height + "px"
	        };
	        var scrollComponentStyle = {
	            width: width + "px",
	            height: (height + 20) + "px",
	            cursor: isDragging ? 'move' : 'default'
	        };
	        var canvasComponentStyle = {
	            width: canvasWidth + "px",
	            height: height + "px"
	        };
	        return (React.createElement("div", {style: this.props.style, ref: 'container', className: 'react-calendar-timeline'}, React.createElement("div", {style: outerComponentStyle, className: 'rct-outer'}, sidebarWidth > 0 ? this.sidebar(height, groupHeights, headerHeight) : null, React.createElement("div", {ref: 'scrollComponent', className: 'rct-scroll', style: scrollComponentStyle, onClick: this.scrollAreaClick, onScroll: this.onScroll, onWheel: this.onWheel, onMouseDown: this.handleMouseDown, onMouseMove: this.handleMouseMove, onMouseUp: this.handleMouseUp, onMouseLeave: this.handleMouseUp, onDragStart: function (e) {
	            // disable dragging
	            e.preventDefault();
	            e.stopPropagation();
	        }}, React.createElement("div", {ref: 'canvasComponent', className: 'rct-canvas', style: canvasComponentStyle, onDoubleClick: this.handleDoubleClick}, this.items(canvasTimeStart, zoom, canvasTimeEnd, canvasWidth, minUnit, dimensionItems, groupHeights, groupTops), this.verticalLines(canvasTimeStart, zoom, canvasTimeEnd, canvasWidth, minUnit, timeSteps, height, headerHeight), this.horizontalLines(canvasTimeStart, zoom, canvasTimeEnd, canvasWidth, groupHeights, headerHeight), this.todayLine(canvasTimeStart, zoom, canvasTimeEnd, canvasWidth, minUnit, height, headerHeight), this.infoLabel(), this.header(canvasTimeStart, zoom, canvasTimeEnd, canvasWidth, minUnit, timeSteps, headerLabelGroupHeight, headerLabelHeight))))));
	    };
	    ReactCalendarTimeline.defaultProps = {
	        sidebarWidth: 150,
	        dragSnap: 1000 * 60 * 15,
	        minResizeWidth: 20,
	        fixedHeader: 'none',
	        zIndexStart: 10,
	        lineHeight: 30,
	        headerLabelGroupHeight: 30,
	        headerLabelHeight: 30,
	        itemHeightRatio: 0.65,
	        minZoom: 60 * 60 * 1000,
	        maxZoom: 5 * 365.24 * 86400 * 1000,
	        canChangeGroup: true,
	        canMove: true,
	        canResize: true,
	        useResizeHandle: false,
	        stackItems: false,
	        traditionalZoom: false,
	        onItemMove: null,
	        onItemResize: null,
	        onItemClick: null,
	        onItemSelect: null,
	        onCanvasClick: null,
	        onItemDoubleClick: null,
	        onItemContextMenu: null,
	        moveResizeValidator: null,
	        dayBackground: null,
	        defaultTimeStart: null,
	        defaultTimeEnd: null,
	        itemTouchSendsClick: false,
	        style: {},
	        keys: defaultKeys,
	        timeSteps: defaultTimeSteps,
	        // if you pass in visibleTimeStart and visibleTimeEnd, you must also pass onTimeChange(visibleTimeStart, visibleTimeEnd),
	        // which needs to update the props visibleTimeStart and visibleTimeEnd to the ones passed
	        visibleTimeStart: null,
	        visibleTimeEnd: null,
	        onTimeChange: function (visibleTimeStart, visibleTimeEnd) {
	            this.updateScrollCanvas(visibleTimeStart, visibleTimeEnd);
	        },
	        // called after the calendar loads and the visible time has been calculated
	        onTimeInit: null,
	        // called when the canvas area of the calendar changes
	        onBoundsChange: null,
	        children: null,
	        hideClippedTitle: true,
	        clickTolerance: 3,
	    };
	    return ReactCalendarTimeline;
	}(React.Component));
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.default = ReactCalendarTimeline;
	//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGltZWxpbmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ0aW1lbGluZS50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsSUFBWSxLQUFLLFdBQU0sT0FDdkIsQ0FBQyxDQUQ2QjtBQUM5QixJQUFZLE1BQU0sV0FBTSxRQUN4QixDQUFDLENBRCtCO0FBQ2hDLFFBQU8saUJBRVAsQ0FBQyxDQUZ1QjtBQUV4QixzQkFBa0IsZUFDbEIsQ0FBQyxDQURnQztBQUNqQywyQkFBc0IscUJBQ3RCLENBQUMsQ0FEMEM7QUFDM0Msd0JBQW9CLGtCQUNwQixDQUFDLENBRHFDO0FBQ3RDLHVCQUFtQixpQkFDbkIsQ0FBQyxDQURtQztBQUNwQywrQkFBMEIsd0JBQzFCLENBQUMsQ0FEaUQ7QUFDbEQsaUNBQTRCLDBCQUM1QixDQUFDLENBRHFEO0FBQ3RELDJCQUFzQixvQkFFdEIsQ0FBQyxDQUZ5QztBQUUxQyx5QkFJTyxZQUdQLENBQUMsQ0FIa0I7QUFHbkIsSUFBTSxXQUFXLEdBQUc7SUFDbEIsVUFBVSxFQUFFLElBQUk7SUFDaEIsYUFBYSxFQUFFLE9BQU87SUFDdEIsU0FBUyxFQUFFLElBQUk7SUFDZixZQUFZLEVBQUUsT0FBTztJQUNyQixlQUFlLEVBQUUsT0FBTztJQUN4QixZQUFZLEVBQUUsT0FBTztJQUNyQixnQkFBZ0IsRUFBRSxZQUFZO0lBQzlCLGNBQWMsRUFBRSxVQUFVO0lBQzFCLGdCQUFnQixFQUFFLFdBQVc7Q0FDOUIsQ0FBQztBQUVGLElBQU0sZ0JBQWdCLEdBQUc7SUFDdkIsTUFBTSxFQUFFLENBQUM7SUFDVCxNQUFNLEVBQUUsQ0FBQztJQUNULElBQUksRUFBRSxDQUFDO0lBQ1AsR0FBRyxFQUFFLENBQUM7SUFDTixLQUFLLEVBQUUsQ0FBQztJQUNSLElBQUksRUFBRSxDQUFDO0NBQ1IsQ0FBQztBQW1HRjtJQUFtRCx5Q0FBNkI7SUF5RTVFLCtCQUFhLEtBQUs7UUF6RXRCLGlCQW16QkM7UUF6dUJPLGtCQUFNLEtBQUssQ0FBQyxDQUFDO1FBNEVuQixlQUFVLEdBQUcsVUFBQyxDQUFDO1lBQ2IsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDM0IsQ0FBQyxDQUFDLGNBQWMsRUFBRSxDQUFDO2dCQUVuQixLQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUMvRSxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO2dCQUM3QixLQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQTtZQUM3QixDQUFDO1lBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsTUFBTSxLQUFLLENBQUMsSUFBSSxLQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsS0FBSyxPQUFPLENBQUMsQ0FBQyxDQUFDO2dCQUN4RSxDQUFDLENBQUMsY0FBYyxFQUFFLENBQUM7Z0JBRW5CLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDO2dCQUM3QixJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQztnQkFFN0IsS0FBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztnQkFDOUIsS0FBSSxDQUFDLGdCQUFnQixHQUFHLEVBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLE9BQU8sRUFBRSxNQUFNLENBQUMsV0FBVyxFQUFDLENBQUM7Z0JBQ2xFLEtBQUksQ0FBQyxlQUFlLEdBQUcsRUFBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsT0FBTyxFQUFFLE1BQU0sQ0FBQyxXQUFXLEVBQUMsQ0FBQTtZQUNsRSxDQUFDO1FBQ0gsQ0FBQyxDQUFDO1FBRUYsY0FBUyxHQUFHLFVBQUMsQ0FBQztZQUNaLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxJQUFJLEtBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztnQkFDaEQsQ0FBQyxDQUFDLGNBQWMsRUFBRSxDQUFDO2dCQUNuQixNQUFNLENBQUE7WUFDUixDQUFDO1lBQ0QsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLGlCQUFpQixJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3JELENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztnQkFFbkIsSUFBSSxhQUFhLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUUxRSxJQUFJLGNBQWMsR0FBRyw0QkFBaUIsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQ3hELElBQUksU0FBUyxHQUFHLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsY0FBYyxDQUFDLENBQUMsQ0FBQztnQkFFckYsRUFBRSxDQUFDLENBQUMsYUFBYSxLQUFLLENBQUMsSUFBSSxLQUFJLENBQUMsaUJBQWlCLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDeEQsS0FBSSxDQUFDLFVBQVUsQ0FBQyxLQUFJLENBQUMsaUJBQWlCLEdBQUcsYUFBYSxFQUFFLFNBQVMsR0FBRyxLQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUN0RixLQUFJLENBQUMsaUJBQWlCLEdBQUcsYUFBYSxDQUFBO2dCQUN4QyxDQUFDO1lBQ0gsQ0FBQztZQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsZUFBZSxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsTUFBTSxLQUFLLENBQUMsSUFBSSxLQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsS0FBSyxPQUFPLENBQUMsQ0FBQyxDQUFDO2dCQUNoRyxDQUFDLENBQUMsY0FBYyxFQUFFLENBQUM7Z0JBRW5CLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDO2dCQUM3QixJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQztnQkFFN0IsSUFBSSxNQUFNLEdBQUcsQ0FBQyxHQUFHLEtBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDO2dCQUN4QywwQ0FBMEM7Z0JBRTFDLElBQUksT0FBTyxHQUFHLENBQUMsR0FBRyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO2dCQUMxQyxJQUFJLE9BQU8sR0FBRyxDQUFDLEdBQUcsS0FBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQztnQkFFMUMsS0FBSSxDQUFDLGVBQWUsR0FBRyxFQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBQyxDQUFDO2dCQUVwQyxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUN0RCxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUV0RCxFQUFFLENBQUMsQ0FBQyxNQUFNLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUM7b0JBQ3hCLEtBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsSUFBSSxNQUFNLENBQUM7Z0JBQ25ELENBQUM7Z0JBQ0QsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztvQkFDVixNQUFNLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsS0FBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUMsQ0FBQTtnQkFDOUUsQ0FBQztZQUNILENBQUM7UUFDSCxDQUFDLENBQUM7UUFFRixhQUFRLEdBQUcsVUFBQyxDQUFDO1lBQ1gsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQztnQkFDM0IsQ0FBQyxDQUFDLGNBQWMsRUFBRSxDQUFDO2dCQUVuQixLQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFBO1lBQy9CLENBQUM7WUFDRCxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztnQkFDekIsQ0FBQyxDQUFDLGNBQWMsRUFBRSxDQUFDO2dCQUVuQixLQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztnQkFDNUIsS0FBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQTtZQUM5QixDQUFDO1FBQ0gsQ0FBQyxDQUFDO1FBc0JGLGFBQVEsR0FBRztZQUNULElBQU0sZUFBZSxHQUFHLEtBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDO1lBQ2xELElBQU0sZUFBZSxHQUFHLEtBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDO1lBQ25ELElBQU0sT0FBTyxHQUFHLGVBQWUsQ0FBQyxVQUFVLENBQUM7WUFDM0MsSUFBTSxJQUFJLEdBQUcsS0FBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLEdBQUcsS0FBSSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQztZQUNyRSxJQUFNLEtBQUssR0FBRyxLQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQztZQUMvQixJQUFNLGdCQUFnQixHQUFHLGVBQWUsR0FBRyxDQUFDLElBQUksR0FBRyxPQUFPLEdBQUcsS0FBSyxDQUFDLENBQUM7WUFFcEUsb0NBQW9DO1lBQ3BDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sR0FBRyxLQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUNyQyxLQUFJLENBQUMsUUFBUSxDQUFDO29CQUNaLGVBQWUsRUFBRSxLQUFJLENBQUMsS0FBSyxDQUFDLGVBQWUsR0FBRyxJQUFJO2lCQUNuRCxDQUFDLENBQUM7Z0JBQ0gsZUFBZSxDQUFDLFVBQVUsSUFBSSxLQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQTtZQUNoRCxDQUFDO1lBQ0QsRUFBRSxDQUFDLENBQUMsT0FBTyxHQUFHLEtBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQ3JDLEtBQUksQ0FBQyxRQUFRLENBQUM7b0JBQ1osZUFBZSxFQUFFLEtBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxHQUFHLElBQUk7aUJBQ25ELENBQUMsQ0FBQztnQkFDSCxlQUFlLENBQUMsVUFBVSxJQUFJLEtBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFBO1lBQ2hELENBQUM7WUFFRCxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixLQUFLLGdCQUFnQixJQUFJLEtBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxLQUFLLGdCQUFnQixHQUFHLElBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQzlHLEtBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsQ0FBQyxnQkFBZ0IsRUFBRSxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsQ0FBQTtZQUMvRSxDQUFDO1FBQ0gsQ0FBQyxDQUFDO1FBNkVGLFlBQU8sR0FBRyxVQUFDLENBQUM7WUFDRixpREFBZSxDQUFnQjtZQUN2QyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztnQkFDZCxDQUFDLENBQUMsY0FBYyxFQUFFLENBQUM7Z0JBQ25CLElBQU0sY0FBYyxHQUFHLDRCQUFpQixDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQztnQkFDMUQsSUFBTSxTQUFTLEdBQUcsQ0FBQyxDQUFDLE9BQU8sR0FBRyxjQUFjLENBQUMsQ0FBQyxDQUFDO2dCQUMvQyxLQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsTUFBTSxHQUFHLEVBQUUsRUFBRSxTQUFTLEdBQUcsS0FBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQTtZQUNwRSxDQUFDO1lBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUN0QixDQUFDLENBQUMsY0FBYyxFQUFFLENBQUM7Z0JBQ25CLElBQU0sZUFBZSxHQUFHLEtBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDO2dCQUNsRCxlQUFlLENBQUMsVUFBVSxJQUFJLENBQUMsQ0FBQyxNQUFNLENBQUE7WUFDeEMsQ0FBQztZQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFDcEIsSUFBTSxjQUFjLEdBQUcsNEJBQWlCLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDO2dCQUMxRCxJQUFNLFNBQVMsR0FBRyxDQUFDLENBQUMsT0FBTyxHQUFHLGNBQWMsQ0FBQyxDQUFDLENBQUM7Z0JBQy9DLEtBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxNQUFNLEdBQUcsR0FBRyxFQUFFLFNBQVMsR0FBRyxLQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFBO1lBQ3JFLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDTixFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsS0FBSyxPQUFPLENBQUMsQ0FBQyxDQUFDO29CQUN2QyxDQUFDLENBQUMsY0FBYyxFQUFFLENBQUM7b0JBQ25CLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDbkIsRUFBRSxDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDOzRCQUNyQixLQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLElBQUksQ0FBQyxDQUFDLE1BQU0sQ0FBQTt3QkFDbEQsQ0FBQztvQkFDSCxDQUFDO29CQUNELEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDbkIsTUFBTSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLE1BQU0sQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDO3dCQUNuRSxFQUFFLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDOzRCQUNwQixJQUFNLGNBQWMsR0FBRyw0QkFBaUIsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLENBQUM7NEJBQzFELElBQU0sU0FBUyxHQUFHLENBQUMsQ0FBQyxPQUFPLEdBQUcsY0FBYyxDQUFDLENBQUMsQ0FBQzs0QkFDL0MsS0FBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLE1BQU0sR0FBRyxFQUFFLEVBQUUsU0FBUyxHQUFHLEtBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUE7d0JBQ3BFLENBQUM7b0JBQ0gsQ0FBQztnQkFDSCxDQUFDO1lBQ0gsQ0FBQztRQUNILENBQUMsQ0FBQztRQXVCRixlQUFVLEdBQUcsVUFBQyxJQUFJLEVBQUUsSUFBSTtZQUN0QixJQUFJLGdCQUFnQixHQUFHLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUN0QyxJQUFJLGNBQWMsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUN6RCxJQUFJLElBQUksR0FBRyxjQUFjLEdBQUcsZ0JBQWdCLENBQUM7WUFFN0MsMkNBQTJDO1lBQzNDLEVBQUUsQ0FBQyxDQUFDLElBQUksR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDO2dCQUNsQixNQUFNLENBQUE7WUFDUixDQUFDO1lBRUQsK0RBQStEO1lBQy9ELEVBQUUsQ0FBQyxDQUFDLElBQUksS0FBSyxNQUFNLElBQUksS0FBSSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsS0FBSyxnQkFBZ0IsSUFBSSxLQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsS0FBSyxjQUFjLENBQUMsQ0FBQyxDQUFDO2dCQUN4SCxJQUFJLFFBQVEsR0FBRyxzQkFBVyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUVqQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDO2dCQUNwRCxjQUFjLEdBQUcsTUFBTSxDQUFDLGdCQUFnQixDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxRQUFRLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFFLHFDQUFxQztnQkFDNUcsSUFBSSxHQUFHLGNBQWMsR0FBRyxnQkFBZ0IsQ0FBQTtZQUMxQyxDQUFDO1lBRUQsS0FBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxDQUFDLGdCQUFnQixFQUFFLGdCQUFnQixHQUFHLElBQUksQ0FBQyxDQUFBO1FBQy9FLENBQUMsQ0FBQztRQUVGLGVBQVUsR0FBRyxVQUFDLElBQUksRUFBRSxTQUFVLEVBQUUsQ0FBRTtZQUNoQyxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLG1CQUFtQixJQUFJLFNBQVMsS0FBSyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2xHLEVBQUUsQ0FBQyxDQUFDLElBQUksSUFBSSxLQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7b0JBQ25DLEtBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQTtnQkFDakMsQ0FBQztZQUNILENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDTixLQUFJLENBQUMsUUFBUSxDQUFDLEVBQUMsWUFBWSxFQUFFLElBQUksRUFBQyxDQUFDLENBQUM7Z0JBQ3BDLEVBQUUsQ0FBQyxDQUFDLElBQUksSUFBSSxLQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7b0JBQ3BDLEtBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQTtnQkFDbEMsQ0FBQztZQUNILENBQUM7UUFDSCxDQUFDLENBQUM7UUFpQkYsb0JBQWUsR0FBRyxVQUFDLENBQUM7WUFDbEIsNkJBQTZCO1lBRTdCLEVBQUUsQ0FBQyxDQUFDLENBQUMsZ0NBQXFCLENBQUMsQ0FBQyxDQUFDLE1BQU0sRUFBRSxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2pELEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztvQkFDNUIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQTtnQkFDdkIsQ0FBQztnQkFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO29CQUNwQyxJQUFBLGlDQUErQyxFQUF4QyxXQUFHLEVBQUUsWUFBSSxDQUErQjtvQkFDL0MsRUFBRSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxHQUFHLEdBQUcsS0FBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQzt3QkFDL0MsSUFBTSxPQUFPLEdBQUcsZUFBSSxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUFFLEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFBO3dCQUN4RSxLQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxPQUFPLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFBO29CQUM1QyxDQUFDO2dCQUNILENBQUM7WUFDSCxDQUFDO1FBQ0gsQ0FBQyxDQUFBO1FBRUQsYUFBUSxHQUFHLFVBQUMsSUFBSSxFQUFFLFFBQVEsRUFBRSxhQUFhO1lBQ3ZDLElBQUksUUFBUSxHQUFHLEtBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQ2hELElBQU0sSUFBSSxHQUFHLEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDO1lBRTdCLEtBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQ1osWUFBWSxFQUFFLElBQUk7Z0JBQ2xCLFFBQVEsRUFBRSxRQUFRO2dCQUNsQixhQUFhLEVBQUUsYUFBYTtnQkFDNUIsY0FBYyxFQUFFLFFBQVEsR0FBRyxlQUFJLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxFQUFFO2FBQ25FLENBQUMsQ0FBQTtRQUNKLENBQUMsQ0FBQTtRQUVELGFBQVEsR0FBRyxVQUFDLElBQUksRUFBRSxRQUFRLEVBQUUsYUFBYTtZQUN2QyxLQUFJLENBQUMsUUFBUSxDQUFDLEVBQUMsWUFBWSxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLGNBQWMsRUFBRSxJQUFJLEVBQUMsQ0FBQyxDQUFBO1lBQ3pFLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztnQkFDMUIsS0FBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLFFBQVEsRUFBRSxhQUFhLENBQUMsQ0FBQTtZQUN0RCxDQUFDO1FBQ0gsQ0FBQyxDQUFBO1FBRUQsaUJBQVksR0FBRyxVQUFDLElBQUksRUFBRSxZQUFZO1lBQ2hDLEtBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQ1osWUFBWSxFQUFFLElBQUk7Z0JBQ2xCLFNBQVMsRUFBRSxZQUFZO2FBQ3hCLENBQUMsQ0FBQTtRQUNKLENBQUMsQ0FBQTtRQUVELGdCQUFXLEdBQUcsVUFBQyxJQUFJLEVBQUUsWUFBWTtZQUMvQixLQUFJLENBQUMsUUFBUSxDQUFDLEVBQUMsWUFBWSxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQztZQUNyRCxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7Z0JBQzVCLEtBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxZQUFZLENBQUMsQ0FBQTtZQUM3QyxDQUFDO1FBQ0gsQ0FBQyxDQUFBO1FBRUQsb0JBQWUsR0FBRyxVQUFDLENBQUM7WUFDVixxQ0FBUyxDQUFlO1lBQ3hCLG1CQUFLLENBQU07WUFDbkIsSUFBQSxnQkFBZ0UsRUFBeEQsa0RBQXNCLEVBQUUsd0NBQWlCLENBQWU7WUFDaEUsSUFBTSxZQUFZLEdBQUcsc0JBQXNCLEdBQUcsaUJBQWlCLENBQUE7WUFFL0QsRUFBRSxDQUFDLENBQUMsS0FBSyxHQUFHLFNBQVMsR0FBRyxZQUFZLENBQUMsQ0FBQyxDQUFDO2dCQUNyQyxLQUFJLENBQUMsUUFBUSxDQUFDLEVBQUMsVUFBVSxFQUFFLElBQUksRUFBRSxpQkFBaUIsRUFBRSxDQUFDLENBQUMsS0FBSyxFQUFFLGdCQUFnQixFQUFFLENBQUMsQ0FBQyxLQUFLLEVBQUMsQ0FBQyxDQUFBO1lBQzFGLENBQUM7UUFDSCxDQUFDLENBQUE7UUFFRCxvQkFBZSxHQUFHLFVBQUMsQ0FBQztZQUNoQixFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxLQUFLLENBQUMsSUFBSSxLQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7Z0JBQzNDLG9EQUFvRDtnQkFDcEQsS0FBSSxDQUFDLFFBQVEsQ0FBQyxFQUFDLFVBQVUsRUFBRSxLQUFLLEVBQUUsaUJBQWlCLEVBQUUsSUFBSSxFQUFFLGdCQUFnQixFQUFFLElBQUksRUFBQyxDQUFDLENBQUM7WUFDeEYsQ0FBQztZQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsSUFBSSxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxJQUFJLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO2dCQUN2RixLQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLElBQUksS0FBSSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDO2dCQUM5RSxLQUFJLENBQUMsUUFBUSxDQUFDLEVBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDLEtBQUssRUFBQyxDQUFDLENBQUM7WUFDL0MsQ0FBQztRQUNMLENBQUMsQ0FBQTtRQUVELGtCQUFhLEdBQUcsVUFBQyxDQUFDO1lBQ1IscURBQWlCLENBQWdCO1lBQ3pDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsaUJBQWlCLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztnQkFDdkUsS0FBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQTtZQUN6QixDQUFDO1lBRUQsS0FBSSxDQUFDLFFBQVEsQ0FBQyxFQUFDLFVBQVUsRUFBRSxLQUFLLEVBQUUsaUJBQWlCLEVBQUUsSUFBSSxFQUFFLGdCQUFnQixFQUFFLElBQUksRUFBQyxDQUFDLENBQUE7UUFDckYsQ0FBQyxDQUFBO1FBb0xELHNCQUFpQixHQUFHLFVBQUMsQ0FBQztZQUNwQixJQUFBLGdCQUFxRyxFQUE3RixvQ0FBZSxFQUFFLGdCQUFLLEVBQUUsc0NBQWdCLEVBQUUsa0NBQWMsRUFBRSx3QkFBUyxFQUFFLHdCQUFTLENBQWU7WUFDckcsSUFBTSxJQUFJLEdBQUcsY0FBYyxHQUFHLGdCQUFnQixDQUFBO1lBQzlDLElBQU0sYUFBYSxHQUFHLGVBQWUsR0FBRyxJQUFJLEdBQUcsQ0FBQyxDQUFBO1lBQ2hELElBQU0sV0FBVyxHQUFHLEtBQUssR0FBRyxDQUFDLENBQUE7WUFDckIsbUJBQUssRUFBRSxlQUFLLENBQU07WUFDMUIsSUFBTSxLQUFLLEdBQUcsQ0FBQyxhQUFhLEdBQUcsZUFBZSxDQUFDLEdBQUcsV0FBVyxDQUFBO1lBQzdELElBQU0sWUFBWSxHQUFHLEtBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLHFCQUFxQixFQUFFLENBQUM7WUFDdkUsSUFBSSxZQUFZLEdBQUcsZ0JBQWdCLEdBQUcsS0FBSyxHQUFHLENBQUMsS0FBSyxHQUFHLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQTtZQUN6RSxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hCLFlBQVksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksR0FBRyxLQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxHQUFHLEtBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFBO1lBQ3JGLENBQUM7WUFFRCxJQUFJLFVBQVUsR0FBRyxDQUFDLENBQUE7WUFDbEIsR0FBRyxDQUFDLENBQVksVUFBc0IsRUFBdEIsS0FBQSxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUF0QixjQUFzQixFQUF0QixJQUFzQixDQUFDO2dCQUFsQyxJQUFJLEdBQUcsU0FBQTtnQkFDVixJQUFJLElBQUksR0FBRyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUE7Z0JBQ3pCLEVBQUUsQ0FBQyxDQUFDLEtBQUssR0FBRyxTQUFTLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQztvQkFDN0IsVUFBVSxHQUFHLFFBQVEsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLENBQUE7Z0JBQ2hDLENBQUM7Z0JBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ04sS0FBSyxDQUFBO2dCQUNQLENBQUM7YUFDRjtZQUVELEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDO2dCQUNuQyxLQUFJLENBQUMsS0FBSyxDQUFDLG1CQUFtQixDQUFDLFlBQVksRUFBRSxLQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFBO1lBQzdFLENBQUM7UUFDSCxDQUFDLENBQUM7UUFycEJJLElBQUksZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO1FBQzVCLElBQUksY0FBYyxHQUFHLElBQUksQ0FBQztRQUUxQixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztZQUM3RCxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ3pELGNBQWMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLEVBQUUsQ0FBQTtRQUN0RCxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1lBQ3BFLGdCQUFnQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLENBQUM7WUFDL0MsY0FBYyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFBO1FBQzVDLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLGdCQUFnQixHQUFHLElBQUksQ0FBQyxHQUFHLE9BQVIsSUFBSSxFQUFRLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLGVBQUksQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUMsT0FBTyxFQUFFLEVBQTdCLENBQTZCLENBQUMsQ0FBQyxDQUFDO1lBQzVGLGNBQWMsR0FBRyxJQUFJLENBQUMsR0FBRyxPQUFSLElBQUksRUFBUSxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxlQUFJLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDLE9BQU8sRUFBRSxFQUEzQixDQUEyQixDQUFDLENBQUMsQ0FBQztZQUV4RixFQUFFLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztnQkFDekMsZ0JBQWdCLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQyxPQUFPLEVBQUUsR0FBRyxLQUFLLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQztnQkFDM0QsY0FBYyxHQUFHLElBQUksSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFLEdBQUcsS0FBSyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUE7WUFDMUQsQ0FBQztZQUVELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztnQkFDMUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLEVBQUUsY0FBYyxDQUFDLENBQUE7WUFDekQsQ0FBQztRQUNQLENBQUM7UUFFRCxJQUFJLENBQUMsS0FBSyxHQUFHO1lBQ1gsS0FBSyxFQUFFLElBQUk7WUFFWCxnQkFBZ0IsRUFBRSxnQkFBZ0I7WUFDbEMsY0FBYyxFQUFFLGNBQWM7WUFDOUIsZUFBZSxFQUFFLGdCQUFnQixHQUFHLENBQUMsY0FBYyxHQUFHLGdCQUFnQixDQUFDO1lBRXZFLFlBQVksRUFBRSxJQUFJO1lBQ2xCLFFBQVEsRUFBRSxJQUFJO1lBQ2QsY0FBYyxFQUFFLElBQUk7WUFDcEIsU0FBUyxFQUFFLElBQUk7WUFDZixVQUFVLEVBQUUsS0FBSztZQUNqQixTQUFTLEVBQUUsQ0FBQztZQUNaLFlBQVksRUFBRSxJQUFJO1NBQ25CLENBQUM7UUFFRixJQUFBLHFKQUVvSixFQURsSixrQ0FBYyxFQUFFLGtCQUFNLEVBQUUsOEJBQVksRUFBRSx3QkFBUyxDQUNvRztRQUVySixJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsR0FBRyxjQUFjLENBQUM7UUFDM0MsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQzNCLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxHQUFHLFlBQVksQ0FBQztRQUN2QyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUE7SUFDbEMsQ0FBQztJQUVELGlEQUFpQixHQUFqQjtRQUFBLGlCQWdCQztRQWZDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUVkLElBQUksQ0FBQyxtQkFBbUIsR0FBRztZQUN6QixXQUFXLEVBQUUsVUFBQyxLQUFLO2dCQUNqQixLQUFJLENBQUMsTUFBTSxFQUFFLENBQUE7WUFDZixDQUFDO1NBQ0YsQ0FBQztRQUVGLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFFNUQsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztRQUU5QixJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ2hGLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDOUUsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLGtCQUFrQixDQUFDLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQTtJQUM3RSxDQUFDO0lBRUQsb0RBQW9CLEdBQXBCO1FBQ0UsTUFBTSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQztRQUMvRCxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMscUJBQXFCLENBQUMsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ25GLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDakYsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLHFCQUFxQixDQUFDLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQTtJQUNoRixDQUFDO0lBOEVELHNDQUFNLEdBQU47UUFDRSxzSUFBc0k7UUFDdEksSUFBQSxnREFBOEYsRUFBdkYseUJBQXFCLEVBQUUscUJBQWlCLENBQWdEO1FBQy9GLElBQUksS0FBSyxHQUFHLGNBQWMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQztRQUVyRCxJQUFBLG9KQUVtSixFQURqSixrQ0FBYyxFQUFFLGtCQUFNLEVBQUUsOEJBQVksRUFBRSx3QkFBUyxDQUNtRztRQUVwSixJQUFJLENBQUMsUUFBUSxDQUFDO1lBQ1osS0FBSyxFQUFFLEtBQUs7WUFDWixTQUFTLEVBQUUsWUFBWSxHQUFHLE1BQU0sQ0FBQyxXQUFXO1lBQzVDLGNBQWMsRUFBRSxjQUFjO1lBQzlCLE1BQU0sRUFBRSxNQUFNO1lBQ2QsWUFBWSxFQUFFLFlBQVk7WUFDMUIsU0FBUyxFQUFFLFNBQVM7U0FDckIsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQTtJQUM5QyxDQUFDO0lBNkJELHlEQUF5QixHQUF6QixVQUEyQixTQUFTO1FBQzFCLGlEQUFnQixFQUFFLHlDQUFjLEVBQUUsdUJBQUssRUFBRSx5QkFBTSxDQUFlO1FBRXRFLEVBQUUsQ0FBQyxDQUFDLGdCQUFnQixJQUFJLGNBQWMsQ0FBQyxDQUFDLENBQUM7WUFDdkMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGdCQUFnQixFQUFFLGNBQWMsRUFBRSxLQUFLLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLElBQUksTUFBTSxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxNQUFNLENBQUMsQ0FBQTtRQUN0SSxDQUFDO1FBRUQsRUFBRSxDQUFDLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxJQUFJLE1BQU0sS0FBSyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDL0QsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssRUFBRSxNQUFNLENBQUMsQ0FBQTtRQUN0QyxDQUFDO0lBQ0gsQ0FBQztJQUVELGdEQUFnQixHQUFoQixVQUFrQixLQUFLLEVBQUUsTUFBTTtRQUM3QixJQUFBLGVBQStFLEVBQXZFLG9DQUFlLEVBQUUsc0NBQWdCLEVBQUUsa0NBQWMsRUFBRSxnQkFBSyxDQUFnQjtRQUNoRixJQUFBLDZGQUU0RixFQUQxRixrQ0FBYyxFQUFFLGtCQUFNLEVBQUUsOEJBQVksRUFBRSx3QkFBUyxDQUM0QztRQUU3RixJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsZ0JBQUEsY0FBYyxFQUFFLFFBQUEsTUFBTSxFQUFFLGNBQUEsWUFBWSxFQUFFLFdBQUEsU0FBUyxFQUFFLENBQUMsQ0FBQTtJQUNwRSxDQUFDO0lBRUQsa0RBQWtCLEdBQWxCLFVBQW9CLGdCQUFnQixFQUFFLGNBQWMsRUFBRSxxQkFBc0IsRUFBRSxZQUFhLEVBQUUsYUFBYztRQUN6RyxJQUFNLGtCQUFrQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDO1FBQ3RELElBQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLENBQUM7UUFDeEUsSUFBTSxPQUFPLEdBQUcsY0FBYyxHQUFHLGdCQUFnQixDQUFDO1FBQ2xELElBQU0sS0FBSyxHQUFHLFlBQVksSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQztRQUMvQyxJQUFNLE1BQU0sR0FBRyxhQUFhLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7UUFFbEQsSUFBSSxRQUFRLEdBQVU7WUFDcEIsZ0JBQWdCLEVBQUUsZ0JBQWdCO1lBQ2xDLGNBQWMsRUFBRSxjQUFjO1NBQy9CLENBQUM7UUFFRixJQUFJLFdBQVcsR0FBRyxLQUFLLENBQUM7UUFFeEIsSUFBTSxhQUFhLEdBQUcsZ0JBQWdCLElBQUksa0JBQWtCLEdBQUcsT0FBTyxHQUFHLEdBQUc7WUFDdEQsZ0JBQWdCLElBQUksa0JBQWtCLEdBQUcsT0FBTyxHQUFHLEdBQUc7WUFDdEQsY0FBYyxJQUFJLGtCQUFrQixHQUFHLE9BQU8sR0FBRyxHQUFHO1lBQ3BELGNBQWMsSUFBSSxrQkFBa0IsR0FBRyxPQUFPLEdBQUcsR0FBRyxDQUFDO1FBRTNFLGtEQUFrRDtRQUNsRCxFQUFFLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO1lBQ2xCLG1DQUFtQztZQUNuQyxJQUFNLGFBQWEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLENBQUMsZ0JBQWdCLEdBQUcsa0JBQWtCLENBQUMsR0FBRyxPQUFPLENBQUMsQ0FBQztZQUN2RyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLEtBQUssYUFBYSxDQUFDLENBQUMsQ0FBQztnQkFDM0QsV0FBVyxHQUFHLElBQUksQ0FBQTtZQUNwQixDQUFDO1FBQ0gsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sV0FBVyxHQUFHLElBQUksQ0FBQTtRQUNwQixDQUFDO1FBRUQsRUFBRSxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztZQUNoQix5Q0FBeUM7WUFDekMsUUFBUSxDQUFDLGVBQWUsR0FBRyxnQkFBZ0IsR0FBRyxPQUFPLENBQUM7WUFDdEQsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO1lBRXhELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztnQkFDOUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxRQUFRLENBQUMsZUFBZSxHQUFHLE9BQU8sR0FBRyxDQUFDLENBQUMsQ0FBQTtZQUM3RixDQUFDO1FBQ0gsQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLFdBQVcsSUFBSSxxQkFBcUIsQ0FBQyxDQUFDLENBQUM7WUFDekMsSUFBTSxlQUFlLEdBQUcsUUFBUSxDQUFDLGVBQWUsR0FBRyxRQUFRLENBQUMsZUFBZSxHQUFHLGtCQUFrQixDQUFDO1lBQ2pHLElBQUEsd0dBRXVHLEVBRHJHLGtDQUFjLEVBQUUsa0JBQU0sRUFBRSw4QkFBWSxFQUFFLHdCQUFTLENBQ3VEO1lBQ3hHLFFBQVEsQ0FBQyxjQUFjLEdBQUcsY0FBYyxDQUFDO1lBQ3pDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1lBQ3pCLFFBQVEsQ0FBQyxZQUFZLEdBQUcsWUFBWSxDQUFDO1lBQ3JDLFFBQVEsQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFBO1FBQ2hDLENBQUM7UUFFRCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFBO0lBQ3pCLENBQUM7SUFxQ0Qsc0NBQU0sR0FBTixVQUFRLENBQUM7UUFDUCxDQUFDLENBQUMsY0FBYyxFQUFFLENBQUM7UUFFbkIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQTtJQUN2QixDQUFDO0lBRUQsdUNBQU8sR0FBUCxVQUFTLENBQUM7UUFDUixDQUFDLENBQUMsY0FBYyxFQUFFLENBQUM7UUFFbkIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQTtJQUN2QixDQUFDO0lBRUQsMENBQVUsR0FBVixVQUFZLEtBQUssRUFBRSxNQUFZO1FBQVosc0JBQVksR0FBWixZQUFZO1FBQzdCLElBQUEsZUFBdUMsRUFBL0Isb0JBQU8sRUFBRSxvQkFBTyxDQUFnQjtRQUN4QyxJQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixDQUFDO1FBQ3hFLElBQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUMsRUFBRSxPQUFPLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLDBCQUEwQjtRQUM3RyxJQUFNLG1CQUFtQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUMsR0FBRyxNQUFNLENBQUMsQ0FBQztRQUVuRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsbUJBQW1CLEVBQUUsbUJBQW1CLEdBQUcsT0FBTyxDQUFDLENBQUE7SUFDeEYsQ0FBQztJQXFDRCxtREFBbUIsR0FBbkIsVUFBcUIsQ0FBQztRQUNwQixJQUFBLGVBQTJDLEVBQW5DLDBCQUFVLEVBQUUsc0JBQVEsQ0FBZTtRQUMzQyxJQUFBLGVBQThELEVBQXRELGdCQUFLLEVBQUUsc0NBQWdCLEVBQUUsa0NBQWMsQ0FBZTtRQUU5RCxJQUFNLGNBQWMsR0FBRyw0QkFBaUIsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLENBQUE7UUFDekQsSUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLE9BQU8sR0FBRyxjQUFjLENBQUMsQ0FBQyxDQUFBO1FBQ3RDLElBQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLEdBQUcsY0FBYyxDQUFDLENBQUMsQ0FBQTtRQUV0QyxJQUFNLEdBQUcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBVSxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsVUFBVSxDQUFDLENBQUE7UUFDM0QsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLEdBQUcsS0FBSyxHQUFHLENBQUMsY0FBYyxHQUFHLGdCQUFnQixDQUFDLENBQUMsQ0FBQTtRQUN6RixJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEdBQUcsUUFBUSxDQUFDLEdBQUcsUUFBUSxDQUFBO1FBRTdDLE1BQU0sQ0FBQyxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQTtJQUNwQixDQUFDO0lBaUZELHlDQUFTLEdBQVQsVUFBVyxlQUFlLEVBQUUsSUFBSSxFQUFFLGFBQWEsRUFBRSxXQUFXLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxZQUFZO1FBQ3pGLE1BQU0sQ0FBQyxDQUNMLG9CQUFDLG9CQUFTLEdBQUMsZUFBZSxFQUFFLGVBQWdCLEVBQ2pDLGFBQWEsRUFBRSxhQUFjLEVBQzdCLFdBQVcsRUFBRSxXQUFZLEVBQ3pCLFVBQVUsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVcsRUFDbEMsU0FBUyxFQUFFLGtCQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUUsRUFDdEMsTUFBTSxFQUFFLE1BQU8sRUFDZixZQUFZLEVBQUUsWUFBYSxFQUNwQyxDQUNILENBQUE7SUFDSCxDQUFDO0lBRUQsNkNBQWEsR0FBYixVQUFlLGVBQWUsRUFBRSxJQUFJLEVBQUUsYUFBYSxFQUFFLFdBQVcsRUFBRSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxZQUFZO1FBQ3hHLE1BQU0sQ0FBQyxDQUNMLG9CQUFDLHdCQUFhLEdBQUMsZUFBZSxFQUFFLGVBQWdCLEVBQ2pDLGFBQWEsRUFBRSxhQUFjLEVBQzdCLFdBQVcsRUFBRSxXQUFZLEVBQ3pCLFVBQVUsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVcsRUFDbEMsU0FBUyxFQUFFLGtCQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUUsRUFDdEMsT0FBTyxFQUFFLE9BQVEsRUFDakIsU0FBUyxFQUFFLFNBQVUsRUFDckIsV0FBVyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBWSxFQUNwQyxNQUFNLEVBQUUsTUFBTyxFQUNmLFlBQVksRUFBRSxZQUFhLEVBQ3hDLENBQ0gsQ0FBQTtJQUNILENBQUM7SUFFRCwrQ0FBZSxHQUFmLFVBQWlCLGVBQWUsRUFBRSxJQUFJLEVBQUUsYUFBYSxFQUFFLFdBQVcsRUFBRSxZQUFZLEVBQUUsWUFBWTtRQUM1RixNQUFNLENBQUMsQ0FDTCxvQkFBQywwQkFBZSxHQUFDLFdBQVcsRUFBRSxXQUFZLEVBQ3pCLFVBQVUsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVcsRUFDbEMsU0FBUyxFQUFFLGtCQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUUsRUFDdEMsTUFBTSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTyxFQUMxQixZQUFZLEVBQUUsWUFBYSxFQUMzQixZQUFZLEVBQUUsWUFBYSxFQUMxQyxDQUNILENBQUE7SUFDSCxDQUFDO0lBRUQscUNBQUssR0FBTCxVQUFPLGVBQWUsRUFBRSxJQUFJLEVBQUUsYUFBYSxFQUFFLFdBQVcsRUFBRSxPQUFPLEVBQUUsY0FBYyxFQUFFLFlBQVksRUFBRSxTQUFTO1FBQ3hHLE1BQU0sQ0FBQyxDQUNMLG9CQUFDLGVBQUssR0FBQyxlQUFlLEVBQUUsZUFBZ0IsRUFDakMsYUFBYSxFQUFFLGFBQWMsRUFDN0IsV0FBVyxFQUFFLFdBQVksRUFDekIsVUFBVSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVyxFQUNsQyxTQUFTLEVBQUUsa0JBQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBRSxFQUN0QyxjQUFjLEVBQUUsY0FBZSxFQUMvQixPQUFPLEVBQUUsT0FBUSxFQUNqQixZQUFZLEVBQUUsWUFBYSxFQUMzQixTQUFTLEVBQUUsU0FBVSxFQUNyQixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFNLEVBQ3hCLE1BQU0sRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU8sRUFDMUIsSUFBSSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSyxFQUN0QixZQUFZLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFhLEVBQ3RDLFFBQVEsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVMsRUFDOUIsY0FBYyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBZSxFQUMxQyxjQUFjLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFlLEVBQzFDLE9BQU8sRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQVEsRUFDNUIsU0FBUyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBVSxFQUNoQyxlQUFlLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxlQUFnQixFQUM1QyxtQkFBbUIsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLG1CQUFvQixFQUNwRCxTQUFTLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFVLEVBQ2hDLFVBQVUsRUFBRSxJQUFJLENBQUMsVUFBVyxFQUM1QixRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVMsRUFDeEIsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFTLEVBQ3hCLGlCQUFpQixFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsaUJBQWtCLEVBQ2hELGlCQUFpQixFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsaUJBQWtCLEVBQ2hELFlBQVksRUFBRSxJQUFJLENBQUMsWUFBYSxFQUNoQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVksRUFDOUIsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxnQkFBaUIsRUFDbkQsQ0FDSCxDQUFBO0lBQ0gsQ0FBQztJQUVELHlDQUFTLEdBQVQ7UUFDRSxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUE7UUFFaEIsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ3hCLEtBQUssR0FBTSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLFVBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFnQixDQUFBO1FBQ3RGLENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ2hDLEtBQUssR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUE7UUFDcEQsQ0FBQztRQUVELE1BQU0sQ0FBQyxLQUFLLEdBQUcsb0JBQUMsb0JBQVMsR0FBQyxLQUFLLEVBQUUsS0FBTSxFQUFHLEdBQUcsRUFBRSxDQUFBO0lBQ2pELENBQUM7SUFFRCxzQ0FBTSxHQUFOLFVBQVEsZUFBZSxFQUFFLElBQUksRUFBRSxhQUFhLEVBQUUsV0FBVyxFQUFFLE9BQU8sRUFBRSxTQUFTLEVBQUUsc0JBQXNCLEVBQUUsaUJBQWlCO1FBQ3RILE1BQU0sQ0FBQyxDQUNMLG9CQUFDLGdCQUFNLEdBQUMsZUFBZSxFQUFFLGVBQWdCLEVBQ2pDLGFBQWEsRUFBRSxhQUFjLEVBQzdCLFdBQVcsRUFBRSxXQUFZLEVBQ3pCLFVBQVUsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVcsRUFDbEMsT0FBTyxFQUFFLE9BQVEsRUFDakIsU0FBUyxFQUFFLFNBQVUsRUFDckIsc0JBQXNCLEVBQUUsc0JBQXVCLEVBQy9DLGlCQUFpQixFQUFFLGlCQUFrQixFQUNyQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFNLEVBQ3hCLElBQUksRUFBRSxJQUFLLEVBQ1gsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxnQkFBaUIsRUFDOUMsY0FBYyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBZSxFQUMxQyxXQUFXLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFZLEVBQ3BDLE1BQU0sRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsR0FBRyxDQUFFLEVBQ25DLFVBQVUsRUFBRSxJQUFJLENBQUMsVUFBVyxFQUFHLENBQ3hDLENBQUE7SUFDSCxDQUFDO0lBRUQsdUNBQU8sR0FBUCxVQUFTLE1BQU0sRUFBRSxZQUFZLEVBQUUsWUFBWTtRQUN6QyxNQUFNLENBQUMsQ0FDTCxvQkFBQyxpQkFBTyxHQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU8sRUFDMUIsSUFBSSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSyxFQUV0QixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFhLEVBQy9CLFVBQVUsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVcsRUFDbEMsWUFBWSxFQUFFLFlBQWEsRUFDM0IsTUFBTSxFQUFFLE1BQU8sRUFDZixZQUFZLEVBQUUsWUFBYSxFQUUzQixXQUFXLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFZLEVBQ3BDLE1BQU0sRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsR0FBRyxDQUFFLEdBQ3pDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUyxDQUNiLENBQ1gsQ0FBQTtJQUNILENBQUM7SUFFRCwwQ0FBVSxHQUFWLFVBQVksS0FBSyxFQUFFLE1BQU0sRUFBRSxlQUFlLEVBQUUsZ0JBQWdCLEVBQUUsY0FBYyxFQUFFLEtBQUs7UUFDakYsSUFBQSxlQUF5SCxFQUFqSCxjQUFJLEVBQUUsc0JBQVEsRUFBRSwwQkFBVSxFQUFFLGtEQUFzQixFQUFFLHdDQUFpQixFQUFFLDBCQUFVLEVBQUUsb0NBQWUsQ0FBZTtRQUN6SCxJQUFBLGVBQXFGLEVBQTdFLDhCQUFZLEVBQUUsc0JBQVEsRUFBRSw4QkFBWSxFQUFFLHdCQUFTLEVBQUUsZ0NBQWEsQ0FBZTtRQUNyRixJQUFNLElBQUksR0FBRyxjQUFjLEdBQUcsZ0JBQWdCLENBQUE7UUFDOUMsSUFBTSxhQUFhLEdBQUcsZUFBZSxHQUFHLElBQUksR0FBRyxDQUFDLENBQUE7UUFDaEQsSUFBTSxXQUFXLEdBQUcsS0FBSyxHQUFHLENBQUMsQ0FBQTtRQUM3QixJQUFNLFlBQVksR0FBRyxzQkFBc0IsR0FBRyxpQkFBaUIsQ0FBQTtRQUUvRCxJQUFNLFlBQVksR0FBRywwQkFBZSxDQUFDLEtBQUssRUFBRSxlQUFlLEVBQUUsYUFBYSxFQUFFLElBQUksQ0FBQyxDQUFBO1FBQ2pGLElBQU0sV0FBVyxHQUFHLHlCQUFjLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFBO1FBRWhELElBQUksY0FBYyxHQUFHLFlBQVksQ0FBQyxHQUFHLENBQUMsVUFBQSxJQUFJO1lBQ3hDLE1BQU0sQ0FBQztnQkFDTCxFQUFFLEVBQUUsZUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDO2dCQUM5QixVQUFVLEVBQUUsOEJBQW1CLENBQzdCLElBQUksRUFDSixXQUFXLENBQUMsZUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsRUFDMUMsSUFBSSxFQUNKLGVBQWUsRUFDZixhQUFhLEVBQ2IsV0FBVyxFQUNYLFFBQVEsRUFDUixVQUFVLEVBQ1YsWUFBWSxFQUNaLFFBQVEsRUFDUixZQUFZLEVBQ1osU0FBUyxFQUNULGFBQWEsRUFDYixlQUFlLENBQ2hCO2FBQ0YsQ0FBQTtRQUNILENBQUMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxjQUVJLENBQUM7UUFDVCxFQUFFLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1lBQ1osY0FBYyxHQUFHLGdCQUFLLENBQUM7UUFDNUIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0gsY0FBYyxHQUFHLGtCQUFPLENBQUM7UUFDOUIsQ0FBQztRQUVELElBQUEsMEVBS0MsRUFMTyxrQkFBTSxFQUFFLDhCQUFZLEVBQUUsd0JBQVMsQ0FLckM7UUFFRixNQUFNLENBQUMsRUFBQyxnQkFBQSxjQUFjLEVBQUUsUUFBQSxNQUFNLEVBQUUsY0FBQSxZQUFZLEVBQUUsV0FBQSxTQUFTLEVBQUMsQ0FBQTtJQUMxRCxDQUFDO0lBOEJELHNDQUFNLEdBQU47UUFDRSxJQUFBLGVBQXdHLEVBQWhHLGdCQUFLLEVBQUUsa0JBQU0sRUFBRSxrREFBc0IsRUFBRSx3Q0FBaUIsRUFBRSw4QkFBWSxFQUFFLHdCQUFTLENBQWdCO1FBQ3pHLElBQUEsZUFBdUgsRUFBL0csOEJBQVksRUFBRSw4QkFBWSxFQUFFLDBCQUFVLEVBQUUsZ0JBQUssRUFBRSxzQ0FBZ0IsRUFBRSxrQ0FBYyxFQUFFLG9DQUFlLENBQWU7UUFDdkgsSUFBQSxlQUFvRSxFQUE5RCxrQ0FBYyxFQUFFLGtCQUFNLEVBQUUsOEJBQVksRUFBRSx3QkFBUyxDQUFlO1FBQ3BFLElBQU0sSUFBSSxHQUFHLGNBQWMsR0FBRyxnQkFBZ0IsQ0FBQTtRQUM5QyxJQUFNLGFBQWEsR0FBRyxlQUFlLEdBQUcsSUFBSSxHQUFHLENBQUMsQ0FBQTtRQUNoRCxJQUFNLFdBQVcsR0FBRyxLQUFLLEdBQUcsQ0FBQyxDQUFBO1FBQzdCLElBQU0sT0FBTyxHQUFHLHFCQUFVLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxTQUFTLENBQUMsQ0FBQTtRQUNsRCxJQUFNLFlBQVksR0FBRyxzQkFBc0IsR0FBRyxpQkFBaUIsQ0FBQTtRQUUvRCxFQUFFLENBQUMsQ0FBQyxZQUFZLElBQUksWUFBWSxDQUFDLENBQUMsQ0FBQztZQUNqQyxJQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRSxNQUFNLEVBQUUsZUFBZSxFQUFFLGdCQUFnQixFQUFFLGNBQWMsRUFBRSxLQUFLLENBQUMsQ0FBQTtZQUM3RyxjQUFjLEdBQUcsWUFBWSxDQUFDLGNBQWMsQ0FBQTtZQUM1QyxNQUFNLEdBQUcsWUFBWSxDQUFDLE1BQU0sQ0FBQTtZQUM1QixZQUFZLEdBQUcsWUFBWSxDQUFDLFlBQVksQ0FBQTtZQUN4QyxTQUFTLEdBQUcsWUFBWSxDQUFDLFNBQVMsQ0FBQTtRQUNwQyxDQUFDO1FBRUQsSUFBTSxtQkFBbUIsR0FBRztZQUMxQixNQUFNLEVBQUssTUFBTSxPQUFJO1NBQ3RCLENBQUE7UUFFRCxJQUFNLG9CQUFvQixHQUFHO1lBQzNCLEtBQUssRUFBSyxLQUFLLE9BQUk7WUFDbkIsTUFBTSxFQUFFLENBQUcsTUFBTSxHQUFHLEVBQUUsUUFBSTtZQUMxQixNQUFNLEVBQUUsVUFBVSxHQUFHLE1BQU0sR0FBRyxTQUFTO1NBQ3hDLENBQUE7UUFFRCxJQUFNLG9CQUFvQixHQUFHO1lBQzNCLEtBQUssRUFBSyxXQUFXLE9BQUk7WUFDekIsTUFBTSxFQUFLLE1BQU0sT0FBSTtTQUN0QixDQUFBO1FBRUQsTUFBTSxDQUFDLENBQ0wscUJBQUMsR0FBRyxJQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQU0sRUFBQyxHQUFHLEVBQUMsV0FBVyxFQUFDLFNBQVMsRUFBQyx5QkFBeUIsR0FDL0UscUJBQUMsR0FBRyxJQUFDLEtBQUssRUFBRSxtQkFBb0IsRUFBQyxTQUFTLEVBQUMsV0FBVyxHQUNuRCxZQUFZLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLFlBQVksRUFBRSxZQUFZLENBQUMsR0FBRyxJQUFLLEVBQzVFLHFCQUFDLEdBQUcsSUFBQyxHQUFHLEVBQUMsaUJBQWlCLEVBQ3JCLFNBQVMsRUFBQyxZQUFZLEVBQ3RCLEtBQUssRUFBRSxvQkFBcUIsRUFDNUIsT0FBTyxFQUFFLElBQUksQ0FBQyxlQUFnQixFQUM5QixRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVMsRUFDeEIsT0FBTyxFQUFFLElBQUksQ0FBQyxPQUFRLEVBQ3RCLFdBQVcsRUFBRSxJQUFJLENBQUMsZUFBZ0IsRUFDbEMsV0FBVyxFQUFFLElBQUksQ0FBQyxlQUFnQixFQUNsQyxTQUFTLEVBQUUsSUFBSSxDQUFDLGFBQWMsRUFDOUIsWUFBWSxFQUFFLElBQUksQ0FBQyxhQUFjLEVBQ2pDLFdBQVcsRUFBRSxVQUFDLENBQUM7WUFDWCxtQkFBbUI7WUFDbkIsQ0FBQyxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQ25CLENBQUMsQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN4QixDQUFFLEdBRUwscUJBQUMsR0FBRyxJQUFDLEdBQUcsRUFBQyxpQkFBaUIsRUFDckIsU0FBUyxFQUFDLFlBQVksRUFDdEIsS0FBSyxFQUFFLG9CQUFxQixFQUM1QixhQUFhLEVBQUcsSUFBSSxDQUFDLGlCQUFtQixHQUUxQyxJQUFJLENBQUMsS0FBSyxDQUFDLGVBQWUsRUFBRSxJQUFJLEVBQUUsYUFBYSxFQUFFLFdBQVcsRUFBRSxPQUFPLEVBQUUsY0FBYyxFQUFFLFlBQVksRUFBRSxTQUFTLENBQUUsRUFDaEgsSUFBSSxDQUFDLGFBQWEsQ0FBQyxlQUFlLEVBQUUsSUFBSSxFQUFFLGFBQWEsRUFBRSxXQUFXLEVBQUUsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsWUFBWSxDQUFFLEVBQ2hILElBQUksQ0FBQyxlQUFlLENBQUMsZUFBZSxFQUFFLElBQUksRUFBRSxhQUFhLEVBQUUsV0FBVyxFQUFFLFlBQVksRUFBRSxZQUFZLENBQUUsRUFDcEcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxlQUFlLEVBQUUsSUFBSSxFQUFFLGFBQWEsRUFBRSxXQUFXLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxZQUFZLENBQUUsRUFDakcsSUFBSSxDQUFDLFNBQVMsRUFBRyxFQUNqQixJQUFJLENBQUMsTUFBTSxDQUNWLGVBQWUsRUFDZixJQUFJLEVBQ0osYUFBYSxFQUNiLFdBQVcsRUFDWCxPQUFPLEVBQ1AsU0FBUyxFQUNULHNCQUFzQixFQUN0QixpQkFBaUIsQ0FFbEIsQ0FDRyxDQUNGLENBQ0YsQ0FDRixDQUNQLENBQUE7SUFDSCxDQUFDO0lBanpCUSxrQ0FBWSxHQUFHO1FBQ2xCLFlBQVksRUFBRSxHQUFHO1FBQ2pCLFFBQVEsRUFBRSxJQUFJLEdBQUcsRUFBRSxHQUFHLEVBQUU7UUFDeEIsY0FBYyxFQUFFLEVBQUU7UUFDbEIsV0FBVyxFQUFFLE1BQU07UUFDbkIsV0FBVyxFQUFFLEVBQUU7UUFDZixVQUFVLEVBQUUsRUFBRTtRQUNkLHNCQUFzQixFQUFFLEVBQUU7UUFDMUIsaUJBQWlCLEVBQUUsRUFBRTtRQUNyQixlQUFlLEVBQUUsSUFBSTtRQUVyQixPQUFPLEVBQUUsRUFBRSxHQUFHLEVBQUUsR0FBRyxJQUFJO1FBQ3ZCLE9BQU8sRUFBRSxDQUFDLEdBQUcsTUFBTSxHQUFHLEtBQUssR0FBRyxJQUFJO1FBRWxDLGNBQWMsRUFBRSxJQUFJO1FBQ3BCLE9BQU8sRUFBRSxJQUFJO1FBQ2IsU0FBUyxFQUFFLElBQUk7UUFDZixlQUFlLEVBQUUsS0FBSztRQUV0QixVQUFVLEVBQUUsS0FBSztRQUVqQixlQUFlLEVBQUUsS0FBSztRQUV0QixVQUFVLEVBQUUsSUFBSTtRQUNoQixZQUFZLEVBQUUsSUFBSTtRQUNsQixXQUFXLEVBQUUsSUFBSTtRQUNqQixZQUFZLEVBQUUsSUFBSTtRQUNsQixhQUFhLEVBQUUsSUFBSTtRQUNuQixpQkFBaUIsRUFBRSxJQUFJO1FBQ3ZCLGlCQUFpQixFQUFFLElBQUk7UUFFdkIsbUJBQW1CLEVBQUUsSUFBSTtRQUV6QixhQUFhLEVBQUUsSUFBSTtRQUVuQixnQkFBZ0IsRUFBRSxJQUFJO1FBQ3RCLGNBQWMsRUFBRSxJQUFJO1FBRXBCLG1CQUFtQixFQUFFLEtBQUs7UUFFMUIsS0FBSyxFQUFFLEVBQUU7UUFDVCxJQUFJLEVBQUUsV0FBVztRQUNqQixTQUFTLEVBQUUsZ0JBQWdCO1FBRTNCLHlIQUF5SDtRQUN6SCx5RkFBeUY7UUFDekYsZ0JBQWdCLEVBQUUsSUFBSTtRQUN0QixjQUFjLEVBQUUsSUFBSTtRQUNwQixZQUFZLEVBQUUsVUFBVSxnQkFBZ0IsRUFBRSxjQUFjO1lBQ3hELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxnQkFBZ0IsRUFBRSxjQUFjLENBQUMsQ0FBQTtRQUN6RCxDQUFDO1FBQ0QsMkVBQTJFO1FBQzNFLFVBQVUsRUFBRSxJQUFJO1FBQ2hCLHNEQUFzRDtRQUN0RCxjQUFjLEVBQUUsSUFBSTtRQUNwQixRQUFRLEVBQUUsSUFBSTtRQUNkLGdCQUFnQixFQUFFLElBQUk7UUFFdEIsY0FBYyxFQUFFLENBQUM7S0FDcEIsQ0FBQztJQXV2Qk4sNEJBQUM7QUFBRCxDQUFDLEFBbnpCRCxDQUFtRCxLQUFLLENBQUMsU0FBUyxHQW16QmpFO0FBbnpCRDt1Q0FtekJDLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCdcbmltcG9ydCAqIGFzIG1vbWVudCBmcm9tICdtb21lbnQnXG5pbXBvcnQgJy4vdGltZWxpbmUuc2NzcydcblxuaW1wb3J0IEl0ZW1zIGZyb20gJy4vaXRlbXMvaXRlbXMnXG5pbXBvcnQgSW5mb0xhYmVsIGZyb20gJy4vbGF5b3V0L2luZm8tbGFiZWwnXG5pbXBvcnQgU2lkZWJhciBmcm9tICcuL2xheW91dC9zaWRlYmFyJ1xuaW1wb3J0IEhlYWRlciBmcm9tICcuL2xheW91dC9oZWFkZXInXG5pbXBvcnQgVmVydGljYWxMaW5lcyBmcm9tICcuL2xpbmVzL3ZlcnRpY2FsLWxpbmVzJ1xuaW1wb3J0IEhvcml6b250YWxMaW5lcyBmcm9tICcuL2xpbmVzL2hvcml6b250YWwtbGluZXMnXG5pbXBvcnQgVG9kYXlMaW5lIGZyb20gJy4vbGluZXMvdG9kYXktbGluZSdcblxuaW1wb3J0IHtcbiAgICBnZXRNaW5Vbml0LCBnZXROZXh0VW5pdCwgZ2V0UGFyZW50UG9zaXRpb24sIF9nZXQsIF9sZW5ndGgsIHN0YWNrLCBub3N0YWNrLFxuICAgIGNhbGN1bGF0ZURpbWVuc2lvbnMsIGdldEdyb3VwT3JkZXJzLCBnZXRWaXNpYmxlSXRlbXMsXG4gICAgaGFzU29tZVBhcmVudFRoZUNsYXNzXG59IGZyb20gJy4vdXRpbHMudHMnXG5cblxuY29uc3QgZGVmYXVsdEtleXMgPSB7XG4gIGdyb3VwSWRLZXk6ICdpZCcsXG4gIGdyb3VwVGl0bGVLZXk6ICd0aXRsZScsXG4gIGl0ZW1JZEtleTogJ2lkJyxcbiAgaXRlbVRpdGxlS2V5OiAndGl0bGUnLFxuICBpdGVtRGl2VGl0bGVLZXk6ICd0aXRsZScsXG4gIGl0ZW1Hcm91cEtleTogJ2dyb3VwJyxcbiAgaXRlbVRpbWVTdGFydEtleTogJ3N0YXJ0X3RpbWUnLFxuICBpdGVtVGltZUVuZEtleTogJ2VuZF90aW1lJyxcbiAgaXRlbVRpdGxlSFRNTEtleTogJ3RpdGxlSFRNTCcsXG59O1xuXG5jb25zdCBkZWZhdWx0VGltZVN0ZXBzID0ge1xuICBzZWNvbmQ6IDEsXG4gIG1pbnV0ZTogMSxcbiAgaG91cjogMSxcbiAgZGF5OiAxLFxuICBtb250aDogMSxcbiAgeWVhcjogMVxufTtcblxuXG5pbnRlcmZhY2UgS2V5cyB7XG4gICAgW2tleTogc3RyaW5nXTogc3RyaW5nXG4gICAgaXRlbUlkS2V5XG4gICAgaXRlbUdyb3VwS2V5XG4gICAgZ3JvdXBJZEtleVxuICAgIGdyb3VwVGl0bGVLZXlcbn1cblxuXG5pbnRlcmZhY2UgUHJvcHMge1xuICAgIGdyb3VwczogYW55W11cbiAgICBpdGVtczogYW55W11cbiAgICBzaWRlYmFyV2lkdGg6IG51bWJlclxuICAgIGRyYWdTbmFwOiBudW1iZXJcbiAgICBtaW5SZXNpemVXaWR0aDogbnVtYmVyXG4gICAgZml4ZWRIZWFkZXI6ICdmaXhlZCcgfCAnYWJzb2x1dGUnIHwgJ25vbmUnXG4gICAgekluZGV4U3RhcnQ6IG51bWJlclxuICAgIGxpbmVIZWlnaHQ6IG51bWJlclxuICAgIGhlYWRlckxhYmVsR3JvdXBIZWlnaHQ6IG51bWJlclxuICAgIGhlYWRlckxhYmVsSGVpZ2h0OiBudW1iZXJcbiAgICBpdGVtSGVpZ2h0UmF0aW86IG51bWJlclxuXG4gICAgbWluWm9vbTogbnVtYmVyXG4gICAgbWF4Wm9vbTogbnVtYmVyXG5cbiAgICBjYW5DaGFuZ2VHcm91cDogYm9vbGVhblxuICAgIGNhbk1vdmU6IGJvb2xlYW5cbiAgICBjYW5SZXNpemU6IGJvb2xlYW5cbiAgICB1c2VSZXNpemVIYW5kbGU6IGJvb2xlYW5cblxuICAgIHN0YWNrSXRlbXM6IGJvb2xlYW5cblxuICAgIHRyYWRpdGlvbmFsWm9vbTogYm9vbGVhblxuXG4gICAgaXRlbVRvdWNoU2VuZHNDbGljazogYm9vbGVhblxuXG4gICAgb25JdGVtTW92ZTogKGl0ZW0sIGRyYWdUaW1lLCBuZXdHcm91cE9yZGVyKSA9PiB2b2lkXG4gICAgb25JdGVtUmVzaXplOiAoaXRlbSwgbmV3UmVzaXplRW5kKSA9PiB2b2lkXG4gICAgb25JdGVtQ2xpY2s6IChpdGVtLCBlKSA9PiB2b2lkXG4gICAgb25JdGVtU2VsZWN0OiAoaXRlbSwgZSkgPT4gdm9pZFxuICAgIG9uQ2FudmFzQ2xpY2s6IChncm91cElkLCB0aW1lLCBlKSA9PiB2b2lkXG4gICAgb25JdGVtRG91YmxlQ2xpY2s6ICgpID0+IHZvaWRcbiAgICBvbkl0ZW1Db250ZXh0TWVudTogKCkgPT4gdm9pZFxuICAgIG9uQ2FudmFzRG91YmxlQ2xpY2s6ICh0aW1lUG9zaXRpb24sIGdyb3VwKSA9PiB2b2lkXG5cbiAgICBtb3ZlUmVzaXplVmFsaWRhdG9yOiAoKSA9PiB2b2lkXG5cbiAgICBkYXlCYWNrZ3JvdW5kOiAoKSA9PiB2b2lkXG5cbiAgICBzdHlsZToge1twcm9wOiBzdHJpbmddOiBzdHJpbmd9XG4gICAga2V5czogS2V5c1xuXG4gICAgdGltZVN0ZXBzOiB7W3Byb3A6IHN0cmluZ106IHN0cmluZ31cblxuICAgIGRlZmF1bHRUaW1lU3RhcnQ6IHtbcHJvcDogc3RyaW5nXTogc3RyaW5nfVxuICAgIGRlZmF1bHRUaW1lRW5kOiB7W3Byb3A6IHN0cmluZ106IHN0cmluZ31cblxuICAgIHZpc2libGVUaW1lU3RhcnQ/XG4gICAgdmlzaWJsZVRpbWVFbmQ/XG4gICAgb25UaW1lQ2hhbmdlOiAoKSA9PiB2b2lkXG4gICAgb25UaW1lSW5pdDogKHZpc2libGVUaW1lU3RhcnQsIHZpc2libGVUaW1lRW5kKSA9PiB2b2lkXG4gICAgb25Cb3VuZHNDaGFuZ2U6IChjYW52YXNUaW1lU3RhcnQsIGNhbnZhc1RpbWVTdGFydFpvb20pID0+IHZvaWRcblxuICAgIGhpZGVDbGlwcGVkVGl0bGU/OiBib29sZWFuXG5cbiAgICBjbGlja1RvbGVyYW5jZT86IG51bWJlclxufVxuXG5cbmludGVyZmFjZSBTdGF0ZSB7XG4gICAgY2FudmFzVGltZVN0YXJ0P1xuICAgIHdpZHRoPzogbnVtYmVyXG5cbiAgICB2aXNpYmxlVGltZVN0YXJ0P1xuICAgIHZpc2libGVUaW1lRW5kP1xuXG4gICAgc2VsZWN0ZWRJdGVtP1xuICAgIGRyYWdUaW1lP1xuICAgIGRyYWdHcm91cFRpdGxlP1xuICAgIHJlc2l6ZUVuZD9cbiAgICBpc0RyYWdnaW5nPzogYm9vbGVhblxuICAgIHRvcE9mZnNldD86IG51bWJlclxuICAgIHJlc2l6aW5nSXRlbT9cblxuICAgIGRpbWVuc2lvbkl0ZW1zP1xuICAgIGhlaWdodD9cbiAgICBncm91cEhlaWdodHM/XG4gICAgZ3JvdXBUb3BzP1xuXG4gICAgZHJhZ2dpbmdJdGVtP1xuICAgIG5ld0dyb3VwT3JkZXI/XG4gICAgZHJhZ1N0YXJ0UG9zaXRpb24/XG4gICAgZHJhZ0xhc3RQb3NpdGlvbj9cbn1cblxuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBSZWFjdENhbGVuZGFyVGltZWxpbmUgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQ8UHJvcHMsIFN0YXRlPiB7XG4gICAgc3RhdGljIGRlZmF1bHRQcm9wcyA9IHtcbiAgICAgICAgc2lkZWJhcldpZHRoOiAxNTAsXG4gICAgICAgIGRyYWdTbmFwOiAxMDAwICogNjAgKiAxNSwgLy8gMTVtaW5cbiAgICAgICAgbWluUmVzaXplV2lkdGg6IDIwLFxuICAgICAgICBmaXhlZEhlYWRlcjogJ25vbmUnLCAvLyBmaXhlZCBvciBhYnNvbHV0ZSBvciBub25lXG4gICAgICAgIHpJbmRleFN0YXJ0OiAxMCxcbiAgICAgICAgbGluZUhlaWdodDogMzAsXG4gICAgICAgIGhlYWRlckxhYmVsR3JvdXBIZWlnaHQ6IDMwLFxuICAgICAgICBoZWFkZXJMYWJlbEhlaWdodDogMzAsXG4gICAgICAgIGl0ZW1IZWlnaHRSYXRpbzogMC42NSxcblxuICAgICAgICBtaW5ab29tOiA2MCAqIDYwICogMTAwMCwgLy8gMSBob3VyXG4gICAgICAgIG1heFpvb206IDUgKiAzNjUuMjQgKiA4NjQwMCAqIDEwMDAsIC8vIDUgeWVhcnNcblxuICAgICAgICBjYW5DaGFuZ2VHcm91cDogdHJ1ZSxcbiAgICAgICAgY2FuTW92ZTogdHJ1ZSxcbiAgICAgICAgY2FuUmVzaXplOiB0cnVlLFxuICAgICAgICB1c2VSZXNpemVIYW5kbGU6IGZhbHNlLFxuXG4gICAgICAgIHN0YWNrSXRlbXM6IGZhbHNlLFxuXG4gICAgICAgIHRyYWRpdGlvbmFsWm9vbTogZmFsc2UsXG5cbiAgICAgICAgb25JdGVtTW92ZTogbnVsbCxcbiAgICAgICAgb25JdGVtUmVzaXplOiBudWxsLFxuICAgICAgICBvbkl0ZW1DbGljazogbnVsbCxcbiAgICAgICAgb25JdGVtU2VsZWN0OiBudWxsLFxuICAgICAgICBvbkNhbnZhc0NsaWNrOiBudWxsLFxuICAgICAgICBvbkl0ZW1Eb3VibGVDbGljazogbnVsbCxcbiAgICAgICAgb25JdGVtQ29udGV4dE1lbnU6IG51bGwsXG5cbiAgICAgICAgbW92ZVJlc2l6ZVZhbGlkYXRvcjogbnVsbCxcblxuICAgICAgICBkYXlCYWNrZ3JvdW5kOiBudWxsLFxuXG4gICAgICAgIGRlZmF1bHRUaW1lU3RhcnQ6IG51bGwsXG4gICAgICAgIGRlZmF1bHRUaW1lRW5kOiBudWxsLFxuXG4gICAgICAgIGl0ZW1Ub3VjaFNlbmRzQ2xpY2s6IGZhbHNlLFxuXG4gICAgICAgIHN0eWxlOiB7fSxcbiAgICAgICAga2V5czogZGVmYXVsdEtleXMsXG4gICAgICAgIHRpbWVTdGVwczogZGVmYXVsdFRpbWVTdGVwcyxcblxuICAgICAgICAvLyBpZiB5b3UgcGFzcyBpbiB2aXNpYmxlVGltZVN0YXJ0IGFuZCB2aXNpYmxlVGltZUVuZCwgeW91IG11c3QgYWxzbyBwYXNzIG9uVGltZUNoYW5nZSh2aXNpYmxlVGltZVN0YXJ0LCB2aXNpYmxlVGltZUVuZCksXG4gICAgICAgIC8vIHdoaWNoIG5lZWRzIHRvIHVwZGF0ZSB0aGUgcHJvcHMgdmlzaWJsZVRpbWVTdGFydCBhbmQgdmlzaWJsZVRpbWVFbmQgdG8gdGhlIG9uZXMgcGFzc2VkXG4gICAgICAgIHZpc2libGVUaW1lU3RhcnQ6IG51bGwsXG4gICAgICAgIHZpc2libGVUaW1lRW5kOiBudWxsLFxuICAgICAgICBvblRpbWVDaGFuZ2U6IGZ1bmN0aW9uICh2aXNpYmxlVGltZVN0YXJ0LCB2aXNpYmxlVGltZUVuZCkge1xuICAgICAgICB0aGlzLnVwZGF0ZVNjcm9sbENhbnZhcyh2aXNpYmxlVGltZVN0YXJ0LCB2aXNpYmxlVGltZUVuZClcbiAgICAgICAgfSxcbiAgICAgICAgLy8gY2FsbGVkIGFmdGVyIHRoZSBjYWxlbmRhciBsb2FkcyBhbmQgdGhlIHZpc2libGUgdGltZSBoYXMgYmVlbiBjYWxjdWxhdGVkXG4gICAgICAgIG9uVGltZUluaXQ6IG51bGwsXG4gICAgICAgIC8vIGNhbGxlZCB3aGVuIHRoZSBjYW52YXMgYXJlYSBvZiB0aGUgY2FsZW5kYXIgY2hhbmdlc1xuICAgICAgICBvbkJvdW5kc0NoYW5nZTogbnVsbCxcbiAgICAgICAgY2hpbGRyZW46IG51bGwsXG4gICAgICAgIGhpZGVDbGlwcGVkVGl0bGU6IHRydWUsXG5cbiAgICAgICAgY2xpY2tUb2xlcmFuY2U6IDMsXG4gICAgfTtcblxuICAgIHJlc2l6ZUV2ZW50TGlzdGVuZXI7XG4gICAgbGFzdFRvdWNoRGlzdGFuY2U7XG4gICAgc2luZ2xlVG91Y2hTdGFydDtcbiAgICBsYXN0U2luZ2xlVG91Y2g7XG5cbiAgICByZWZzOiB7XG4gICAgICAgIFtrZXk6IHN0cmluZ106IEVsZW1lbnRcbiAgICAgICAgY29udGFpbmVyOiBFbGVtZW50XG4gICAgICAgIHNjcm9sbENvbXBvbmVudDogRWxlbWVudFxuICAgIH07XG5cbiAgICBjb25zdHJ1Y3RvciAocHJvcHMpIHtcbiAgICAgICAgc3VwZXIocHJvcHMpO1xuXG4gICAgICAgIGxldCB2aXNpYmxlVGltZVN0YXJ0ID0gbnVsbDtcbiAgICAgICAgbGV0IHZpc2libGVUaW1lRW5kID0gbnVsbDtcblxuICAgICAgICBpZiAodGhpcy5wcm9wcy5kZWZhdWx0VGltZVN0YXJ0ICYmIHRoaXMucHJvcHMuZGVmYXVsdFRpbWVFbmQpIHtcbiAgICAgICAgICB2aXNpYmxlVGltZVN0YXJ0ID0gdGhpcy5wcm9wcy5kZWZhdWx0VGltZVN0YXJ0LnZhbHVlT2YoKTtcbiAgICAgICAgICB2aXNpYmxlVGltZUVuZCA9IHRoaXMucHJvcHMuZGVmYXVsdFRpbWVFbmQudmFsdWVPZigpXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5wcm9wcy52aXNpYmxlVGltZVN0YXJ0ICYmIHRoaXMucHJvcHMudmlzaWJsZVRpbWVFbmQpIHtcbiAgICAgICAgICB2aXNpYmxlVGltZVN0YXJ0ID0gdGhpcy5wcm9wcy52aXNpYmxlVGltZVN0YXJ0O1xuICAgICAgICAgIHZpc2libGVUaW1lRW5kID0gdGhpcy5wcm9wcy52aXNpYmxlVGltZUVuZFxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHZpc2libGVUaW1lU3RhcnQgPSBNYXRoLm1pbiguLi50aGlzLnByb3BzLml0ZW1zLm1hcChpdGVtID0+IF9nZXQoaXRlbSwgJ3N0YXJ0JykuZ2V0VGltZSgpKSk7XG4gICAgICAgICAgdmlzaWJsZVRpbWVFbmQgPSBNYXRoLm1heCguLi50aGlzLnByb3BzLml0ZW1zLm1hcChpdGVtID0+IF9nZXQoaXRlbSwgJ2VuZCcpLmdldFRpbWUoKSkpO1xuXG4gICAgICAgICAgaWYgKCF2aXNpYmxlVGltZVN0YXJ0IHx8ICF2aXNpYmxlVGltZUVuZCkge1xuICAgICAgICAgICAgdmlzaWJsZVRpbWVTdGFydCA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpIC0gODY0MDAgKiA3ICogMTAwMDtcbiAgICAgICAgICAgIHZpc2libGVUaW1lRW5kID0gbmV3IERhdGUoKS5nZXRUaW1lKCkgKyA4NjQwMCAqIDcgKiAxMDAwXG4gICAgICAgICAgfVxuXG4gICAgICAgICAgaWYgKHRoaXMucHJvcHMub25UaW1lSW5pdCkge1xuICAgICAgICAgICAgdGhpcy5wcm9wcy5vblRpbWVJbml0KHZpc2libGVUaW1lU3RhcnQsIHZpc2libGVUaW1lRW5kKVxuICAgICAgICAgIH1cbiAgICB9XG5cbiAgICB0aGlzLnN0YXRlID0ge1xuICAgICAgd2lkdGg6IDEwMDAsXG5cbiAgICAgIHZpc2libGVUaW1lU3RhcnQ6IHZpc2libGVUaW1lU3RhcnQsXG4gICAgICB2aXNpYmxlVGltZUVuZDogdmlzaWJsZVRpbWVFbmQsXG4gICAgICBjYW52YXNUaW1lU3RhcnQ6IHZpc2libGVUaW1lU3RhcnQgLSAodmlzaWJsZVRpbWVFbmQgLSB2aXNpYmxlVGltZVN0YXJ0KSxcblxuICAgICAgc2VsZWN0ZWRJdGVtOiBudWxsLFxuICAgICAgZHJhZ1RpbWU6IG51bGwsXG4gICAgICBkcmFnR3JvdXBUaXRsZTogbnVsbCxcbiAgICAgIHJlc2l6ZUVuZDogbnVsbCxcbiAgICAgIGlzRHJhZ2dpbmc6IGZhbHNlLFxuICAgICAgdG9wT2Zmc2V0OiAwLFxuICAgICAgcmVzaXppbmdJdGVtOiBudWxsXG4gICAgfTtcblxuICAgIGNvbnN0IHtcbiAgICAgIGRpbWVuc2lvbkl0ZW1zLCBoZWlnaHQsIGdyb3VwSGVpZ2h0cywgZ3JvdXBUb3BzXG4gICAgfSA9IHRoaXMuc3RhY2tJdGVtcyhwcm9wcy5pdGVtcywgcHJvcHMuZ3JvdXBzLCB0aGlzLnN0YXRlLmNhbnZhc1RpbWVTdGFydCwgdGhpcy5zdGF0ZS52aXNpYmxlVGltZVN0YXJ0LCB0aGlzLnN0YXRlLnZpc2libGVUaW1lRW5kLCB0aGlzLnN0YXRlLndpZHRoKTtcblxuICAgIHRoaXMuc3RhdGUuZGltZW5zaW9uSXRlbXMgPSBkaW1lbnNpb25JdGVtcztcbiAgICB0aGlzLnN0YXRlLmhlaWdodCA9IGhlaWdodDtcbiAgICB0aGlzLnN0YXRlLmdyb3VwSGVpZ2h0cyA9IGdyb3VwSGVpZ2h0cztcbiAgICB0aGlzLnN0YXRlLmdyb3VwVG9wcyA9IGdyb3VwVG9wc1xuICB9XG5cbiAgY29tcG9uZW50RGlkTW91bnQgKCkge1xuICAgIHRoaXMucmVzaXplKCk7XG5cbiAgICB0aGlzLnJlc2l6ZUV2ZW50TGlzdGVuZXIgPSB7XG4gICAgICBoYW5kbGVFdmVudDogKGV2ZW50KSA9PiB7XG4gICAgICAgIHRoaXMucmVzaXplKClcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3Jlc2l6ZScsIHRoaXMucmVzaXplRXZlbnRMaXN0ZW5lcik7XG5cbiAgICB0aGlzLmxhc3RUb3VjaERpc3RhbmNlID0gbnVsbDtcblxuICAgIHRoaXMucmVmc1snc2Nyb2xsQ29tcG9uZW50J11bJ2FkZEV2ZW50TGlzdGVuZXInXSgndG91Y2hzdGFydCcsIHRoaXMudG91Y2hTdGFydCk7XG4gICAgdGhpcy5yZWZzWydzY3JvbGxDb21wb25lbnQnXVsnYWRkRXZlbnRMaXN0ZW5lciddKCd0b3VjaG1vdmUnLCB0aGlzLnRvdWNoTW92ZSk7XG4gICAgdGhpcy5yZWZzWydzY3JvbGxDb21wb25lbnQnXVsnYWRkRXZlbnRMaXN0ZW5lciddKCd0b3VjaGVuZCcsIHRoaXMudG91Y2hFbmQpXG4gIH1cblxuICBjb21wb25lbnRXaWxsVW5tb3VudCAoKSB7XG4gICAgd2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ3Jlc2l6ZScsIHRoaXMucmVzaXplRXZlbnRMaXN0ZW5lcik7XG4gICAgdGhpcy5yZWZzWydzY3JvbGxDb21wb25lbnQnXVsncmVtb3ZlRXZlbnRMaXN0ZW5lciddKCd0b3VjaHN0YXJ0JywgdGhpcy50b3VjaFN0YXJ0KTtcbiAgICB0aGlzLnJlZnNbJ3Njcm9sbENvbXBvbmVudCddWydyZW1vdmVFdmVudExpc3RlbmVyJ10oJ3RvdWNobW92ZScsIHRoaXMudG91Y2hNb3ZlKTtcbiAgICB0aGlzLnJlZnNbJ3Njcm9sbENvbXBvbmVudCddWydyZW1vdmVFdmVudExpc3RlbmVyJ10oJ3RvdWNoZW5kJywgdGhpcy50b3VjaEVuZClcbiAgfVxuXG4gIHRvdWNoU3RhcnQgPSAoZSkgPT4ge1xuICAgIGlmIChlLnRvdWNoZXMubGVuZ3RoID09PSAyKSB7XG4gICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgIHRoaXMubGFzdFRvdWNoRGlzdGFuY2UgPSBNYXRoLmFicyhlLnRvdWNoZXNbMF0uc2NyZWVuWCAtIGUudG91Y2hlc1sxXS5zY3JlZW5YKTtcbiAgICAgIHRoaXMuc2luZ2xlVG91Y2hTdGFydCA9IG51bGw7XG4gICAgICB0aGlzLmxhc3RTaW5nbGVUb3VjaCA9IG51bGxcbiAgICB9IGVsc2UgaWYgKGUudG91Y2hlcy5sZW5ndGggPT09IDEgJiYgdGhpcy5wcm9wcy5maXhlZEhlYWRlciA9PT0gJ2ZpeGVkJykge1xuICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICBsZXQgeCA9IGUudG91Y2hlc1swXS5jbGllbnRYO1xuICAgICAgbGV0IHkgPSBlLnRvdWNoZXNbMF0uY2xpZW50WTtcblxuICAgICAgdGhpcy5sYXN0VG91Y2hEaXN0YW5jZSA9IG51bGw7XG4gICAgICB0aGlzLnNpbmdsZVRvdWNoU3RhcnQgPSB7eDogeCwgeTogeSwgc2NyZWVuWTogd2luZG93LnBhZ2VZT2Zmc2V0fTtcbiAgICAgIHRoaXMubGFzdFNpbmdsZVRvdWNoID0ge3g6IHgsIHk6IHksIHNjcmVlblk6IHdpbmRvdy5wYWdlWU9mZnNldH1cbiAgICB9XG4gIH07XG5cbiAgdG91Y2hNb3ZlID0gKGUpID0+IHtcbiAgICBpZiAodGhpcy5zdGF0ZS5kcmFnVGltZSB8fCB0aGlzLnN0YXRlLnJlc2l6ZUVuZCkge1xuICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgcmV0dXJuXG4gICAgfVxuICAgIGlmICh0aGlzLmxhc3RUb3VjaERpc3RhbmNlICYmIGUudG91Y2hlcy5sZW5ndGggPT09IDIpIHtcbiAgICAgIGUucHJldmVudERlZmF1bHQoKTtcblxuICAgICAgbGV0IHRvdWNoRGlzdGFuY2UgPSBNYXRoLmFicyhlLnRvdWNoZXNbMF0uc2NyZWVuWCAtIGUudG91Y2hlc1sxXS5zY3JlZW5YKTtcblxuICAgICAgbGV0IHBhcmVudFBvc2l0aW9uID0gZ2V0UGFyZW50UG9zaXRpb24oZS5jdXJyZW50VGFyZ2V0KTtcbiAgICAgIGxldCB4UG9zaXRpb24gPSAoZS50b3VjaGVzWzBdLnNjcmVlblggKyBlLnRvdWNoZXNbMV0uc2NyZWVuWCkgLyAyIC0gcGFyZW50UG9zaXRpb24ueDtcblxuICAgICAgaWYgKHRvdWNoRGlzdGFuY2UgIT09IDAgJiYgdGhpcy5sYXN0VG91Y2hEaXN0YW5jZSAhPT0gMCkge1xuICAgICAgICB0aGlzLmNoYW5nZVpvb20odGhpcy5sYXN0VG91Y2hEaXN0YW5jZSAvIHRvdWNoRGlzdGFuY2UsIHhQb3NpdGlvbiAvIHRoaXMuc3RhdGUud2lkdGgpO1xuICAgICAgICB0aGlzLmxhc3RUb3VjaERpc3RhbmNlID0gdG91Y2hEaXN0YW5jZVxuICAgICAgfVxuICAgIH0gZWxzZSBpZiAodGhpcy5sYXN0U2luZ2xlVG91Y2ggJiYgZS50b3VjaGVzLmxlbmd0aCA9PT0gMSAmJiB0aGlzLnByb3BzLmZpeGVkSGVhZGVyID09PSAnZml4ZWQnKSB7XG4gICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgIGxldCB4ID0gZS50b3VjaGVzWzBdLmNsaWVudFg7XG4gICAgICBsZXQgeSA9IGUudG91Y2hlc1swXS5jbGllbnRZO1xuXG4gICAgICBsZXQgZGVsdGFYID0geCAtIHRoaXMubGFzdFNpbmdsZVRvdWNoLng7XG4gICAgICAvLyBsZXQgZGVsdGFZID0geSAtIHRoaXMubGFzdFNpbmdsZVRvdWNoLnlcblxuICAgICAgbGV0IGRlbHRhWDAgPSB4IC0gdGhpcy5zaW5nbGVUb3VjaFN0YXJ0Lng7XG4gICAgICBsZXQgZGVsdGFZMCA9IHkgLSB0aGlzLnNpbmdsZVRvdWNoU3RhcnQueTtcblxuICAgICAgdGhpcy5sYXN0U2luZ2xlVG91Y2ggPSB7eDogeCwgeTogeX07XG5cbiAgICAgIGxldCBtb3ZlWCA9IE1hdGguYWJzKGRlbHRhWDApICogMyA+IE1hdGguYWJzKGRlbHRhWTApO1xuICAgICAgbGV0IG1vdmVZID0gTWF0aC5hYnMoZGVsdGFZMCkgKiAzID4gTWF0aC5hYnMoZGVsdGFYMCk7XG5cbiAgICAgIGlmIChkZWx0YVggIT09IDAgJiYgbW92ZVgpIHtcbiAgICAgICAgICB0aGlzLnJlZnMuc2Nyb2xsQ29tcG9uZW50LnNjcm9sbExlZnQgLT0gZGVsdGFYO1xuICAgICAgfVxuICAgICAgaWYgKG1vdmVZKSB7XG4gICAgICAgIHdpbmRvdy5zY3JvbGxUbyh3aW5kb3cucGFnZVhPZmZzZXQsIHRoaXMuc2luZ2xlVG91Y2hTdGFydC5zY3JlZW5ZIC0gZGVsdGFZMClcbiAgICAgIH1cbiAgICB9XG4gIH07XG5cbiAgdG91Y2hFbmQgPSAoZSkgPT4ge1xuICAgIGlmICh0aGlzLmxhc3RUb3VjaERpc3RhbmNlKSB7XG4gICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgIHRoaXMubGFzdFRvdWNoRGlzdGFuY2UgPSBudWxsXG4gICAgfVxuICAgIGlmICh0aGlzLmxhc3RTaW5nbGVUb3VjaCkge1xuICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICB0aGlzLmxhc3RTaW5nbGVUb3VjaCA9IG51bGw7XG4gICAgICB0aGlzLnNpbmdsZVRvdWNoU3RhcnQgPSBudWxsXG4gICAgfVxuICB9O1xuXG4gIHJlc2l6ZSAoKSB7XG4gICAgLy8gRklYTUUgY3VycmVudGx5IHdoZW4gdGhlIGNvbXBvbmVudCBjcmVhdGVzIGEgc2Nyb2xsIHRoZSBzY3JvbGxiYXIgaXMgbm90IHVzZWQgaW4gdGhlIGluaXRpYWwgd2lkdGggY2FsY3VsYXRpb24sIHJlc2l6aW5nIGZpeGVzIHRoaXNcbiAgICBjb25zdCB7d2lkdGg6IGNvbnRhaW5lcldpZHRoLCB0b3A6IGNvbnRhaW5lclRvcH0gPSB0aGlzLnJlZnMuY29udGFpbmVyLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xuICAgIGxldCB3aWR0aCA9IGNvbnRhaW5lcldpZHRoIC0gdGhpcy5wcm9wcy5zaWRlYmFyV2lkdGg7XG5cbiAgICBjb25zdCB7XG4gICAgICBkaW1lbnNpb25JdGVtcywgaGVpZ2h0LCBncm91cEhlaWdodHMsIGdyb3VwVG9wc1xuICAgIH0gPSB0aGlzLnN0YWNrSXRlbXModGhpcy5wcm9wcy5pdGVtcywgdGhpcy5wcm9wcy5ncm91cHMsIHRoaXMuc3RhdGUuY2FudmFzVGltZVN0YXJ0LCB0aGlzLnN0YXRlLnZpc2libGVUaW1lU3RhcnQsIHRoaXMuc3RhdGUudmlzaWJsZVRpbWVFbmQsIHdpZHRoKTtcblxuICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgd2lkdGg6IHdpZHRoLFxuICAgICAgdG9wT2Zmc2V0OiBjb250YWluZXJUb3AgKyB3aW5kb3cucGFnZVlPZmZzZXQsXG4gICAgICBkaW1lbnNpb25JdGVtczogZGltZW5zaW9uSXRlbXMsXG4gICAgICBoZWlnaHQ6IGhlaWdodCxcbiAgICAgIGdyb3VwSGVpZ2h0czogZ3JvdXBIZWlnaHRzLFxuICAgICAgZ3JvdXBUb3BzOiBncm91cFRvcHNcbiAgICB9KTtcbiAgICB0aGlzLnJlZnMuc2Nyb2xsQ29tcG9uZW50LnNjcm9sbExlZnQgPSB3aWR0aFxuICB9XG5cbiAgb25TY3JvbGwgPSAoKSA9PiB7XG4gICAgY29uc3Qgc2Nyb2xsQ29tcG9uZW50ID0gdGhpcy5yZWZzLnNjcm9sbENvbXBvbmVudDtcbiAgICBjb25zdCBjYW52YXNUaW1lU3RhcnQgPSB0aGlzLnN0YXRlLmNhbnZhc1RpbWVTdGFydDtcbiAgICBjb25zdCBzY3JvbGxYID0gc2Nyb2xsQ29tcG9uZW50LnNjcm9sbExlZnQ7XG4gICAgY29uc3Qgem9vbSA9IHRoaXMuc3RhdGUudmlzaWJsZVRpbWVFbmQgLSB0aGlzLnN0YXRlLnZpc2libGVUaW1lU3RhcnQ7XG4gICAgY29uc3Qgd2lkdGggPSB0aGlzLnN0YXRlLndpZHRoO1xuICAgIGNvbnN0IHZpc2libGVUaW1lU3RhcnQgPSBjYW52YXNUaW1lU3RhcnQgKyAoem9vbSAqIHNjcm9sbFggLyB3aWR0aCk7XG5cbiAgICAvLyBtb3ZlIHRoZSB2aXJ0dWFsIGNhbnZhcyBpZiBuZWVkZWRcbiAgICBpZiAoc2Nyb2xsWCA8IHRoaXMuc3RhdGUud2lkdGggKiAwLjUpIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICBjYW52YXNUaW1lU3RhcnQ6IHRoaXMuc3RhdGUuY2FudmFzVGltZVN0YXJ0IC0gem9vbVxuICAgICAgfSk7XG4gICAgICBzY3JvbGxDb21wb25lbnQuc2Nyb2xsTGVmdCArPSB0aGlzLnN0YXRlLndpZHRoXG4gICAgfVxuICAgIGlmIChzY3JvbGxYID4gdGhpcy5zdGF0ZS53aWR0aCAqIDEuNSkge1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgIGNhbnZhc1RpbWVTdGFydDogdGhpcy5zdGF0ZS5jYW52YXNUaW1lU3RhcnQgKyB6b29tXG4gICAgICB9KTtcbiAgICAgIHNjcm9sbENvbXBvbmVudC5zY3JvbGxMZWZ0IC09IHRoaXMuc3RhdGUud2lkdGhcbiAgICB9XG5cbiAgICBpZiAodGhpcy5zdGF0ZS52aXNpYmxlVGltZVN0YXJ0ICE9PSB2aXNpYmxlVGltZVN0YXJ0IHx8IHRoaXMuc3RhdGUudmlzaWJsZVRpbWVFbmQgIT09IHZpc2libGVUaW1lU3RhcnQgKyB6b29tKSB7XG4gICAgICB0aGlzLnByb3BzLm9uVGltZUNoYW5nZS5iaW5kKHRoaXMpKHZpc2libGVUaW1lU3RhcnQsIHZpc2libGVUaW1lU3RhcnQgKyB6b29tKVxuICAgIH1cbiAgfTtcblxuICBjb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzIChuZXh0UHJvcHMpIHtcbiAgICBjb25zdCB7IHZpc2libGVUaW1lU3RhcnQsIHZpc2libGVUaW1lRW5kLCBpdGVtcywgZ3JvdXBzIH0gPSBuZXh0UHJvcHM7XG5cbiAgICBpZiAodmlzaWJsZVRpbWVTdGFydCAmJiB2aXNpYmxlVGltZUVuZCkge1xuICAgICAgdGhpcy51cGRhdGVTY3JvbGxDYW52YXModmlzaWJsZVRpbWVTdGFydCwgdmlzaWJsZVRpbWVFbmQsIGl0ZW1zICE9PSB0aGlzLnByb3BzLml0ZW1zIHx8IGdyb3VwcyAhPT0gdGhpcy5wcm9wcy5ncm91cHMsIGl0ZW1zLCBncm91cHMpXG4gICAgfVxuXG4gICAgaWYgKGl0ZW1zICE9PSB0aGlzLnByb3BzLml0ZW1zIHx8IGdyb3VwcyAhPT0gdGhpcy5wcm9wcy5ncm91cHMpIHtcbiAgICAgIHRoaXMudXBkYXRlRGltZW5zaW9ucyhpdGVtcywgZ3JvdXBzKVxuICAgIH1cbiAgfVxuXG4gIHVwZGF0ZURpbWVuc2lvbnMgKGl0ZW1zLCBncm91cHMpIHtcbiAgICBjb25zdCB7IGNhbnZhc1RpbWVTdGFydCwgdmlzaWJsZVRpbWVTdGFydCwgdmlzaWJsZVRpbWVFbmQsIHdpZHRoIH0gPSB0aGlzLnN0YXRlO1xuICAgIGNvbnN0IHtcbiAgICAgIGRpbWVuc2lvbkl0ZW1zLCBoZWlnaHQsIGdyb3VwSGVpZ2h0cywgZ3JvdXBUb3BzXG4gICAgfSA9IHRoaXMuc3RhY2tJdGVtcyhpdGVtcywgZ3JvdXBzLCBjYW52YXNUaW1lU3RhcnQsIHZpc2libGVUaW1lU3RhcnQsIHZpc2libGVUaW1lRW5kLCB3aWR0aCk7XG5cbiAgICB0aGlzLnNldFN0YXRlKHsgZGltZW5zaW9uSXRlbXMsIGhlaWdodCwgZ3JvdXBIZWlnaHRzLCBncm91cFRvcHMgfSlcbiAgfVxuXG4gIHVwZGF0ZVNjcm9sbENhbnZhcyAodmlzaWJsZVRpbWVTdGFydCwgdmlzaWJsZVRpbWVFbmQsIGZvcmNlVXBkYXRlRGltZW5zaW9ucz8sIHVwZGF0ZWRJdGVtcz8sIHVwZGF0ZWRHcm91cHM/KSB7XG4gICAgY29uc3Qgb2xkQ2FudmFzVGltZVN0YXJ0ID0gdGhpcy5zdGF0ZS5jYW52YXNUaW1lU3RhcnQ7XG4gICAgY29uc3Qgb2xkWm9vbSA9IHRoaXMuc3RhdGUudmlzaWJsZVRpbWVFbmQgLSB0aGlzLnN0YXRlLnZpc2libGVUaW1lU3RhcnQ7XG4gICAgY29uc3QgbmV3Wm9vbSA9IHZpc2libGVUaW1lRW5kIC0gdmlzaWJsZVRpbWVTdGFydDtcbiAgICBjb25zdCBpdGVtcyA9IHVwZGF0ZWRJdGVtcyB8fCB0aGlzLnByb3BzLml0ZW1zO1xuICAgIGNvbnN0IGdyb3VwcyA9IHVwZGF0ZWRHcm91cHMgfHwgdGhpcy5wcm9wcy5ncm91cHM7XG5cbiAgICBsZXQgbmV3U3RhdGU6IFN0YXRlID0ge1xuICAgICAgdmlzaWJsZVRpbWVTdGFydDogdmlzaWJsZVRpbWVTdGFydCxcbiAgICAgIHZpc2libGVUaW1lRW5kOiB2aXNpYmxlVGltZUVuZFxuICAgIH07XG5cbiAgICBsZXQgcmVzZXRDYW52YXMgPSBmYWxzZTtcblxuICAgIGNvbnN0IGNhbktlZXBDYW52YXMgPSB2aXNpYmxlVGltZVN0YXJ0ID49IG9sZENhbnZhc1RpbWVTdGFydCArIG9sZFpvb20gKiAwLjUgJiZcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdmlzaWJsZVRpbWVTdGFydCA8PSBvbGRDYW52YXNUaW1lU3RhcnQgKyBvbGRab29tICogMS41ICYmXG4gICAgICAgICAgICAgICAgICAgICAgICAgIHZpc2libGVUaW1lRW5kID49IG9sZENhbnZhc1RpbWVTdGFydCArIG9sZFpvb20gKiAxLjUgJiZcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdmlzaWJsZVRpbWVFbmQgPD0gb2xkQ2FudmFzVGltZVN0YXJ0ICsgb2xkWm9vbSAqIDIuNTtcblxuICAgIC8vIGlmIG5ldyB2aXNpYmxlIHRpbWUgaXMgaW4gdGhlIHJpZ2h0IGNhbnZhcyBhcmVhXG4gICAgaWYgKGNhbktlZXBDYW52YXMpIHtcbiAgICAgIC8vIGJ1dCB3ZSBuZWVkIHRvIHVwZGF0ZSB0aGUgc2Nyb2xsXG4gICAgICBjb25zdCBuZXdTY3JvbGxMZWZ0ID0gTWF0aC5yb3VuZCh0aGlzLnN0YXRlLndpZHRoICogKHZpc2libGVUaW1lU3RhcnQgLSBvbGRDYW52YXNUaW1lU3RhcnQpIC8gbmV3Wm9vbSk7XG4gICAgICBpZiAodGhpcy5yZWZzLnNjcm9sbENvbXBvbmVudC5zY3JvbGxMZWZ0ICE9PSBuZXdTY3JvbGxMZWZ0KSB7XG4gICAgICAgIHJlc2V0Q2FudmFzID0gdHJ1ZVxuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICByZXNldENhbnZhcyA9IHRydWVcbiAgICB9XG5cbiAgICBpZiAocmVzZXRDYW52YXMpIHtcbiAgICAgIC8vIFRvZG86IG5lZWQgdG8gY2FsY3VsYXRlIG5ldyBkaW1lbnNpb25zXG4gICAgICBuZXdTdGF0ZS5jYW52YXNUaW1lU3RhcnQgPSB2aXNpYmxlVGltZVN0YXJ0IC0gbmV3Wm9vbTtcbiAgICAgIHRoaXMucmVmcy5zY3JvbGxDb21wb25lbnQuc2Nyb2xsTGVmdCA9IHRoaXMuc3RhdGUud2lkdGg7XG5cbiAgICAgIGlmICh0aGlzLnByb3BzLm9uQm91bmRzQ2hhbmdlKSB7XG4gICAgICAgIHRoaXMucHJvcHMub25Cb3VuZHNDaGFuZ2UobmV3U3RhdGUuY2FudmFzVGltZVN0YXJ0LCBuZXdTdGF0ZS5jYW52YXNUaW1lU3RhcnQgKyBuZXdab29tICogMylcbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAocmVzZXRDYW52YXMgfHwgZm9yY2VVcGRhdGVEaW1lbnNpb25zKSB7XG4gICAgICBjb25zdCBjYW52YXNUaW1lU3RhcnQgPSBuZXdTdGF0ZS5jYW52YXNUaW1lU3RhcnQgPyBuZXdTdGF0ZS5jYW52YXNUaW1lU3RhcnQgOiBvbGRDYW52YXNUaW1lU3RhcnQ7XG4gICAgICBjb25zdCB7XG4gICAgICAgIGRpbWVuc2lvbkl0ZW1zLCBoZWlnaHQsIGdyb3VwSGVpZ2h0cywgZ3JvdXBUb3BzXG4gICAgICB9ID0gdGhpcy5zdGFja0l0ZW1zKGl0ZW1zLCBncm91cHMsIGNhbnZhc1RpbWVTdGFydCwgdmlzaWJsZVRpbWVTdGFydCwgdmlzaWJsZVRpbWVFbmQsIHRoaXMuc3RhdGUud2lkdGgpO1xuICAgICAgbmV3U3RhdGUuZGltZW5zaW9uSXRlbXMgPSBkaW1lbnNpb25JdGVtcztcbiAgICAgIG5ld1N0YXRlLmhlaWdodCA9IGhlaWdodDtcbiAgICAgIG5ld1N0YXRlLmdyb3VwSGVpZ2h0cyA9IGdyb3VwSGVpZ2h0cztcbiAgICAgIG5ld1N0YXRlLmdyb3VwVG9wcyA9IGdyb3VwVG9wc1xuICAgIH1cblxuICAgIHRoaXMuc2V0U3RhdGUobmV3U3RhdGUpXG4gIH1cblxuICBvbldoZWVsID0gKGUpID0+IHtcbiAgICBjb25zdCB7IHRyYWRpdGlvbmFsWm9vbSB9ID0gdGhpcy5wcm9wcztcbiAgICBpZiAoZS5jdHJsS2V5KSB7XG4gICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICBjb25zdCBwYXJlbnRQb3NpdGlvbiA9IGdldFBhcmVudFBvc2l0aW9uKGUuY3VycmVudFRhcmdldCk7XG4gICAgICBjb25zdCB4UG9zaXRpb24gPSBlLmNsaWVudFggLSBwYXJlbnRQb3NpdGlvbi54O1xuICAgICAgdGhpcy5jaGFuZ2Vab29tKDEuMCArIGUuZGVsdGFZIC8gNTAsIHhQb3NpdGlvbiAvIHRoaXMuc3RhdGUud2lkdGgpXG4gICAgfSBlbHNlIGlmIChlLnNoaWZ0S2V5KSB7XG4gICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICBjb25zdCBzY3JvbGxDb21wb25lbnQgPSB0aGlzLnJlZnMuc2Nyb2xsQ29tcG9uZW50O1xuICAgICAgc2Nyb2xsQ29tcG9uZW50LnNjcm9sbExlZnQgKz0gZS5kZWx0YVlcbiAgICB9IGVsc2UgaWYgKGUuYWx0S2V5KSB7XG4gICAgICBjb25zdCBwYXJlbnRQb3NpdGlvbiA9IGdldFBhcmVudFBvc2l0aW9uKGUuY3VycmVudFRhcmdldCk7XG4gICAgICBjb25zdCB4UG9zaXRpb24gPSBlLmNsaWVudFggLSBwYXJlbnRQb3NpdGlvbi54O1xuICAgICAgdGhpcy5jaGFuZ2Vab29tKDEuMCArIGUuZGVsdGFZIC8gNTAwLCB4UG9zaXRpb24gLyB0aGlzLnN0YXRlLndpZHRoKVxuICAgIH0gZWxzZSB7XG4gICAgICBpZiAodGhpcy5wcm9wcy5maXhlZEhlYWRlciA9PT0gJ2ZpeGVkJykge1xuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIGlmIChlLmRlbHRhWCAhPT0gMCkge1xuICAgICAgICAgIGlmICghdHJhZGl0aW9uYWxab29tKSB7XG4gICAgICAgICAgICB0aGlzLnJlZnMuc2Nyb2xsQ29tcG9uZW50LnNjcm9sbExlZnQgKz0gZS5kZWx0YVhcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGUuZGVsdGFZICE9PSAwKSB7XG4gICAgICAgICAgd2luZG93LnNjcm9sbFRvKHdpbmRvdy5wYWdlWE9mZnNldCwgd2luZG93LnBhZ2VZT2Zmc2V0ICsgZS5kZWx0YVkpO1xuICAgICAgICAgIGlmICh0cmFkaXRpb25hbFpvb20pIHtcbiAgICAgICAgICAgIGNvbnN0IHBhcmVudFBvc2l0aW9uID0gZ2V0UGFyZW50UG9zaXRpb24oZS5jdXJyZW50VGFyZ2V0KTtcbiAgICAgICAgICAgIGNvbnN0IHhQb3NpdGlvbiA9IGUuY2xpZW50WCAtIHBhcmVudFBvc2l0aW9uLng7XG4gICAgICAgICAgICB0aGlzLmNoYW5nZVpvb20oMS4wICsgZS5kZWx0YVkgLyA1MCwgeFBvc2l0aW9uIC8gdGhpcy5zdGF0ZS53aWR0aClcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH07XG5cbiAgem9vbUluIChlKSB7XG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgdGhpcy5jaGFuZ2Vab29tKDAuNzUpXG4gIH1cblxuICB6b29tT3V0IChlKSB7XG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgdGhpcy5jaGFuZ2Vab29tKDEuMjUpXG4gIH1cblxuICBjaGFuZ2Vab29tIChzY2FsZSwgb2Zmc2V0ID0gMC41KSB7XG4gICAgY29uc3QgeyBtaW5ab29tLCBtYXhab29tIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IG9sZFpvb20gPSB0aGlzLnN0YXRlLnZpc2libGVUaW1lRW5kIC0gdGhpcy5zdGF0ZS52aXNpYmxlVGltZVN0YXJ0O1xuICAgIGNvbnN0IG5ld1pvb20gPSBNYXRoLm1pbihNYXRoLm1heChNYXRoLnJvdW5kKG9sZFpvb20gKiBzY2FsZSksIG1pblpvb20pLCBtYXhab29tKTsgLy8gbWluIDEgbWluLCBtYXggMjAgeWVhcnNcbiAgICBjb25zdCBuZXdWaXNpYmxlVGltZVN0YXJ0ID0gTWF0aC5yb3VuZCh0aGlzLnN0YXRlLnZpc2libGVUaW1lU3RhcnQgKyAob2xkWm9vbSAtIG5ld1pvb20pICogb2Zmc2V0KTtcblxuICAgIHRoaXMucHJvcHMub25UaW1lQ2hhbmdlLmJpbmQodGhpcykobmV3VmlzaWJsZVRpbWVTdGFydCwgbmV3VmlzaWJsZVRpbWVTdGFydCArIG5ld1pvb20pXG4gIH1cblxuICBzaG93UGVyaW9kID0gKGZyb20sIHVuaXQpID0+IHtcbiAgICBsZXQgdmlzaWJsZVRpbWVTdGFydCA9IGZyb20udmFsdWVPZigpO1xuICAgIGxldCB2aXNpYmxlVGltZUVuZCA9IG1vbWVudChmcm9tKS5hZGQoMSwgdW5pdCkudmFsdWVPZigpO1xuICAgIGxldCB6b29tID0gdmlzaWJsZVRpbWVFbmQgLSB2aXNpYmxlVGltZVN0YXJ0O1xuXG4gICAgLy8gY2FuJ3Qgem9vbSBpbiBtb3JlIHRoYW4gdG8gc2hvdyBvbmUgaG91clxuICAgIGlmICh6b29tIDwgMzYwMDAwKSB7XG4gICAgICByZXR1cm5cbiAgICB9XG5cbiAgICAvLyBjbGlja2VkIG9uIHRoZSBiaWcgaGVhZGVyIGFuZCBhbHJlYWR5IGZvY3VzZWQgaGVyZSwgem9vbSBvdXRcbiAgICBpZiAodW5pdCAhPT0gJ3llYXInICYmIHRoaXMuc3RhdGUudmlzaWJsZVRpbWVTdGFydCA9PT0gdmlzaWJsZVRpbWVTdGFydCAmJiB0aGlzLnN0YXRlLnZpc2libGVUaW1lRW5kID09PSB2aXNpYmxlVGltZUVuZCkge1xuICAgICAgbGV0IG5leHRVbml0ID0gZ2V0TmV4dFVuaXQodW5pdCk7XG5cbiAgICAgIHZpc2libGVUaW1lU3RhcnQgPSBmcm9tLnN0YXJ0T2YobmV4dFVuaXQpLnZhbHVlT2YoKTtcbiAgICAgIHZpc2libGVUaW1lRW5kID0gbW9tZW50KHZpc2libGVUaW1lU3RhcnQpLmFkZCgxLCBuZXh0VW5pdCkudmFsdWVPZigpOyAgLy8gVE9ETzogY2hlY2sgdHlwZSBvZiB2aXNpYmxlVGltZUVuZFxuICAgICAgem9vbSA9IHZpc2libGVUaW1lRW5kIC0gdmlzaWJsZVRpbWVTdGFydFxuICAgIH1cblxuICAgIHRoaXMucHJvcHMub25UaW1lQ2hhbmdlLmJpbmQodGhpcykodmlzaWJsZVRpbWVTdGFydCwgdmlzaWJsZVRpbWVTdGFydCArIHpvb20pXG4gIH07XG5cbiAgc2VsZWN0SXRlbSA9IChpdGVtLCBjbGlja1R5cGU/LCBlPykgPT4ge1xuICAgIGlmICh0aGlzLnN0YXRlLnNlbGVjdGVkSXRlbSA9PT0gaXRlbSB8fCAodGhpcy5wcm9wcy5pdGVtVG91Y2hTZW5kc0NsaWNrICYmIGNsaWNrVHlwZSA9PT0gJ3RvdWNoJykpIHtcbiAgICAgIGlmIChpdGVtICYmIHRoaXMucHJvcHMub25JdGVtQ2xpY2spIHtcbiAgICAgICAgdGhpcy5wcm9wcy5vbkl0ZW1DbGljayhpdGVtLCBlKVxuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHtzZWxlY3RlZEl0ZW06IGl0ZW19KTtcbiAgICAgIGlmIChpdGVtICYmIHRoaXMucHJvcHMub25JdGVtU2VsZWN0KSB7XG4gICAgICAgIHRoaXMucHJvcHMub25JdGVtU2VsZWN0KGl0ZW0sIGUpXG4gICAgICB9XG4gICAgfVxuICB9O1xuXG4gIHJvd0FuZFRpbWVGcm9tRXZlbnQgKGUpIHtcbiAgICBjb25zdCB7IGxpbmVIZWlnaHQsIGRyYWdTbmFwIH0gPSB0aGlzLnByb3BzXG4gICAgY29uc3QgeyB3aWR0aCwgdmlzaWJsZVRpbWVTdGFydCwgdmlzaWJsZVRpbWVFbmQgfSA9IHRoaXMuc3RhdGVcblxuICAgIGNvbnN0IHBhcmVudFBvc2l0aW9uID0gZ2V0UGFyZW50UG9zaXRpb24oZS5jdXJyZW50VGFyZ2V0KVxuICAgIGNvbnN0IHggPSBlLmNsaWVudFggLSBwYXJlbnRQb3NpdGlvbi54XG4gICAgY29uc3QgeSA9IGUuY2xpZW50WSAtIHBhcmVudFBvc2l0aW9uLnlcblxuICAgIGNvbnN0IHJvdyA9IE1hdGguZmxvb3IoKHkgLSAobGluZUhlaWdodCAqIDIpKSAvIGxpbmVIZWlnaHQpXG4gICAgbGV0IHRpbWUgPSBNYXRoLnJvdW5kKHZpc2libGVUaW1lU3RhcnQgKyB4IC8gd2lkdGggKiAodmlzaWJsZVRpbWVFbmQgLSB2aXNpYmxlVGltZVN0YXJ0KSlcbiAgICB0aW1lID0gTWF0aC5mbG9vcih0aW1lIC8gZHJhZ1NuYXApICogZHJhZ1NuYXBcblxuICAgIHJldHVybiBbcm93LCB0aW1lXVxuICB9XG5cbiAgc2Nyb2xsQXJlYUNsaWNrID0gKGUpID0+IHtcbiAgICAvLyBpZiBub3QgY2xpY2tpbmcgb24gYW4gaXRlbVxuXG4gICAgaWYgKCFoYXNTb21lUGFyZW50VGhlQ2xhc3MoZS50YXJnZXQsICdyY3QtaXRlbScpKSB7XG4gICAgICBpZiAodGhpcy5zdGF0ZS5zZWxlY3RlZEl0ZW0pIHtcbiAgICAgICAgdGhpcy5zZWxlY3RJdGVtKG51bGwpXG4gICAgICB9IGVsc2UgaWYgKHRoaXMucHJvcHMub25DYW52YXNDbGljaykge1xuICAgICAgICBjb25zdCBbcm93LCB0aW1lXSA9IHRoaXMucm93QW5kVGltZUZyb21FdmVudChlKVxuICAgICAgICBpZiAocm93ID49IDAgJiYgcm93IDwgdGhpcy5wcm9wcy5ncm91cHMubGVuZ3RoKSB7XG4gICAgICAgICAgY29uc3QgZ3JvdXBJZCA9IF9nZXQodGhpcy5wcm9wcy5ncm91cHNbcm93XSwgdGhpcy5wcm9wcy5rZXlzLmdyb3VwSWRLZXkpXG4gICAgICAgICAgdGhpcy5wcm9wcy5vbkNhbnZhc0NsaWNrKGdyb3VwSWQsIHRpbWUsIGUpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBkcmFnSXRlbSA9IChpdGVtLCBkcmFnVGltZSwgbmV3R3JvdXBPcmRlcikgPT4ge1xuICAgIGxldCBuZXdHcm91cCA9IHRoaXMucHJvcHMuZ3JvdXBzW25ld0dyb3VwT3JkZXJdO1xuICAgIGNvbnN0IGtleXMgPSB0aGlzLnByb3BzLmtleXM7XG5cbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIGRyYWdnaW5nSXRlbTogaXRlbSxcbiAgICAgIGRyYWdUaW1lOiBkcmFnVGltZSxcbiAgICAgIG5ld0dyb3VwT3JkZXI6IG5ld0dyb3VwT3JkZXIsXG4gICAgICBkcmFnR3JvdXBUaXRsZTogbmV3R3JvdXAgPyBfZ2V0KG5ld0dyb3VwLCBrZXlzLmdyb3VwVGl0bGVLZXkpIDogJydcbiAgICB9KVxuICB9XG5cbiAgZHJvcEl0ZW0gPSAoaXRlbSwgZHJhZ1RpbWUsIG5ld0dyb3VwT3JkZXIpID0+IHtcbiAgICB0aGlzLnNldFN0YXRlKHtkcmFnZ2luZ0l0ZW06IG51bGwsIGRyYWdUaW1lOiBudWxsLCBkcmFnR3JvdXBUaXRsZTogbnVsbH0pXG4gICAgaWYgKHRoaXMucHJvcHMub25JdGVtTW92ZSkge1xuICAgICAgdGhpcy5wcm9wcy5vbkl0ZW1Nb3ZlKGl0ZW0sIGRyYWdUaW1lLCBuZXdHcm91cE9yZGVyKVxuICAgIH1cbiAgfVxuXG4gIHJlc2l6aW5nSXRlbSA9IChpdGVtLCBuZXdSZXNpemVFbmQpID0+IHtcbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIHJlc2l6aW5nSXRlbTogaXRlbSxcbiAgICAgIHJlc2l6ZUVuZDogbmV3UmVzaXplRW5kXG4gICAgfSlcbiAgfVxuXG4gIHJlc2l6ZWRJdGVtID0gKGl0ZW0sIG5ld1Jlc2l6ZUVuZCkgPT4ge1xuICAgIHRoaXMuc2V0U3RhdGUoe3Jlc2l6aW5nSXRlbTogbnVsbCwgcmVzaXplRW5kOiBudWxsfSk7XG4gICAgaWYgKHRoaXMucHJvcHMub25JdGVtUmVzaXplKSB7XG4gICAgICB0aGlzLnByb3BzLm9uSXRlbVJlc2l6ZShpdGVtLCBuZXdSZXNpemVFbmQpXG4gICAgfVxuICB9XG5cbiAgaGFuZGxlTW91c2VEb3duID0gKGUpID0+IHtcbiAgICBjb25zdCB7IHRvcE9mZnNldCB9ID0gdGhpcy5zdGF0ZVxuICAgIGNvbnN0IHsgcGFnZVkgfSA9IGVcbiAgICBjb25zdCB7IGhlYWRlckxhYmVsR3JvdXBIZWlnaHQsIGhlYWRlckxhYmVsSGVpZ2h0IH0gPSB0aGlzLnByb3BzXG4gICAgY29uc3QgaGVhZGVySGVpZ2h0ID0gaGVhZGVyTGFiZWxHcm91cEhlaWdodCArIGhlYWRlckxhYmVsSGVpZ2h0XG5cbiAgICBpZiAocGFnZVkgLSB0b3BPZmZzZXQgPiBoZWFkZXJIZWlnaHQpIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoe2lzRHJhZ2dpbmc6IHRydWUsIGRyYWdTdGFydFBvc2l0aW9uOiBlLnBhZ2VYLCBkcmFnTGFzdFBvc2l0aW9uOiBlLnBhZ2VYfSlcbiAgICB9XG4gIH1cblxuICBoYW5kbGVNb3VzZU1vdmUgPSAoZSkgPT4ge1xuICAgICAgaWYgKGUuYnV0dG9ucyA9PT0gMCAmJiB0aGlzLnN0YXRlLmlzRHJhZ2dpbmcpIHtcbiAgICAgICAgICAvLyBkaXNhYmxlIGRyYWdnaW5nIGlmIG1vdXNlIGJ1dHRvbnMgYXJlIG5vdCBwcmVzc2VkXG4gICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7aXNEcmFnZ2luZzogZmFsc2UsIGRyYWdTdGFydFBvc2l0aW9uOiBudWxsLCBkcmFnTGFzdFBvc2l0aW9uOiBudWxsfSk7XG4gICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUuaXNEcmFnZ2luZyAmJiAhdGhpcy5zdGF0ZS5kcmFnZ2luZ0l0ZW0gJiYgIXRoaXMuc3RhdGUucmVzaXppbmdJdGVtKSB7XG4gICAgICAgICAgdGhpcy5yZWZzLnNjcm9sbENvbXBvbmVudC5zY3JvbGxMZWZ0ICs9IHRoaXMuc3RhdGUuZHJhZ0xhc3RQb3NpdGlvbiAtIGUucGFnZVg7XG4gICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7ZHJhZ0xhc3RQb3NpdGlvbjogZS5wYWdlWH0pO1xuICAgICAgfVxuICB9XG5cbiAgaGFuZGxlTW91c2VVcCA9IChlKSA9PiB7XG4gICAgY29uc3QgeyBkcmFnU3RhcnRQb3NpdGlvbiB9ID0gdGhpcy5zdGF0ZTtcbiAgICBpZiAoTWF0aC5hYnMoZHJhZ1N0YXJ0UG9zaXRpb24gLSBlLnBhZ2VYKSA8PSB0aGlzLnByb3BzLmNsaWNrVG9sZXJhbmNlKSB7XG4gICAgICB0aGlzLnNjcm9sbEFyZWFDbGljayhlKVxuICAgIH1cblxuICAgIHRoaXMuc2V0U3RhdGUoe2lzRHJhZ2dpbmc6IGZhbHNlLCBkcmFnU3RhcnRQb3NpdGlvbjogbnVsbCwgZHJhZ0xhc3RQb3NpdGlvbjogbnVsbH0pXG4gIH1cblxuICB0b2RheUxpbmUgKGNhbnZhc1RpbWVTdGFydCwgem9vbSwgY2FudmFzVGltZUVuZCwgY2FudmFzV2lkdGgsIG1pblVuaXQsIGhlaWdodCwgaGVhZGVySGVpZ2h0KSB7XG4gICAgcmV0dXJuIChcbiAgICAgIDxUb2RheUxpbmUgY2FudmFzVGltZVN0YXJ0PXtjYW52YXNUaW1lU3RhcnR9XG4gICAgICAgICAgICAgICAgIGNhbnZhc1RpbWVFbmQ9e2NhbnZhc1RpbWVFbmR9XG4gICAgICAgICAgICAgICAgIGNhbnZhc1dpZHRoPXtjYW52YXNXaWR0aH1cbiAgICAgICAgICAgICAgICAgbGluZUhlaWdodD17dGhpcy5wcm9wcy5saW5lSGVpZ2h0fVxuICAgICAgICAgICAgICAgICBsaW5lQ291bnQ9e19sZW5ndGgodGhpcy5wcm9wcy5ncm91cHMpfVxuICAgICAgICAgICAgICAgICBoZWlnaHQ9e2hlaWdodH1cbiAgICAgICAgICAgICAgICAgaGVhZGVySGVpZ2h0PXtoZWFkZXJIZWlnaHR9XG4gICAgICAvPlxuICAgIClcbiAgfVxuXG4gIHZlcnRpY2FsTGluZXMgKGNhbnZhc1RpbWVTdGFydCwgem9vbSwgY2FudmFzVGltZUVuZCwgY2FudmFzV2lkdGgsIG1pblVuaXQsIHRpbWVTdGVwcywgaGVpZ2h0LCBoZWFkZXJIZWlnaHQpIHtcbiAgICByZXR1cm4gKFxuICAgICAgPFZlcnRpY2FsTGluZXMgY2FudmFzVGltZVN0YXJ0PXtjYW52YXNUaW1lU3RhcnR9XG4gICAgICAgICAgICAgICAgICAgICBjYW52YXNUaW1lRW5kPXtjYW52YXNUaW1lRW5kfVxuICAgICAgICAgICAgICAgICAgICAgY2FudmFzV2lkdGg9e2NhbnZhc1dpZHRofVxuICAgICAgICAgICAgICAgICAgICAgbGluZUhlaWdodD17dGhpcy5wcm9wcy5saW5lSGVpZ2h0fVxuICAgICAgICAgICAgICAgICAgICAgbGluZUNvdW50PXtfbGVuZ3RoKHRoaXMucHJvcHMuZ3JvdXBzKX1cbiAgICAgICAgICAgICAgICAgICAgIG1pblVuaXQ9e21pblVuaXR9XG4gICAgICAgICAgICAgICAgICAgICB0aW1lU3RlcHM9e3RpbWVTdGVwc31cbiAgICAgICAgICAgICAgICAgICAgIGZpeGVkSGVhZGVyPXt0aGlzLnByb3BzLmZpeGVkSGVhZGVyfVxuICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0PXtoZWlnaHR9XG4gICAgICAgICAgICAgICAgICAgICBoZWFkZXJIZWlnaHQ9e2hlYWRlckhlaWdodH1cbiAgICAgIC8+XG4gICAgKVxuICB9XG5cbiAgaG9yaXpvbnRhbExpbmVzIChjYW52YXNUaW1lU3RhcnQsIHpvb20sIGNhbnZhc1RpbWVFbmQsIGNhbnZhc1dpZHRoLCBncm91cEhlaWdodHMsIGhlYWRlckhlaWdodCkge1xuICAgIHJldHVybiAoXG4gICAgICA8SG9yaXpvbnRhbExpbmVzIGNhbnZhc1dpZHRoPXtjYW52YXNXaWR0aH1cbiAgICAgICAgICAgICAgICAgICAgICAgbGluZUhlaWdodD17dGhpcy5wcm9wcy5saW5lSGVpZ2h0fVxuICAgICAgICAgICAgICAgICAgICAgICBsaW5lQ291bnQ9e19sZW5ndGgodGhpcy5wcm9wcy5ncm91cHMpfVxuICAgICAgICAgICAgICAgICAgICAgICBncm91cHM9e3RoaXMucHJvcHMuZ3JvdXBzfVxuICAgICAgICAgICAgICAgICAgICAgICBncm91cEhlaWdodHM9e2dyb3VwSGVpZ2h0c31cbiAgICAgICAgICAgICAgICAgICAgICAgaGVhZGVySGVpZ2h0PXtoZWFkZXJIZWlnaHR9XG4gICAgICAvPlxuICAgIClcbiAgfVxuXG4gIGl0ZW1zIChjYW52YXNUaW1lU3RhcnQsIHpvb20sIGNhbnZhc1RpbWVFbmQsIGNhbnZhc1dpZHRoLCBtaW5Vbml0LCBkaW1lbnNpb25JdGVtcywgZ3JvdXBIZWlnaHRzLCBncm91cFRvcHMpIHtcbiAgICByZXR1cm4gKFxuICAgICAgPEl0ZW1zIGNhbnZhc1RpbWVTdGFydD17Y2FudmFzVGltZVN0YXJ0fVxuICAgICAgICAgICAgIGNhbnZhc1RpbWVFbmQ9e2NhbnZhc1RpbWVFbmR9XG4gICAgICAgICAgICAgY2FudmFzV2lkdGg9e2NhbnZhc1dpZHRofVxuICAgICAgICAgICAgIGxpbmVIZWlnaHQ9e3RoaXMucHJvcHMubGluZUhlaWdodH1cbiAgICAgICAgICAgICBsaW5lQ291bnQ9e19sZW5ndGgodGhpcy5wcm9wcy5ncm91cHMpfVxuICAgICAgICAgICAgIGRpbWVuc2lvbkl0ZW1zPXtkaW1lbnNpb25JdGVtc31cbiAgICAgICAgICAgICBtaW5Vbml0PXttaW5Vbml0fVxuICAgICAgICAgICAgIGdyb3VwSGVpZ2h0cz17Z3JvdXBIZWlnaHRzfVxuICAgICAgICAgICAgIGdyb3VwVG9wcz17Z3JvdXBUb3BzfVxuICAgICAgICAgICAgIGl0ZW1zPXt0aGlzLnByb3BzLml0ZW1zfVxuICAgICAgICAgICAgIGdyb3Vwcz17dGhpcy5wcm9wcy5ncm91cHN9XG4gICAgICAgICAgICAga2V5cz17dGhpcy5wcm9wcy5rZXlzfVxuICAgICAgICAgICAgIHNlbGVjdGVkSXRlbT17dGhpcy5zdGF0ZS5zZWxlY3RlZEl0ZW19XG4gICAgICAgICAgICAgZHJhZ1NuYXA9e3RoaXMucHJvcHMuZHJhZ1NuYXB9XG4gICAgICAgICAgICAgbWluUmVzaXplV2lkdGg9e3RoaXMucHJvcHMubWluUmVzaXplV2lkdGh9XG4gICAgICAgICAgICAgY2FuQ2hhbmdlR3JvdXA9e3RoaXMucHJvcHMuY2FuQ2hhbmdlR3JvdXB9XG4gICAgICAgICAgICAgY2FuTW92ZT17dGhpcy5wcm9wcy5jYW5Nb3ZlfVxuICAgICAgICAgICAgIGNhblJlc2l6ZT17dGhpcy5wcm9wcy5jYW5SZXNpemV9XG4gICAgICAgICAgICAgdXNlUmVzaXplSGFuZGxlPXt0aGlzLnByb3BzLnVzZVJlc2l6ZUhhbmRsZX1cbiAgICAgICAgICAgICBtb3ZlUmVzaXplVmFsaWRhdG9yPXt0aGlzLnByb3BzLm1vdmVSZXNpemVWYWxpZGF0b3J9XG4gICAgICAgICAgICAgdG9wT2Zmc2V0PXt0aGlzLnN0YXRlLnRvcE9mZnNldH1cbiAgICAgICAgICAgICBpdGVtU2VsZWN0PXt0aGlzLnNlbGVjdEl0ZW19XG4gICAgICAgICAgICAgaXRlbURyYWc9e3RoaXMuZHJhZ0l0ZW19XG4gICAgICAgICAgICAgaXRlbURyb3A9e3RoaXMuZHJvcEl0ZW19XG4gICAgICAgICAgICAgb25JdGVtRG91YmxlQ2xpY2s9e3RoaXMucHJvcHMub25JdGVtRG91YmxlQ2xpY2t9XG4gICAgICAgICAgICAgb25JdGVtQ29udGV4dE1lbnU9e3RoaXMucHJvcHMub25JdGVtQ29udGV4dE1lbnV9XG4gICAgICAgICAgICAgaXRlbVJlc2l6aW5nPXt0aGlzLnJlc2l6aW5nSXRlbX1cbiAgICAgICAgICAgICBpdGVtUmVzaXplZD17dGhpcy5yZXNpemVkSXRlbX1cbiAgICAgICAgICAgICBoaWRlQ2xpcHBlZFRpdGxlPXt0aGlzLnByb3BzLmhpZGVDbGlwcGVkVGl0bGV9XG4gICAgICAvPlxuICAgIClcbiAgfVxuXG4gIGluZm9MYWJlbCAoKSB7XG4gICAgbGV0IGxhYmVsID0gbnVsbFxuXG4gICAgaWYgKHRoaXMuc3RhdGUuZHJhZ1RpbWUpIHtcbiAgICAgIGxhYmVsID0gYCR7bW9tZW50KHRoaXMuc3RhdGUuZHJhZ1RpbWUpLmZvcm1hdCgnTExMJyl9LCAke3RoaXMuc3RhdGUuZHJhZ0dyb3VwVGl0bGV9YFxuICAgIH0gZWxzZSBpZiAodGhpcy5zdGF0ZS5yZXNpemVFbmQpIHtcbiAgICAgIGxhYmVsID0gbW9tZW50KHRoaXMuc3RhdGUucmVzaXplRW5kKS5mb3JtYXQoJ0xMTCcpXG4gICAgfVxuXG4gICAgcmV0dXJuIGxhYmVsID8gPEluZm9MYWJlbCBsYWJlbD17bGFiZWx9IC8+IDogJydcbiAgfVxuXG4gIGhlYWRlciAoY2FudmFzVGltZVN0YXJ0LCB6b29tLCBjYW52YXNUaW1lRW5kLCBjYW52YXNXaWR0aCwgbWluVW5pdCwgdGltZVN0ZXBzLCBoZWFkZXJMYWJlbEdyb3VwSGVpZ2h0LCBoZWFkZXJMYWJlbEhlaWdodCkge1xuICAgIHJldHVybiAoXG4gICAgICA8SGVhZGVyIGNhbnZhc1RpbWVTdGFydD17Y2FudmFzVGltZVN0YXJ0fVxuICAgICAgICAgICAgICBjYW52YXNUaW1lRW5kPXtjYW52YXNUaW1lRW5kfVxuICAgICAgICAgICAgICBjYW52YXNXaWR0aD17Y2FudmFzV2lkdGh9XG4gICAgICAgICAgICAgIGxpbmVIZWlnaHQ9e3RoaXMucHJvcHMubGluZUhlaWdodH1cbiAgICAgICAgICAgICAgbWluVW5pdD17bWluVW5pdH1cbiAgICAgICAgICAgICAgdGltZVN0ZXBzPXt0aW1lU3RlcHN9XG4gICAgICAgICAgICAgIGhlYWRlckxhYmVsR3JvdXBIZWlnaHQ9e2hlYWRlckxhYmVsR3JvdXBIZWlnaHR9XG4gICAgICAgICAgICAgIGhlYWRlckxhYmVsSGVpZ2h0PXtoZWFkZXJMYWJlbEhlaWdodH1cbiAgICAgICAgICAgICAgd2lkdGg9e3RoaXMuc3RhdGUud2lkdGh9XG4gICAgICAgICAgICAgIHpvb209e3pvb219XG4gICAgICAgICAgICAgIHZpc2libGVUaW1lU3RhcnQ9e3RoaXMuc3RhdGUudmlzaWJsZVRpbWVTdGFydH1cbiAgICAgICAgICAgICAgdmlzaWJsZVRpbWVFbmQ9e3RoaXMuc3RhdGUudmlzaWJsZVRpbWVFbmR9XG4gICAgICAgICAgICAgIGZpeGVkSGVhZGVyPXt0aGlzLnByb3BzLmZpeGVkSGVhZGVyfVxuICAgICAgICAgICAgICB6SW5kZXg9e3RoaXMucHJvcHMuekluZGV4U3RhcnQgKyAxfVxuICAgICAgICAgICAgICBzaG93UGVyaW9kPXt0aGlzLnNob3dQZXJpb2R9IC8+XG4gICAgKVxuICB9XG5cbiAgc2lkZWJhciAoaGVpZ2h0LCBncm91cEhlaWdodHMsIGhlYWRlckhlaWdodCkge1xuICAgIHJldHVybiAoXG4gICAgICA8U2lkZWJhciBncm91cHM9e3RoaXMucHJvcHMuZ3JvdXBzfVxuICAgICAgICAgICAgICAga2V5cz17dGhpcy5wcm9wcy5rZXlzfVxuXG4gICAgICAgICAgICAgICB3aWR0aD17dGhpcy5wcm9wcy5zaWRlYmFyV2lkdGh9XG4gICAgICAgICAgICAgICBsaW5lSGVpZ2h0PXt0aGlzLnByb3BzLmxpbmVIZWlnaHR9XG4gICAgICAgICAgICAgICBncm91cEhlaWdodHM9e2dyb3VwSGVpZ2h0c31cbiAgICAgICAgICAgICAgIGhlaWdodD17aGVpZ2h0fVxuICAgICAgICAgICAgICAgaGVhZGVySGVpZ2h0PXtoZWFkZXJIZWlnaHR9XG5cbiAgICAgICAgICAgICAgIGZpeGVkSGVhZGVyPXt0aGlzLnByb3BzLmZpeGVkSGVhZGVyfVxuICAgICAgICAgICAgICAgekluZGV4PXt0aGlzLnByb3BzLnpJbmRleFN0YXJ0ICsgMn0+XG4gICAgICAgIHt0aGlzLnByb3BzLmNoaWxkcmVufVxuICAgICAgPC9TaWRlYmFyPlxuICAgIClcbiAgfVxuXG4gIHN0YWNrSXRlbXMgKGl0ZW1zLCBncm91cHMsIGNhbnZhc1RpbWVTdGFydCwgdmlzaWJsZVRpbWVTdGFydCwgdmlzaWJsZVRpbWVFbmQsIHdpZHRoKSB7XG4gICAgY29uc3QgeyBrZXlzLCBkcmFnU25hcCwgbGluZUhlaWdodCwgaGVhZGVyTGFiZWxHcm91cEhlaWdodCwgaGVhZGVyTGFiZWxIZWlnaHQsIHN0YWNrSXRlbXMsIGl0ZW1IZWlnaHRSYXRpbyB9ID0gdGhpcy5wcm9wc1xuICAgIGNvbnN0IHsgZHJhZ2dpbmdJdGVtLCBkcmFnVGltZSwgcmVzaXppbmdJdGVtLCByZXNpemVFbmQsIG5ld0dyb3VwT3JkZXIgfSA9IHRoaXMuc3RhdGVcbiAgICBjb25zdCB6b29tID0gdmlzaWJsZVRpbWVFbmQgLSB2aXNpYmxlVGltZVN0YXJ0XG4gICAgY29uc3QgY2FudmFzVGltZUVuZCA9IGNhbnZhc1RpbWVTdGFydCArIHpvb20gKiAzXG4gICAgY29uc3QgY2FudmFzV2lkdGggPSB3aWR0aCAqIDNcbiAgICBjb25zdCBoZWFkZXJIZWlnaHQgPSBoZWFkZXJMYWJlbEdyb3VwSGVpZ2h0ICsgaGVhZGVyTGFiZWxIZWlnaHRcblxuICAgIGNvbnN0IHZpc2libGVJdGVtcyA9IGdldFZpc2libGVJdGVtcyhpdGVtcywgY2FudmFzVGltZVN0YXJ0LCBjYW52YXNUaW1lRW5kLCBrZXlzKVxuICAgIGNvbnN0IGdyb3VwT3JkZXJzID0gZ2V0R3JvdXBPcmRlcnMoZ3JvdXBzLCBrZXlzKVxuXG4gICAgbGV0IGRpbWVuc2lvbkl0ZW1zID0gdmlzaWJsZUl0ZW1zLm1hcChpdGVtID0+IHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIGlkOiBfZ2V0KGl0ZW0sIGtleXMuaXRlbUlkS2V5KSxcbiAgICAgICAgZGltZW5zaW9uczogY2FsY3VsYXRlRGltZW5zaW9ucyhcbiAgICAgICAgICBpdGVtLFxuICAgICAgICAgIGdyb3VwT3JkZXJzW19nZXQoaXRlbSwga2V5cy5pdGVtR3JvdXBLZXkpXSxcbiAgICAgICAgICBrZXlzLFxuICAgICAgICAgIGNhbnZhc1RpbWVTdGFydCxcbiAgICAgICAgICBjYW52YXNUaW1lRW5kLFxuICAgICAgICAgIGNhbnZhc1dpZHRoLFxuICAgICAgICAgIGRyYWdTbmFwLFxuICAgICAgICAgIGxpbmVIZWlnaHQsXG4gICAgICAgICAgZHJhZ2dpbmdJdGVtLFxuICAgICAgICAgIGRyYWdUaW1lLFxuICAgICAgICAgIHJlc2l6aW5nSXRlbSxcbiAgICAgICAgICByZXNpemVFbmQsXG4gICAgICAgICAgbmV3R3JvdXBPcmRlcixcbiAgICAgICAgICBpdGVtSGVpZ2h0UmF0aW9cbiAgICAgICAgKVxuICAgICAgfVxuICAgIH0pO1xuXG4gICAgbGV0IHN0YWNraW5nTWV0aG9kOiAoXG4gICAgICAgIGl0ZW1zLCBncm91cE9yZGVycywgbGluZUhlaWdodCwgaGVhZGVySGVpZ2h0LCBmb3JjZT9cbiAgICApID0+IGFueTtcbiAgICBpZiAoc3RhY2tJdGVtcykge1xuICAgICAgICAgc3RhY2tpbmdNZXRob2QgPSBzdGFjaztcbiAgICB9IGVsc2Uge1xuICAgICAgICAgc3RhY2tpbmdNZXRob2QgPSBub3N0YWNrO1xuICAgIH1cblxuICAgIGNvbnN0IHsgaGVpZ2h0LCBncm91cEhlaWdodHMsIGdyb3VwVG9wcyB9ID0gc3RhY2tpbmdNZXRob2QoXG4gICAgICBkaW1lbnNpb25JdGVtcyxcbiAgICAgIGdyb3VwT3JkZXJzLFxuICAgICAgbGluZUhlaWdodCxcbiAgICAgIGhlYWRlckhlaWdodFxuICAgICk7XG5cbiAgICByZXR1cm4ge2RpbWVuc2lvbkl0ZW1zLCBoZWlnaHQsIGdyb3VwSGVpZ2h0cywgZ3JvdXBUb3BzfVxuICB9XG5cbiAgaGFuZGxlRG91YmxlQ2xpY2sgPSAoZSkgPT4ge1xuICAgIGNvbnN0IHsgY2FudmFzVGltZVN0YXJ0LCB3aWR0aCwgdmlzaWJsZVRpbWVTdGFydCwgdmlzaWJsZVRpbWVFbmQsIGdyb3VwVG9wcywgdG9wT2Zmc2V0IH0gPSB0aGlzLnN0YXRlXG4gICAgY29uc3Qgem9vbSA9IHZpc2libGVUaW1lRW5kIC0gdmlzaWJsZVRpbWVTdGFydFxuICAgIGNvbnN0IGNhbnZhc1RpbWVFbmQgPSBjYW52YXNUaW1lU3RhcnQgKyB6b29tICogM1xuICAgIGNvbnN0IGNhbnZhc1dpZHRoID0gd2lkdGggKiAzXG4gICAgY29uc3QgeyBwYWdlWCwgcGFnZVkgfSA9IGVcbiAgICBjb25zdCByYXRpbyA9IChjYW52YXNUaW1lRW5kIC0gY2FudmFzVGltZVN0YXJ0KSAvIGNhbnZhc1dpZHRoXG4gICAgY29uc3QgYm91bmRpbmdSZWN0ID0gdGhpcy5yZWZzLnNjcm9sbENvbXBvbmVudC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcbiAgICBsZXQgdGltZVBvc2l0aW9uID0gdmlzaWJsZVRpbWVTdGFydCArIHJhdGlvICogKHBhZ2VYIC0gYm91bmRpbmdSZWN0LmxlZnQpXG4gICAgaWYgKHRoaXMucHJvcHMuZHJhZ1NuYXApIHtcbiAgICAgIHRpbWVQb3NpdGlvbiA9IE1hdGgucm91bmQodGltZVBvc2l0aW9uIC8gdGhpcy5wcm9wcy5kcmFnU25hcCkgKiB0aGlzLnByb3BzLmRyYWdTbmFwXG4gICAgfVxuXG4gICAgbGV0IGdyb3VwSW5kZXggPSAwXG4gICAgZm9yICh2YXIga2V5IG9mIE9iamVjdC5rZXlzKGdyb3VwVG9wcykpIHtcbiAgICAgIHZhciBpdGVtID0gZ3JvdXBUb3BzW2tleV1cbiAgICAgIGlmIChwYWdlWSAtIHRvcE9mZnNldCA+IGl0ZW0pIHtcbiAgICAgICAgZ3JvdXBJbmRleCA9IHBhcnNlSW50KGtleSwgMTApXG4gICAgICB9IGVsc2Uge1xuICAgICAgICBicmVha1xuICAgICAgfVxuICAgIH1cblxuICAgIGlmICh0aGlzLnByb3BzLm9uQ2FudmFzRG91YmxlQ2xpY2spIHtcbiAgICAgIHRoaXMucHJvcHMub25DYW52YXNEb3VibGVDbGljayh0aW1lUG9zaXRpb24sIHRoaXMucHJvcHMuZ3JvdXBzW2dyb3VwSW5kZXhdKVxuICAgIH1cbiAgfTtcblxuICByZW5kZXIgKCkge1xuICAgIGNvbnN0IHsgaXRlbXMsIGdyb3VwcywgaGVhZGVyTGFiZWxHcm91cEhlaWdodCwgaGVhZGVyTGFiZWxIZWlnaHQsIHNpZGViYXJXaWR0aCwgdGltZVN0ZXBzIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IHsgZHJhZ2dpbmdJdGVtLCByZXNpemluZ0l0ZW0sIGlzRHJhZ2dpbmcsIHdpZHRoLCB2aXNpYmxlVGltZVN0YXJ0LCB2aXNpYmxlVGltZUVuZCwgY2FudmFzVGltZVN0YXJ0IH0gPSB0aGlzLnN0YXRlXG4gICAgbGV0IHsgZGltZW5zaW9uSXRlbXMsIGhlaWdodCwgZ3JvdXBIZWlnaHRzLCBncm91cFRvcHMgfSA9IHRoaXMuc3RhdGVcbiAgICBjb25zdCB6b29tID0gdmlzaWJsZVRpbWVFbmQgLSB2aXNpYmxlVGltZVN0YXJ0XG4gICAgY29uc3QgY2FudmFzVGltZUVuZCA9IGNhbnZhc1RpbWVTdGFydCArIHpvb20gKiAzXG4gICAgY29uc3QgY2FudmFzV2lkdGggPSB3aWR0aCAqIDNcbiAgICBjb25zdCBtaW5Vbml0ID0gZ2V0TWluVW5pdCh6b29tLCB3aWR0aCwgdGltZVN0ZXBzKVxuICAgIGNvbnN0IGhlYWRlckhlaWdodCA9IGhlYWRlckxhYmVsR3JvdXBIZWlnaHQgKyBoZWFkZXJMYWJlbEhlaWdodFxuXG4gICAgaWYgKGRyYWdnaW5nSXRlbSB8fCByZXNpemluZ0l0ZW0pIHtcbiAgICAgIGNvbnN0IHN0YWNrUmVzdWx0cyA9IHRoaXMuc3RhY2tJdGVtcyhpdGVtcywgZ3JvdXBzLCBjYW52YXNUaW1lU3RhcnQsIHZpc2libGVUaW1lU3RhcnQsIHZpc2libGVUaW1lRW5kLCB3aWR0aClcbiAgICAgIGRpbWVuc2lvbkl0ZW1zID0gc3RhY2tSZXN1bHRzLmRpbWVuc2lvbkl0ZW1zXG4gICAgICBoZWlnaHQgPSBzdGFja1Jlc3VsdHMuaGVpZ2h0XG4gICAgICBncm91cEhlaWdodHMgPSBzdGFja1Jlc3VsdHMuZ3JvdXBIZWlnaHRzXG4gICAgICBncm91cFRvcHMgPSBzdGFja1Jlc3VsdHMuZ3JvdXBUb3BzXG4gICAgfVxuXG4gICAgY29uc3Qgb3V0ZXJDb21wb25lbnRTdHlsZSA9IHtcbiAgICAgIGhlaWdodDogYCR7aGVpZ2h0fXB4YFxuICAgIH1cblxuICAgIGNvbnN0IHNjcm9sbENvbXBvbmVudFN0eWxlID0ge1xuICAgICAgd2lkdGg6IGAke3dpZHRofXB4YCxcbiAgICAgIGhlaWdodDogYCR7aGVpZ2h0ICsgMjB9cHhgLFxuICAgICAgY3Vyc29yOiBpc0RyYWdnaW5nID8gJ21vdmUnIDogJ2RlZmF1bHQnXG4gICAgfVxuXG4gICAgY29uc3QgY2FudmFzQ29tcG9uZW50U3R5bGUgPSB7XG4gICAgICB3aWR0aDogYCR7Y2FudmFzV2lkdGh9cHhgLFxuICAgICAgaGVpZ2h0OiBgJHtoZWlnaHR9cHhgXG4gICAgfVxuXG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgc3R5bGU9e3RoaXMucHJvcHMuc3R5bGV9IHJlZj0nY29udGFpbmVyJyBjbGFzc05hbWU9J3JlYWN0LWNhbGVuZGFyLXRpbWVsaW5lJz5cbiAgICAgICAgPGRpdiBzdHlsZT17b3V0ZXJDb21wb25lbnRTdHlsZX0gY2xhc3NOYW1lPSdyY3Qtb3V0ZXInPlxuICAgICAgICAgIHtzaWRlYmFyV2lkdGggPiAwID8gdGhpcy5zaWRlYmFyKGhlaWdodCwgZ3JvdXBIZWlnaHRzLCBoZWFkZXJIZWlnaHQpIDogbnVsbH1cbiAgICAgICAgICA8ZGl2IHJlZj0nc2Nyb2xsQ29tcG9uZW50J1xuICAgICAgICAgICAgICAgY2xhc3NOYW1lPSdyY3Qtc2Nyb2xsJ1xuICAgICAgICAgICAgICAgc3R5bGU9e3Njcm9sbENvbXBvbmVudFN0eWxlfVxuICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5zY3JvbGxBcmVhQ2xpY2t9XG4gICAgICAgICAgICAgICBvblNjcm9sbD17dGhpcy5vblNjcm9sbH1cbiAgICAgICAgICAgICAgIG9uV2hlZWw9e3RoaXMub25XaGVlbH1cbiAgICAgICAgICAgICAgIG9uTW91c2VEb3duPXt0aGlzLmhhbmRsZU1vdXNlRG93bn1cbiAgICAgICAgICAgICAgIG9uTW91c2VNb3ZlPXt0aGlzLmhhbmRsZU1vdXNlTW92ZX1cbiAgICAgICAgICAgICAgIG9uTW91c2VVcD17dGhpcy5oYW5kbGVNb3VzZVVwfVxuICAgICAgICAgICAgICAgb25Nb3VzZUxlYXZlPXt0aGlzLmhhbmRsZU1vdXNlVXB9XG4gICAgICAgICAgICAgICBvbkRyYWdTdGFydD17KGUpID0+IHtcbiAgICAgICAgICAgICAgICAgICAvLyBkaXNhYmxlIGRyYWdnaW5nXG4gICAgICAgICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgICAgICAgICAgICB9fVxuICAgICAgICAgID5cbiAgICAgICAgICAgIDxkaXYgcmVmPSdjYW52YXNDb21wb25lbnQnXG4gICAgICAgICAgICAgICAgIGNsYXNzTmFtZT0ncmN0LWNhbnZhcydcbiAgICAgICAgICAgICAgICAgc3R5bGU9e2NhbnZhc0NvbXBvbmVudFN0eWxlfVxuICAgICAgICAgICAgICAgICBvbkRvdWJsZUNsaWNrPXsgdGhpcy5oYW5kbGVEb3VibGVDbGljayB9XG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIHt0aGlzLml0ZW1zKGNhbnZhc1RpbWVTdGFydCwgem9vbSwgY2FudmFzVGltZUVuZCwgY2FudmFzV2lkdGgsIG1pblVuaXQsIGRpbWVuc2lvbkl0ZW1zLCBncm91cEhlaWdodHMsIGdyb3VwVG9wcyl9XG4gICAgICAgICAgICAgIHt0aGlzLnZlcnRpY2FsTGluZXMoY2FudmFzVGltZVN0YXJ0LCB6b29tLCBjYW52YXNUaW1lRW5kLCBjYW52YXNXaWR0aCwgbWluVW5pdCwgdGltZVN0ZXBzLCBoZWlnaHQsIGhlYWRlckhlaWdodCl9XG4gICAgICAgICAgICAgIHt0aGlzLmhvcml6b250YWxMaW5lcyhjYW52YXNUaW1lU3RhcnQsIHpvb20sIGNhbnZhc1RpbWVFbmQsIGNhbnZhc1dpZHRoLCBncm91cEhlaWdodHMsIGhlYWRlckhlaWdodCl9XG4gICAgICAgICAgICAgIHt0aGlzLnRvZGF5TGluZShjYW52YXNUaW1lU3RhcnQsIHpvb20sIGNhbnZhc1RpbWVFbmQsIGNhbnZhc1dpZHRoLCBtaW5Vbml0LCBoZWlnaHQsIGhlYWRlckhlaWdodCl9XG4gICAgICAgICAgICAgIHt0aGlzLmluZm9MYWJlbCgpfVxuICAgICAgICAgICAgICB7dGhpcy5oZWFkZXIoXG4gICAgICAgICAgICAgICAgY2FudmFzVGltZVN0YXJ0LFxuICAgICAgICAgICAgICAgIHpvb20sXG4gICAgICAgICAgICAgICAgY2FudmFzVGltZUVuZCxcbiAgICAgICAgICAgICAgICBjYW52YXNXaWR0aCxcbiAgICAgICAgICAgICAgICBtaW5Vbml0LFxuICAgICAgICAgICAgICAgIHRpbWVTdGVwcyxcbiAgICAgICAgICAgICAgICBoZWFkZXJMYWJlbEdyb3VwSGVpZ2h0LFxuICAgICAgICAgICAgICAgIGhlYWRlckxhYmVsSGVpZ2h0XG4gICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICApXG4gIH1cbn1cbiJdfQ==

/***/ },
/* 2 */
/*!**************************************************************************************!*\
  !*** external {"root":"React","commonjs":"react","commonjs2":"react","amd":"react"} ***!
  \**************************************************************************************/
/***/ function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_2__;

/***/ },
/* 3 */
/*!*************************!*\
  !*** external "moment" ***!
  \*************************/
/***/ function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_3__;

/***/ },
/* 4 */
/*!*******************************!*\
  !*** ./src/lib/timeline.scss ***!
  \*******************************/
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 5 */,
/* 6 */,
/* 7 */,
/* 8 */
/*!*********************************!*\
  !*** ./src/lib/items/items.tsx ***!
  \*********************************/
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var React = __webpack_require__(/*! react */ 2);
	var item_1 = __webpack_require__(/*! ./item */ 9);
	var utils_1 = __webpack_require__(/*! ../utils */ 11);
	var Items = (function (_super) {
	    __extends(Items, _super);
	    function Items() {
	        _super.apply(this, arguments);
	    }
	    Items.prototype.shouldComponentUpdate = function (nextProps, nextState) {
	        return !(utils_1.arraysEqual(nextProps.groups, this.props.groups) &&
	            utils_1.arraysEqual(nextProps.items, this.props.items) &&
	            nextProps.keys === this.props.keys &&
	            nextProps.canvasTimeStart === this.props.canvasTimeStart &&
	            nextProps.canvasTimeEnd === this.props.canvasTimeEnd &&
	            nextProps.canvasWidth === this.props.canvasWidth &&
	            nextProps.selectedItem === this.props.selectedItem &&
	            nextProps.lineHeight === this.props.lineHeight &&
	            nextProps.dragSnap === this.props.dragSnap &&
	            nextProps.minResizeWidth === this.props.minResizeWidth &&
	            nextProps.canChangeGroup === this.props.canChangeGroup &&
	            nextProps.canMove === this.props.canMove &&
	            nextProps.canResize === this.props.canResize &&
	            nextProps.dimensionItems === this.props.dimensionItems &&
	            nextProps.topOffset === this.props.topOffset);
	    };
	    Items.prototype.getGroupOrders = function () {
	        var groupIdKey = this.props.keys.groupIdKey;
	        var groupOrders = {};
	        for (var i = 0; i < this.props.groups.length; i++) {
	            groupOrders[utils_1._get(this.props.groups[i], groupIdKey)] = i;
	        }
	        return groupOrders;
	    };
	    Items.prototype.getVisibleItems = function (canvasTimeStart, canvasTimeEnd, groupOrders) {
	        var _a = this.props.keys, itemTimeStartKey = _a.itemTimeStartKey, itemTimeEndKey = _a.itemTimeEndKey;
	        return this.props.items.filter(function (item) {
	            return utils_1._get(item, itemTimeStartKey) <= canvasTimeEnd && utils_1._get(item, itemTimeEndKey) >= canvasTimeStart;
	        });
	    };
	    Items.prototype.render = function () {
	        var _this = this;
	        var _a = this.props, canvasTimeStart = _a.canvasTimeStart, canvasTimeEnd = _a.canvasTimeEnd, dimensionItems = _a.dimensionItems;
	        var _b = this.props.keys, itemIdKey = _b.itemIdKey, itemGroupKey = _b.itemGroupKey;
	        var groupOrders = this.getGroupOrders();
	        var visibleItems = this.getVisibleItems(canvasTimeStart, canvasTimeEnd, groupOrders);
	        var sortedDimensionItems = utils_1.keyBy(dimensionItems, 'id');
	        // const timeDiff = Math.floor((canvasTimeEnd - canvasTimeStart) / 24)
	        // const start = Math.floor(canvasTimeStart / timeDiff) * timeDiff
	        // const end = Math.floor(canvasTimeEnd / timeDiff) * timeDiff
	        // const canvasTimeLength = (canvasTimeEnd - canvasTimeStart)
	        // const ratio = canvasWidth / (canvasTimeEnd - canvasTimeStart)
	        //
	        // let itemGroups = []
	        //
	        // for (let i = start; i < end + timeDiff; i += timeDiff) {
	        //   itemGroups.push({
	        //     start: i,
	        //     end: i + timeDiff,
	        //     left: Math.round((i - canvasTimeStart) * ratio, 2),
	        //     items: visibleItems.filter(item => item.start >= i && item.start < i + timeDiff)
	        //   })
	        // }
	        return (React.createElement("div", {className: 'rct-items'}, visibleItems.map(function (item) { return React.createElement(item_1.default, {key: utils_1._get(item, itemIdKey), item: item, keys: _this.props.keys, order: groupOrders[utils_1._get(item, itemGroupKey)], dimensions: sortedDimensionItems[utils_1._get(item, itemIdKey)].dimensions, selected: _this.props.selectedItem === utils_1._get(item, itemIdKey), canChangeGroup: utils_1._get(item, 'canChangeGroup') !== undefined ? utils_1._get(item, 'canChangeGroup') : _this.props.canChangeGroup, canMove: utils_1._get(item, 'canMove') !== undefined ? utils_1._get(item, 'canMove') : _this.props.canMove, canResize: utils_1._get(item, 'canResize') !== undefined ? utils_1._get(item, 'canResize') : _this.props.canResize, useResizeHandle: _this.props.useResizeHandle, topOffset: _this.props.topOffset, groupHeights: _this.props.groupHeights, groupTops: _this.props.groupTops, canvasTimeStart: _this.props.canvasTimeStart, canvasTimeEnd: _this.props.canvasTimeEnd, canvasWidth: _this.props.canvasWidth, lineHeight: _this.props.lineHeight, dragSnap: _this.props.dragSnap, minResizeWidth: _this.props.minResizeWidth, onResizing: _this.props.itemResizing, onResized: _this.props.itemResized, moveResizeValidator: _this.props.moveResizeValidator, onDrag: _this.props.itemDrag, onDrop: _this.props.itemDrop, onItemDoubleClick: _this.props.onItemDoubleClick, onContextMenu: _this.props.onItemContextMenu, onSelect: _this.props.itemSelect, hideClippedTitle: _this.props.hideClippedTitle}); })));
	        // NB: itemgroups commented out for now as they made performacne horrible when zooming in/out
	        //
	        // return (
	        //   <div>
	        //     {itemGroups.map(group => (
	        //       <div key={`timegroup-${group.start}-${group.end}`} style={{position: 'absolute', top: '0', left: `${group.left}px`}}>
	        //         <ItemGroup {...this.props} items={group.items} canvasTimeStart={group.start} canvasTimeEnd={group.start + canvasTimeLength} groupOrders={groupOrders} />
	        //       </div>
	        //     ))}
	        //   </div>
	        // )
	    };
	    return Items;
	}(React.Component));
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.default = Items;
	//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaXRlbXMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpdGVtcy50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsSUFBWSxLQUFLLFdBQU0sT0FDdkIsQ0FBQyxDQUQ2QjtBQUM5QixxQkFBaUIsUUFFakIsQ0FBQyxDQUZ3QjtBQUV6QixzQkFBeUMsVUFHekMsQ0FBQyxDQUhrRDtBQTRDbkQ7SUFBbUMseUJBQTJCO0lBQTlEO1FBQW1DLDhCQUEyQjtJQWdIOUQsQ0FBQztJQS9HQyxxQ0FBcUIsR0FBckIsVUFBdUIsU0FBUyxFQUFFLFNBQVM7UUFDekMsTUFBTSxDQUFDLENBQUMsQ0FBQyxtQkFBVyxDQUFDLFNBQVMsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7WUFDaEQsbUJBQVcsQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO1lBQzlDLFNBQVMsQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJO1lBQ2xDLFNBQVMsQ0FBQyxlQUFlLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxlQUFlO1lBQ3hELFNBQVMsQ0FBQyxhQUFhLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhO1lBQ3BELFNBQVMsQ0FBQyxXQUFXLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXO1lBQ2hELFNBQVMsQ0FBQyxZQUFZLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZO1lBQ2xELFNBQVMsQ0FBQyxVQUFVLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVO1lBQzlDLFNBQVMsQ0FBQyxRQUFRLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRO1lBQzFDLFNBQVMsQ0FBQyxjQUFjLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjO1lBQ3RELFNBQVMsQ0FBQyxjQUFjLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjO1lBQ3RELFNBQVMsQ0FBQyxPQUFPLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPO1lBQ3hDLFNBQVMsQ0FBQyxTQUFTLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTO1lBQzVDLFNBQVMsQ0FBQyxjQUFjLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjO1lBQ3RELFNBQVMsQ0FBQyxTQUFTLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQ3BELENBQUE7SUFDSCxDQUFDO0lBRUQsOEJBQWMsR0FBZDtRQUNVLDJDQUFVLENBQW9CO1FBRXRDLElBQUksV0FBVyxHQUFHLEVBQUUsQ0FBQTtRQUVwQixHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBQ2xELFdBQVcsQ0FBQyxZQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsVUFBVSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUE7UUFDekQsQ0FBQztRQUVELE1BQU0sQ0FBQyxXQUFXLENBQUE7SUFDcEIsQ0FBQztJQUVELCtCQUFlLEdBQWYsVUFBaUIsZUFBZSxFQUFFLGFBQWEsRUFBRSxXQUFXO1FBQzFELElBQUEsb0JBQTRELEVBQXBELHNDQUFnQixFQUFFLGtDQUFjLENBQW9CO1FBRTVELE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsVUFBQSxJQUFJO1lBQ2pDLE1BQU0sQ0FBQyxZQUFJLENBQUMsSUFBSSxFQUFFLGdCQUFnQixDQUFDLElBQUksYUFBYSxJQUFJLFlBQUksQ0FBQyxJQUFJLEVBQUUsY0FBYyxDQUFDLElBQUksZUFBZSxDQUFBO1FBQ3ZHLENBQUMsQ0FBQyxDQUFBO0lBQ0osQ0FBQztJQUVELHNCQUFNLEdBQU47UUFBQSxpQkF1RUM7UUF0RUMsSUFBQSxlQUFxRSxFQUE3RCxvQ0FBZSxFQUFFLGdDQUFhLEVBQUUsa0NBQWMsQ0FBZTtRQUNyRSxJQUFBLG9CQUFtRCxFQUEzQyx3QkFBUyxFQUFFLDhCQUFZLENBQW9CO1FBRW5ELElBQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQTtRQUN6QyxJQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLGVBQWUsRUFBRSxhQUFhLEVBQUUsV0FBVyxDQUFDLENBQUE7UUFDdEYsSUFBTSxvQkFBb0IsR0FBRyxhQUFLLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxDQUFBO1FBRXhELHNFQUFzRTtRQUV0RSxrRUFBa0U7UUFDbEUsOERBQThEO1FBRTlELDZEQUE2RDtRQUM3RCxnRUFBZ0U7UUFDaEUsRUFBRTtRQUNGLHNCQUFzQjtRQUN0QixFQUFFO1FBQ0YsMkRBQTJEO1FBQzNELHNCQUFzQjtRQUN0QixnQkFBZ0I7UUFDaEIseUJBQXlCO1FBQ3pCLDBEQUEwRDtRQUMxRCx1RkFBdUY7UUFDdkYsT0FBTztRQUNQLElBQUk7UUFFSixNQUFNLENBQUMsQ0FDTCxxQkFBQyxHQUFHLElBQUMsU0FBUyxFQUFDLFdBQVcsR0FDdkIsWUFBWSxDQUFDLEdBQUcsQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLG9CQUFDLGNBQUksR0FBQyxHQUFHLEVBQUUsWUFBSSxDQUFDLElBQUksRUFBRSxTQUFTLENBQUUsRUFDM0IsSUFBSSxFQUFFLElBQUssRUFDWCxJQUFJLEVBQUUsS0FBSSxDQUFDLEtBQUssQ0FBQyxJQUFLLEVBQ3RCLEtBQUssRUFBRSxXQUFXLENBQUMsWUFBSSxDQUFDLElBQUksRUFBRSxZQUFZLENBQUMsQ0FBRSxFQUM3QyxVQUFVLEVBQUUsb0JBQW9CLENBQUMsWUFBSSxDQUFDLElBQUksRUFBRSxTQUFTLENBQUMsQ0FBQyxDQUFDLFVBQVcsRUFDbkUsUUFBUSxFQUFFLEtBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxLQUFLLFlBQUksQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFFLEVBQzVELGNBQWMsRUFBRSxZQUFJLENBQUMsSUFBSSxFQUFFLGdCQUFnQixDQUFDLEtBQUssU0FBUyxHQUFHLFlBQUksQ0FBQyxJQUFJLEVBQUUsZ0JBQWdCLENBQUMsR0FBRyxLQUFJLENBQUMsS0FBSyxDQUFDLGNBQWUsRUFDdEgsT0FBTyxFQUFFLFlBQUksQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLEtBQUssU0FBUyxHQUFHLFlBQUksQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLEdBQUcsS0FBSSxDQUFDLEtBQUssQ0FBQyxPQUFRLEVBQzFGLFNBQVMsRUFBRSxZQUFJLENBQUMsSUFBSSxFQUFFLFdBQVcsQ0FBQyxLQUFLLFNBQVMsR0FBRyxZQUFJLENBQUMsSUFBSSxFQUFFLFdBQVcsQ0FBQyxHQUFHLEtBQUksQ0FBQyxLQUFLLENBQUMsU0FBVSxFQUNsRyxlQUFlLEVBQUUsS0FBSSxDQUFDLEtBQUssQ0FBQyxlQUFnQixFQUM1QyxTQUFTLEVBQUUsS0FBSSxDQUFDLEtBQUssQ0FBQyxTQUFVLEVBQ2hDLFlBQVksRUFBRSxLQUFJLENBQUMsS0FBSyxDQUFDLFlBQWEsRUFDdEMsU0FBUyxFQUFFLEtBQUksQ0FBQyxLQUFLLENBQUMsU0FBVSxFQUNoQyxlQUFlLEVBQUUsS0FBSSxDQUFDLEtBQUssQ0FBQyxlQUFnQixFQUM1QyxhQUFhLEVBQUUsS0FBSSxDQUFDLEtBQUssQ0FBQyxhQUFjLEVBQ3hDLFdBQVcsRUFBRSxLQUFJLENBQUMsS0FBSyxDQUFDLFdBQVksRUFDcEMsVUFBVSxFQUFFLEtBQUksQ0FBQyxLQUFLLENBQUMsVUFBVyxFQUNsQyxRQUFRLEVBQUUsS0FBSSxDQUFDLEtBQUssQ0FBQyxRQUFTLEVBQzlCLGNBQWMsRUFBRSxLQUFJLENBQUMsS0FBSyxDQUFDLGNBQWUsRUFDMUMsVUFBVSxFQUFFLEtBQUksQ0FBQyxLQUFLLENBQUMsWUFBYSxFQUNwQyxTQUFTLEVBQUUsS0FBSSxDQUFDLEtBQUssQ0FBQyxXQUFZLEVBQ2xDLG1CQUFtQixFQUFFLEtBQUksQ0FBQyxLQUFLLENBQUMsbUJBQW9CLEVBQ3BELE1BQU0sRUFBRSxLQUFJLENBQUMsS0FBSyxDQUFDLFFBQVMsRUFDNUIsTUFBTSxFQUFFLEtBQUksQ0FBQyxLQUFLLENBQUMsUUFBUyxFQUM1QixpQkFBaUIsRUFBRSxLQUFJLENBQUMsS0FBSyxDQUFDLGlCQUFrQixFQUNoRCxhQUFhLEVBQUUsS0FBSSxDQUFDLEtBQUssQ0FBQyxpQkFBa0IsRUFDNUMsUUFBUSxFQUFFLEtBQUksQ0FBQyxLQUFLLENBQUMsVUFBVyxFQUNoQyxnQkFBZ0IsRUFBRSxLQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFpQixFQUFFLEVBM0J0RCxDQTJCc0QsQ0FBRSxDQUM5RSxDQUNQLENBQUE7UUFFRCw2RkFBNkY7UUFDN0YsRUFBRTtRQUNGLFdBQVc7UUFDWCxVQUFVO1FBQ1YsaUNBQWlDO1FBQ2pDLDhIQUE4SDtRQUM5SCxtS0FBbUs7UUFDbkssZUFBZTtRQUNmLFVBQVU7UUFDVixXQUFXO1FBQ1gsSUFBSTtJQUNOLENBQUM7SUFDSCxZQUFDO0FBQUQsQ0FBQyxBQWhIRCxDQUFtQyxLQUFLLENBQUMsU0FBUyxHQWdIakQ7QUFoSEQ7dUJBZ0hDLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCdcbmltcG9ydCBJdGVtIGZyb20gJy4vaXRlbSdcblxuaW1wb3J0IHsgX2dldCwgYXJyYXlzRXF1YWwsIGtleUJ5IH0gZnJvbSAnLi4vdXRpbHMnXG5cblxuaW50ZXJmYWNlIFByb3BzIHtcbiAgICBncm91cHM6IGFueVtdXG4gICAgaXRlbXM6IGFueVtdXG5cbiAgICBjYW52YXNUaW1lU3RhcnQ6IG51bWJlclxuICAgIGNhbnZhc1RpbWVFbmQ6IG51bWJlclxuICAgIGNhbnZhc1dpZHRoOiBudW1iZXJcbiAgICBsaW5lSGVpZ2h0OiBudW1iZXJcblxuICAgIGRyYWdTbmFwPzogbnVtYmVyXG4gICAgbWluUmVzaXplV2lkdGg6IG51bWJlclxuICAgIHNlbGVjdGVkSXRlbT86IHN0cmluZyB8IG51bWJlclxuXG4gICAgY2FuQ2hhbmdlR3JvdXA6IGJvb2xlYW5cbiAgICBjYW5Nb3ZlOiBib29sZWFuXG4gICAgY2FuUmVzaXplOiBib29sZWFuXG5cbiAgICBrZXlzOiB7W2tleTogc3RyaW5nXTogc3RyaW5nfVxuXG4gICAgbW92ZVJlc2l6ZVZhbGlkYXRvcj86ICgpID0+IHZvaWRcbiAgICBpdGVtU2VsZWN0PzogKGl0ZW0sIGNsaWNrVHlwZT8sIGU/KSA9PiB2b2lkXG4gICAgaXRlbURyYWc/OiAoaXRlbSwgZHJhZ1RpbWUsIG5ld0dyb3VwT3JkZXIpID0+IHZvaWRcbiAgICBpdGVtRHJvcD86IChpdGVtLCBkcmFnVGltZSwgbmV3R3JvdXBPcmRlcikgPT4gdm9pZFxuICAgIGl0ZW1SZXNpemluZz86IChpdGVtLCBuZXdSZXNpemVFbmQpID0+IHZvaWRcbiAgICBpdGVtUmVzaXplZD86IChpdGVtLCBuZXdSZXNpemVFbmQpID0+IHZvaWRcblxuICAgIG9uSXRlbURvdWJsZUNsaWNrPzogKCkgPT4gdm9pZFxuICAgIG9uSXRlbUNvbnRleHRNZW51PzogKCkgPT4gdm9pZFxuXG4gICAgZGltZW5zaW9uSXRlbXM/XG4gICAgdG9wT2Zmc2V0XG4gICAgdXNlUmVzaXplSGFuZGxlP1xuICAgIGdyb3VwSGVpZ2h0cz9cbiAgICBncm91cFRvcHM/XG4gICAgbGluZUNvdW50P1xuICAgIG1pblVuaXQ/XG5cbiAgICBoaWRlQ2xpcHBlZFRpdGxlOiBib29sZWFuXG59XG5cblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgSXRlbXMgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQ8UHJvcHMsIGFueT4ge1xuICBzaG91bGRDb21wb25lbnRVcGRhdGUgKG5leHRQcm9wcywgbmV4dFN0YXRlKSB7XG4gICAgcmV0dXJuICEoYXJyYXlzRXF1YWwobmV4dFByb3BzLmdyb3VwcywgdGhpcy5wcm9wcy5ncm91cHMpICYmXG4gICAgICAgICAgICAgYXJyYXlzRXF1YWwobmV4dFByb3BzLml0ZW1zLCB0aGlzLnByb3BzLml0ZW1zKSAmJlxuICAgICAgICAgICAgIG5leHRQcm9wcy5rZXlzID09PSB0aGlzLnByb3BzLmtleXMgJiZcbiAgICAgICAgICAgICBuZXh0UHJvcHMuY2FudmFzVGltZVN0YXJ0ID09PSB0aGlzLnByb3BzLmNhbnZhc1RpbWVTdGFydCAmJlxuICAgICAgICAgICAgIG5leHRQcm9wcy5jYW52YXNUaW1lRW5kID09PSB0aGlzLnByb3BzLmNhbnZhc1RpbWVFbmQgJiZcbiAgICAgICAgICAgICBuZXh0UHJvcHMuY2FudmFzV2lkdGggPT09IHRoaXMucHJvcHMuY2FudmFzV2lkdGggJiZcbiAgICAgICAgICAgICBuZXh0UHJvcHMuc2VsZWN0ZWRJdGVtID09PSB0aGlzLnByb3BzLnNlbGVjdGVkSXRlbSAmJlxuICAgICAgICAgICAgIG5leHRQcm9wcy5saW5lSGVpZ2h0ID09PSB0aGlzLnByb3BzLmxpbmVIZWlnaHQgJiZcbiAgICAgICAgICAgICBuZXh0UHJvcHMuZHJhZ1NuYXAgPT09IHRoaXMucHJvcHMuZHJhZ1NuYXAgJiZcbiAgICAgICAgICAgICBuZXh0UHJvcHMubWluUmVzaXplV2lkdGggPT09IHRoaXMucHJvcHMubWluUmVzaXplV2lkdGggJiZcbiAgICAgICAgICAgICBuZXh0UHJvcHMuY2FuQ2hhbmdlR3JvdXAgPT09IHRoaXMucHJvcHMuY2FuQ2hhbmdlR3JvdXAgJiZcbiAgICAgICAgICAgICBuZXh0UHJvcHMuY2FuTW92ZSA9PT0gdGhpcy5wcm9wcy5jYW5Nb3ZlICYmXG4gICAgICAgICAgICAgbmV4dFByb3BzLmNhblJlc2l6ZSA9PT0gdGhpcy5wcm9wcy5jYW5SZXNpemUgJiZcbiAgICAgICAgICAgICBuZXh0UHJvcHMuZGltZW5zaW9uSXRlbXMgPT09IHRoaXMucHJvcHMuZGltZW5zaW9uSXRlbXMgJiZcbiAgICAgICAgICAgICBuZXh0UHJvcHMudG9wT2Zmc2V0ID09PSB0aGlzLnByb3BzLnRvcE9mZnNldFxuICAgIClcbiAgfVxuXG4gIGdldEdyb3VwT3JkZXJzICgpIHtcbiAgICBjb25zdCB7IGdyb3VwSWRLZXkgfSA9IHRoaXMucHJvcHMua2V5c1xuXG4gICAgbGV0IGdyb3VwT3JkZXJzID0ge31cblxuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5wcm9wcy5ncm91cHMubGVuZ3RoOyBpKyspIHtcbiAgICAgIGdyb3VwT3JkZXJzW19nZXQodGhpcy5wcm9wcy5ncm91cHNbaV0sIGdyb3VwSWRLZXkpXSA9IGlcbiAgICB9XG5cbiAgICByZXR1cm4gZ3JvdXBPcmRlcnNcbiAgfVxuXG4gIGdldFZpc2libGVJdGVtcyAoY2FudmFzVGltZVN0YXJ0LCBjYW52YXNUaW1lRW5kLCBncm91cE9yZGVycykge1xuICAgIGNvbnN0IHsgaXRlbVRpbWVTdGFydEtleSwgaXRlbVRpbWVFbmRLZXkgfSA9IHRoaXMucHJvcHMua2V5c1xuXG4gICAgcmV0dXJuIHRoaXMucHJvcHMuaXRlbXMuZmlsdGVyKGl0ZW0gPT4ge1xuICAgICAgcmV0dXJuIF9nZXQoaXRlbSwgaXRlbVRpbWVTdGFydEtleSkgPD0gY2FudmFzVGltZUVuZCAmJiBfZ2V0KGl0ZW0sIGl0ZW1UaW1lRW5kS2V5KSA+PSBjYW52YXNUaW1lU3RhcnRcbiAgICB9KVxuICB9XG5cbiAgcmVuZGVyICgpIHtcbiAgICBjb25zdCB7IGNhbnZhc1RpbWVTdGFydCwgY2FudmFzVGltZUVuZCwgZGltZW5zaW9uSXRlbXMgfSA9IHRoaXMucHJvcHNcbiAgICBjb25zdCB7IGl0ZW1JZEtleSwgaXRlbUdyb3VwS2V5IH0gPSB0aGlzLnByb3BzLmtleXNcblxuICAgIGNvbnN0IGdyb3VwT3JkZXJzID0gdGhpcy5nZXRHcm91cE9yZGVycygpXG4gICAgY29uc3QgdmlzaWJsZUl0ZW1zID0gdGhpcy5nZXRWaXNpYmxlSXRlbXMoY2FudmFzVGltZVN0YXJ0LCBjYW52YXNUaW1lRW5kLCBncm91cE9yZGVycylcbiAgICBjb25zdCBzb3J0ZWREaW1lbnNpb25JdGVtcyA9IGtleUJ5KGRpbWVuc2lvbkl0ZW1zLCAnaWQnKVxuXG4gICAgLy8gY29uc3QgdGltZURpZmYgPSBNYXRoLmZsb29yKChjYW52YXNUaW1lRW5kIC0gY2FudmFzVGltZVN0YXJ0KSAvIDI0KVxuXG4gICAgLy8gY29uc3Qgc3RhcnQgPSBNYXRoLmZsb29yKGNhbnZhc1RpbWVTdGFydCAvIHRpbWVEaWZmKSAqIHRpbWVEaWZmXG4gICAgLy8gY29uc3QgZW5kID0gTWF0aC5mbG9vcihjYW52YXNUaW1lRW5kIC8gdGltZURpZmYpICogdGltZURpZmZcblxuICAgIC8vIGNvbnN0IGNhbnZhc1RpbWVMZW5ndGggPSAoY2FudmFzVGltZUVuZCAtIGNhbnZhc1RpbWVTdGFydClcbiAgICAvLyBjb25zdCByYXRpbyA9IGNhbnZhc1dpZHRoIC8gKGNhbnZhc1RpbWVFbmQgLSBjYW52YXNUaW1lU3RhcnQpXG4gICAgLy9cbiAgICAvLyBsZXQgaXRlbUdyb3VwcyA9IFtdXG4gICAgLy9cbiAgICAvLyBmb3IgKGxldCBpID0gc3RhcnQ7IGkgPCBlbmQgKyB0aW1lRGlmZjsgaSArPSB0aW1lRGlmZikge1xuICAgIC8vICAgaXRlbUdyb3Vwcy5wdXNoKHtcbiAgICAvLyAgICAgc3RhcnQ6IGksXG4gICAgLy8gICAgIGVuZDogaSArIHRpbWVEaWZmLFxuICAgIC8vICAgICBsZWZ0OiBNYXRoLnJvdW5kKChpIC0gY2FudmFzVGltZVN0YXJ0KSAqIHJhdGlvLCAyKSxcbiAgICAvLyAgICAgaXRlbXM6IHZpc2libGVJdGVtcy5maWx0ZXIoaXRlbSA9PiBpdGVtLnN0YXJ0ID49IGkgJiYgaXRlbS5zdGFydCA8IGkgKyB0aW1lRGlmZilcbiAgICAvLyAgIH0pXG4gICAgLy8gfVxuXG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPSdyY3QtaXRlbXMnPlxuICAgICAgICB7dmlzaWJsZUl0ZW1zLm1hcChpdGVtID0+IDxJdGVtIGtleT17X2dldChpdGVtLCBpdGVtSWRLZXkpfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGl0ZW09e2l0ZW19XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAga2V5cz17dGhpcy5wcm9wcy5rZXlzfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9yZGVyPXtncm91cE9yZGVyc1tfZ2V0KGl0ZW0sIGl0ZW1Hcm91cEtleSldfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpbWVuc2lvbnM9e3NvcnRlZERpbWVuc2lvbkl0ZW1zW19nZXQoaXRlbSwgaXRlbUlkS2V5KV0uZGltZW5zaW9uc31cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZWxlY3RlZD17dGhpcy5wcm9wcy5zZWxlY3RlZEl0ZW0gPT09IF9nZXQoaXRlbSwgaXRlbUlkS2V5KX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYW5DaGFuZ2VHcm91cD17X2dldChpdGVtLCAnY2FuQ2hhbmdlR3JvdXAnKSAhPT0gdW5kZWZpbmVkID8gX2dldChpdGVtLCAnY2FuQ2hhbmdlR3JvdXAnKSA6IHRoaXMucHJvcHMuY2FuQ2hhbmdlR3JvdXB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FuTW92ZT17X2dldChpdGVtLCAnY2FuTW92ZScpICE9PSB1bmRlZmluZWQgPyBfZ2V0KGl0ZW0sICdjYW5Nb3ZlJykgOiB0aGlzLnByb3BzLmNhbk1vdmV9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FuUmVzaXplPXtfZ2V0KGl0ZW0sICdjYW5SZXNpemUnKSAhPT0gdW5kZWZpbmVkID8gX2dldChpdGVtLCAnY2FuUmVzaXplJykgOiB0aGlzLnByb3BzLmNhblJlc2l6ZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB1c2VSZXNpemVIYW5kbGU9e3RoaXMucHJvcHMudXNlUmVzaXplSGFuZGxlfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRvcE9mZnNldD17dGhpcy5wcm9wcy50b3BPZmZzZXR9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZ3JvdXBIZWlnaHRzPXt0aGlzLnByb3BzLmdyb3VwSGVpZ2h0c31cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBncm91cFRvcHM9e3RoaXMucHJvcHMuZ3JvdXBUb3BzfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhbnZhc1RpbWVTdGFydD17dGhpcy5wcm9wcy5jYW52YXNUaW1lU3RhcnR9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FudmFzVGltZUVuZD17dGhpcy5wcm9wcy5jYW52YXNUaW1lRW5kfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhbnZhc1dpZHRoPXt0aGlzLnByb3BzLmNhbnZhc1dpZHRofVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxpbmVIZWlnaHQ9e3RoaXMucHJvcHMubGluZUhlaWdodH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkcmFnU25hcD17dGhpcy5wcm9wcy5kcmFnU25hcH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtaW5SZXNpemVXaWR0aD17dGhpcy5wcm9wcy5taW5SZXNpemVXaWR0aH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvblJlc2l6aW5nPXt0aGlzLnByb3BzLml0ZW1SZXNpemluZ31cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvblJlc2l6ZWQ9e3RoaXMucHJvcHMuaXRlbVJlc2l6ZWR9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbW92ZVJlc2l6ZVZhbGlkYXRvcj17dGhpcy5wcm9wcy5tb3ZlUmVzaXplVmFsaWRhdG9yfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uRHJhZz17dGhpcy5wcm9wcy5pdGVtRHJhZ31cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkRyb3A9e3RoaXMucHJvcHMuaXRlbURyb3B9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25JdGVtRG91YmxlQ2xpY2s9e3RoaXMucHJvcHMub25JdGVtRG91YmxlQ2xpY2t9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25Db250ZXh0TWVudT17dGhpcy5wcm9wcy5vbkl0ZW1Db250ZXh0TWVudX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvblNlbGVjdD17dGhpcy5wcm9wcy5pdGVtU2VsZWN0fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhpZGVDbGlwcGVkVGl0bGU9e3RoaXMucHJvcHMuaGlkZUNsaXBwZWRUaXRsZX0vPil9XG4gICAgICA8L2Rpdj5cbiAgICApXG5cbiAgICAvLyBOQjogaXRlbWdyb3VwcyBjb21tZW50ZWQgb3V0IGZvciBub3cgYXMgdGhleSBtYWRlIHBlcmZvcm1hY25lIGhvcnJpYmxlIHdoZW4gem9vbWluZyBpbi9vdXRcbiAgICAvL1xuICAgIC8vIHJldHVybiAoXG4gICAgLy8gICA8ZGl2PlxuICAgIC8vICAgICB7aXRlbUdyb3Vwcy5tYXAoZ3JvdXAgPT4gKFxuICAgIC8vICAgICAgIDxkaXYga2V5PXtgdGltZWdyb3VwLSR7Z3JvdXAuc3RhcnR9LSR7Z3JvdXAuZW5kfWB9IHN0eWxlPXt7cG9zaXRpb246ICdhYnNvbHV0ZScsIHRvcDogJzAnLCBsZWZ0OiBgJHtncm91cC5sZWZ0fXB4YH19PlxuICAgIC8vICAgICAgICAgPEl0ZW1Hcm91cCB7Li4udGhpcy5wcm9wc30gaXRlbXM9e2dyb3VwLml0ZW1zfSBjYW52YXNUaW1lU3RhcnQ9e2dyb3VwLnN0YXJ0fSBjYW52YXNUaW1lRW5kPXtncm91cC5zdGFydCArIGNhbnZhc1RpbWVMZW5ndGh9IGdyb3VwT3JkZXJzPXtncm91cE9yZGVyc30gLz5cbiAgICAvLyAgICAgICA8L2Rpdj5cbiAgICAvLyAgICAgKSl9XG4gICAgLy8gICA8L2Rpdj5cbiAgICAvLyApXG4gIH1cbn1cbiJdfQ==

/***/ },
/* 9 */
/*!********************************!*\
  !*** ./src/lib/items/item.tsx ***!
  \********************************/
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var React = __webpack_require__(/*! react */ 2);
	var interact = __webpack_require__(/*! interact.js */ 10);
	var moment = __webpack_require__(/*! moment */ 3);
	var utils_1 = __webpack_require__(/*! ../utils */ 11);
	var Item = (function (_super) {
	    __extends(Item, _super);
	    function Item(props) {
	        var _this = this;
	        _super.call(this, props);
	        this.onMouseDown = function (e) {
	            if (!_this.state.interactMounted) {
	                e.preventDefault();
	                _this.startedClicking = true;
	            }
	        };
	        this.onMouseUp = function (e) {
	            if (!_this.state.interactMounted && _this.startedClicking) {
	                _this.startedClicking = false;
	                _this.actualClick(e, 'click');
	            }
	        };
	        this.onTouchStart = function (e) {
	            if (!_this.state.interactMounted) {
	                e.preventDefault();
	                _this.startedTouching = true;
	            }
	        };
	        this.onTouchEnd = function (e) {
	            if (!_this.state.interactMounted && _this.startedTouching) {
	                _this.startedTouching = false;
	                _this.actualClick(e, 'touch');
	            }
	        };
	        this.handleDoubleClick = function (e) {
	            e.preventDefault();
	            e.stopPropagation();
	            if (_this.props.onItemDoubleClick) {
	                _this.props.onItemDoubleClick(_this.itemId, e);
	            }
	        };
	        this.handleContextMenu = function (e) {
	            if (_this.props.onContextMenu) {
	                e.preventDefault();
	                e.stopPropagation();
	                _this.props.onContextMenu(_this.itemId, e);
	            }
	        };
	        this.handleClick = function (ev) {
	            ev.preventDefault();
	            ev.stopPropagation();
	            _this.setState({ tooltip: !_this.state.tooltip });
	        };
	        this.determineTooltipPosition = function () {
	            if (!_this.refs['item']) {
	                return 'top';
	            }
	            return _this.refs['item'].offsetTop > 100 ? 'top' : 'bottom';
	        };
	        this.checkContentSize = function () {
	            var el = _this.refs['content'];
	            if (!el) {
	                return false;
	            }
	            if (_this.props.hideClippedTitle) {
	                var textIsClipped = el.scrollWidth > el.clientWidth;
	                if (textIsClipped && !_this.state.contentIsHidden) {
	                    _this.setState({ contentIsHidden: true });
	                }
	                else if (!textIsClipped && _this.state.contentIsHidden) {
	                    _this.setState({ contentIsHidden: false });
	                }
	            }
	        };
	        this.cacheDataFromProps(props);
	        this.state = {
	            interactMounted: false,
	            dragging: null,
	            dragStart: null,
	            preDragPosition: null,
	            dragTime: null,
	            dragGroupDelta: null,
	            resizing: null,
	            resizeStart: null,
	            resizeTime: null,
	            tooltip: false,
	            contentIsHidden: false,
	        };
	    }
	    Item.prototype.shouldComponentUpdate = function (nextProps, nextState) {
	        var shouldUpdate = nextState.dragging !== this.state.dragging ||
	            nextState.dragTime !== this.state.dragTime ||
	            nextState.dragGroupDelta !== this.state.dragGroupDelta ||
	            nextState.resizing !== this.state.resizing ||
	            nextState.resizeTime !== this.state.resizeTime ||
	            nextProps.keys !== this.props.keys ||
	            nextProps.selected !== this.props.selected ||
	            nextProps.item !== this.props.item ||
	            nextProps.canvasTimeStart !== this.props.canvasTimeStart ||
	            nextProps.canvasTimeEnd !== this.props.canvasTimeEnd ||
	            nextProps.canvasWidth !== this.props.canvasWidth ||
	            nextProps.lineHeight !== this.props.lineHeight ||
	            nextProps.order !== this.props.order ||
	            nextProps.dragSnap !== this.props.dragSnap ||
	            nextProps.minResizeWidth !== this.props.minResizeWidth ||
	            nextProps.selected !== this.props.selected ||
	            nextProps.canChangeGroup !== this.props.canChangeGroup ||
	            nextProps.topOffset !== this.props.topOffset ||
	            nextProps.canMove !== this.props.canMove ||
	            nextProps.canResize !== this.props.canResize ||
	            nextProps.dimensions !== this.props.dimensions ||
	            nextState.tooltip !== this.state.tooltip ||
	            nextState.contentIsHidden !== this.state.contentIsHidden;
	        return shouldUpdate;
	    };
	    Item.prototype.cacheDataFromProps = function (props) {
	        this.itemId = utils_1._get(props.item, props.keys.itemIdKey);
	        this.itemTitle = utils_1._get(props.item, props.keys.itemTitleKey);
	        this.itemTitleHTML = utils_1._get(props.item, props.keys.itemTitleHTMLKey);
	        this.itemDivTitle = props.keys.itemDivTitleKey ? utils_1._get(props.item, props.keys.itemDivTitleKey) : this.itemTitle;
	        this.itemTimeStart = utils_1._get(props.item, props.keys.itemTimeStartKey);
	        this.itemTimeEnd = utils_1._get(props.item, props.keys.itemTimeEndKey);
	    };
	    Item.prototype.coordinateToTimeRatio = function (props) {
	        if (props === void 0) { props = this.props; }
	        return (props.canvasTimeEnd - props.canvasTimeStart) / props.canvasWidth;
	    };
	    Item.prototype.dragTimeSnap = function (dragTime, considerOffset) {
	        var dragSnap = this.props.dragSnap;
	        if (dragSnap) {
	            var offset = considerOffset ? moment().utcOffset() * 60 * 1000 : 0;
	            return Math.round(dragTime / dragSnap) * dragSnap - offset % dragSnap;
	        }
	        else {
	            return dragTime;
	        }
	    };
	    Item.prototype.resizeTimeSnap = function (dragTime) {
	        var dragSnap = this.props.dragSnap;
	        if (dragSnap) {
	            var endTime = this.itemTimeEnd % dragSnap;
	            return Math.round((dragTime - endTime) / dragSnap) * dragSnap + endTime;
	        }
	        else {
	            return dragTime;
	        }
	    };
	    Item.prototype.dragTime = function (e) {
	        var startTime = this.itemTimeStart;
	        if (this.state.dragging) {
	            var deltaX = e.pageX - this.state.dragStart.x;
	            var timeDelta = deltaX * this.coordinateToTimeRatio();
	            return this.dragTimeSnap(startTime + timeDelta, true);
	        }
	        else {
	            return startTime;
	        }
	    };
	    Item.prototype.dragGroupDelta = function (e) {
	        var _a = this.props, groupTops = _a.groupTops, order = _a.order, topOffset = _a.topOffset;
	        if (this.state.dragging) {
	            if (!this.props.canChangeGroup) {
	                return 0;
	            }
	            var groupDelta = 0;
	            for (var _i = 0, _b = Object.keys(groupTops); _i < _b.length; _i++) {
	                var key = _b[_i];
	                var item = groupTops[key];
	                if (e.pageY - topOffset > item) {
	                    groupDelta = parseInt(key, 10) - order;
	                }
	                else {
	                    break;
	                }
	            }
	            if (this.props.order + groupDelta < 0) {
	                return 0 - this.props.order;
	            }
	            else {
	                return groupDelta;
	            }
	        }
	        else {
	            return 0;
	        }
	    };
	    Item.prototype.resizeTimeDelta = function (e) {
	        var length = this.itemTimeEnd - this.itemTimeStart;
	        var timeDelta = this.dragTimeSnap((e.pageX - this.state.resizeStart) * this.coordinateToTimeRatio());
	        if (length + timeDelta < (this.props.dragSnap || 1000)) {
	            return (this.props.dragSnap || 1000) - length;
	        }
	        else {
	            return timeDelta;
	        }
	    };
	    Item.prototype.componentDidMount = function () {
	        this.checkContentSize();
	    };
	    Item.prototype.mountInteract = function () {
	        var _this = this;
	        var rightResize = this.props.useResizeHandle ? this.refs.dragRight : true;
	        interact(this.refs.item)
	            .resizable({
	            edges: { left: false, right: rightResize, top: false, bottom: false },
	            enabled: this.props.selected && this.canResize()
	        })
	            .draggable({
	            enabled: this.props.selected
	        })
	            .on('dragstart', function (e) {
	            if (_this.props.selected) {
	                _this.setState({
	                    dragging: true,
	                    dragStart: { x: e.pageX, y: e.pageY },
	                    preDragPosition: { x: e.target.offsetLeft, y: e.target.offsetTop },
	                    dragTime: _this.itemTimeStart,
	                    dragGroupDelta: 0
	                });
	            }
	            else {
	                return false;
	            }
	        })
	            .on('dragmove', function (e) {
	            if (_this.state.dragging) {
	                var dragTime = _this.dragTime(e);
	                var dragGroupDelta = _this.dragGroupDelta(e);
	                if (_this.props.moveResizeValidator) {
	                    dragTime = _this.props.moveResizeValidator('move', _this.props.item, dragTime);
	                }
	                if (_this.props.onDrag) {
	                    _this.props.onDrag(_this.itemId, dragTime, _this.props.order + dragGroupDelta);
	                }
	                _this.setState({
	                    dragTime: dragTime,
	                    dragGroupDelta: dragGroupDelta
	                });
	            }
	        })
	            .on('dragend', function (e) {
	            if (_this.state.dragging) {
	                if (_this.props.onDrop) {
	                    var dragTime = _this.dragTime(e);
	                    if (_this.props.moveResizeValidator) {
	                        dragTime = _this.props.moveResizeValidator('move', _this.props.item, dragTime);
	                    }
	                    _this.props.onDrop(_this.itemId, dragTime, _this.props.order + _this.dragGroupDelta(e));
	                }
	                _this.setState({
	                    dragging: false,
	                    dragStart: null,
	                    preDragPosition: null,
	                    dragTime: null,
	                    dragGroupDelta: null
	                });
	            }
	        })
	            .on('resizestart', function (e) {
	            if (_this.props.selected) {
	                _this.setState({
	                    resizing: true,
	                    resizeStart: e.pageX,
	                    newResizeEnd: 0
	                });
	            }
	            else {
	                return false;
	            }
	        })
	            .on('resizemove', function (e) {
	            if (_this.state.resizing) {
	                var newResizeEnd = _this.resizeTimeSnap(_this.itemTimeEnd + _this.resizeTimeDelta(e));
	                if (_this.props.moveResizeValidator) {
	                    newResizeEnd = _this.props.moveResizeValidator('resize', _this.props.item, newResizeEnd);
	                }
	                if (_this.props.onResizing) {
	                    _this.props.onResizing(_this.itemId, newResizeEnd);
	                }
	                _this.setState({
	                    newResizeEnd: newResizeEnd
	                });
	            }
	        })
	            .on('resizeend', function (e) {
	            if (_this.state.resizing) {
	                var newResizeEnd = _this.resizeTimeSnap(_this.itemTimeEnd + _this.resizeTimeDelta(e));
	                if (_this.props.moveResizeValidator) {
	                    newResizeEnd = _this.props.moveResizeValidator('resize', _this.props.item, newResizeEnd);
	                }
	                if (_this.props.onResized && _this.resizeTimeDelta(e) !== 0) {
	                    _this.props.onResized(_this.itemId, newResizeEnd);
	                }
	                _this.setState({
	                    resizing: null,
	                    resizeStart: null,
	                    newResizeEnd: null
	                });
	            }
	        })
	            .on('tap', function (e) {
	            _this.actualClick(e, e.pointerType === 'mouse' ? 'click' : 'touch');
	        });
	        this.setState({
	            interactMounted: true
	        });
	    };
	    Item.prototype.canResize = function (props) {
	        if (props === void 0) { props = this.props; }
	        if (!props.canResize) {
	            return false;
	        }
	        var width = parseInt(this.props.dimensions.width, 10);
	        return width >= props.minResizeWidth;
	    };
	    Item.prototype.canMove = function (props) {
	        if (props === void 0) { props = this.props; }
	        return !!props.canMove;
	    };
	    Item.prototype.componentWillReceiveProps = function (nextProps) {
	        this.cacheDataFromProps(nextProps);
	        var interactMounted = this.state.interactMounted;
	        var couldDrag = this.props.selected && this.canMove(this.props);
	        var couldResize = this.props.selected && this.canResize(this.props);
	        var willBeAbleToDrag = nextProps.selected && this.canMove(nextProps);
	        var willBeAbleToResize = nextProps.selected && this.canResize(nextProps);
	        if (nextProps.selected && !interactMounted) {
	            this.mountInteract();
	            interactMounted = true;
	        }
	        if (interactMounted && couldResize !== willBeAbleToResize) {
	            interact(this.refs.item)
	                .resizable({ enabled: willBeAbleToResize });
	        }
	        if (interactMounted && couldDrag !== willBeAbleToDrag) {
	            interact(this.refs.item)
	                .draggable({ enabled: willBeAbleToDrag });
	        }
	        if (!nextProps.selected && this.props.selected) {
	            this.setState({ tooltip: false });
	            this.checkContentSize();
	        }
	    };
	    Item.prototype.actualClick = function (e, clickType) {
	        if (this.props.onSelect) {
	            this.props.onSelect(this.itemId, clickType, e);
	        }
	    };
	    Item.prototype.render = function () {
	        var dimensions = this.props.dimensions;
	        if (typeof this.props.order === 'undefined' || this.props.order === null) {
	            return null;
	        }
	        var classNames = 'rct-item' +
	            (this.props.selected ? ' selected' : '') +
	            (this.canMove(this.props) ? ' can-move' : '') +
	            (this.canResize(this.props) ? ' can-resize' : '') +
	            (this.props.item.className ? " " + this.props.item.className : '');
	        var style = {
	            left: dimensions.left + "px",
	            top: dimensions.top + "px",
	            width: dimensions.width + "px",
	            height: dimensions.height + "px",
	        };
	        if (this.props.item.color) {
	            style['backgroundColor'] = this.props.item.color;
	        }
	        return (React.createElement("div", {key: this.itemId, ref: 'item', className: classNames, title: this.itemDivTitle, onMouseDown: this.onMouseDown, onMouseUp: this.onMouseUp, onTouchStart: this.onTouchStart, onTouchEnd: this.onTouchEnd, onDoubleClick: this.handleDoubleClick, onContextMenu: this.handleContextMenu, style: style}, React.createElement("div", {title: this.itemDivTitle, className: "\n                rct-item-tooltip\n                " + (this.state.tooltip ? 'active' : '') + "\n                " + this.determineTooltipPosition() + "\n            ", style: {
	            height: dimensions.height + "px",
	        }, onClick: this.handleClick}, "^"), React.createElement("style", {dangerouslySetInnerHTML: {
	            __html: "\n                .rct-item-tooltip.top:before {\n                    bottom: " + (dimensions.height - 1) + "px;\n                }\n                .rct-item-tooltip.top:after {\n                    bottom: " + (dimensions.height + 5) + "px;\n                }\n\n                .rct-item-tooltip.bottom:after {\n                    top: " + (dimensions.height + 5) + "px;\n                }\n            "
	        }}), React.createElement("div", {className: 'rct-item-overflow'}, React.createElement("div", {ref: 'content', className: 'rct-item-content', style: {
	            visibility: this.state.contentIsHidden ? 'hidden' : 'visible'
	        }}, this.itemTitleHTML ? (React.createElement("div", {dangerouslySetInnerHTML: { __html: this.itemTitleHTML }})) : (this.itemTitle))), this.props.useResizeHandle ? React.createElement("div", {ref: 'dragRight', className: 'rct-drag-right'}) : ''));
	    };
	    Item.defaultProps = {
	        selected: false,
	        hideClippedTitle: true,
	    };
	    return Item;
	}(React.Component));
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.default = Item;
	//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaXRlbS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIml0ZW0udHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLElBQVksS0FBSyxXQUFNLE9BQ3ZCLENBQUMsQ0FENkI7QUFDOUIsSUFBWSxRQUFRLFdBQU0sYUFDMUIsQ0FBQyxDQURzQztBQUN2QyxJQUFZLE1BQU0sV0FBTSxRQUV4QixDQUFDLENBRitCO0FBRWhDLHNCQUFtQixVQUduQixDQUFDLENBSDRCO0FBbUM3QjtJQUFrQyx3QkFBMkI7SUFxQjNELGNBQWEsS0FBSztRQXJCcEIsaUJBd2RDO1FBbGNHLGtCQUFNLEtBQUssQ0FBQyxDQUFDO1FBd1NmLGdCQUFXLEdBQUcsVUFBQyxDQUFDO1lBQ2QsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7Z0JBQ2hDLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQTtnQkFDbEIsS0FBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUE7WUFDN0IsQ0FBQztRQUNILENBQUMsQ0FBQztRQUVGLGNBQVMsR0FBRyxVQUFDLENBQUM7WUFDWixFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxJQUFJLEtBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDO2dCQUN4RCxLQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQTtnQkFDNUIsS0FBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUE7WUFDOUIsQ0FBQztRQUNILENBQUMsQ0FBQztRQUVGLGlCQUFZLEdBQUcsVUFBQyxDQUFDO1lBQ2YsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7Z0JBQ2hDLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQTtnQkFDbEIsS0FBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUE7WUFDN0IsQ0FBQztRQUNILENBQUMsQ0FBQztRQUVGLGVBQVUsR0FBRyxVQUFDLENBQUM7WUFDYixFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxJQUFJLEtBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDO2dCQUN4RCxLQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQTtnQkFDNUIsS0FBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUE7WUFDOUIsQ0FBQztRQUNILENBQUMsQ0FBQztRQUVGLHNCQUFpQixHQUFHLFVBQUMsQ0FBQztZQUNwQixDQUFDLENBQUMsY0FBYyxFQUFFLENBQUE7WUFDbEIsQ0FBQyxDQUFDLGVBQWUsRUFBRSxDQUFBO1lBQ25CLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO2dCQUNqQyxLQUFJLENBQUMsS0FBSyxDQUFDLGlCQUFpQixDQUFDLEtBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUE7WUFDOUMsQ0FBQztRQUNILENBQUMsQ0FBQztRQUVGLHNCQUFpQixHQUFHLFVBQUMsQ0FBQztZQUNwQixFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7Z0JBQzdCLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQTtnQkFDbEIsQ0FBQyxDQUFDLGVBQWUsRUFBRSxDQUFBO2dCQUNuQixLQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFBO1lBQzFDLENBQUM7UUFDSCxDQUFDLENBQUM7UUFRRixnQkFBVyxHQUFHLFVBQUMsRUFBRTtZQUNmLEVBQUUsQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUNwQixFQUFFLENBQUMsZUFBZSxFQUFFLENBQUM7WUFDckIsS0FBSSxDQUFDLFFBQVEsQ0FBQyxFQUFDLE9BQU8sRUFBRSxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFDLENBQUMsQ0FBQztRQUNoRCxDQUFDLENBQUM7UUFFRiw2QkFBd0IsR0FBRztZQUN2QixFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNyQixNQUFNLENBQUMsS0FBSyxDQUFDO1lBQ2pCLENBQUM7WUFDRCxNQUFNLENBQUMsS0FBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLEdBQUcsR0FBRyxHQUFHLEtBQUssR0FBRyxRQUFRLENBQUM7UUFDaEUsQ0FBQyxDQUFDO1FBRUYscUJBQWdCLEdBQUc7WUFDZixJQUFNLEVBQUUsR0FBRyxLQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ2hDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDTixNQUFNLENBQUMsS0FBSyxDQUFDO1lBQ2pCLENBQUM7WUFDRCxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQztnQkFDOUIsSUFBTSxhQUFhLEdBQUcsRUFBRSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUMsV0FBVyxDQUFDO2dCQUN0RCxFQUFFLENBQUMsQ0FBQyxhQUFhLElBQUksQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7b0JBQy9DLEtBQUksQ0FBQyxRQUFRLENBQUMsRUFBQyxlQUFlLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQztnQkFDM0MsQ0FBQztnQkFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxhQUFhLElBQUksS0FBSSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDO29CQUN0RCxLQUFJLENBQUMsUUFBUSxDQUFDLEVBQUMsZUFBZSxFQUFFLEtBQUssRUFBQyxDQUFDLENBQUM7Z0JBQzVDLENBQUM7WUFDTCxDQUFDO1FBQ0wsQ0FBQyxDQUFDO1FBbFhBLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUUvQixJQUFJLENBQUMsS0FBSyxHQUFHO1lBQ1gsZUFBZSxFQUFFLEtBQUs7WUFFdEIsUUFBUSxFQUFFLElBQUk7WUFDZCxTQUFTLEVBQUUsSUFBSTtZQUNmLGVBQWUsRUFBRSxJQUFJO1lBQ3JCLFFBQVEsRUFBRSxJQUFJO1lBQ2QsY0FBYyxFQUFFLElBQUk7WUFFcEIsUUFBUSxFQUFFLElBQUk7WUFDZCxXQUFXLEVBQUUsSUFBSTtZQUNqQixVQUFVLEVBQUUsSUFBSTtZQUVoQixPQUFPLEVBQUUsS0FBSztZQUNkLGVBQWUsRUFBRSxLQUFLO1NBQ3ZCLENBQUE7SUFDSCxDQUFDO0lBRUQsb0NBQXFCLEdBQXJCLFVBQXVCLFNBQVMsRUFBRSxTQUFTO1FBQ3pDLElBQUksWUFBWSxHQUFHLFNBQVMsQ0FBQyxRQUFRLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRO1lBQzFDLFNBQVMsQ0FBQyxRQUFRLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRO1lBQzFDLFNBQVMsQ0FBQyxjQUFjLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjO1lBQ3RELFNBQVMsQ0FBQyxRQUFRLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRO1lBQzFDLFNBQVMsQ0FBQyxVQUFVLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVO1lBQzlDLFNBQVMsQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJO1lBQ2xDLFNBQVMsQ0FBQyxRQUFRLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRO1lBQzFDLFNBQVMsQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJO1lBQ2xDLFNBQVMsQ0FBQyxlQUFlLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxlQUFlO1lBQ3hELFNBQVMsQ0FBQyxhQUFhLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhO1lBQ3BELFNBQVMsQ0FBQyxXQUFXLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXO1lBQ2hELFNBQVMsQ0FBQyxVQUFVLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVO1lBQzlDLFNBQVMsQ0FBQyxLQUFLLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLO1lBQ3BDLFNBQVMsQ0FBQyxRQUFRLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRO1lBQzFDLFNBQVMsQ0FBQyxjQUFjLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjO1lBQ3RELFNBQVMsQ0FBQyxRQUFRLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRO1lBQzFDLFNBQVMsQ0FBQyxjQUFjLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjO1lBQ3RELFNBQVMsQ0FBQyxTQUFTLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTO1lBQzVDLFNBQVMsQ0FBQyxPQUFPLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPO1lBQ3hDLFNBQVMsQ0FBQyxTQUFTLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTO1lBQzVDLFNBQVMsQ0FBQyxVQUFVLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVO1lBQzlDLFNBQVMsQ0FBQyxPQUFPLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPO1lBQ3hDLFNBQVMsQ0FBQyxlQUFlLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUM7UUFDNUUsTUFBTSxDQUFDLFlBQVksQ0FBQTtJQUNyQixDQUFDO0lBRUQsaUNBQWtCLEdBQWxCLFVBQW9CLEtBQUs7UUFDdkIsSUFBSSxDQUFDLE1BQU0sR0FBRyxZQUFJLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3JELElBQUksQ0FBQyxTQUFTLEdBQUcsWUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUMzRCxJQUFJLENBQUMsYUFBYSxHQUFHLFlBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUNuRSxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsZUFBZSxHQUFHLFlBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUMvRyxJQUFJLENBQUMsYUFBYSxHQUFHLFlBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUNuRSxJQUFJLENBQUMsV0FBVyxHQUFHLFlBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUE7SUFDaEUsQ0FBQztJQUVELG9DQUFxQixHQUFyQixVQUF1QixLQUFrQjtRQUFsQixxQkFBa0IsR0FBbEIsUUFBUSxJQUFJLENBQUMsS0FBSztRQUN2QyxNQUFNLENBQUMsQ0FBQyxLQUFLLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQyxlQUFlLENBQUMsR0FBRyxLQUFLLENBQUMsV0FBVyxDQUFBO0lBQzFFLENBQUM7SUFFRCwyQkFBWSxHQUFaLFVBQWMsUUFBUSxFQUFFLGNBQWU7UUFDN0Isa0NBQVEsQ0FBZTtRQUMvQixFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ2IsSUFBTSxNQUFNLEdBQUcsY0FBYyxHQUFHLE1BQU0sRUFBRSxDQUFDLFNBQVMsRUFBRSxHQUFHLEVBQUUsR0FBRyxJQUFJLEdBQUcsQ0FBQyxDQUFBO1lBQ3BFLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUMsR0FBRyxRQUFRLEdBQUcsTUFBTSxHQUFHLFFBQVEsQ0FBQTtRQUN2RSxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixNQUFNLENBQUMsUUFBUSxDQUFBO1FBQ2pCLENBQUM7SUFDSCxDQUFDO0lBRUQsNkJBQWMsR0FBZCxVQUFnQixRQUFRO1FBQ2Qsa0NBQVEsQ0FBZTtRQUMvQixFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ2IsSUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLFdBQVcsR0FBRyxRQUFRLENBQUE7WUFDM0MsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDLEdBQUcsUUFBUSxDQUFDLEdBQUcsUUFBUSxHQUFHLE9BQU8sQ0FBQTtRQUN6RSxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixNQUFNLENBQUMsUUFBUSxDQUFBO1FBQ2pCLENBQUM7SUFDSCxDQUFDO0lBRUQsdUJBQVEsR0FBUixVQUFVLENBQUM7UUFDVCxJQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFBO1FBRXBDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUN4QixJQUFNLE1BQU0sR0FBRyxDQUFDLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQTtZQUMvQyxJQUFNLFNBQVMsR0FBRyxNQUFNLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixFQUFFLENBQUE7WUFFdkQsTUFBTSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxHQUFHLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQTtRQUN2RCxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixNQUFNLENBQUMsU0FBUyxDQUFBO1FBQ2xCLENBQUM7SUFDSCxDQUFDO0lBRUQsNkJBQWMsR0FBZCxVQUFnQixDQUFDO1FBQ2YsSUFBQSxlQUFnRCxFQUF6Qyx3QkFBUyxFQUFFLGdCQUFLLEVBQUUsd0JBQVMsQ0FBYztRQUNoRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDeEIsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7Z0JBQy9CLE1BQU0sQ0FBQyxDQUFDLENBQUE7WUFDVixDQUFDO1lBQ0QsSUFBSSxVQUFVLEdBQUcsQ0FBQyxDQUFBO1lBRWxCLEdBQUcsQ0FBQyxDQUFZLFVBQXNCLEVBQXRCLEtBQUEsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBdEIsY0FBc0IsRUFBdEIsSUFBc0IsQ0FBQztnQkFBbEMsSUFBSSxHQUFHLFNBQUE7Z0JBQ1YsSUFBSSxJQUFJLEdBQUcsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFBO2dCQUN6QixFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLFNBQVMsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDO29CQUMvQixVQUFVLEdBQUcsUUFBUSxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUE7Z0JBQ3hDLENBQUM7Z0JBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ04sS0FBSyxDQUFBO2dCQUNQLENBQUM7YUFDRjtZQUVELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLFVBQVUsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN0QyxNQUFNLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFBO1lBQzdCLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDTixNQUFNLENBQUMsVUFBVSxDQUFBO1lBQ25CLENBQUM7UUFDSCxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixNQUFNLENBQUMsQ0FBQyxDQUFBO1FBQ1YsQ0FBQztJQUNILENBQUM7SUFFRCw4QkFBZSxHQUFmLFVBQWlCLENBQUM7UUFDaEIsSUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFBO1FBQ3BELElBQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixFQUFFLENBQUMsQ0FBQTtRQUV0RyxFQUFFLENBQUMsQ0FBQyxNQUFNLEdBQUcsU0FBUyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3ZELE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxHQUFHLE1BQU0sQ0FBQTtRQUMvQyxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixNQUFNLENBQUMsU0FBUyxDQUFBO1FBQ2xCLENBQUM7SUFDSCxDQUFDO0lBRUQsZ0NBQWlCLEdBQWpCO1FBQ0ksSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7SUFDNUIsQ0FBQztJQUVELDRCQUFhLEdBQWI7UUFBQSxpQkFvSEM7UUFuSEMsSUFBTSxXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFBO1FBQzNFLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQzthQUNyQixTQUFTLENBQUM7WUFDVCxLQUFLLEVBQUUsRUFBQyxJQUFJLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxXQUFXLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFDO1lBQ25FLE9BQU8sRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1NBQ2pELENBQUM7YUFDRCxTQUFTLENBQUM7WUFDVCxPQUFPLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRO1NBQzdCLENBQUM7YUFDRCxFQUFFLENBQUMsV0FBVyxFQUFFLFVBQUMsQ0FBQztZQUNqQixFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hCLEtBQUksQ0FBQyxRQUFRLENBQUM7b0JBQ1osUUFBUSxFQUFFLElBQUk7b0JBQ2QsU0FBUyxFQUFFLEVBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxLQUFLLEVBQUM7b0JBQ25DLGVBQWUsRUFBRSxFQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLFVBQVUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUM7b0JBQ2hFLFFBQVEsRUFBRSxLQUFJLENBQUMsYUFBYTtvQkFDNUIsY0FBYyxFQUFFLENBQUM7aUJBQ2xCLENBQUMsQ0FBQTtZQUNKLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDTixNQUFNLENBQUMsS0FBSyxDQUFBO1lBQ2QsQ0FBQztRQUNILENBQUMsQ0FBQzthQUNELEVBQUUsQ0FBQyxVQUFVLEVBQUUsVUFBQyxDQUFDO1lBQ2hCLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztnQkFDeEIsSUFBSSxRQUFRLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQTtnQkFDL0IsSUFBSSxjQUFjLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtnQkFFM0MsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUM7b0JBQ25DLFFBQVEsR0FBRyxLQUFJLENBQUMsS0FBSyxDQUFDLG1CQUFtQixDQUFDLE1BQU0sRUFBRSxLQUFJLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQTtnQkFDOUUsQ0FBQztnQkFFRCxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7b0JBQ3RCLEtBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxNQUFNLEVBQUUsUUFBUSxFQUFFLEtBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLGNBQWMsQ0FBQyxDQUFBO2dCQUM3RSxDQUFDO2dCQUVELEtBQUksQ0FBQyxRQUFRLENBQUM7b0JBQ1osUUFBUSxFQUFFLFFBQVE7b0JBQ2xCLGNBQWMsRUFBRSxjQUFjO2lCQUMvQixDQUFDLENBQUE7WUFDSixDQUFDO1FBQ0gsQ0FBQyxDQUFDO2FBQ0QsRUFBRSxDQUFDLFNBQVMsRUFBRSxVQUFDLENBQUM7WUFDZixFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hCLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztvQkFDdEIsSUFBSSxRQUFRLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQTtvQkFFL0IsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUM7d0JBQ25DLFFBQVEsR0FBRyxLQUFJLENBQUMsS0FBSyxDQUFDLG1CQUFtQixDQUFDLE1BQU0sRUFBRSxLQUFJLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQTtvQkFDOUUsQ0FBQztvQkFFRCxLQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFJLENBQUMsTUFBTSxFQUFFLFFBQVEsRUFBRSxLQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7Z0JBQ3JGLENBQUM7Z0JBRUQsS0FBSSxDQUFDLFFBQVEsQ0FBQztvQkFDWixRQUFRLEVBQUUsS0FBSztvQkFDZixTQUFTLEVBQUUsSUFBSTtvQkFDZixlQUFlLEVBQUUsSUFBSTtvQkFDckIsUUFBUSxFQUFFLElBQUk7b0JBQ2QsY0FBYyxFQUFFLElBQUk7aUJBQ3JCLENBQUMsQ0FBQTtZQUNKLENBQUM7UUFDSCxDQUFDLENBQUM7YUFDRCxFQUFFLENBQUMsYUFBYSxFQUFFLFVBQUMsQ0FBQztZQUNuQixFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hCLEtBQUksQ0FBQyxRQUFRLENBQUM7b0JBQ1osUUFBUSxFQUFFLElBQUk7b0JBQ2QsV0FBVyxFQUFFLENBQUMsQ0FBQyxLQUFLO29CQUNwQixZQUFZLEVBQUUsQ0FBQztpQkFDaEIsQ0FBQyxDQUFBO1lBQ0osQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNOLE1BQU0sQ0FBQyxLQUFLLENBQUE7WUFDZCxDQUFDO1FBQ0gsQ0FBQyxDQUFDO2FBQ0QsRUFBRSxDQUFDLFlBQVksRUFBRSxVQUFDLENBQUM7WUFDbEIsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUN4QixJQUFJLFlBQVksR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLEtBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBO2dCQUVsRixFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQztvQkFDbkMsWUFBWSxHQUFHLEtBQUksQ0FBQyxLQUFLLENBQUMsbUJBQW1CLENBQUMsUUFBUSxFQUFFLEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLFlBQVksQ0FBQyxDQUFBO2dCQUN4RixDQUFDO2dCQUVELEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztvQkFDMUIsS0FBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsS0FBSSxDQUFDLE1BQU0sRUFBRSxZQUFZLENBQUMsQ0FBQTtnQkFDbEQsQ0FBQztnQkFFRCxLQUFJLENBQUMsUUFBUSxDQUFDO29CQUNaLFlBQVksRUFBRSxZQUFZO2lCQUMzQixDQUFDLENBQUE7WUFDSixDQUFDO1FBQ0gsQ0FBQyxDQUFDO2FBQ0QsRUFBRSxDQUFDLFdBQVcsRUFBRSxVQUFDLENBQUM7WUFDakIsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUN4QixJQUFJLFlBQVksR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLEtBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBO2dCQUVsRixFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQztvQkFDbkMsWUFBWSxHQUFHLEtBQUksQ0FBQyxLQUFLLENBQUMsbUJBQW1CLENBQUMsUUFBUSxFQUFFLEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLFlBQVksQ0FBQyxDQUFBO2dCQUN4RixDQUFDO2dCQUVELEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxJQUFJLEtBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDMUQsS0FBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsS0FBSSxDQUFDLE1BQU0sRUFBRSxZQUFZLENBQUMsQ0FBQTtnQkFDakQsQ0FBQztnQkFDRCxLQUFJLENBQUMsUUFBUSxDQUFDO29CQUNaLFFBQVEsRUFBRSxJQUFJO29CQUNkLFdBQVcsRUFBRSxJQUFJO29CQUNqQixZQUFZLEVBQUUsSUFBSTtpQkFDbkIsQ0FBQyxDQUFBO1lBQ0osQ0FBQztRQUNILENBQUMsQ0FBQzthQUNELEVBQUUsQ0FBQyxLQUFLLEVBQUUsVUFBQyxDQUFDO1lBQ1gsS0FBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLFdBQVcsS0FBSyxPQUFPLEdBQUcsT0FBTyxHQUFHLE9BQU8sQ0FBQyxDQUFBO1FBQ3BFLENBQUMsQ0FBQyxDQUFBO1FBRUosSUFBSSxDQUFDLFFBQVEsQ0FBQztZQUNaLGVBQWUsRUFBRSxJQUFJO1NBQ3RCLENBQUMsQ0FBQTtJQUNKLENBQUM7SUFFRCx3QkFBUyxHQUFULFVBQVcsS0FBa0I7UUFBbEIscUJBQWtCLEdBQWxCLFFBQVEsSUFBSSxDQUFDLEtBQUs7UUFDM0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztZQUNyQixNQUFNLENBQUMsS0FBSyxDQUFBO1FBQ2QsQ0FBQztRQUNELElBQUksS0FBSyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLENBQUE7UUFDckQsTUFBTSxDQUFDLEtBQUssSUFBSSxLQUFLLENBQUMsY0FBYyxDQUFBO0lBQ3RDLENBQUM7SUFFRCxzQkFBTyxHQUFQLFVBQVMsS0FBa0I7UUFBbEIscUJBQWtCLEdBQWxCLFFBQVEsSUFBSSxDQUFDLEtBQUs7UUFDekIsTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFBO0lBQ3hCLENBQUM7SUFFRCx3Q0FBeUIsR0FBekIsVUFBMkIsU0FBUztRQUNsQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsU0FBUyxDQUFDLENBQUE7UUFFNUIsZ0RBQWUsQ0FBZTtRQUNwQyxJQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUNqRSxJQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUNyRSxJQUFNLGdCQUFnQixHQUFHLFNBQVMsQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQTtRQUN0RSxJQUFNLGtCQUFrQixHQUFHLFNBQVMsQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQTtRQUUxRSxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztZQUMzQyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUE7WUFDcEIsZUFBZSxHQUFHLElBQUksQ0FBQTtRQUN4QixDQUFDO1FBRUQsRUFBRSxDQUFDLENBQUMsZUFBZSxJQUFJLFdBQVcsS0FBSyxrQkFBa0IsQ0FBQyxDQUFDLENBQUM7WUFDMUQsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO2lCQUNyQixTQUFTLENBQUMsRUFBQyxPQUFPLEVBQUUsa0JBQWtCLEVBQUMsQ0FBQyxDQUFBO1FBQzdDLENBQUM7UUFDRCxFQUFFLENBQUMsQ0FBQyxlQUFlLElBQUksU0FBUyxLQUFLLGdCQUFnQixDQUFDLENBQUMsQ0FBQztZQUN0RCxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7aUJBQ3JCLFNBQVMsQ0FBQyxFQUFDLE9BQU8sRUFBRSxnQkFBZ0IsRUFBQyxDQUFDLENBQUE7UUFDM0MsQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDN0MsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFDLE9BQU8sRUFBRSxLQUFLLEVBQUMsQ0FBQyxDQUFDO1lBQ2hDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQzVCLENBQUM7SUFDSCxDQUFDO0lBOENELDBCQUFXLEdBQVgsVUFBYSxDQUFDLEVBQUUsU0FBUztRQUN2QixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDeEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxTQUFTLEVBQUUsQ0FBQyxDQUFDLENBQUE7UUFDaEQsQ0FBQztJQUNILENBQUM7SUE4QkQscUJBQU0sR0FBTjtRQUNFLElBQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDO1FBQ3pDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEtBQUssV0FBVyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDekUsTUFBTSxDQUFDLElBQUksQ0FBQTtRQUNiLENBQUM7UUFFRCxJQUFNLFVBQVUsR0FBRyxVQUFVO1lBQ1YsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxXQUFXLEdBQUcsRUFBRSxDQUFDO1lBQ3hDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsV0FBVyxHQUFHLEVBQUUsQ0FBQztZQUM3QyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLGFBQWEsR0FBRyxFQUFFLENBQUM7WUFDakQsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsTUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFXLEdBQUcsRUFBRSxDQUFDLENBQUM7UUFFdEYsSUFBTSxLQUFLLEdBQUc7WUFDWixJQUFJLEVBQUssVUFBVSxDQUFDLElBQUksT0FBSTtZQUM1QixHQUFHLEVBQUssVUFBVSxDQUFDLEdBQUcsT0FBSTtZQUMxQixLQUFLLEVBQUssVUFBVSxDQUFDLEtBQUssT0FBSTtZQUM5QixNQUFNLEVBQUssVUFBVSxDQUFDLE1BQU0sT0FBSTtTQUVqQyxDQUFDO1FBQ0YsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUN4QixLQUFLLENBQUMsaUJBQWlCLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDckQsQ0FBQztRQUVELE1BQU0sQ0FBQyxDQUNMLHFCQUFDLEdBQUcsSUFBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLE1BQU8sRUFDakIsR0FBRyxFQUFDLE1BQU0sRUFDVixTQUFTLEVBQUUsVUFBVyxFQUN0QixLQUFLLEVBQUUsSUFBSSxDQUFDLFlBQWEsRUFDekIsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFZLEVBQzlCLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBVSxFQUMxQixZQUFZLEVBQUUsSUFBSSxDQUFDLFlBQWEsRUFDaEMsVUFBVSxFQUFFLElBQUksQ0FBQyxVQUFXLEVBQzVCLGFBQWEsRUFBRSxJQUFJLENBQUMsaUJBQWtCLEVBQ3RDLGFBQWEsRUFBRSxJQUFJLENBQUMsaUJBQWtCLEVBQ3RDLEtBQUssRUFBRSxLQUFNLEdBQ2hCLHFCQUFDLEdBQUcsSUFDQSxLQUFLLEVBQUUsSUFBSSxDQUFDLFlBQWEsRUFDekIsU0FBUyxFQUFFLDBEQUVMLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLFFBQVEsR0FBRyxFQUFFLDJCQUNsQyxJQUFJLENBQUMsd0JBQXdCLEVBQUUsbUJBQ25DLEVBQ0YsS0FBSyxFQUFFO1lBQ0gsTUFBTSxFQUFLLFVBQVUsQ0FBQyxNQUFNLE9BQUk7U0FDbEMsRUFDRixPQUFPLEVBQUUsSUFBSSxDQUFDLFdBQVksT0FDdEIsRUFDUixxQkFBQyxLQUFLLElBQUMsdUJBQXVCLEVBQUU7WUFDNUIsTUFBTSxFQUFFLG9GQUVVLFVBQVUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyw2R0FHckIsVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLCtHQUl4QixVQUFVLENBQUMsTUFBTSxHQUFHLENBQUMsMENBRW5DO1NBQ0gsRUFBRyxFQUNMLHFCQUFDLEdBQUcsSUFBQyxTQUFTLEVBQUMsbUJBQW1CLEdBQ2hDLHFCQUFDLEdBQUcsSUFBQyxHQUFHLEVBQUMsU0FBUyxFQUFDLFNBQVMsRUFBQyxrQkFBa0IsRUFBQyxLQUFLLEVBQUU7WUFDckQsVUFBVSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxHQUFHLFFBQVEsR0FBRyxTQUFTO1NBQzdELEdBQ0MsSUFBSSxDQUFDLGFBQWEsR0FBRyxDQUNsQixxQkFBQyxHQUFHLElBQUMsdUJBQXVCLEVBQUUsRUFBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLGFBQWEsRUFBRSxFQUFHLENBQ2pFLEdBQUcsQ0FDQSxJQUFJLENBQUMsU0FBUyxDQUNoQixDQUNFLENBQ0YsRUFDSixJQUFJLENBQUMsS0FBSyxDQUFDLGVBQWUsR0FBRyxxQkFBQyxHQUFHLElBQUMsR0FBRyxFQUFDLFdBQVcsRUFBQyxTQUFTLEVBQUMsZ0JBQWdCLEVBQU8sR0FBRyxFQUFJLENBQ3hGLENBQ1AsQ0FBQTtJQUNILENBQUM7SUF0ZFEsaUJBQVksR0FBRztRQUNsQixRQUFRLEVBQUUsS0FBSztRQUNmLGdCQUFnQixFQUFFLElBQUk7S0FDekIsQ0FBQztJQW9kTixXQUFDO0FBQUQsQ0FBQyxBQXhkRCxDQUFrQyxLQUFLLENBQUMsU0FBUyxHQXdkaEQ7QUF4ZEQ7c0JBd2RDLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCdcbmltcG9ydCAqIGFzIGludGVyYWN0IGZyb20gJ2ludGVyYWN0LmpzJ1xuaW1wb3J0ICogYXMgbW9tZW50IGZyb20gJ21vbWVudCdcblxuaW1wb3J0IHtfZ2V0fSBmcm9tICcuLi91dGlscydcblxuXG5pbnRlcmZhY2UgUHJvcHMge1xuICAgIGdyb3VwSGVpZ2h0cz9cbiAgICBncm91cFRvcHM/XG4gICAga2V5cz9cbiAgICBzZWxlY3RlZD9cbiAgICBpdGVtP1xuICAgIGNhbnZhc1RpbWVTdGFydD9cbiAgICBjYW52YXNUaW1lRW5kP1xuICAgIGNhbnZhc1dpZHRoP1xuICAgIGxpbmVIZWlnaHQ/XG4gICAgb3JkZXI/XG4gICAgZHJhZ1NuYXA/XG4gICAgbWluUmVzaXplV2lkdGg/XG4gICAgY2FuQ2hhbmdlR3JvdXA/XG4gICAgdG9wT2Zmc2V0P1xuICAgIGNhbk1vdmU/XG4gICAgY2FuUmVzaXplP1xuICAgIGRpbWVuc2lvbnM/XG4gICAgdXNlUmVzaXplSGFuZGxlP1xuICAgIG1vdmVSZXNpemVWYWxpZGF0b3I/OiAoZXZlbnRUeXBlLCBpdGVtLCB0aW1lKSA9PiB2b2lkXG4gICAgb25EcmFnPzogKGlkLCB0aW1lLCBvcmRlcikgPT4gdm9pZFxuICAgIG9uRHJvcD86IChpZCwgdGltZSwgb3JkZXIpID0+IHZvaWRcbiAgICBvblJlc2l6aW5nPzogKGlkLCBuZXdSZXNpemVFbmQpID0+IHZvaWRcbiAgICBvblJlc2l6ZWQ/OiAoaXRlbUlkLCBuZXdSZXNpemVFbmQpID0+IHZvaWRcbiAgICBvbkl0ZW1Eb3VibGVDbGljaz86IChpdGVtSWQsIGUpID0+IHZvaWRcbiAgICBvbkNvbnRleHRNZW51PzogKGl0ZW1JZCwgZSkgPT4gdm9pZFxuICAgIG9uU2VsZWN0PzogKGl0ZW1JZCwgY2xpY2tUeXBlLCBlKSA9PiB2b2lkXG5cbiAgICBoaWRlQ2xpcHBlZFRpdGxlPzogYm9vbGVhblxufVxuXG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEl0ZW0gZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQ8UHJvcHMsIGFueT4ge1xuICAgIHN0YXRpYyBkZWZhdWx0UHJvcHMgPSB7XG4gICAgICAgIHNlbGVjdGVkOiBmYWxzZSxcbiAgICAgICAgaGlkZUNsaXBwZWRUaXRsZTogdHJ1ZSxcbiAgICB9O1xuXG4gICAgaXRlbUlkO1xuICAgIGl0ZW1UaXRsZTtcbiAgICBpdGVtVGl0bGVIVE1MO1xuICAgIGl0ZW1EaXZUaXRsZTtcbiAgICBpdGVtVGltZVN0YXJ0O1xuICAgIGl0ZW1UaW1lRW5kO1xuICAgIHN0YXJ0ZWRDbGlja2luZztcbiAgICBzdGFydGVkVG91Y2hpbmc7XG5cbiAgICByZWZzOiB7XG4gICAgICAgIFtrZXk6IHN0cmluZ106IGFueVxuICAgICAgICBkcmFnUmlnaHRcbiAgICAgICAgaXRlbVxuICAgIH07XG5cbiAgY29uc3RydWN0b3IgKHByb3BzKSB7XG4gICAgc3VwZXIocHJvcHMpO1xuXG4gICAgdGhpcy5jYWNoZURhdGFGcm9tUHJvcHMocHJvcHMpO1xuXG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgIGludGVyYWN0TW91bnRlZDogZmFsc2UsXG5cbiAgICAgIGRyYWdnaW5nOiBudWxsLFxuICAgICAgZHJhZ1N0YXJ0OiBudWxsLFxuICAgICAgcHJlRHJhZ1Bvc2l0aW9uOiBudWxsLFxuICAgICAgZHJhZ1RpbWU6IG51bGwsXG4gICAgICBkcmFnR3JvdXBEZWx0YTogbnVsbCxcblxuICAgICAgcmVzaXppbmc6IG51bGwsXG4gICAgICByZXNpemVTdGFydDogbnVsbCxcbiAgICAgIHJlc2l6ZVRpbWU6IG51bGwsXG5cbiAgICAgIHRvb2x0aXA6IGZhbHNlLFxuICAgICAgY29udGVudElzSGlkZGVuOiBmYWxzZSxcbiAgICB9XG4gIH1cblxuICBzaG91bGRDb21wb25lbnRVcGRhdGUgKG5leHRQcm9wcywgbmV4dFN0YXRlKSB7XG4gICAgdmFyIHNob3VsZFVwZGF0ZSA9IG5leHRTdGF0ZS5kcmFnZ2luZyAhPT0gdGhpcy5zdGF0ZS5kcmFnZ2luZyB8fFxuICAgICAgICAgICAgICAgICAgICAgICBuZXh0U3RhdGUuZHJhZ1RpbWUgIT09IHRoaXMuc3RhdGUuZHJhZ1RpbWUgfHxcbiAgICAgICAgICAgICAgICAgICAgICAgbmV4dFN0YXRlLmRyYWdHcm91cERlbHRhICE9PSB0aGlzLnN0YXRlLmRyYWdHcm91cERlbHRhIHx8XG4gICAgICAgICAgICAgICAgICAgICAgIG5leHRTdGF0ZS5yZXNpemluZyAhPT0gdGhpcy5zdGF0ZS5yZXNpemluZyB8fFxuICAgICAgICAgICAgICAgICAgICAgICBuZXh0U3RhdGUucmVzaXplVGltZSAhPT0gdGhpcy5zdGF0ZS5yZXNpemVUaW1lIHx8XG4gICAgICAgICAgICAgICAgICAgICAgIG5leHRQcm9wcy5rZXlzICE9PSB0aGlzLnByb3BzLmtleXMgfHxcbiAgICAgICAgICAgICAgICAgICAgICAgbmV4dFByb3BzLnNlbGVjdGVkICE9PSB0aGlzLnByb3BzLnNlbGVjdGVkIHx8XG4gICAgICAgICAgICAgICAgICAgICAgIG5leHRQcm9wcy5pdGVtICE9PSB0aGlzLnByb3BzLml0ZW0gfHxcbiAgICAgICAgICAgICAgICAgICAgICAgbmV4dFByb3BzLmNhbnZhc1RpbWVTdGFydCAhPT0gdGhpcy5wcm9wcy5jYW52YXNUaW1lU3RhcnQgfHxcbiAgICAgICAgICAgICAgICAgICAgICAgbmV4dFByb3BzLmNhbnZhc1RpbWVFbmQgIT09IHRoaXMucHJvcHMuY2FudmFzVGltZUVuZCB8fFxuICAgICAgICAgICAgICAgICAgICAgICBuZXh0UHJvcHMuY2FudmFzV2lkdGggIT09IHRoaXMucHJvcHMuY2FudmFzV2lkdGggfHxcbiAgICAgICAgICAgICAgICAgICAgICAgbmV4dFByb3BzLmxpbmVIZWlnaHQgIT09IHRoaXMucHJvcHMubGluZUhlaWdodCB8fFxuICAgICAgICAgICAgICAgICAgICAgICBuZXh0UHJvcHMub3JkZXIgIT09IHRoaXMucHJvcHMub3JkZXIgfHxcbiAgICAgICAgICAgICAgICAgICAgICAgbmV4dFByb3BzLmRyYWdTbmFwICE9PSB0aGlzLnByb3BzLmRyYWdTbmFwIHx8XG4gICAgICAgICAgICAgICAgICAgICAgIG5leHRQcm9wcy5taW5SZXNpemVXaWR0aCAhPT0gdGhpcy5wcm9wcy5taW5SZXNpemVXaWR0aCB8fFxuICAgICAgICAgICAgICAgICAgICAgICBuZXh0UHJvcHMuc2VsZWN0ZWQgIT09IHRoaXMucHJvcHMuc2VsZWN0ZWQgfHxcbiAgICAgICAgICAgICAgICAgICAgICAgbmV4dFByb3BzLmNhbkNoYW5nZUdyb3VwICE9PSB0aGlzLnByb3BzLmNhbkNoYW5nZUdyb3VwIHx8XG4gICAgICAgICAgICAgICAgICAgICAgIG5leHRQcm9wcy50b3BPZmZzZXQgIT09IHRoaXMucHJvcHMudG9wT2Zmc2V0IHx8XG4gICAgICAgICAgICAgICAgICAgICAgIG5leHRQcm9wcy5jYW5Nb3ZlICE9PSB0aGlzLnByb3BzLmNhbk1vdmUgfHxcbiAgICAgICAgICAgICAgICAgICAgICAgbmV4dFByb3BzLmNhblJlc2l6ZSAhPT0gdGhpcy5wcm9wcy5jYW5SZXNpemUgfHxcbiAgICAgICAgICAgICAgICAgICAgICAgbmV4dFByb3BzLmRpbWVuc2lvbnMgIT09IHRoaXMucHJvcHMuZGltZW5zaW9ucyB8fFxuICAgICAgICAgICAgICAgICAgICAgICBuZXh0U3RhdGUudG9vbHRpcCAhPT0gdGhpcy5zdGF0ZS50b29sdGlwIHx8XG4gICAgICAgICAgICAgICAgICAgICAgIG5leHRTdGF0ZS5jb250ZW50SXNIaWRkZW4gIT09IHRoaXMuc3RhdGUuY29udGVudElzSGlkZGVuO1xuICAgIHJldHVybiBzaG91bGRVcGRhdGVcbiAgfVxuXG4gIGNhY2hlRGF0YUZyb21Qcm9wcyAocHJvcHMpIHtcbiAgICB0aGlzLml0ZW1JZCA9IF9nZXQocHJvcHMuaXRlbSwgcHJvcHMua2V5cy5pdGVtSWRLZXkpO1xuICAgIHRoaXMuaXRlbVRpdGxlID0gX2dldChwcm9wcy5pdGVtLCBwcm9wcy5rZXlzLml0ZW1UaXRsZUtleSk7XG4gICAgdGhpcy5pdGVtVGl0bGVIVE1MID0gX2dldChwcm9wcy5pdGVtLCBwcm9wcy5rZXlzLml0ZW1UaXRsZUhUTUxLZXkpO1xuICAgIHRoaXMuaXRlbURpdlRpdGxlID0gcHJvcHMua2V5cy5pdGVtRGl2VGl0bGVLZXkgPyBfZ2V0KHByb3BzLml0ZW0sIHByb3BzLmtleXMuaXRlbURpdlRpdGxlS2V5KSA6IHRoaXMuaXRlbVRpdGxlO1xuICAgIHRoaXMuaXRlbVRpbWVTdGFydCA9IF9nZXQocHJvcHMuaXRlbSwgcHJvcHMua2V5cy5pdGVtVGltZVN0YXJ0S2V5KTtcbiAgICB0aGlzLml0ZW1UaW1lRW5kID0gX2dldChwcm9wcy5pdGVtLCBwcm9wcy5rZXlzLml0ZW1UaW1lRW5kS2V5KVxuICB9XG5cbiAgY29vcmRpbmF0ZVRvVGltZVJhdGlvIChwcm9wcyA9IHRoaXMucHJvcHMpIHtcbiAgICByZXR1cm4gKHByb3BzLmNhbnZhc1RpbWVFbmQgLSBwcm9wcy5jYW52YXNUaW1lU3RhcnQpIC8gcHJvcHMuY2FudmFzV2lkdGhcbiAgfVxuXG4gIGRyYWdUaW1lU25hcCAoZHJhZ1RpbWUsIGNvbnNpZGVyT2Zmc2V0Pykge1xuICAgIGNvbnN0IHsgZHJhZ1NuYXAgfSA9IHRoaXMucHJvcHNcbiAgICBpZiAoZHJhZ1NuYXApIHtcbiAgICAgIGNvbnN0IG9mZnNldCA9IGNvbnNpZGVyT2Zmc2V0ID8gbW9tZW50KCkudXRjT2Zmc2V0KCkgKiA2MCAqIDEwMDAgOiAwXG4gICAgICByZXR1cm4gTWF0aC5yb3VuZChkcmFnVGltZSAvIGRyYWdTbmFwKSAqIGRyYWdTbmFwIC0gb2Zmc2V0ICUgZHJhZ1NuYXBcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIGRyYWdUaW1lXG4gICAgfVxuICB9XG5cbiAgcmVzaXplVGltZVNuYXAgKGRyYWdUaW1lKSB7XG4gICAgY29uc3QgeyBkcmFnU25hcCB9ID0gdGhpcy5wcm9wc1xuICAgIGlmIChkcmFnU25hcCkge1xuICAgICAgY29uc3QgZW5kVGltZSA9IHRoaXMuaXRlbVRpbWVFbmQgJSBkcmFnU25hcFxuICAgICAgcmV0dXJuIE1hdGgucm91bmQoKGRyYWdUaW1lIC0gZW5kVGltZSkgLyBkcmFnU25hcCkgKiBkcmFnU25hcCArIGVuZFRpbWVcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIGRyYWdUaW1lXG4gICAgfVxuICB9XG5cbiAgZHJhZ1RpbWUgKGUpIHtcbiAgICBjb25zdCBzdGFydFRpbWUgPSB0aGlzLml0ZW1UaW1lU3RhcnRcblxuICAgIGlmICh0aGlzLnN0YXRlLmRyYWdnaW5nKSB7XG4gICAgICBjb25zdCBkZWx0YVggPSBlLnBhZ2VYIC0gdGhpcy5zdGF0ZS5kcmFnU3RhcnQueFxuICAgICAgY29uc3QgdGltZURlbHRhID0gZGVsdGFYICogdGhpcy5jb29yZGluYXRlVG9UaW1lUmF0aW8oKVxuXG4gICAgICByZXR1cm4gdGhpcy5kcmFnVGltZVNuYXAoc3RhcnRUaW1lICsgdGltZURlbHRhLCB0cnVlKVxuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gc3RhcnRUaW1lXG4gICAgfVxuICB9XG5cbiAgZHJhZ0dyb3VwRGVsdGEgKGUpIHtcbiAgICBjb25zdCB7Z3JvdXBUb3BzLCBvcmRlciwgdG9wT2Zmc2V0fSA9IHRoaXMucHJvcHNcbiAgICBpZiAodGhpcy5zdGF0ZS5kcmFnZ2luZykge1xuICAgICAgaWYgKCF0aGlzLnByb3BzLmNhbkNoYW5nZUdyb3VwKSB7XG4gICAgICAgIHJldHVybiAwXG4gICAgICB9XG4gICAgICBsZXQgZ3JvdXBEZWx0YSA9IDBcblxuICAgICAgZm9yICh2YXIga2V5IG9mIE9iamVjdC5rZXlzKGdyb3VwVG9wcykpIHtcbiAgICAgICAgdmFyIGl0ZW0gPSBncm91cFRvcHNba2V5XVxuICAgICAgICBpZiAoZS5wYWdlWSAtIHRvcE9mZnNldCA+IGl0ZW0pIHtcbiAgICAgICAgICBncm91cERlbHRhID0gcGFyc2VJbnQoa2V5LCAxMCkgLSBvcmRlclxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGJyZWFrXG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgaWYgKHRoaXMucHJvcHMub3JkZXIgKyBncm91cERlbHRhIDwgMCkge1xuICAgICAgICByZXR1cm4gMCAtIHRoaXMucHJvcHMub3JkZXJcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiBncm91cERlbHRhXG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiAwXG4gICAgfVxuICB9XG5cbiAgcmVzaXplVGltZURlbHRhIChlKSB7XG4gICAgY29uc3QgbGVuZ3RoID0gdGhpcy5pdGVtVGltZUVuZCAtIHRoaXMuaXRlbVRpbWVTdGFydFxuICAgIGNvbnN0IHRpbWVEZWx0YSA9IHRoaXMuZHJhZ1RpbWVTbmFwKChlLnBhZ2VYIC0gdGhpcy5zdGF0ZS5yZXNpemVTdGFydCkgKiB0aGlzLmNvb3JkaW5hdGVUb1RpbWVSYXRpbygpKVxuXG4gICAgaWYgKGxlbmd0aCArIHRpbWVEZWx0YSA8ICh0aGlzLnByb3BzLmRyYWdTbmFwIHx8IDEwMDApKSB7XG4gICAgICByZXR1cm4gKHRoaXMucHJvcHMuZHJhZ1NuYXAgfHwgMTAwMCkgLSBsZW5ndGhcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIHRpbWVEZWx0YVxuICAgIH1cbiAgfVxuXG4gIGNvbXBvbmVudERpZE1vdW50ICgpIHtcbiAgICAgIHRoaXMuY2hlY2tDb250ZW50U2l6ZSgpO1xuICB9XG5cbiAgbW91bnRJbnRlcmFjdCAoKSB7XG4gICAgY29uc3QgcmlnaHRSZXNpemUgPSB0aGlzLnByb3BzLnVzZVJlc2l6ZUhhbmRsZSA/IHRoaXMucmVmcy5kcmFnUmlnaHQgOiB0cnVlXG4gICAgaW50ZXJhY3QodGhpcy5yZWZzLml0ZW0pXG4gICAgICAucmVzaXphYmxlKHtcbiAgICAgICAgZWRnZXM6IHtsZWZ0OiBmYWxzZSwgcmlnaHQ6IHJpZ2h0UmVzaXplLCB0b3A6IGZhbHNlLCBib3R0b206IGZhbHNlfSxcbiAgICAgICAgZW5hYmxlZDogdGhpcy5wcm9wcy5zZWxlY3RlZCAmJiB0aGlzLmNhblJlc2l6ZSgpXG4gICAgICB9KVxuICAgICAgLmRyYWdnYWJsZSh7XG4gICAgICAgIGVuYWJsZWQ6IHRoaXMucHJvcHMuc2VsZWN0ZWRcbiAgICAgIH0pXG4gICAgICAub24oJ2RyYWdzdGFydCcsIChlKSA9PiB7XG4gICAgICAgIGlmICh0aGlzLnByb3BzLnNlbGVjdGVkKSB7XG4gICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICBkcmFnZ2luZzogdHJ1ZSxcbiAgICAgICAgICAgIGRyYWdTdGFydDoge3g6IGUucGFnZVgsIHk6IGUucGFnZVl9LFxuICAgICAgICAgICAgcHJlRHJhZ1Bvc2l0aW9uOiB7eDogZS50YXJnZXQub2Zmc2V0TGVmdCwgeTogZS50YXJnZXQub2Zmc2V0VG9wfSxcbiAgICAgICAgICAgIGRyYWdUaW1lOiB0aGlzLml0ZW1UaW1lU3RhcnQsXG4gICAgICAgICAgICBkcmFnR3JvdXBEZWx0YTogMFxuICAgICAgICAgIH0pXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgcmV0dXJuIGZhbHNlXG4gICAgICAgIH1cbiAgICAgIH0pXG4gICAgICAub24oJ2RyYWdtb3ZlJywgKGUpID0+IHtcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuZHJhZ2dpbmcpIHtcbiAgICAgICAgICBsZXQgZHJhZ1RpbWUgPSB0aGlzLmRyYWdUaW1lKGUpXG4gICAgICAgICAgbGV0IGRyYWdHcm91cERlbHRhID0gdGhpcy5kcmFnR3JvdXBEZWx0YShlKVxuXG4gICAgICAgICAgaWYgKHRoaXMucHJvcHMubW92ZVJlc2l6ZVZhbGlkYXRvcikge1xuICAgICAgICAgICAgZHJhZ1RpbWUgPSB0aGlzLnByb3BzLm1vdmVSZXNpemVWYWxpZGF0b3IoJ21vdmUnLCB0aGlzLnByb3BzLml0ZW0sIGRyYWdUaW1lKVxuICAgICAgICAgIH1cblxuICAgICAgICAgIGlmICh0aGlzLnByb3BzLm9uRHJhZykge1xuICAgICAgICAgICAgdGhpcy5wcm9wcy5vbkRyYWcodGhpcy5pdGVtSWQsIGRyYWdUaW1lLCB0aGlzLnByb3BzLm9yZGVyICsgZHJhZ0dyb3VwRGVsdGEpXG4gICAgICAgICAgfVxuXG4gICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICBkcmFnVGltZTogZHJhZ1RpbWUsXG4gICAgICAgICAgICBkcmFnR3JvdXBEZWx0YTogZHJhZ0dyb3VwRGVsdGFcbiAgICAgICAgICB9KVxuICAgICAgICB9XG4gICAgICB9KVxuICAgICAgLm9uKCdkcmFnZW5kJywgKGUpID0+IHtcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuZHJhZ2dpbmcpIHtcbiAgICAgICAgICBpZiAodGhpcy5wcm9wcy5vbkRyb3ApIHtcbiAgICAgICAgICAgIGxldCBkcmFnVGltZSA9IHRoaXMuZHJhZ1RpbWUoZSlcblxuICAgICAgICAgICAgaWYgKHRoaXMucHJvcHMubW92ZVJlc2l6ZVZhbGlkYXRvcikge1xuICAgICAgICAgICAgICBkcmFnVGltZSA9IHRoaXMucHJvcHMubW92ZVJlc2l6ZVZhbGlkYXRvcignbW92ZScsIHRoaXMucHJvcHMuaXRlbSwgZHJhZ1RpbWUpXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHRoaXMucHJvcHMub25Ecm9wKHRoaXMuaXRlbUlkLCBkcmFnVGltZSwgdGhpcy5wcm9wcy5vcmRlciArIHRoaXMuZHJhZ0dyb3VwRGVsdGEoZSkpXG4gICAgICAgICAgfVxuXG4gICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICBkcmFnZ2luZzogZmFsc2UsXG4gICAgICAgICAgICBkcmFnU3RhcnQ6IG51bGwsXG4gICAgICAgICAgICBwcmVEcmFnUG9zaXRpb246IG51bGwsXG4gICAgICAgICAgICBkcmFnVGltZTogbnVsbCxcbiAgICAgICAgICAgIGRyYWdHcm91cERlbHRhOiBudWxsXG4gICAgICAgICAgfSlcbiAgICAgICAgfVxuICAgICAgfSlcbiAgICAgIC5vbigncmVzaXplc3RhcnQnLCAoZSkgPT4ge1xuICAgICAgICBpZiAodGhpcy5wcm9wcy5zZWxlY3RlZCkge1xuICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgcmVzaXppbmc6IHRydWUsXG4gICAgICAgICAgICByZXNpemVTdGFydDogZS5wYWdlWCxcbiAgICAgICAgICAgIG5ld1Jlc2l6ZUVuZDogMFxuICAgICAgICAgIH0pXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgcmV0dXJuIGZhbHNlXG4gICAgICAgIH1cbiAgICAgIH0pXG4gICAgICAub24oJ3Jlc2l6ZW1vdmUnLCAoZSkgPT4ge1xuICAgICAgICBpZiAodGhpcy5zdGF0ZS5yZXNpemluZykge1xuICAgICAgICAgIGxldCBuZXdSZXNpemVFbmQgPSB0aGlzLnJlc2l6ZVRpbWVTbmFwKHRoaXMuaXRlbVRpbWVFbmQgKyB0aGlzLnJlc2l6ZVRpbWVEZWx0YShlKSlcblxuICAgICAgICAgIGlmICh0aGlzLnByb3BzLm1vdmVSZXNpemVWYWxpZGF0b3IpIHtcbiAgICAgICAgICAgIG5ld1Jlc2l6ZUVuZCA9IHRoaXMucHJvcHMubW92ZVJlc2l6ZVZhbGlkYXRvcigncmVzaXplJywgdGhpcy5wcm9wcy5pdGVtLCBuZXdSZXNpemVFbmQpXG4gICAgICAgICAgfVxuXG4gICAgICAgICAgaWYgKHRoaXMucHJvcHMub25SZXNpemluZykge1xuICAgICAgICAgICAgdGhpcy5wcm9wcy5vblJlc2l6aW5nKHRoaXMuaXRlbUlkLCBuZXdSZXNpemVFbmQpXG4gICAgICAgICAgfVxuXG4gICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICBuZXdSZXNpemVFbmQ6IG5ld1Jlc2l6ZUVuZFxuICAgICAgICAgIH0pXG4gICAgICAgIH1cbiAgICAgIH0pXG4gICAgICAub24oJ3Jlc2l6ZWVuZCcsIChlKSA9PiB7XG4gICAgICAgIGlmICh0aGlzLnN0YXRlLnJlc2l6aW5nKSB7XG4gICAgICAgICAgbGV0IG5ld1Jlc2l6ZUVuZCA9IHRoaXMucmVzaXplVGltZVNuYXAodGhpcy5pdGVtVGltZUVuZCArIHRoaXMucmVzaXplVGltZURlbHRhKGUpKVxuXG4gICAgICAgICAgaWYgKHRoaXMucHJvcHMubW92ZVJlc2l6ZVZhbGlkYXRvcikge1xuICAgICAgICAgICAgbmV3UmVzaXplRW5kID0gdGhpcy5wcm9wcy5tb3ZlUmVzaXplVmFsaWRhdG9yKCdyZXNpemUnLCB0aGlzLnByb3BzLml0ZW0sIG5ld1Jlc2l6ZUVuZClcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBpZiAodGhpcy5wcm9wcy5vblJlc2l6ZWQgJiYgdGhpcy5yZXNpemVUaW1lRGVsdGEoZSkgIT09IDApIHtcbiAgICAgICAgICAgIHRoaXMucHJvcHMub25SZXNpemVkKHRoaXMuaXRlbUlkLCBuZXdSZXNpemVFbmQpXG4gICAgICAgICAgfVxuICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgcmVzaXppbmc6IG51bGwsXG4gICAgICAgICAgICByZXNpemVTdGFydDogbnVsbCxcbiAgICAgICAgICAgIG5ld1Jlc2l6ZUVuZDogbnVsbFxuICAgICAgICAgIH0pXG4gICAgICAgIH1cbiAgICAgIH0pXG4gICAgICAub24oJ3RhcCcsIChlKSA9PiB7XG4gICAgICAgIHRoaXMuYWN0dWFsQ2xpY2soZSwgZS5wb2ludGVyVHlwZSA9PT0gJ21vdXNlJyA/ICdjbGljaycgOiAndG91Y2gnKVxuICAgICAgfSlcblxuICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgaW50ZXJhY3RNb3VudGVkOiB0cnVlXG4gICAgfSlcbiAgfVxuXG4gIGNhblJlc2l6ZSAocHJvcHMgPSB0aGlzLnByb3BzKSB7XG4gICAgaWYgKCFwcm9wcy5jYW5SZXNpemUpIHtcbiAgICAgIHJldHVybiBmYWxzZVxuICAgIH1cbiAgICBsZXQgd2lkdGggPSBwYXJzZUludCh0aGlzLnByb3BzLmRpbWVuc2lvbnMud2lkdGgsIDEwKVxuICAgIHJldHVybiB3aWR0aCA+PSBwcm9wcy5taW5SZXNpemVXaWR0aFxuICB9XG5cbiAgY2FuTW92ZSAocHJvcHMgPSB0aGlzLnByb3BzKSB7XG4gICAgcmV0dXJuICEhcHJvcHMuY2FuTW92ZVxuICB9XG5cbiAgY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wcyAobmV4dFByb3BzKSB7XG4gICAgdGhpcy5jYWNoZURhdGFGcm9tUHJvcHMobmV4dFByb3BzKVxuXG4gICAgbGV0IHsgaW50ZXJhY3RNb3VudGVkIH0gPSB0aGlzLnN0YXRlXG4gICAgY29uc3QgY291bGREcmFnID0gdGhpcy5wcm9wcy5zZWxlY3RlZCAmJiB0aGlzLmNhbk1vdmUodGhpcy5wcm9wcylcbiAgICBjb25zdCBjb3VsZFJlc2l6ZSA9IHRoaXMucHJvcHMuc2VsZWN0ZWQgJiYgdGhpcy5jYW5SZXNpemUodGhpcy5wcm9wcylcbiAgICBjb25zdCB3aWxsQmVBYmxlVG9EcmFnID0gbmV4dFByb3BzLnNlbGVjdGVkICYmIHRoaXMuY2FuTW92ZShuZXh0UHJvcHMpXG4gICAgY29uc3Qgd2lsbEJlQWJsZVRvUmVzaXplID0gbmV4dFByb3BzLnNlbGVjdGVkICYmIHRoaXMuY2FuUmVzaXplKG5leHRQcm9wcylcblxuICAgIGlmIChuZXh0UHJvcHMuc2VsZWN0ZWQgJiYgIWludGVyYWN0TW91bnRlZCkge1xuICAgICAgdGhpcy5tb3VudEludGVyYWN0KClcbiAgICAgIGludGVyYWN0TW91bnRlZCA9IHRydWVcbiAgICB9XG5cbiAgICBpZiAoaW50ZXJhY3RNb3VudGVkICYmIGNvdWxkUmVzaXplICE9PSB3aWxsQmVBYmxlVG9SZXNpemUpIHtcbiAgICAgIGludGVyYWN0KHRoaXMucmVmcy5pdGVtKVxuICAgICAgICAucmVzaXphYmxlKHtlbmFibGVkOiB3aWxsQmVBYmxlVG9SZXNpemV9KVxuICAgIH1cbiAgICBpZiAoaW50ZXJhY3RNb3VudGVkICYmIGNvdWxkRHJhZyAhPT0gd2lsbEJlQWJsZVRvRHJhZykge1xuICAgICAgaW50ZXJhY3QodGhpcy5yZWZzLml0ZW0pXG4gICAgICAgIC5kcmFnZ2FibGUoe2VuYWJsZWQ6IHdpbGxCZUFibGVUb0RyYWd9KVxuICAgIH1cblxuICAgIGlmICghbmV4dFByb3BzLnNlbGVjdGVkICYmIHRoaXMucHJvcHMuc2VsZWN0ZWQpIHtcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7dG9vbHRpcDogZmFsc2V9KTtcbiAgICAgICAgdGhpcy5jaGVja0NvbnRlbnRTaXplKCk7XG4gICAgfVxuICB9XG5cbiAgb25Nb3VzZURvd24gPSAoZSkgPT4ge1xuICAgIGlmICghdGhpcy5zdGF0ZS5pbnRlcmFjdE1vdW50ZWQpIHtcbiAgICAgIGUucHJldmVudERlZmF1bHQoKVxuICAgICAgdGhpcy5zdGFydGVkQ2xpY2tpbmcgPSB0cnVlXG4gICAgfVxuICB9O1xuXG4gIG9uTW91c2VVcCA9IChlKSA9PiB7XG4gICAgaWYgKCF0aGlzLnN0YXRlLmludGVyYWN0TW91bnRlZCAmJiB0aGlzLnN0YXJ0ZWRDbGlja2luZykge1xuICAgICAgdGhpcy5zdGFydGVkQ2xpY2tpbmcgPSBmYWxzZVxuICAgICAgdGhpcy5hY3R1YWxDbGljayhlLCAnY2xpY2snKVxuICAgIH1cbiAgfTtcblxuICBvblRvdWNoU3RhcnQgPSAoZSkgPT4ge1xuICAgIGlmICghdGhpcy5zdGF0ZS5pbnRlcmFjdE1vdW50ZWQpIHtcbiAgICAgIGUucHJldmVudERlZmF1bHQoKVxuICAgICAgdGhpcy5zdGFydGVkVG91Y2hpbmcgPSB0cnVlXG4gICAgfVxuICB9O1xuXG4gIG9uVG91Y2hFbmQgPSAoZSkgPT4ge1xuICAgIGlmICghdGhpcy5zdGF0ZS5pbnRlcmFjdE1vdW50ZWQgJiYgdGhpcy5zdGFydGVkVG91Y2hpbmcpIHtcbiAgICAgIHRoaXMuc3RhcnRlZFRvdWNoaW5nID0gZmFsc2VcbiAgICAgIHRoaXMuYWN0dWFsQ2xpY2soZSwgJ3RvdWNoJylcbiAgICB9XG4gIH07XG5cbiAgaGFuZGxlRG91YmxlQ2xpY2sgPSAoZSkgPT4ge1xuICAgIGUucHJldmVudERlZmF1bHQoKVxuICAgIGUuc3RvcFByb3BhZ2F0aW9uKClcbiAgICBpZiAodGhpcy5wcm9wcy5vbkl0ZW1Eb3VibGVDbGljaykge1xuICAgICAgdGhpcy5wcm9wcy5vbkl0ZW1Eb3VibGVDbGljayh0aGlzLml0ZW1JZCwgZSlcbiAgICB9XG4gIH07XG5cbiAgaGFuZGxlQ29udGV4dE1lbnUgPSAoZSkgPT4ge1xuICAgIGlmICh0aGlzLnByb3BzLm9uQ29udGV4dE1lbnUpIHtcbiAgICAgIGUucHJldmVudERlZmF1bHQoKVxuICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKVxuICAgICAgdGhpcy5wcm9wcy5vbkNvbnRleHRNZW51KHRoaXMuaXRlbUlkLCBlKVxuICAgIH1cbiAgfTtcblxuICBhY3R1YWxDbGljayAoZSwgY2xpY2tUeXBlKSB7XG4gICAgaWYgKHRoaXMucHJvcHMub25TZWxlY3QpIHtcbiAgICAgIHRoaXMucHJvcHMub25TZWxlY3QodGhpcy5pdGVtSWQsIGNsaWNrVHlwZSwgZSlcbiAgICB9XG4gIH1cblxuICBoYW5kbGVDbGljayA9IChldikgPT4ge1xuICAgIGV2LnByZXZlbnREZWZhdWx0KCk7XG4gICAgZXYuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgdGhpcy5zZXRTdGF0ZSh7dG9vbHRpcDogIXRoaXMuc3RhdGUudG9vbHRpcH0pO1xuICB9O1xuXG4gIGRldGVybWluZVRvb2x0aXBQb3NpdGlvbiA9ICgpID0+IHtcbiAgICAgIGlmICghdGhpcy5yZWZzWydpdGVtJ10pIHtcbiAgICAgICAgICByZXR1cm4gJ3RvcCc7XG4gICAgICB9XG4gICAgICByZXR1cm4gdGhpcy5yZWZzWydpdGVtJ10ub2Zmc2V0VG9wID4gMTAwID8gJ3RvcCcgOiAnYm90dG9tJztcbiAgfTtcblxuICBjaGVja0NvbnRlbnRTaXplID0gKCkgPT4ge1xuICAgICAgY29uc3QgZWwgPSB0aGlzLnJlZnNbJ2NvbnRlbnQnXTtcbiAgICAgIGlmICghZWwpIHtcbiAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG4gICAgICBpZiAodGhpcy5wcm9wcy5oaWRlQ2xpcHBlZFRpdGxlKSB7XG4gICAgICAgICAgY29uc3QgdGV4dElzQ2xpcHBlZCA9IGVsLnNjcm9sbFdpZHRoID4gZWwuY2xpZW50V2lkdGg7XG4gICAgICAgICAgaWYgKHRleHRJc0NsaXBwZWQgJiYgIXRoaXMuc3RhdGUuY29udGVudElzSGlkZGVuKSB7XG4gICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2NvbnRlbnRJc0hpZGRlbjogdHJ1ZX0pO1xuICAgICAgICAgIH0gZWxzZSBpZiAoIXRleHRJc0NsaXBwZWQgJiYgdGhpcy5zdGF0ZS5jb250ZW50SXNIaWRkZW4pIHtcbiAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7Y29udGVudElzSGlkZGVuOiBmYWxzZX0pO1xuICAgICAgICAgIH1cbiAgICAgIH1cbiAgfTtcblxuICByZW5kZXIgKCkge1xuICAgIGNvbnN0IGRpbWVuc2lvbnMgPSB0aGlzLnByb3BzLmRpbWVuc2lvbnM7XG4gICAgaWYgKHR5cGVvZiB0aGlzLnByb3BzLm9yZGVyID09PSAndW5kZWZpbmVkJyB8fCB0aGlzLnByb3BzLm9yZGVyID09PSBudWxsKSB7XG4gICAgICByZXR1cm4gbnVsbFxuICAgIH1cblxuICAgIGNvbnN0IGNsYXNzTmFtZXMgPSAncmN0LWl0ZW0nICtcbiAgICAgICAgICAgICAgICAgICAgICAgKHRoaXMucHJvcHMuc2VsZWN0ZWQgPyAnIHNlbGVjdGVkJyA6ICcnKSArXG4gICAgICAgICAgICAgICAgICAgICAgICh0aGlzLmNhbk1vdmUodGhpcy5wcm9wcykgPyAnIGNhbi1tb3ZlJyA6ICcnKSArXG4gICAgICAgICAgICAgICAgICAgICAgICh0aGlzLmNhblJlc2l6ZSh0aGlzLnByb3BzKSA/ICcgY2FuLXJlc2l6ZScgOiAnJykgK1xuICAgICAgICAgICAgICAgICAgICAgICAodGhpcy5wcm9wcy5pdGVtLmNsYXNzTmFtZSA/IGAgJHt0aGlzLnByb3BzLml0ZW0uY2xhc3NOYW1lfWAgOiAnJyk7XG5cbiAgICBjb25zdCBzdHlsZSA9IHtcbiAgICAgIGxlZnQ6IGAke2RpbWVuc2lvbnMubGVmdH1weGAsXG4gICAgICB0b3A6IGAke2RpbWVuc2lvbnMudG9wfXB4YCxcbiAgICAgIHdpZHRoOiBgJHtkaW1lbnNpb25zLndpZHRofXB4YCxcbiAgICAgIGhlaWdodDogYCR7ZGltZW5zaW9ucy5oZWlnaHR9cHhgLFxuICAgICAgLy8gbGluZUhlaWdodDogYCR7ZGltZW5zaW9ucy5oZWlnaHR9cHhgXG4gICAgfTtcbiAgICBpZiAodGhpcy5wcm9wcy5pdGVtLmNvbG9yKSB7XG4gICAgICAgIHN0eWxlWydiYWNrZ3JvdW5kQ29sb3InXSA9IHRoaXMucHJvcHMuaXRlbS5jb2xvcjtcbiAgICB9XG5cbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBrZXk9e3RoaXMuaXRlbUlkfVxuICAgICAgICAgICByZWY9J2l0ZW0nXG4gICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3NOYW1lc31cbiAgICAgICAgICAgdGl0bGU9e3RoaXMuaXRlbURpdlRpdGxlfVxuICAgICAgICAgICBvbk1vdXNlRG93bj17dGhpcy5vbk1vdXNlRG93bn1cbiAgICAgICAgICAgb25Nb3VzZVVwPXt0aGlzLm9uTW91c2VVcH1cbiAgICAgICAgICAgb25Ub3VjaFN0YXJ0PXt0aGlzLm9uVG91Y2hTdGFydH1cbiAgICAgICAgICAgb25Ub3VjaEVuZD17dGhpcy5vblRvdWNoRW5kfVxuICAgICAgICAgICBvbkRvdWJsZUNsaWNrPXt0aGlzLmhhbmRsZURvdWJsZUNsaWNrfVxuICAgICAgICAgICBvbkNvbnRleHRNZW51PXt0aGlzLmhhbmRsZUNvbnRleHRNZW51fVxuICAgICAgICAgICBzdHlsZT17c3R5bGV9PlxuICAgICAgICA8ZGl2XG4gICAgICAgICAgICB0aXRsZT17dGhpcy5pdGVtRGl2VGl0bGV9XG4gICAgICAgICAgICBjbGFzc05hbWU9e2BcbiAgICAgICAgICAgICAgICByY3QtaXRlbS10b29sdGlwXG4gICAgICAgICAgICAgICAgJHt0aGlzLnN0YXRlLnRvb2x0aXAgPyAnYWN0aXZlJyA6ICcnfVxuICAgICAgICAgICAgICAgICR7dGhpcy5kZXRlcm1pbmVUb29sdGlwUG9zaXRpb24oKX1cbiAgICAgICAgICAgIGB9XG4gICAgICAgICAgICBzdHlsZT17e1xuICAgICAgICAgICAgICAgIGhlaWdodDogYCR7ZGltZW5zaW9ucy5oZWlnaHR9cHhgLFxuICAgICAgICAgICAgfX1cbiAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMuaGFuZGxlQ2xpY2t9XG4gICAgICAgID5ePC9kaXY+XG4gICAgICAgIDxzdHlsZSBkYW5nZXJvdXNseVNldElubmVySFRNTD17e1xuICAgICAgICAgICAgX19odG1sOiBgXG4gICAgICAgICAgICAgICAgLnJjdC1pdGVtLXRvb2x0aXAudG9wOmJlZm9yZSB7XG4gICAgICAgICAgICAgICAgICAgIGJvdHRvbTogJHtkaW1lbnNpb25zLmhlaWdodCAtIDF9cHg7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC5yY3QtaXRlbS10b29sdGlwLnRvcDphZnRlciB7XG4gICAgICAgICAgICAgICAgICAgIGJvdHRvbTogJHtkaW1lbnNpb25zLmhlaWdodCArIDV9cHg7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgLnJjdC1pdGVtLXRvb2x0aXAuYm90dG9tOmFmdGVyIHtcbiAgICAgICAgICAgICAgICAgICAgdG9wOiAke2RpbWVuc2lvbnMuaGVpZ2h0ICsgNX1weDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBgXG4gICAgICAgIH19IC8+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPSdyY3QtaXRlbS1vdmVyZmxvdyc+XG4gICAgICAgICAgPGRpdiByZWY9J2NvbnRlbnQnIGNsYXNzTmFtZT0ncmN0LWl0ZW0tY29udGVudCcgc3R5bGU9e3tcbiAgICAgICAgICAgIHZpc2liaWxpdHk6IHRoaXMuc3RhdGUuY29udGVudElzSGlkZGVuID8gJ2hpZGRlbicgOiAndmlzaWJsZSdcbiAgICAgICAgICB9fT5cbiAgICAgICAgICAgIHt0aGlzLml0ZW1UaXRsZUhUTUwgPyAoXG4gICAgICAgICAgICAgICAgPGRpdiBkYW5nZXJvdXNseVNldElubmVySFRNTD17e19faHRtbDogdGhpcy5pdGVtVGl0bGVIVE1MfX0gLz5cbiAgICAgICAgICAgICkgOiAoXG4gICAgICAgICAgICAgICAgdGhpcy5pdGVtVGl0bGVcbiAgICAgICAgICAgICl9XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICB7IHRoaXMucHJvcHMudXNlUmVzaXplSGFuZGxlID8gPGRpdiByZWY9J2RyYWdSaWdodCcgY2xhc3NOYW1lPSdyY3QtZHJhZy1yaWdodCc+PC9kaXY+IDogJycgfVxuICAgICAgPC9kaXY+XG4gICAgKVxuICB9XG59XG4iXX0=

/***/ },
/* 10 */
/*!***********************************************************************************************************!*\
  !*** external {"root":"interact","commonjs":"interact.js","commonjs2":"interact.js","amd":"interact.js"} ***!
  \***********************************************************************************************************/
/***/ function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_10__;

/***/ },
/* 11 */
/*!**************************!*\
  !*** ./src/lib/utils.ts ***!
  \**************************/
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var moment = __webpack_require__(/*! moment */ 3);
	var EPSILON = 0.001;
	// so we could use both immutable.js objects and regular objects
	function _get(object, key) {
	    return typeof object.get === 'function' ? object.get(key) : object[key];
	}
	exports._get = _get;
	function _length(object) {
	    return typeof object.count === 'function' ? object.count() : object.length;
	}
	exports._length = _length;
	function arraysEqual(array1, array2) {
	    return (_length(array1) === _length(array2)) && array1.every(function (element, index) {
	        return element === _get(array2, index);
	    });
	}
	exports.arraysEqual = arraysEqual;
	function iterateTimes(start, end, unit, timeSteps, callback) {
	    var time = moment(start).startOf(unit);
	    if (timeSteps[unit] && timeSteps[unit] > 1) {
	        var value = time.get(unit);
	        time.set(unit, value - (value % timeSteps[unit]));
	    }
	    while (time.valueOf() < end) {
	        var unitStr = (unit + "s");
	        var nextTime = moment(time).add(timeSteps[unit] || 1, unitStr);
	        callback(time, nextTime);
	        time = nextTime;
	    }
	}
	exports.iterateTimes = iterateTimes;
	function getMinUnit(zoom, width, timeSteps) {
	    var timeDividers = {
	        second: 1000,
	        minute: 60,
	        hour: 60,
	        day: 24,
	        month: 30,
	        year: 12
	    };
	    var minUnit = 'year';
	    var breakCount = zoom;
	    var minCellWidth = 17;
	    Object.keys(timeDividers).some(function (unit) {
	        breakCount = breakCount / timeDividers[unit];
	        var cellCount = breakCount / timeSteps[unit];
	        var countNeeded = width / (timeSteps[unit] && timeSteps[unit] > 1 ? 3 * minCellWidth : minCellWidth);
	        if (cellCount < countNeeded) {
	            minUnit = unit;
	            return true;
	        }
	    });
	    return minUnit;
	}
	exports.getMinUnit = getMinUnit;
	function getNextUnit(unit) {
	    var nextUnits = {
	        second: 'minute',
	        minute: 'hour',
	        hour: 'day',
	        day: 'month',
	        month: 'year'
	    };
	    return nextUnits[unit] || '';
	}
	exports.getNextUnit = getNextUnit;
	function getParentPosition(element) {
	    var xPosition = 0;
	    var yPosition = 0;
	    var first = true;
	    while (element) {
	        xPosition += (element.offsetLeft - (first ? 0 : element.scrollLeft) + element.clientLeft);
	        yPosition += (element.offsetTop - (first ? 0 : element.scrollTop) + element.clientTop);
	        element = element.offsetParent;
	        first = false;
	    }
	    return { x: xPosition, y: yPosition };
	}
	exports.getParentPosition = getParentPosition;
	function coordinateToTimeRatio(canvasTimeStart, canvasTimeEnd, canvasWidth) {
	    return (canvasTimeEnd - canvasTimeStart) / canvasWidth;
	}
	exports.coordinateToTimeRatio = coordinateToTimeRatio;
	function calculateDimensions(item, order, keys, canvasTimeStart, canvasTimeEnd, canvasWidth, dragSnap, lineHeight, draggingItem, dragTime, resizingItem, resizeEnd, newGroupOrder, itemHeightRatio) {
	    var itemId = _get(item, keys.itemIdKey);
	    var itemTimeStart = _get(item, keys.itemTimeStartKey);
	    var itemTimeEnd = _get(item, keys.itemTimeEndKey);
	    var isDragging = itemId === draggingItem;
	    var isResizing = itemId === resizingItem;
	    var x = isDragging ? dragTime : itemTimeStart;
	    var w = Math.max((isResizing ? resizeEnd : itemTimeEnd) - itemTimeStart, dragSnap);
	    var collisionX = itemTimeStart;
	    var collisionW = w;
	    if (isDragging) {
	        if (itemTimeStart >= dragTime) {
	            collisionX = dragTime;
	            collisionW = Math.max(itemTimeEnd - dragTime, dragSnap);
	        }
	        else {
	            collisionW = Math.max(dragTime - itemTimeStart + w, dragSnap);
	        }
	    }
	    var h = lineHeight * itemHeightRatio;
	    var ratio = 1 / coordinateToTimeRatio(canvasTimeStart, canvasTimeEnd, canvasWidth);
	    return {
	        left: (x - canvasTimeStart) * ratio,
	        top: null,
	        width: Math.max(w * ratio, 3),
	        height: h,
	        order: isDragging ? newGroupOrder : order,
	        stack: true,
	        lineHeight: lineHeight,
	        collisionLeft: collisionX,
	        originalLeft: itemTimeStart,
	        collisionWidth: collisionW,
	        isDragging: isDragging
	    };
	}
	exports.calculateDimensions = calculateDimensions;
	function getGroupOrders(groups, keys) {
	    var groupIdKey = keys.groupIdKey;
	    var groupOrders = {};
	    for (var i = 0; i < groups.length; i++) {
	        groupOrders[_get(groups[i], groupIdKey)] = i;
	    }
	    return groupOrders;
	}
	exports.getGroupOrders = getGroupOrders;
	function getVisibleItems(items, canvasTimeStart, canvasTimeEnd, keys) {
	    var itemTimeStartKey = keys.itemTimeStartKey, itemTimeEndKey = keys.itemTimeEndKey;
	    return items.filter(function (item) {
	        return _get(item, itemTimeStartKey) <= canvasTimeEnd && _get(item, itemTimeEndKey) >= canvasTimeStart;
	    });
	}
	exports.getVisibleItems = getVisibleItems;
	function collision(a, b, lineHeight) {
	    // var verticalMargin = (lineHeight - a.height)/2;
	    var verticalMargin = 0;
	    return ((a.collisionLeft + EPSILON) < (b.collisionLeft + b.collisionWidth) &&
	        (a.collisionLeft + a.collisionWidth - EPSILON) > b.collisionLeft &&
	        (a.top - verticalMargin + EPSILON) < (b.top + b.height) &&
	        (a.top + a.height + verticalMargin - EPSILON) > b.top);
	}
	exports.collision = collision;
	function stack(items, groupOrders, lineHeight, headerHeight, force) {
	    var i, iMax;
	    var totalHeight = headerHeight;
	    var groupHeights = {};
	    var groupTops = {};
	    var groupedItems = groupBy(items, function (item) {
	        return item.dimensions.order;
	    });
	    if (force) {
	        // reset top position of all items
	        for (i = 0, iMax = items.length; i < iMax; i++) {
	            items[i].dimensions.top = null;
	        }
	    }
	    for (var _i = 0, _a = Object.keys(groupOrders); _i < _a.length; _i++) {
	        var url = _a[_i];
	        var key = groupOrders[url];
	        // calculate new, non-overlapping positions
	        var group = groupedItems[key] || [];
	        groupTops[key] = totalHeight;
	        var groupHeight = 0;
	        var verticalMargin = 0;
	        for (i = 0, iMax = group.length; i < iMax; i++) {
	            var item = group[i];
	            verticalMargin = (item.dimensions.lineHeight - item.dimensions.height);
	            if (item.dimensions.stack && item.dimensions.top === null) {
	                item.dimensions.top = totalHeight + verticalMargin;
	                groupHeight = Math.max(groupHeight, item.dimensions.lineHeight);
	                do {
	                    var collidingItem = null;
	                    for (var j = 0, jj = group.length; j < jj; j++) {
	                        var other = group[j];
	                        if (other.top !== null && other !== item && other.dimensions.stack && collision(item.dimensions, other.dimensions, item.dimensions.lineHeight)) {
	                            collidingItem = other;
	                            break;
	                        }
	                        else {
	                        }
	                    }
	                    if (collidingItem != null) {
	                        // There is a collision. Reposition the items above the colliding element
	                        item.dimensions.top = collidingItem.dimensions.top + collidingItem.dimensions.lineHeight;
	                        groupHeight = Math.max(groupHeight, item.dimensions.top + item.dimensions.height - totalHeight);
	                    }
	                } while (collidingItem);
	            }
	        }
	        groupHeights[key] = Math.max(groupHeight + verticalMargin, lineHeight);
	        totalHeight += Math.max(groupHeight + verticalMargin, lineHeight);
	    }
	    return {
	        height: totalHeight,
	        groupHeights: groupHeights,
	        groupTops: groupTops
	    };
	}
	exports.stack = stack;
	function nostack(items, groupOrders, lineHeight, headerHeight, force) {
	    var i, iMax;
	    var totalHeight = headerHeight;
	    var groupHeights = {};
	    var groupTops = {};
	    var groupedItems = groupBy(items, function (item) {
	        return item.dimensions.order;
	    });
	    if (force) {
	        // reset top position of all items
	        for (i = 0, iMax = items.length; i < iMax; i++) {
	            items[i].dimensions.top = null;
	        }
	    }
	    for (var _i = 0, _a = Object.keys(groupOrders); _i < _a.length; _i++) {
	        var url = _a[_i];
	        var key = groupOrders[url];
	        // calculate new, non-overlapping positions
	        var group = groupedItems[key] || [];
	        groupTops[key] = totalHeight;
	        var groupHeight = 0;
	        for (i = 0, iMax = group.length; i < iMax; i++) {
	            var item = group[i];
	            var verticalMargin = (item.dimensions.lineHeight - item.dimensions.height) / 2;
	            if (item.dimensions.top === null) {
	                item.dimensions.top = totalHeight + verticalMargin;
	                groupHeight = Math.max(groupHeight, item.dimensions.lineHeight);
	            }
	        }
	        groupHeights[key] = Math.max(groupHeight, lineHeight);
	        totalHeight += Math.max(groupHeight, lineHeight);
	    }
	    return {
	        height: totalHeight,
	        groupHeights: groupHeights,
	        groupTops: groupTops
	    };
	}
	exports.nostack = nostack;
	function keyBy(value, key) {
	    var obj = {};
	    value.forEach(function (element, index, array) {
	        obj[element[key]] = element;
	    });
	    return obj;
	}
	exports.keyBy = keyBy;
	function groupBy(collection, groupFunction) {
	    var obj = {};
	    collection.forEach(function (element, index, array) {
	        var key = groupFunction(element);
	        if (!obj[key]) {
	            obj[key] = [];
	        }
	        obj[key].push(element);
	    });
	    return obj;
	}
	exports.groupBy = groupBy;
	function hasSomeParentTheClass(element, classname) {
	    if (element.className && element.className.split(' ').indexOf(classname) >= 0)
	        return true;
	    return element.parentNode && hasSomeParentTheClass(element.parentNode, classname);
	}
	exports.hasSomeParentTheClass = hasSomeParentTheClass;
	function createGradientPattern(lineHeight, color1, color2, borderColor) {
	    if (borderColor) {
	        if (!color2 || color1 === color2) {
	            return 'repeating-linear-gradient(to bottom, ' +
	                (color1 + ",") +
	                (color1 + " " + (lineHeight - 1) + "px,") +
	                (borderColor + " " + (lineHeight - 1) + "px,") +
	                (borderColor + " " + lineHeight + "px") +
	                ')';
	        }
	        else {
	            return 'repeating-linear-gradient(to bottom, ' +
	                (color1 + ",") +
	                (color1 + " " + (lineHeight - 1) + "px,") +
	                (borderColor + " " + (lineHeight - 1) + "px,") +
	                (borderColor + " " + lineHeight + "px,") +
	                (color2 + " " + lineHeight + "px,") +
	                (color2 + " " + (lineHeight * 2 - 1) + "px,") +
	                (borderColor + " " + (lineHeight * 2 - 1) + "px,") +
	                (borderColor + " " + lineHeight * 2 + "px") +
	                ')';
	        }
	    }
	    else {
	        if (!color2 || color1 === color2) {
	            return color1;
	        }
	        else {
	            return "repeating-linear-gradient(to bottom," + color1 + "," + color1 + " " + lineHeight + "px," + color2 + " " + lineHeight + "px," + color2 + " " + lineHeight * 2 + "px)";
	        }
	    }
	}
	exports.createGradientPattern = createGradientPattern;
	//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXRpbHMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ1dGlscy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsSUFBWSxNQUFNLFdBQU0sUUFFeEIsQ0FBQyxDQUYrQjtBQUVoQyxJQUFNLE9BQU8sR0FBRyxLQUFLLENBQUM7QUFFdEIsZ0VBQWdFO0FBQ2hFLGNBQXNCLE1BQU0sRUFBRSxHQUFHO0lBQy9CLE1BQU0sQ0FBQyxPQUFPLE1BQU0sQ0FBQyxHQUFHLEtBQUssVUFBVSxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFBO0FBQ3pFLENBQUM7QUFGZSxZQUFJLE9BRW5CLENBQUE7QUFFRCxpQkFBeUIsTUFBTTtJQUM3QixNQUFNLENBQUMsT0FBTyxNQUFNLENBQUMsS0FBSyxLQUFLLFVBQVUsR0FBRyxNQUFNLENBQUMsS0FBSyxFQUFFLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQTtBQUM1RSxDQUFDO0FBRmUsZUFBTyxVQUV0QixDQUFBO0FBRUQscUJBQTZCLE1BQU0sRUFBRSxNQUFNO0lBQ3pDLE1BQU0sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxNQUFNLENBQUMsS0FBSyxDQUFDLFVBQUMsT0FBTyxFQUFFLEtBQUs7UUFDMUUsTUFBTSxDQUFDLE9BQU8sS0FBSyxJQUFJLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxDQUFBO0lBQ3hDLENBQUMsQ0FBQyxDQUFBO0FBQ0osQ0FBQztBQUplLG1CQUFXLGNBSTFCLENBQUE7QUFFRCxzQkFBOEIsS0FBSyxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLFFBQVE7SUFDakUsSUFBSSxJQUFJLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUV2QyxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksU0FBUyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDM0MsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMzQixJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxLQUFLLEdBQUcsQ0FBQyxLQUFLLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQTtJQUNuRCxDQUFDO0lBRUQsT0FBTyxJQUFJLENBQUMsT0FBTyxFQUFFLEdBQUcsR0FBRyxFQUFFLENBQUM7UUFDMUIsSUFBTSxPQUFPLEdBQUcsQ0FBRyxJQUFJLE9BQXdCLENBQUM7UUFDbEQsSUFBSSxRQUFRLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQy9ELFFBQVEsQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDekIsSUFBSSxHQUFHLFFBQVEsQ0FBQTtJQUNqQixDQUFDO0FBQ0gsQ0FBQztBQWRlLG9CQUFZLGVBYzNCLENBQUE7QUFFRCxvQkFBNEIsSUFBSSxFQUFFLEtBQUssRUFBRSxTQUFTO0lBQ2hELElBQUksWUFBWSxHQUFHO1FBQ2pCLE1BQU0sRUFBRSxJQUFJO1FBQ1osTUFBTSxFQUFFLEVBQUU7UUFDVixJQUFJLEVBQUUsRUFBRTtRQUNSLEdBQUcsRUFBRSxFQUFFO1FBQ1AsS0FBSyxFQUFFLEVBQUU7UUFDVCxJQUFJLEVBQUUsRUFBRTtLQUNULENBQUM7SUFFRixJQUFJLE9BQU8sR0FBRyxNQUFNLENBQUM7SUFDckIsSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDO0lBQ3RCLElBQU0sWUFBWSxHQUFHLEVBQUUsQ0FBQztJQUV4QixNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLElBQUk7UUFDakMsVUFBVSxHQUFHLFVBQVUsR0FBRyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDN0MsSUFBTSxTQUFTLEdBQUcsVUFBVSxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMvQyxJQUFNLFdBQVcsR0FBRyxLQUFLLEdBQUcsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksU0FBUyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsWUFBWSxHQUFHLFlBQVksQ0FBQyxDQUFDO1FBRXZHLEVBQUUsQ0FBQyxDQUFDLFNBQVMsR0FBRyxXQUFXLENBQUMsQ0FBQyxDQUFDO1lBQzVCLE9BQU8sR0FBRyxJQUFJLENBQUM7WUFDZixNQUFNLENBQUMsSUFBSSxDQUFBO1FBQ2IsQ0FBQztJQUNILENBQUMsQ0FBQyxDQUFDO0lBRUgsTUFBTSxDQUFDLE9BQU8sQ0FBQTtBQUNoQixDQUFDO0FBMUJlLGtCQUFVLGFBMEJ6QixDQUFBO0FBRUQscUJBQTZCLElBQUk7SUFDL0IsSUFBSSxTQUFTLEdBQUc7UUFDZCxNQUFNLEVBQUUsUUFBUTtRQUNoQixNQUFNLEVBQUUsTUFBTTtRQUNkLElBQUksRUFBRSxLQUFLO1FBQ1gsR0FBRyxFQUFFLE9BQU87UUFDWixLQUFLLEVBQUUsTUFBTTtLQUNkLENBQUM7SUFFRixNQUFNLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQTtBQUM5QixDQUFDO0FBVmUsbUJBQVcsY0FVMUIsQ0FBQTtBQUVELDJCQUFtQyxPQUFPO0lBQ3hDLElBQUksU0FBUyxHQUFHLENBQUMsQ0FBQztJQUNsQixJQUFJLFNBQVMsR0FBRyxDQUFDLENBQUM7SUFDbEIsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDO0lBRWpCLE9BQU8sT0FBTyxFQUFFLENBQUM7UUFDZixTQUFTLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxHQUFHLENBQUMsS0FBSyxHQUFHLENBQUMsR0FBRyxPQUFPLENBQUMsVUFBVSxDQUFDLEdBQUcsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQzFGLFNBQVMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEdBQUcsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxTQUFTLENBQUMsR0FBRyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDdkYsT0FBTyxHQUFHLE9BQU8sQ0FBQyxZQUFZLENBQUM7UUFDL0IsS0FBSyxHQUFHLEtBQUssQ0FBQTtJQUNmLENBQUM7SUFDRCxNQUFNLENBQUMsRUFBRSxDQUFDLEVBQUUsU0FBUyxFQUFFLENBQUMsRUFBRSxTQUFTLEVBQUUsQ0FBQTtBQUN2QyxDQUFDO0FBWmUseUJBQWlCLG9CQVloQyxDQUFBO0FBRUQsK0JBQXVDLGVBQWUsRUFBRSxhQUFhLEVBQUUsV0FBVztJQUNoRixNQUFNLENBQUMsQ0FBQyxhQUFhLEdBQUcsZUFBZSxDQUFDLEdBQUcsV0FBVyxDQUFBO0FBQ3hELENBQUM7QUFGZSw2QkFBcUIsd0JBRXBDLENBQUE7QUFFRCw2QkFBcUMsSUFBSSxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsZUFBZSxFQUFFLGFBQWEsRUFBRSxXQUFXLEVBQUUsUUFBUSxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxTQUFTLEVBQUUsYUFBYSxFQUFFLGVBQWU7SUFDeE0sSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDeEMsSUFBSSxhQUFhLEdBQUcsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztJQUN0RCxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUVsRCxJQUFJLFVBQVUsR0FBRyxNQUFNLEtBQUssWUFBWSxDQUFDO0lBQ3pDLElBQUksVUFBVSxHQUFHLE1BQU0sS0FBSyxZQUFZLENBQUM7SUFFekMsSUFBTSxDQUFDLEdBQUcsVUFBVSxHQUFHLFFBQVEsR0FBRyxhQUFhLENBQUM7SUFFaEQsSUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFVBQVUsR0FBRyxTQUFTLEdBQUcsV0FBVyxDQUFDLEdBQUcsYUFBYSxFQUFFLFFBQVEsQ0FBQyxDQUFDO0lBQ3JGLElBQUksVUFBVSxHQUFHLGFBQWEsQ0FBQztJQUMvQixJQUFJLFVBQVUsR0FBRyxDQUFDLENBQUM7SUFFbkIsRUFBRSxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztRQUNmLEVBQUUsQ0FBQyxDQUFDLGFBQWEsSUFBSSxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQzlCLFVBQVUsR0FBRyxRQUFRLENBQUM7WUFDdEIsVUFBVSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsV0FBVyxHQUFHLFFBQVEsRUFBRSxRQUFRLENBQUMsQ0FBQTtRQUN6RCxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixVQUFVLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLEdBQUcsYUFBYSxHQUFHLENBQUMsRUFBRSxRQUFRLENBQUMsQ0FBQTtRQUMvRCxDQUFDO0lBQ0gsQ0FBQztJQUVELElBQU0sQ0FBQyxHQUFHLFVBQVUsR0FBRyxlQUFlLENBQUM7SUFDdkMsSUFBTSxLQUFLLEdBQUcsQ0FBQyxHQUFHLHFCQUFxQixDQUFDLGVBQWUsRUFBRSxhQUFhLEVBQUUsV0FBVyxDQUFDLENBQUM7SUFFckYsTUFBTSxDQUFDO1FBQ0wsSUFBSSxFQUFFLENBQUMsQ0FBQyxHQUFHLGVBQWUsQ0FBQyxHQUFHLEtBQUs7UUFDbkMsR0FBRyxFQUFFLElBQUk7UUFDVCxLQUFLLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsS0FBSyxFQUFFLENBQUMsQ0FBQztRQUM3QixNQUFNLEVBQUUsQ0FBQztRQUNULEtBQUssRUFBRSxVQUFVLEdBQUcsYUFBYSxHQUFHLEtBQUs7UUFDekMsS0FBSyxFQUFFLElBQUk7UUFDWCxVQUFVLEVBQUUsVUFBVTtRQUN0QixhQUFhLEVBQUUsVUFBVTtRQUN6QixZQUFZLEVBQUUsYUFBYTtRQUMzQixjQUFjLEVBQUUsVUFBVTtRQUMxQixVQUFVLEVBQUUsVUFBVTtLQUN2QixDQUFDO0FBRUosQ0FBQztBQXhDZSwyQkFBbUIsc0JBd0NsQyxDQUFBO0FBRUQsd0JBQWdDLE1BQU0sRUFBRSxJQUFJO0lBQ2xDLGdDQUFVLENBQVU7SUFFNUIsSUFBSSxXQUFXLEdBQUcsRUFBRSxDQUFDO0lBRXJCLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1FBQ3ZDLFdBQVcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLFVBQVUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFBO0lBQzlDLENBQUM7SUFFRCxNQUFNLENBQUMsV0FBVyxDQUFBO0FBQ3BCLENBQUM7QUFWZSxzQkFBYyxpQkFVN0IsQ0FBQTtBQUVELHlCQUFpQyxLQUFLLEVBQUUsZUFBZSxFQUFFLGFBQWEsRUFBRSxJQUFJO0lBQ2xFLDRDQUFnQixFQUFFLG9DQUFjLENBQVU7SUFFbEQsTUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsVUFBQSxJQUFJO1FBQ3RCLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLGdCQUFnQixDQUFDLElBQUksYUFBYSxJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUUsY0FBYyxDQUFDLElBQUksZUFBZSxDQUFBO0lBQ3ZHLENBQUMsQ0FBQyxDQUFBO0FBQ0osQ0FBQztBQU5lLHVCQUFlLGtCQU05QixDQUFBO0FBRUQsbUJBQTJCLENBQUMsRUFBRSxDQUFDLEVBQUUsVUFBVTtJQUN6QyxrREFBa0Q7SUFDbEQsSUFBSSxjQUFjLEdBQUcsQ0FBQyxDQUFDO0lBQ3ZCLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDLGNBQWMsQ0FBQztRQUMxRSxDQUFDLENBQUMsQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDLGNBQWMsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsYUFBYTtRQUNoRSxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsY0FBYyxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDO1FBQ3ZELENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsTUFBTSxHQUFHLGNBQWMsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUE7QUFDeEQsQ0FBQztBQVBlLGlCQUFTLFlBT3hCLENBQUE7QUFFRCxlQUF1QixLQUFLLEVBQUUsV0FBVyxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsS0FBSztJQUN4RSxJQUFJLENBQUMsRUFBRSxJQUFJLENBQUM7SUFFWixJQUFJLFdBQVcsR0FBRyxZQUFZLENBQUM7SUFFL0IsSUFBSSxZQUFZLEdBQUcsRUFBRSxDQUFDO0lBQ3RCLElBQUksU0FBUyxHQUFHLEVBQUUsQ0FBQztJQUVuQixJQUFJLFlBQVksR0FBRyxPQUFPLENBQUMsS0FBSyxFQUFFLFVBQVUsSUFBSTtRQUM5QyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUE7SUFDOUIsQ0FBQyxDQUFDLENBQUM7SUFFSCxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1FBQ1Ysa0NBQWtDO1FBQ2xDLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEVBQUUsSUFBSSxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxHQUFHLElBQUksRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBQy9DLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQTtRQUNoQyxDQUFDO0lBQ0gsQ0FBQztJQUVELEdBQUcsQ0FBQyxDQUFZLFVBQXdCLEVBQXhCLEtBQUEsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsRUFBeEIsY0FBd0IsRUFBeEIsSUFBd0IsQ0FBQztRQUFwQyxJQUFJLEdBQUcsU0FBQTtRQUNWLElBQUksR0FBRyxHQUFHLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUMzQiwyQ0FBMkM7UUFDM0MsSUFBSSxLQUFLLEdBQUcsWUFBWSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUVwQyxTQUFTLENBQUMsR0FBRyxDQUFDLEdBQUcsV0FBVyxDQUFDO1FBRTdCLElBQUksV0FBVyxHQUFHLENBQUMsQ0FBQztRQUNwQixJQUFJLGNBQWMsR0FBRyxDQUFDLENBQUM7UUFDdkIsR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxJQUFJLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEdBQUcsSUFBSSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7WUFDL0MsSUFBSSxJQUFJLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3BCLGNBQWMsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7WUFFdkUsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDMUQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLEdBQUcsV0FBVyxHQUFHLGNBQWMsQ0FBQztnQkFDbkQsV0FBVyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQ2hFLEdBQUcsQ0FBQztvQkFDRixJQUFJLGFBQWEsR0FBRyxJQUFJLENBQUM7b0JBQ3pCLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxFQUFFLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7d0JBQy9DLElBQUksS0FBSyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDckIsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsS0FBSyxJQUFJLElBQUksS0FBSyxLQUFLLElBQUksSUFBSSxLQUFLLENBQUMsVUFBVSxDQUFDLEtBQUssSUFBSSxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDOzRCQUMvSSxhQUFhLEdBQUcsS0FBSyxDQUFDOzRCQUN0QixLQUFLLENBQUE7d0JBQ1AsQ0FBQzt3QkFBQyxJQUFJLENBQUMsQ0FBQzt3QkFFUixDQUFDO29CQUNILENBQUM7b0JBRUQsRUFBRSxDQUFDLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUM7d0JBQzFCLHlFQUF5RTt3QkFDekUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLEdBQUcsYUFBYSxDQUFDLFVBQVUsQ0FBQyxHQUFHLEdBQUcsYUFBYSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUM7d0JBQ3pGLFdBQVcsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxXQUFXLENBQUMsQ0FBQTtvQkFDakcsQ0FBQztnQkFDSCxDQUFDLFFBQVEsYUFBYSxFQUFDO1lBQ3pCLENBQUM7UUFDSCxDQUFDO1FBQ0QsWUFBWSxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsV0FBVyxHQUFHLGNBQWMsRUFBRSxVQUFVLENBQUMsQ0FBQztRQUN2RSxXQUFXLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxXQUFXLEdBQUcsY0FBYyxFQUFFLFVBQVUsQ0FBQyxDQUFBO0tBQ2xFO0lBQ0QsTUFBTSxDQUFDO1FBQ0wsTUFBTSxFQUFFLFdBQVc7UUFDbkIsY0FBQSxZQUFZO1FBQ1osV0FBQSxTQUFTO0tBQ1YsQ0FBQTtBQUNILENBQUM7QUEvRGUsYUFBSyxRQStEcEIsQ0FBQTtBQUVELGlCQUF5QixLQUFLLEVBQUUsV0FBVyxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsS0FBSztJQUMxRSxJQUFJLENBQUMsRUFBRSxJQUFJLENBQUM7SUFFWixJQUFJLFdBQVcsR0FBRyxZQUFZLENBQUM7SUFFL0IsSUFBSSxZQUFZLEdBQUcsRUFBRSxDQUFDO0lBQ3RCLElBQUksU0FBUyxHQUFHLEVBQUUsQ0FBQztJQUVuQixJQUFJLFlBQVksR0FBRyxPQUFPLENBQUMsS0FBSyxFQUFFLFVBQVUsSUFBSTtRQUM5QyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUE7SUFDOUIsQ0FBQyxDQUFDLENBQUM7SUFFSCxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1FBQ1Ysa0NBQWtDO1FBQ2xDLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEVBQUUsSUFBSSxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxHQUFHLElBQUksRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBQy9DLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQTtRQUNoQyxDQUFDO0lBQ0gsQ0FBQztJQUVELEdBQUcsQ0FBQyxDQUFZLFVBQXdCLEVBQXhCLEtBQUEsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsRUFBeEIsY0FBd0IsRUFBeEIsSUFBd0IsQ0FBQztRQUFwQyxJQUFJLEdBQUcsU0FBQTtRQUNWLElBQUksR0FBRyxHQUFHLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUMzQiwyQ0FBMkM7UUFDM0MsSUFBSSxLQUFLLEdBQUcsWUFBWSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUVwQyxTQUFTLENBQUMsR0FBRyxDQUFDLEdBQUcsV0FBVyxDQUFDO1FBRTdCLElBQUksV0FBVyxHQUFHLENBQUMsQ0FBQztRQUNwQixHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUFFLElBQUksR0FBRyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsR0FBRyxJQUFJLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztZQUMvQyxJQUFJLElBQUksR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDcEIsSUFBSSxjQUFjLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUUvRSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUNqQyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsR0FBRyxXQUFXLEdBQUcsY0FBYyxDQUFDO2dCQUNuRCxXQUFXLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQTtZQUNqRSxDQUFDO1FBQ0gsQ0FBQztRQUNELFlBQVksQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxVQUFVLENBQUMsQ0FBQztRQUN0RCxXQUFXLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsVUFBVSxDQUFDLENBQUE7S0FDakQ7SUFDRCxNQUFNLENBQUM7UUFDTCxNQUFNLEVBQUUsV0FBVztRQUNuQixjQUFBLFlBQVk7UUFDWixXQUFBLFNBQVM7S0FDVixDQUFBO0FBQ0gsQ0FBQztBQTVDZSxlQUFPLFVBNEN0QixDQUFBO0FBRUQsZUFBdUIsS0FBSyxFQUFFLEdBQUc7SUFDL0IsSUFBSSxHQUFHLEdBQUcsRUFBRSxDQUFDO0lBRWIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFVLE9BQU8sRUFBRSxLQUFLLEVBQUUsS0FBSztRQUMzQyxHQUFHLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsT0FBTyxDQUFBO0lBQzdCLENBQUMsQ0FBQyxDQUFDO0lBQ0gsTUFBTSxDQUFDLEdBQUcsQ0FBQTtBQUNaLENBQUM7QUFQZSxhQUFLLFFBT3BCLENBQUE7QUFFRCxpQkFBeUIsVUFBVSxFQUFFLGFBQWE7SUFDaEQsSUFBSSxHQUFHLEdBQUcsRUFBRSxDQUFDO0lBRWIsVUFBVSxDQUFDLE9BQU8sQ0FBQyxVQUFVLE9BQU8sRUFBRSxLQUFLLEVBQUUsS0FBSztRQUNoRCxJQUFNLEdBQUcsR0FBRyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDbkMsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2QsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsQ0FBQTtRQUNmLENBQUM7UUFDRCxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFBO0lBQ3hCLENBQUMsQ0FBQyxDQUFDO0lBRUgsTUFBTSxDQUFDLEdBQUcsQ0FBQTtBQUNaLENBQUM7QUFaZSxlQUFPLFVBWXRCLENBQUE7QUFFRCwrQkFBdUMsT0FBTyxFQUFFLFNBQVM7SUFDdkQsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLFNBQVMsSUFBSSxPQUFPLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztJQUMzRixNQUFNLENBQUMsT0FBTyxDQUFDLFVBQVUsSUFBSSxxQkFBcUIsQ0FBQyxPQUFPLENBQUMsVUFBVSxFQUFFLFNBQVMsQ0FBQyxDQUFBO0FBQ25GLENBQUM7QUFIZSw2QkFBcUIsd0JBR3BDLENBQUE7QUFFRCwrQkFBdUMsVUFBVSxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsV0FBVztJQUM1RSxFQUFFLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1FBQ2hCLEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTSxJQUFJLE1BQU0sS0FBSyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ2pDLE1BQU0sQ0FBQyx1Q0FBdUM7Z0JBQ2hDLENBQUcsTUFBTSxPQUFHO2dCQUNaLENBQUcsTUFBTSxVQUFJLFVBQVUsR0FBRyxDQUFDLFVBQUs7Z0JBQ2hDLENBQUcsV0FBVyxVQUFJLFVBQVUsR0FBRyxDQUFDLFVBQUs7Z0JBQ3JDLENBQUcsV0FBVyxTQUFJLFVBQVUsUUFBSTtnQkFDdEMsR0FBRyxDQUFBO1FBQ2IsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sTUFBTSxDQUFDLHVDQUF1QztnQkFDaEMsQ0FBRyxNQUFNLE9BQUc7Z0JBQ1osQ0FBRyxNQUFNLFVBQUksVUFBVSxHQUFHLENBQUMsVUFBSztnQkFDaEMsQ0FBRyxXQUFXLFVBQUksVUFBVSxHQUFHLENBQUMsVUFBSztnQkFDckMsQ0FBRyxXQUFXLFNBQUksVUFBVSxTQUFLO2dCQUNqQyxDQUFHLE1BQU0sU0FBSSxVQUFVLFNBQUs7Z0JBQzVCLENBQUcsTUFBTSxVQUFJLFVBQVUsR0FBRyxDQUFDLEdBQUcsQ0FBQyxVQUFLO2dCQUNwQyxDQUFHLFdBQVcsVUFBSSxVQUFVLEdBQUcsQ0FBQyxHQUFHLENBQUMsVUFBSztnQkFDekMsQ0FBRyxXQUFXLFNBQUksVUFBVSxHQUFHLENBQUMsUUFBSTtnQkFDMUMsR0FBRyxDQUFBO1FBQ2IsQ0FBQztJQUNILENBQUM7SUFBQyxJQUFJLENBQUMsQ0FBQztRQUNOLEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTSxJQUFJLE1BQU0sS0FBSyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ2pDLE1BQU0sQ0FBQyxNQUFNLENBQUE7UUFDZixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixNQUFNLENBQUMseUNBQXVDLE1BQU0sU0FBSSxNQUFNLFNBQUksVUFBVSxXQUFNLE1BQU0sU0FBSSxVQUFVLFdBQU0sTUFBTSxTQUFJLFVBQVUsR0FBRyxDQUFDLFFBQUssQ0FBQTtRQUMzSSxDQUFDO0lBQ0gsQ0FBQztBQUNILENBQUM7QUE1QmUsNkJBQXFCLHdCQTRCcEMsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIG1vbWVudCBmcm9tICdtb21lbnQnXG5cbmNvbnN0IEVQU0lMT04gPSAwLjAwMTtcblxuLy8gc28gd2UgY291bGQgdXNlIGJvdGggaW1tdXRhYmxlLmpzIG9iamVjdHMgYW5kIHJlZ3VsYXIgb2JqZWN0c1xuZXhwb3J0IGZ1bmN0aW9uIF9nZXQgKG9iamVjdCwga2V5KSB7XG4gIHJldHVybiB0eXBlb2Ygb2JqZWN0LmdldCA9PT0gJ2Z1bmN0aW9uJyA/IG9iamVjdC5nZXQoa2V5KSA6IG9iamVjdFtrZXldXG59XG5cbmV4cG9ydCBmdW5jdGlvbiBfbGVuZ3RoIChvYmplY3QpIHtcbiAgcmV0dXJuIHR5cGVvZiBvYmplY3QuY291bnQgPT09ICdmdW5jdGlvbicgPyBvYmplY3QuY291bnQoKSA6IG9iamVjdC5sZW5ndGhcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGFycmF5c0VxdWFsIChhcnJheTEsIGFycmF5Mikge1xuICByZXR1cm4gKF9sZW5ndGgoYXJyYXkxKSA9PT0gX2xlbmd0aChhcnJheTIpKSAmJiBhcnJheTEuZXZlcnkoKGVsZW1lbnQsIGluZGV4KSA9PiB7XG4gICAgcmV0dXJuIGVsZW1lbnQgPT09IF9nZXQoYXJyYXkyLCBpbmRleClcbiAgfSlcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGl0ZXJhdGVUaW1lcyAoc3RhcnQsIGVuZCwgdW5pdCwgdGltZVN0ZXBzLCBjYWxsYmFjaykge1xuICBsZXQgdGltZSA9IG1vbWVudChzdGFydCkuc3RhcnRPZih1bml0KTtcblxuICBpZiAodGltZVN0ZXBzW3VuaXRdICYmIHRpbWVTdGVwc1t1bml0XSA+IDEpIHtcbiAgICBsZXQgdmFsdWUgPSB0aW1lLmdldCh1bml0KTtcbiAgICB0aW1lLnNldCh1bml0LCB2YWx1ZSAtICh2YWx1ZSAlIHRpbWVTdGVwc1t1bml0XSkpXG4gIH1cblxuICB3aGlsZSAodGltZS52YWx1ZU9mKCkgPCBlbmQpIHtcbiAgICAgIGNvbnN0IHVuaXRTdHIgPSBgJHt1bml0fXNgIGFzIG1vbWVudC5Vbml0T2ZUaW1lO1xuICAgIGxldCBuZXh0VGltZSA9IG1vbWVudCh0aW1lKS5hZGQodGltZVN0ZXBzW3VuaXRdIHx8IDEsIHVuaXRTdHIpO1xuICAgIGNhbGxiYWNrKHRpbWUsIG5leHRUaW1lKTtcbiAgICB0aW1lID0gbmV4dFRpbWVcbiAgfVxufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0TWluVW5pdCAoem9vbSwgd2lkdGgsIHRpbWVTdGVwcykge1xuICBsZXQgdGltZURpdmlkZXJzID0ge1xuICAgIHNlY29uZDogMTAwMCxcbiAgICBtaW51dGU6IDYwLFxuICAgIGhvdXI6IDYwLFxuICAgIGRheTogMjQsXG4gICAgbW9udGg6IDMwLFxuICAgIHllYXI6IDEyXG4gIH07XG5cbiAgbGV0IG1pblVuaXQgPSAneWVhcic7XG4gIGxldCBicmVha0NvdW50ID0gem9vbTtcbiAgY29uc3QgbWluQ2VsbFdpZHRoID0gMTc7XG5cbiAgT2JqZWN0LmtleXModGltZURpdmlkZXJzKS5zb21lKHVuaXQgPT4ge1xuICAgIGJyZWFrQ291bnQgPSBicmVha0NvdW50IC8gdGltZURpdmlkZXJzW3VuaXRdO1xuICAgIGNvbnN0IGNlbGxDb3VudCA9IGJyZWFrQ291bnQgLyB0aW1lU3RlcHNbdW5pdF07XG4gICAgY29uc3QgY291bnROZWVkZWQgPSB3aWR0aCAvICh0aW1lU3RlcHNbdW5pdF0gJiYgdGltZVN0ZXBzW3VuaXRdID4gMSA/IDMgKiBtaW5DZWxsV2lkdGggOiBtaW5DZWxsV2lkdGgpO1xuXG4gICAgaWYgKGNlbGxDb3VudCA8IGNvdW50TmVlZGVkKSB7XG4gICAgICBtaW5Vbml0ID0gdW5pdDtcbiAgICAgIHJldHVybiB0cnVlXG4gICAgfVxuICB9KTtcblxuICByZXR1cm4gbWluVW5pdFxufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0TmV4dFVuaXQgKHVuaXQpIHtcbiAgbGV0IG5leHRVbml0cyA9IHtcbiAgICBzZWNvbmQ6ICdtaW51dGUnLFxuICAgIG1pbnV0ZTogJ2hvdXInLFxuICAgIGhvdXI6ICdkYXknLFxuICAgIGRheTogJ21vbnRoJyxcbiAgICBtb250aDogJ3llYXInXG4gIH07XG5cbiAgcmV0dXJuIG5leHRVbml0c1t1bml0XSB8fCAnJ1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0UGFyZW50UG9zaXRpb24gKGVsZW1lbnQpIHtcbiAgdmFyIHhQb3NpdGlvbiA9IDA7XG4gIHZhciB5UG9zaXRpb24gPSAwO1xuICB2YXIgZmlyc3QgPSB0cnVlO1xuXG4gIHdoaWxlIChlbGVtZW50KSB7XG4gICAgeFBvc2l0aW9uICs9IChlbGVtZW50Lm9mZnNldExlZnQgLSAoZmlyc3QgPyAwIDogZWxlbWVudC5zY3JvbGxMZWZ0KSArIGVsZW1lbnQuY2xpZW50TGVmdCk7XG4gICAgeVBvc2l0aW9uICs9IChlbGVtZW50Lm9mZnNldFRvcCAtIChmaXJzdCA/IDAgOiBlbGVtZW50LnNjcm9sbFRvcCkgKyBlbGVtZW50LmNsaWVudFRvcCk7XG4gICAgZWxlbWVudCA9IGVsZW1lbnQub2Zmc2V0UGFyZW50O1xuICAgIGZpcnN0ID0gZmFsc2VcbiAgfVxuICByZXR1cm4geyB4OiB4UG9zaXRpb24sIHk6IHlQb3NpdGlvbiB9XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBjb29yZGluYXRlVG9UaW1lUmF0aW8gKGNhbnZhc1RpbWVTdGFydCwgY2FudmFzVGltZUVuZCwgY2FudmFzV2lkdGgpIHtcbiAgcmV0dXJuIChjYW52YXNUaW1lRW5kIC0gY2FudmFzVGltZVN0YXJ0KSAvIGNhbnZhc1dpZHRoXG59XG5cbmV4cG9ydCBmdW5jdGlvbiBjYWxjdWxhdGVEaW1lbnNpb25zIChpdGVtLCBvcmRlciwga2V5cywgY2FudmFzVGltZVN0YXJ0LCBjYW52YXNUaW1lRW5kLCBjYW52YXNXaWR0aCwgZHJhZ1NuYXAsIGxpbmVIZWlnaHQsIGRyYWdnaW5nSXRlbSwgZHJhZ1RpbWUsIHJlc2l6aW5nSXRlbSwgcmVzaXplRW5kLCBuZXdHcm91cE9yZGVyLCBpdGVtSGVpZ2h0UmF0aW8pIHtcbiAgdmFyIGl0ZW1JZCA9IF9nZXQoaXRlbSwga2V5cy5pdGVtSWRLZXkpO1xuICB2YXIgaXRlbVRpbWVTdGFydCA9IF9nZXQoaXRlbSwga2V5cy5pdGVtVGltZVN0YXJ0S2V5KTtcbiAgdmFyIGl0ZW1UaW1lRW5kID0gX2dldChpdGVtLCBrZXlzLml0ZW1UaW1lRW5kS2V5KTtcblxuICB2YXIgaXNEcmFnZ2luZyA9IGl0ZW1JZCA9PT0gZHJhZ2dpbmdJdGVtO1xuICB2YXIgaXNSZXNpemluZyA9IGl0ZW1JZCA9PT0gcmVzaXppbmdJdGVtO1xuXG4gIGNvbnN0IHggPSBpc0RyYWdnaW5nID8gZHJhZ1RpbWUgOiBpdGVtVGltZVN0YXJ0O1xuXG4gIGNvbnN0IHcgPSBNYXRoLm1heCgoaXNSZXNpemluZyA/IHJlc2l6ZUVuZCA6IGl0ZW1UaW1lRW5kKSAtIGl0ZW1UaW1lU3RhcnQsIGRyYWdTbmFwKTtcbiAgbGV0IGNvbGxpc2lvblggPSBpdGVtVGltZVN0YXJ0O1xuICBsZXQgY29sbGlzaW9uVyA9IHc7XG5cbiAgaWYgKGlzRHJhZ2dpbmcpIHtcbiAgICBpZiAoaXRlbVRpbWVTdGFydCA+PSBkcmFnVGltZSkge1xuICAgICAgY29sbGlzaW9uWCA9IGRyYWdUaW1lO1xuICAgICAgY29sbGlzaW9uVyA9IE1hdGgubWF4KGl0ZW1UaW1lRW5kIC0gZHJhZ1RpbWUsIGRyYWdTbmFwKVxuICAgIH0gZWxzZSB7XG4gICAgICBjb2xsaXNpb25XID0gTWF0aC5tYXgoZHJhZ1RpbWUgLSBpdGVtVGltZVN0YXJ0ICsgdywgZHJhZ1NuYXApXG4gICAgfVxuICB9XG5cbiAgY29uc3QgaCA9IGxpbmVIZWlnaHQgKiBpdGVtSGVpZ2h0UmF0aW87XG4gIGNvbnN0IHJhdGlvID0gMSAvIGNvb3JkaW5hdGVUb1RpbWVSYXRpbyhjYW52YXNUaW1lU3RhcnQsIGNhbnZhc1RpbWVFbmQsIGNhbnZhc1dpZHRoKTtcblxuICByZXR1cm4ge1xuICAgIGxlZnQ6ICh4IC0gY2FudmFzVGltZVN0YXJ0KSAqIHJhdGlvLFxuICAgIHRvcDogbnVsbCxcbiAgICB3aWR0aDogTWF0aC5tYXgodyAqIHJhdGlvLCAzKSxcbiAgICBoZWlnaHQ6IGgsXG4gICAgb3JkZXI6IGlzRHJhZ2dpbmcgPyBuZXdHcm91cE9yZGVyIDogb3JkZXIsXG4gICAgc3RhY2s6IHRydWUsXG4gICAgbGluZUhlaWdodDogbGluZUhlaWdodCxcbiAgICBjb2xsaXNpb25MZWZ0OiBjb2xsaXNpb25YLFxuICAgIG9yaWdpbmFsTGVmdDogaXRlbVRpbWVTdGFydCxcbiAgICBjb2xsaXNpb25XaWR0aDogY29sbGlzaW9uVyxcbiAgICBpc0RyYWdnaW5nOiBpc0RyYWdnaW5nXG4gIH07XG5cbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldEdyb3VwT3JkZXJzIChncm91cHMsIGtleXMpIHtcbiAgY29uc3QgeyBncm91cElkS2V5IH0gPSBrZXlzO1xuXG4gIGxldCBncm91cE9yZGVycyA9IHt9O1xuXG4gIGZvciAobGV0IGkgPSAwOyBpIDwgZ3JvdXBzLmxlbmd0aDsgaSsrKSB7XG4gICAgZ3JvdXBPcmRlcnNbX2dldChncm91cHNbaV0sIGdyb3VwSWRLZXkpXSA9IGlcbiAgfVxuXG4gIHJldHVybiBncm91cE9yZGVyc1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0VmlzaWJsZUl0ZW1zIChpdGVtcywgY2FudmFzVGltZVN0YXJ0LCBjYW52YXNUaW1lRW5kLCBrZXlzKSB7XG4gIGNvbnN0IHsgaXRlbVRpbWVTdGFydEtleSwgaXRlbVRpbWVFbmRLZXkgfSA9IGtleXM7XG5cbiAgcmV0dXJuIGl0ZW1zLmZpbHRlcihpdGVtID0+IHtcbiAgICByZXR1cm4gX2dldChpdGVtLCBpdGVtVGltZVN0YXJ0S2V5KSA8PSBjYW52YXNUaW1lRW5kICYmIF9nZXQoaXRlbSwgaXRlbVRpbWVFbmRLZXkpID49IGNhbnZhc1RpbWVTdGFydFxuICB9KVxufVxuXG5leHBvcnQgZnVuY3Rpb24gY29sbGlzaW9uIChhLCBiLCBsaW5lSGVpZ2h0KSB7XG4gIC8vIHZhciB2ZXJ0aWNhbE1hcmdpbiA9IChsaW5lSGVpZ2h0IC0gYS5oZWlnaHQpLzI7XG4gIHZhciB2ZXJ0aWNhbE1hcmdpbiA9IDA7XG4gIHJldHVybiAoKGEuY29sbGlzaW9uTGVmdCArIEVQU0lMT04pIDwgKGIuY29sbGlzaW9uTGVmdCArIGIuY29sbGlzaW9uV2lkdGgpICYmXG4gIChhLmNvbGxpc2lvbkxlZnQgKyBhLmNvbGxpc2lvbldpZHRoIC0gRVBTSUxPTikgPiBiLmNvbGxpc2lvbkxlZnQgJiZcbiAgKGEudG9wIC0gdmVydGljYWxNYXJnaW4gKyBFUFNJTE9OKSA8IChiLnRvcCArIGIuaGVpZ2h0KSAmJlxuICAoYS50b3AgKyBhLmhlaWdodCArIHZlcnRpY2FsTWFyZ2luIC0gRVBTSUxPTikgPiBiLnRvcClcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHN0YWNrIChpdGVtcywgZ3JvdXBPcmRlcnMsIGxpbmVIZWlnaHQsIGhlYWRlckhlaWdodCwgZm9yY2UpIHtcbiAgdmFyIGksIGlNYXg7XG5cbiAgdmFyIHRvdGFsSGVpZ2h0ID0gaGVhZGVySGVpZ2h0O1xuXG4gIHZhciBncm91cEhlaWdodHMgPSB7fTtcbiAgdmFyIGdyb3VwVG9wcyA9IHt9O1xuXG4gIHZhciBncm91cGVkSXRlbXMgPSBncm91cEJ5KGl0ZW1zLCBmdW5jdGlvbiAoaXRlbSkge1xuICAgIHJldHVybiBpdGVtLmRpbWVuc2lvbnMub3JkZXJcbiAgfSk7XG5cbiAgaWYgKGZvcmNlKSB7XG4gICAgLy8gcmVzZXQgdG9wIHBvc2l0aW9uIG9mIGFsbCBpdGVtc1xuICAgIGZvciAoaSA9IDAsIGlNYXggPSBpdGVtcy5sZW5ndGg7IGkgPCBpTWF4OyBpKyspIHtcbiAgICAgIGl0ZW1zW2ldLmRpbWVuc2lvbnMudG9wID0gbnVsbFxuICAgIH1cbiAgfVxuXG4gIGZvciAodmFyIHVybCBvZiBPYmplY3Qua2V5cyhncm91cE9yZGVycykpIHtcbiAgICB2YXIga2V5ID0gZ3JvdXBPcmRlcnNbdXJsXTtcbiAgICAvLyBjYWxjdWxhdGUgbmV3LCBub24tb3ZlcmxhcHBpbmcgcG9zaXRpb25zXG4gICAgdmFyIGdyb3VwID0gZ3JvdXBlZEl0ZW1zW2tleV0gfHwgW107XG5cbiAgICBncm91cFRvcHNba2V5XSA9IHRvdGFsSGVpZ2h0O1xuXG4gICAgdmFyIGdyb3VwSGVpZ2h0ID0gMDtcbiAgICB2YXIgdmVydGljYWxNYXJnaW4gPSAwO1xuICAgIGZvciAoaSA9IDAsIGlNYXggPSBncm91cC5sZW5ndGg7IGkgPCBpTWF4OyBpKyspIHtcbiAgICAgIHZhciBpdGVtID0gZ3JvdXBbaV07XG4gICAgICB2ZXJ0aWNhbE1hcmdpbiA9IChpdGVtLmRpbWVuc2lvbnMubGluZUhlaWdodCAtIGl0ZW0uZGltZW5zaW9ucy5oZWlnaHQpO1xuXG4gICAgICBpZiAoaXRlbS5kaW1lbnNpb25zLnN0YWNrICYmIGl0ZW0uZGltZW5zaW9ucy50b3AgPT09IG51bGwpIHtcbiAgICAgICAgaXRlbS5kaW1lbnNpb25zLnRvcCA9IHRvdGFsSGVpZ2h0ICsgdmVydGljYWxNYXJnaW47XG4gICAgICAgIGdyb3VwSGVpZ2h0ID0gTWF0aC5tYXgoZ3JvdXBIZWlnaHQsIGl0ZW0uZGltZW5zaW9ucy5saW5lSGVpZ2h0KTtcbiAgICAgICAgZG8ge1xuICAgICAgICAgIHZhciBjb2xsaWRpbmdJdGVtID0gbnVsbDtcbiAgICAgICAgICBmb3IgKHZhciBqID0gMCwgamogPSBncm91cC5sZW5ndGg7IGogPCBqajsgaisrKSB7XG4gICAgICAgICAgICB2YXIgb3RoZXIgPSBncm91cFtqXTtcbiAgICAgICAgICAgIGlmIChvdGhlci50b3AgIT09IG51bGwgJiYgb3RoZXIgIT09IGl0ZW0gJiYgb3RoZXIuZGltZW5zaW9ucy5zdGFjayAmJiBjb2xsaXNpb24oaXRlbS5kaW1lbnNpb25zLCBvdGhlci5kaW1lbnNpb25zLCBpdGVtLmRpbWVuc2lvbnMubGluZUhlaWdodCkpIHtcbiAgICAgICAgICAgICAgY29sbGlkaW5nSXRlbSA9IG90aGVyO1xuICAgICAgICAgICAgICBicmVha1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgLy8gY29uc29sZS5sb2coJ2RvbnQgdGVzdCcsIG90aGVyLnRvcCAhPT0gbnVsbCwgb3RoZXIgIT09IGl0ZW0sIG90aGVyLnN0YWNrKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG5cbiAgICAgICAgICBpZiAoY29sbGlkaW5nSXRlbSAhPSBudWxsKSB7XG4gICAgICAgICAgICAvLyBUaGVyZSBpcyBhIGNvbGxpc2lvbi4gUmVwb3NpdGlvbiB0aGUgaXRlbXMgYWJvdmUgdGhlIGNvbGxpZGluZyBlbGVtZW50XG4gICAgICAgICAgICBpdGVtLmRpbWVuc2lvbnMudG9wID0gY29sbGlkaW5nSXRlbS5kaW1lbnNpb25zLnRvcCArIGNvbGxpZGluZ0l0ZW0uZGltZW5zaW9ucy5saW5lSGVpZ2h0O1xuICAgICAgICAgICAgZ3JvdXBIZWlnaHQgPSBNYXRoLm1heChncm91cEhlaWdodCwgaXRlbS5kaW1lbnNpb25zLnRvcCArIGl0ZW0uZGltZW5zaW9ucy5oZWlnaHQgLSB0b3RhbEhlaWdodClcbiAgICAgICAgICB9XG4gICAgICAgIH0gd2hpbGUgKGNvbGxpZGluZ0l0ZW0pXG4gICAgICB9XG4gICAgfVxuICAgIGdyb3VwSGVpZ2h0c1trZXldID0gTWF0aC5tYXgoZ3JvdXBIZWlnaHQgKyB2ZXJ0aWNhbE1hcmdpbiwgbGluZUhlaWdodCk7XG4gICAgdG90YWxIZWlnaHQgKz0gTWF0aC5tYXgoZ3JvdXBIZWlnaHQgKyB2ZXJ0aWNhbE1hcmdpbiwgbGluZUhlaWdodClcbiAgfVxuICByZXR1cm4ge1xuICAgIGhlaWdodDogdG90YWxIZWlnaHQsXG4gICAgZ3JvdXBIZWlnaHRzLFxuICAgIGdyb3VwVG9wc1xuICB9XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBub3N0YWNrIChpdGVtcywgZ3JvdXBPcmRlcnMsIGxpbmVIZWlnaHQsIGhlYWRlckhlaWdodCwgZm9yY2UpIHtcbiAgdmFyIGksIGlNYXg7XG5cbiAgdmFyIHRvdGFsSGVpZ2h0ID0gaGVhZGVySGVpZ2h0O1xuXG4gIHZhciBncm91cEhlaWdodHMgPSB7fTtcbiAgdmFyIGdyb3VwVG9wcyA9IHt9O1xuXG4gIHZhciBncm91cGVkSXRlbXMgPSBncm91cEJ5KGl0ZW1zLCBmdW5jdGlvbiAoaXRlbSkge1xuICAgIHJldHVybiBpdGVtLmRpbWVuc2lvbnMub3JkZXJcbiAgfSk7XG5cbiAgaWYgKGZvcmNlKSB7XG4gICAgLy8gcmVzZXQgdG9wIHBvc2l0aW9uIG9mIGFsbCBpdGVtc1xuICAgIGZvciAoaSA9IDAsIGlNYXggPSBpdGVtcy5sZW5ndGg7IGkgPCBpTWF4OyBpKyspIHtcbiAgICAgIGl0ZW1zW2ldLmRpbWVuc2lvbnMudG9wID0gbnVsbFxuICAgIH1cbiAgfVxuXG4gIGZvciAodmFyIHVybCBvZiBPYmplY3Qua2V5cyhncm91cE9yZGVycykpIHtcbiAgICB2YXIga2V5ID0gZ3JvdXBPcmRlcnNbdXJsXTtcbiAgICAvLyBjYWxjdWxhdGUgbmV3LCBub24tb3ZlcmxhcHBpbmcgcG9zaXRpb25zXG4gICAgdmFyIGdyb3VwID0gZ3JvdXBlZEl0ZW1zW2tleV0gfHwgW107XG5cbiAgICBncm91cFRvcHNba2V5XSA9IHRvdGFsSGVpZ2h0O1xuXG4gICAgdmFyIGdyb3VwSGVpZ2h0ID0gMDtcbiAgICBmb3IgKGkgPSAwLCBpTWF4ID0gZ3JvdXAubGVuZ3RoOyBpIDwgaU1heDsgaSsrKSB7XG4gICAgICB2YXIgaXRlbSA9IGdyb3VwW2ldO1xuICAgICAgdmFyIHZlcnRpY2FsTWFyZ2luID0gKGl0ZW0uZGltZW5zaW9ucy5saW5lSGVpZ2h0IC0gaXRlbS5kaW1lbnNpb25zLmhlaWdodCkgLyAyO1xuXG4gICAgICBpZiAoaXRlbS5kaW1lbnNpb25zLnRvcCA9PT0gbnVsbCkge1xuICAgICAgICBpdGVtLmRpbWVuc2lvbnMudG9wID0gdG90YWxIZWlnaHQgKyB2ZXJ0aWNhbE1hcmdpbjtcbiAgICAgICAgZ3JvdXBIZWlnaHQgPSBNYXRoLm1heChncm91cEhlaWdodCwgaXRlbS5kaW1lbnNpb25zLmxpbmVIZWlnaHQpXG4gICAgICB9XG4gICAgfVxuICAgIGdyb3VwSGVpZ2h0c1trZXldID0gTWF0aC5tYXgoZ3JvdXBIZWlnaHQsIGxpbmVIZWlnaHQpO1xuICAgIHRvdGFsSGVpZ2h0ICs9IE1hdGgubWF4KGdyb3VwSGVpZ2h0LCBsaW5lSGVpZ2h0KVxuICB9XG4gIHJldHVybiB7XG4gICAgaGVpZ2h0OiB0b3RhbEhlaWdodCxcbiAgICBncm91cEhlaWdodHMsXG4gICAgZ3JvdXBUb3BzXG4gIH1cbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGtleUJ5ICh2YWx1ZSwga2V5KSB7XG4gIGxldCBvYmogPSB7fTtcblxuICB2YWx1ZS5mb3JFYWNoKGZ1bmN0aW9uIChlbGVtZW50LCBpbmRleCwgYXJyYXkpIHtcbiAgICBvYmpbZWxlbWVudFtrZXldXSA9IGVsZW1lbnRcbiAgfSk7XG4gIHJldHVybiBvYmpcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdyb3VwQnkgKGNvbGxlY3Rpb24sIGdyb3VwRnVuY3Rpb24pIHtcbiAgbGV0IG9iaiA9IHt9O1xuXG4gIGNvbGxlY3Rpb24uZm9yRWFjaChmdW5jdGlvbiAoZWxlbWVudCwgaW5kZXgsIGFycmF5KSB7XG4gICAgY29uc3Qga2V5ID0gZ3JvdXBGdW5jdGlvbihlbGVtZW50KTtcbiAgICBpZiAoIW9ialtrZXldKSB7XG4gICAgICBvYmpba2V5XSA9IFtdXG4gICAgfVxuICAgIG9ialtrZXldLnB1c2goZWxlbWVudClcbiAgfSk7XG5cbiAgcmV0dXJuIG9ialxufVxuXG5leHBvcnQgZnVuY3Rpb24gaGFzU29tZVBhcmVudFRoZUNsYXNzIChlbGVtZW50LCBjbGFzc25hbWUpIHtcbiAgaWYgKGVsZW1lbnQuY2xhc3NOYW1lICYmIGVsZW1lbnQuY2xhc3NOYW1lLnNwbGl0KCcgJykuaW5kZXhPZihjbGFzc25hbWUpID49IDApIHJldHVybiB0cnVlO1xuICByZXR1cm4gZWxlbWVudC5wYXJlbnROb2RlICYmIGhhc1NvbWVQYXJlbnRUaGVDbGFzcyhlbGVtZW50LnBhcmVudE5vZGUsIGNsYXNzbmFtZSlcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGNyZWF0ZUdyYWRpZW50UGF0dGVybiAobGluZUhlaWdodCwgY29sb3IxLCBjb2xvcjIsIGJvcmRlckNvbG9yKSB7XG4gIGlmIChib3JkZXJDb2xvcikge1xuICAgIGlmICghY29sb3IyIHx8IGNvbG9yMSA9PT0gY29sb3IyKSB7XG4gICAgICByZXR1cm4gJ3JlcGVhdGluZy1saW5lYXItZ3JhZGllbnQodG8gYm90dG9tLCAnICtcbiAgICAgICAgICAgICAgICAgICAgYCR7Y29sb3IxfSxgICtcbiAgICAgICAgICAgICAgICAgICAgYCR7Y29sb3IxfSAke2xpbmVIZWlnaHQgLSAxfXB4LGAgK1xuICAgICAgICAgICAgICAgICAgICBgJHtib3JkZXJDb2xvcn0gJHtsaW5lSGVpZ2h0IC0gMX1weCxgICtcbiAgICAgICAgICAgICAgICAgICAgYCR7Ym9yZGVyQ29sb3J9ICR7bGluZUhlaWdodH1weGAgK1xuICAgICAgICAgICAgICAnKSdcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuICdyZXBlYXRpbmctbGluZWFyLWdyYWRpZW50KHRvIGJvdHRvbSwgJyArXG4gICAgICAgICAgICAgICAgICAgIGAke2NvbG9yMX0sYCArXG4gICAgICAgICAgICAgICAgICAgIGAke2NvbG9yMX0gJHtsaW5lSGVpZ2h0IC0gMX1weCxgICtcbiAgICAgICAgICAgICAgICAgICAgYCR7Ym9yZGVyQ29sb3J9ICR7bGluZUhlaWdodCAtIDF9cHgsYCArXG4gICAgICAgICAgICAgICAgICAgIGAke2JvcmRlckNvbG9yfSAke2xpbmVIZWlnaHR9cHgsYCArXG4gICAgICAgICAgICAgICAgICAgIGAke2NvbG9yMn0gJHtsaW5lSGVpZ2h0fXB4LGAgK1xuICAgICAgICAgICAgICAgICAgICBgJHtjb2xvcjJ9ICR7bGluZUhlaWdodCAqIDIgLSAxfXB4LGAgK1xuICAgICAgICAgICAgICAgICAgICBgJHtib3JkZXJDb2xvcn0gJHtsaW5lSGVpZ2h0ICogMiAtIDF9cHgsYCArXG4gICAgICAgICAgICAgICAgICAgIGAke2JvcmRlckNvbG9yfSAke2xpbmVIZWlnaHQgKiAyfXB4YCArXG4gICAgICAgICAgICAgICcpJ1xuICAgIH1cbiAgfSBlbHNlIHtcbiAgICBpZiAoIWNvbG9yMiB8fCBjb2xvcjEgPT09IGNvbG9yMikge1xuICAgICAgcmV0dXJuIGNvbG9yMVxuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gYHJlcGVhdGluZy1saW5lYXItZ3JhZGllbnQodG8gYm90dG9tLCR7Y29sb3IxfSwke2NvbG9yMX0gJHtsaW5lSGVpZ2h0fXB4LCR7Y29sb3IyfSAke2xpbmVIZWlnaHR9cHgsJHtjb2xvcjJ9ICR7bGluZUhlaWdodCAqIDJ9cHgpYFxuICAgIH1cbiAgfVxufVxuIl19

/***/ },
/* 12 */
/*!***************************************!*\
  !*** ./src/lib/layout/info-label.tsx ***!
  \***************************************/
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var React = __webpack_require__(/*! react */ 2);
	var InfoLabel = (function (_super) {
	    __extends(InfoLabel, _super);
	    function InfoLabel() {
	        _super.apply(this, arguments);
	    }
	    InfoLabel.prototype.render = function () {
	        return (React.createElement("div", {className: 'rct-infolabel'}, this.props.label));
	    };
	    // shouldComponentUpdate = shouldPureComponentUpdate;
	    InfoLabel.defaultProps = {
	        label: ''
	    };
	    return InfoLabel;
	}(React.Component));
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.default = InfoLabel;
	//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5mby1sYWJlbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImluZm8tbGFiZWwudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLElBQVksS0FBSyxXQUFNLE9BSXZCLENBQUMsQ0FKNkI7QUFTOUI7SUFBdUMsNkJBQTJCO0lBQWxFO1FBQXVDLDhCQUEyQjtJQWNsRSxDQUFDO0lBUEMsMEJBQU0sR0FBTjtRQUNFLE1BQU0sQ0FBQyxDQUNMLHFCQUFDLEdBQUcsSUFBQyxTQUFTLEVBQUMsZUFBZSxHQUMzQixJQUFJLENBQUMsS0FBSyxDQUFDLEtBQU0sQ0FDZCxDQUNQLENBQUE7SUFDSCxDQUFDO0lBWkQscURBQXFEO0lBRTVDLHNCQUFZLEdBQUc7UUFDbEIsS0FBSyxFQUFFLEVBQUU7S0FDWixDQUFDO0lBU04sZ0JBQUM7QUFBRCxDQUFDLEFBZEQsQ0FBdUMsS0FBSyxDQUFDLFNBQVMsR0FjckQ7QUFkRDsyQkFjQyxDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnXG4vLyBpbXBvcnQgc2hvdWxkUHVyZUNvbXBvbmVudFVwZGF0ZSBmcm9tICdyZWFjdC1wdXJlLXJlbmRlci9mdW5jdGlvbidcblxuXG5pbnRlcmZhY2UgUHJvcHMge1xuICBsYWJlbDogc3RyaW5nXG59XG5cblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgSW5mb0xhYmVsIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50PFByb3BzLCBhbnk+IHtcbiAgLy8gc2hvdWxkQ29tcG9uZW50VXBkYXRlID0gc2hvdWxkUHVyZUNvbXBvbmVudFVwZGF0ZTtcblxuICAgIHN0YXRpYyBkZWZhdWx0UHJvcHMgPSB7XG4gICAgICAgIGxhYmVsOiAnJ1xuICAgIH07XG5cbiAgcmVuZGVyICgpIHtcbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9J3JjdC1pbmZvbGFiZWwnPlxuICAgICAgICB7dGhpcy5wcm9wcy5sYWJlbH1cbiAgICAgIDwvZGl2PlxuICAgIClcbiAgfVxufVxuIl19

/***/ },
/* 13 */
/*!************************************!*\
  !*** ./src/lib/layout/sidebar.tsx ***!
  \************************************/
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var React = __webpack_require__(/*! react */ 2);
	var utils_1 = __webpack_require__(/*! ../utils */ 11);
	var Sidebar = (function (_super) {
	    __extends(Sidebar, _super);
	    function Sidebar(props) {
	        _super.call(this, props);
	        this.state = {
	            scrollTop: 0,
	            componentTop: 0
	        };
	    }
	    Sidebar.prototype.shouldComponentUpdate = function (nextProps, nextState) {
	        if (nextProps.fixedHeader === 'absolute' && window && window.document && this.state.scrollTop !== nextState.scrollTop) {
	            return true;
	        }
	        return !(utils_1.arraysEqual(nextProps.groups, this.props.groups) &&
	            nextProps.keys === this.props.keys &&
	            nextProps.width === this.props.width &&
	            nextProps.lineHeight === this.props.lineHeight &&
	            nextProps.fixedHeader === this.props.fixedHeader &&
	            nextProps.zIndex === this.props.zIndex &&
	            nextProps.groupHeights === this.props.groupHeights &&
	            nextProps.height === this.props.height);
	    };
	    Sidebar.prototype.scroll = function (e) {
	        if (this.props.fixedHeader === 'absolute' && window && window.document) {
	            var scroll_1 = window.document.body.scrollTop;
	            this.setState({
	                scrollTop: scroll_1
	            });
	        }
	    };
	    Sidebar.prototype.setComponentTop = function () {
	        var viewportOffset = this.refs.sidebar.getBoundingClientRect();
	        this.setState({
	            componentTop: viewportOffset.top
	        });
	    };
	    Sidebar.prototype.componentDidMount = function () {
	        var _this = this;
	        this.setComponentTop();
	        this.scroll();
	        this.scrollEventListener = {
	            handleEvent: function (event) {
	                _this.scroll();
	            }
	        };
	        window.addEventListener('scroll', this.scrollEventListener);
	    };
	    Sidebar.prototype.componentWillUnmount = function () {
	        window.removeEventListener('scroll', this.scrollEventListener);
	    };
	    Sidebar.prototype.componentWillReceiveProps = function () {
	        this.setComponentTop();
	    };
	    Sidebar.prototype.render = function () {
	        var _a = this.props, fixedHeader = _a.fixedHeader, width = _a.width, lineHeight = _a.lineHeight, zIndex = _a.zIndex, groupHeights = _a.groupHeights, height = _a.height, headerHeight = _a.headerHeight;
	        var _b = this.props.keys, groupIdKey = _b.groupIdKey, groupTitleKey = _b.groupTitleKey;
	        var scrollTop = this.state.scrollTop;
	        var sidebarStyle = {
	            width: width + "px",
	            height: height + "px"
	        };
	        var headerStyle = {
	            height: headerHeight + "px",
	            lineHeight: lineHeight + "px",
	            width: width + "px"
	        };
	        var groupsStyle = {
	            width: width + "px"
	        };
	        if (fixedHeader === 'fixed') {
	            headerStyle['position'] = 'fixed';
	            headerStyle['zIndex'] = zIndex;
	            groupsStyle['paddingTop'] = headerStyle.height;
	        }
	        else if (fixedHeader === 'absolute') {
	            var componentTop = this.state.componentTop;
	            if (scrollTop >= componentTop) {
	                headerStyle['position'] = 'absolute';
	                headerStyle['top'] = (scrollTop - componentTop) + "px";
	                headerStyle['left'] = '0';
	                groupsStyle['paddingTop'] = headerStyle.height;
	            }
	        }
	        var header = React.createElement("div", {ref: 'sidebarHeader', className: 'rct-sidebar-header', style: headerStyle}, this.props.children);
	        var groupLines = [];
	        var i = 0;
	        this.props.groups.forEach(function (group, index) {
	            var elementStyle = {
	                height: (groupHeights[index] - 1) + "px",
	                lineHeight: (groupHeights[index] - 1) + "px"
	            };
	            groupLines.push(React.createElement("div", {key: utils_1._get(group, groupIdKey), className: 'rct-sidebar-row' + (i % 2 === 0 ? ' rct-sidebar-row-even' : ' rct-sidebar-row-odd'), style: elementStyle}, utils_1._get(group, groupTitleKey)));
	            i += 1;
	        });
	        return (React.createElement("div", {ref: 'sidebar', className: 'rct-sidebar', style: sidebarStyle}, header, React.createElement("div", {style: groupsStyle}, groupLines)));
	    };
	    Sidebar.defaultProps = {
	        fixedHeader: 'none',
	        zIndex: 12,
	        children: null
	    };
	    return Sidebar;
	}(React.Component));
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.default = Sidebar;
	//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2lkZWJhci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNpZGViYXIudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLElBQVksS0FBSyxXQUFNLE9BRXZCLENBQUMsQ0FGNkI7QUFFOUIsc0JBQWdDLFVBR2hDLENBQUMsQ0FIeUM7QUFnQjFDO0lBQXFDLDJCQUEyQjtJQWM5RCxpQkFBYSxLQUFLO1FBQ2hCLGtCQUFNLEtBQUssQ0FBQyxDQUFDO1FBQ2IsSUFBSSxDQUFDLEtBQUssR0FBRztZQUNYLFNBQVMsRUFBRSxDQUFDO1lBQ1osWUFBWSxFQUFFLENBQUM7U0FDaEIsQ0FBQTtJQUNILENBQUM7SUFFRCx1Q0FBcUIsR0FBckIsVUFBdUIsU0FBUyxFQUFFLFNBQVM7UUFDekMsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLFdBQVcsS0FBSyxVQUFVLElBQUksTUFBTSxJQUFJLE1BQU0sQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLEtBQUssU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDdEgsTUFBTSxDQUFDLElBQUksQ0FBQTtRQUNiLENBQUM7UUFFRCxNQUFNLENBQUMsQ0FBQyxDQUFDLG1CQUFXLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQztZQUNoRCxTQUFTLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSTtZQUNsQyxTQUFTLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSztZQUNwQyxTQUFTLENBQUMsVUFBVSxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVTtZQUM5QyxTQUFTLENBQUMsV0FBVyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVztZQUNoRCxTQUFTLENBQUMsTUFBTSxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTTtZQUN0QyxTQUFTLENBQUMsWUFBWSxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWTtZQUNsRCxTQUFTLENBQUMsTUFBTSxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUE7SUFDbEQsQ0FBQztJQUVELHdCQUFNLEdBQU4sVUFBUSxDQUFFO1FBQ1IsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLEtBQUssVUFBVSxJQUFJLE1BQU0sSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUN2RSxJQUFNLFFBQU0sR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7WUFDOUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztnQkFDWixTQUFTLEVBQUUsUUFBTTthQUNsQixDQUFDLENBQUE7UUFDSixDQUFDO0lBQ0gsQ0FBQztJQUVELGlDQUFlLEdBQWY7UUFDRSxJQUFNLGNBQWMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO1FBQ2pFLElBQUksQ0FBQyxRQUFRLENBQUM7WUFDWixZQUFZLEVBQUUsY0FBYyxDQUFDLEdBQUc7U0FDakMsQ0FBQyxDQUFBO0lBQ0osQ0FBQztJQUVELG1DQUFpQixHQUFqQjtRQUFBLGlCQVdDO1FBVkMsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUVkLElBQUksQ0FBQyxtQkFBbUIsR0FBRztZQUN6QixXQUFXLEVBQUUsVUFBQyxLQUFLO2dCQUNqQixLQUFJLENBQUMsTUFBTSxFQUFFLENBQUE7WUFDZixDQUFDO1NBQ0YsQ0FBQztRQUVGLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUE7SUFDN0QsQ0FBQztJQUVELHNDQUFvQixHQUFwQjtRQUNFLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUE7SUFDaEUsQ0FBQztJQUVELDJDQUF5QixHQUF6QjtRQUNFLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQTtJQUN4QixDQUFDO0lBRUQsd0JBQU0sR0FBTjtRQUNFLElBQUEsZUFFYyxFQURaLDRCQUFXLEVBQUUsZ0JBQUssRUFBRSwwQkFBVSxFQUFFLGtCQUFNLEVBQUUsOEJBQVksRUFBRSxrQkFBTSxFQUFFLDhCQUFZLENBQzdEO1FBRWYsSUFBQSxvQkFBbUQsRUFBNUMsMEJBQVUsRUFBRSxnQ0FBYSxDQUFvQjtRQUdsRCxvQ0FBUyxDQUNJO1FBRWYsSUFBTSxZQUFZLEdBQUc7WUFDbkIsS0FBSyxFQUFLLEtBQUssT0FBSTtZQUNuQixNQUFNLEVBQUssTUFBTSxPQUFJO1NBQ3RCLENBQUM7UUFFRixJQUFNLFdBQVcsR0FBRztZQUNsQixNQUFNLEVBQUssWUFBWSxPQUFJO1lBQzNCLFVBQVUsRUFBSyxVQUFVLE9BQUk7WUFDN0IsS0FBSyxFQUFLLEtBQUssT0FBSTtTQUNwQixDQUFDO1FBRUYsSUFBTSxXQUFXLEdBQUc7WUFDbEIsS0FBSyxFQUFLLEtBQUssT0FBSTtTQUNwQixDQUFDO1FBRUYsRUFBRSxDQUFDLENBQUMsV0FBVyxLQUFLLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDNUIsV0FBVyxDQUFDLFVBQVUsQ0FBQyxHQUFHLE9BQU8sQ0FBQztZQUNsQyxXQUFXLENBQUMsUUFBUSxDQUFDLEdBQUcsTUFBTSxDQUFDO1lBQy9CLFdBQVcsQ0FBQyxZQUFZLENBQUMsR0FBRyxXQUFXLENBQUMsTUFBTSxDQUFBO1FBQ2hELENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsV0FBVyxLQUFLLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDdEMsSUFBSSxZQUFZLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUM7WUFDM0MsRUFBRSxDQUFDLENBQUMsU0FBUyxJQUFJLFlBQVksQ0FBQyxDQUFDLENBQUM7Z0JBQzlCLFdBQVcsQ0FBQyxVQUFVLENBQUMsR0FBRyxVQUFVLENBQUM7Z0JBQ3JDLFdBQVcsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFHLFNBQVMsR0FBRyxZQUFZLFFBQUksQ0FBQztnQkFDckQsV0FBVyxDQUFDLE1BQU0sQ0FBQyxHQUFHLEdBQUcsQ0FBQztnQkFDMUIsV0FBVyxDQUFDLFlBQVksQ0FBQyxHQUFHLFdBQVcsQ0FBQyxNQUFNLENBQUE7WUFDaEQsQ0FBQztRQUNILENBQUM7UUFFRCxJQUFNLE1BQU0sR0FBRyxxQkFBQyxHQUFHLElBQUMsR0FBRyxFQUFDLGVBQWUsRUFBQyxTQUFTLEVBQUMsb0JBQW9CLEVBQUMsS0FBSyxFQUFFLFdBQVksR0FDeEUsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFTLENBQ2pCLENBQUM7UUFFdEIsSUFBSSxVQUFVLEdBQUcsRUFBRSxDQUFDO1FBQ3BCLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUVWLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxVQUFDLEtBQUssRUFBRSxLQUFLO1lBQ3JDLElBQU0sWUFBWSxHQUFHO2dCQUNuQixNQUFNLEVBQUUsQ0FBRyxZQUFZLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxRQUFJO2dCQUN0QyxVQUFVLEVBQUUsQ0FBRyxZQUFZLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxRQUFJO2FBQzNDLENBQUM7WUFFRixVQUFVLENBQUMsSUFBSSxDQUNiLHFCQUFDLEdBQUcsSUFBQyxHQUFHLEVBQUUsWUFBSSxDQUFDLEtBQUssRUFBRSxVQUFVLENBQUUsRUFBQyxTQUFTLEVBQUUsaUJBQWlCLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyx1QkFBdUIsR0FBRyxzQkFBc0IsQ0FBRSxFQUFDLEtBQUssRUFBRSxZQUFhLEdBQ3JKLFlBQUksQ0FBQyxLQUFLLEVBQUUsYUFBYSxDQUFFLENBQ3hCLENBQ1AsQ0FBQztZQUNGLENBQUMsSUFBSSxDQUFDLENBQUE7UUFDUixDQUFDLENBQUMsQ0FBQztRQUVILE1BQU0sQ0FBQyxDQUNMLHFCQUFDLEdBQUcsSUFBQyxHQUFHLEVBQUMsU0FBUyxFQUFDLFNBQVMsRUFBQyxhQUFhLEVBQUMsS0FBSyxFQUFFLFlBQWEsR0FDNUQsTUFBTyxFQUNSLHFCQUFDLEdBQUcsSUFBQyxLQUFLLEVBQUUsV0FBWSxHQUNyQixVQUFXLENBQ1IsQ0FDRixDQUNQLENBQUE7SUFDSCxDQUFDO0lBdklRLG9CQUFZLEdBQUc7UUFDbEIsV0FBVyxFQUFFLE1BQU07UUFDbkIsTUFBTSxFQUFFLEVBQUU7UUFDVixRQUFRLEVBQUUsSUFBSTtLQUNqQixDQUFDO0lBb0lOLGNBQUM7QUFBRCxDQUFDLEFBaEpELENBQXFDLEtBQUssQ0FBQyxTQUFTLEdBZ0puRDtBQWhKRDt5QkFnSkMsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0J1xuXG5pbXBvcnQge19nZXQsIGFycmF5c0VxdWFsfSBmcm9tICcuLi91dGlscydcblxuXG5pbnRlcmZhY2UgUHJvcHMge1xuICAgIGdyb3Vwczoge1twcm9wOiBzdHJpbmddOiBzdHJpbmd9W11cbiAgICB3aWR0aDogbnVtYmVyXG4gICAgbGluZUhlaWdodDogbnVtYmVyXG4gICAgekluZGV4OiBudW1iZXJcbiAgICBmaXhlZEhlYWRlcjogJ2ZpeGVkJyB8ICdhYnNvbHV0ZScgfCAnbm9uZSdcbiAgICBrZXlzOiB7W2tleTogc3RyaW5nXTogc3RyaW5nfVxuICAgIGhlYWRlckhlaWdodD9cbiAgICBncm91cEhlaWdodHM/XG4gICAgaGVpZ2h0P1xufVxuXG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNpZGViYXIgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQ8UHJvcHMsIGFueT4ge1xuICAgIHNjcm9sbEV2ZW50TGlzdGVuZXI7XG4gICAgcmVmczoge1xuICAgICAgICBzaWRlYmFyOiAoRWxlbWVudClcbiAgICAgICAgW2tleTogc3RyaW5nXTogKEVsZW1lbnQpO1xuICAgICAgICBzdGVwSW5wdXQ6IChIVE1MSW5wdXRFbGVtZW50KTtcbiAgICB9O1xuXG4gICAgc3RhdGljIGRlZmF1bHRQcm9wcyA9IHtcbiAgICAgICAgZml4ZWRIZWFkZXI6ICdub25lJyxcbiAgICAgICAgekluZGV4OiAxMixcbiAgICAgICAgY2hpbGRyZW46IG51bGxcbiAgICB9O1xuXG4gIGNvbnN0cnVjdG9yIChwcm9wcykge1xuICAgIHN1cGVyKHByb3BzKTtcbiAgICB0aGlzLnN0YXRlID0ge1xuICAgICAgc2Nyb2xsVG9wOiAwLFxuICAgICAgY29tcG9uZW50VG9wOiAwXG4gICAgfVxuICB9XG5cbiAgc2hvdWxkQ29tcG9uZW50VXBkYXRlIChuZXh0UHJvcHMsIG5leHRTdGF0ZSkge1xuICAgIGlmIChuZXh0UHJvcHMuZml4ZWRIZWFkZXIgPT09ICdhYnNvbHV0ZScgJiYgd2luZG93ICYmIHdpbmRvdy5kb2N1bWVudCAmJiB0aGlzLnN0YXRlLnNjcm9sbFRvcCAhPT0gbmV4dFN0YXRlLnNjcm9sbFRvcCkge1xuICAgICAgcmV0dXJuIHRydWVcbiAgICB9XG5cbiAgICByZXR1cm4gIShhcnJheXNFcXVhbChuZXh0UHJvcHMuZ3JvdXBzLCB0aGlzLnByb3BzLmdyb3VwcykgJiZcbiAgICAgICAgICAgICBuZXh0UHJvcHMua2V5cyA9PT0gdGhpcy5wcm9wcy5rZXlzICYmXG4gICAgICAgICAgICAgbmV4dFByb3BzLndpZHRoID09PSB0aGlzLnByb3BzLndpZHRoICYmXG4gICAgICAgICAgICAgbmV4dFByb3BzLmxpbmVIZWlnaHQgPT09IHRoaXMucHJvcHMubGluZUhlaWdodCAmJlxuICAgICAgICAgICAgIG5leHRQcm9wcy5maXhlZEhlYWRlciA9PT0gdGhpcy5wcm9wcy5maXhlZEhlYWRlciAmJlxuICAgICAgICAgICAgIG5leHRQcm9wcy56SW5kZXggPT09IHRoaXMucHJvcHMuekluZGV4ICYmXG4gICAgICAgICAgICAgbmV4dFByb3BzLmdyb3VwSGVpZ2h0cyA9PT0gdGhpcy5wcm9wcy5ncm91cEhlaWdodHMgJiZcbiAgICAgICAgICAgICBuZXh0UHJvcHMuaGVpZ2h0ID09PSB0aGlzLnByb3BzLmhlaWdodClcbiAgfVxuXG4gIHNjcm9sbCAoZT8pIHtcbiAgICBpZiAodGhpcy5wcm9wcy5maXhlZEhlYWRlciA9PT0gJ2Fic29sdXRlJyAmJiB3aW5kb3cgJiYgd2luZG93LmRvY3VtZW50KSB7XG4gICAgICBjb25zdCBzY3JvbGwgPSB3aW5kb3cuZG9jdW1lbnQuYm9keS5zY3JvbGxUb3A7XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgc2Nyb2xsVG9wOiBzY3JvbGxcbiAgICAgIH0pXG4gICAgfVxuICB9XG5cbiAgc2V0Q29tcG9uZW50VG9wICgpIHtcbiAgICBjb25zdCB2aWV3cG9ydE9mZnNldCA9IHRoaXMucmVmcy5zaWRlYmFyLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xuICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgY29tcG9uZW50VG9wOiB2aWV3cG9ydE9mZnNldC50b3BcbiAgICB9KVxuICB9XG5cbiAgY29tcG9uZW50RGlkTW91bnQgKCkge1xuICAgIHRoaXMuc2V0Q29tcG9uZW50VG9wKCk7XG4gICAgdGhpcy5zY3JvbGwoKTtcblxuICAgIHRoaXMuc2Nyb2xsRXZlbnRMaXN0ZW5lciA9IHtcbiAgICAgIGhhbmRsZUV2ZW50OiAoZXZlbnQpID0+IHtcbiAgICAgICAgdGhpcy5zY3JvbGwoKVxuICAgICAgfVxuICAgIH07XG5cbiAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignc2Nyb2xsJywgdGhpcy5zY3JvbGxFdmVudExpc3RlbmVyKVxuICB9XG5cbiAgY29tcG9uZW50V2lsbFVubW91bnQgKCkge1xuICAgIHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKCdzY3JvbGwnLCB0aGlzLnNjcm9sbEV2ZW50TGlzdGVuZXIpXG4gIH1cblxuICBjb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzICgpIHtcbiAgICB0aGlzLnNldENvbXBvbmVudFRvcCgpXG4gIH1cblxuICByZW5kZXIgKCkge1xuICAgIGNvbnN0IHtcbiAgICAgIGZpeGVkSGVhZGVyLCB3aWR0aCwgbGluZUhlaWdodCwgekluZGV4LCBncm91cEhlaWdodHMsIGhlaWdodCwgaGVhZGVySGVpZ2h0XG4gICAgfSA9IHRoaXMucHJvcHM7XG5cbiAgICBjb25zdCB7Z3JvdXBJZEtleSwgZ3JvdXBUaXRsZUtleX0gPSB0aGlzLnByb3BzLmtleXM7XG5cbiAgICBjb25zdCB7XG4gICAgICBzY3JvbGxUb3BcbiAgICB9ID0gdGhpcy5zdGF0ZTtcblxuICAgIGNvbnN0IHNpZGViYXJTdHlsZSA9IHtcbiAgICAgIHdpZHRoOiBgJHt3aWR0aH1weGAsXG4gICAgICBoZWlnaHQ6IGAke2hlaWdodH1weGBcbiAgICB9O1xuXG4gICAgY29uc3QgaGVhZGVyU3R5bGUgPSB7XG4gICAgICBoZWlnaHQ6IGAke2hlYWRlckhlaWdodH1weGAsXG4gICAgICBsaW5lSGVpZ2h0OiBgJHtsaW5lSGVpZ2h0fXB4YCxcbiAgICAgIHdpZHRoOiBgJHt3aWR0aH1weGBcbiAgICB9O1xuXG4gICAgY29uc3QgZ3JvdXBzU3R5bGUgPSB7XG4gICAgICB3aWR0aDogYCR7d2lkdGh9cHhgXG4gICAgfTtcblxuICAgIGlmIChmaXhlZEhlYWRlciA9PT0gJ2ZpeGVkJykge1xuICAgICAgaGVhZGVyU3R5bGVbJ3Bvc2l0aW9uJ10gPSAnZml4ZWQnO1xuICAgICAgaGVhZGVyU3R5bGVbJ3pJbmRleCddID0gekluZGV4O1xuICAgICAgZ3JvdXBzU3R5bGVbJ3BhZGRpbmdUb3AnXSA9IGhlYWRlclN0eWxlLmhlaWdodFxuICAgIH0gZWxzZSBpZiAoZml4ZWRIZWFkZXIgPT09ICdhYnNvbHV0ZScpIHtcbiAgICAgIGxldCBjb21wb25lbnRUb3AgPSB0aGlzLnN0YXRlLmNvbXBvbmVudFRvcDtcbiAgICAgIGlmIChzY3JvbGxUb3AgPj0gY29tcG9uZW50VG9wKSB7XG4gICAgICAgIGhlYWRlclN0eWxlWydwb3NpdGlvbiddID0gJ2Fic29sdXRlJztcbiAgICAgICAgaGVhZGVyU3R5bGVbJ3RvcCddID0gYCR7c2Nyb2xsVG9wIC0gY29tcG9uZW50VG9wfXB4YDtcbiAgICAgICAgaGVhZGVyU3R5bGVbJ2xlZnQnXSA9ICcwJztcbiAgICAgICAgZ3JvdXBzU3R5bGVbJ3BhZGRpbmdUb3AnXSA9IGhlYWRlclN0eWxlLmhlaWdodFxuICAgICAgfVxuICAgIH1cblxuICAgIGNvbnN0IGhlYWRlciA9IDxkaXYgcmVmPSdzaWRlYmFySGVhZGVyJyBjbGFzc05hbWU9J3JjdC1zaWRlYmFyLWhlYWRlcicgc3R5bGU9e2hlYWRlclN0eWxlfT5cbiAgICAgICAgICAgICAgICAgICAgIHt0aGlzLnByb3BzLmNoaWxkcmVufVxuICAgICAgICAgICAgICAgICAgIDwvZGl2PjtcblxuICAgIGxldCBncm91cExpbmVzID0gW107XG4gICAgbGV0IGkgPSAwO1xuXG4gICAgdGhpcy5wcm9wcy5ncm91cHMuZm9yRWFjaCgoZ3JvdXAsIGluZGV4KSA9PiB7XG4gICAgICBjb25zdCBlbGVtZW50U3R5bGUgPSB7XG4gICAgICAgIGhlaWdodDogYCR7Z3JvdXBIZWlnaHRzW2luZGV4XSAtIDF9cHhgLFxuICAgICAgICBsaW5lSGVpZ2h0OiBgJHtncm91cEhlaWdodHNbaW5kZXhdIC0gMX1weGBcbiAgICAgIH07XG5cbiAgICAgIGdyb3VwTGluZXMucHVzaChcbiAgICAgICAgPGRpdiBrZXk9e19nZXQoZ3JvdXAsIGdyb3VwSWRLZXkpfSBjbGFzc05hbWU9eydyY3Qtc2lkZWJhci1yb3cnICsgKGkgJSAyID09PSAwID8gJyByY3Qtc2lkZWJhci1yb3ctZXZlbicgOiAnIHJjdC1zaWRlYmFyLXJvdy1vZGQnKX0gc3R5bGU9e2VsZW1lbnRTdHlsZX0+XG4gICAgICAgICAge19nZXQoZ3JvdXAsIGdyb3VwVGl0bGVLZXkpfVxuICAgICAgICA8L2Rpdj5cbiAgICAgICk7XG4gICAgICBpICs9IDFcbiAgICB9KTtcblxuICAgIHJldHVybiAoXG4gICAgICA8ZGl2IHJlZj0nc2lkZWJhcicgY2xhc3NOYW1lPSdyY3Qtc2lkZWJhcicgc3R5bGU9e3NpZGViYXJTdHlsZX0+XG4gICAgICAgIHtoZWFkZXJ9XG4gICAgICAgIDxkaXYgc3R5bGU9e2dyb3Vwc1N0eWxlfT5cbiAgICAgICAgICB7Z3JvdXBMaW5lc31cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICApXG4gIH1cbn1cbiJdfQ==

/***/ },
/* 14 */
/*!***********************************!*\
  !*** ./src/lib/layout/header.tsx ***!
  \***********************************/
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var React = __webpack_require__(/*! react */ 2);
	var moment = __webpack_require__(/*! moment */ 3);
	var utils_1 = __webpack_require__(/*! ../utils */ 11);
	var Header = (function (_super) {
	    __extends(Header, _super);
	    function Header(props) {
	        var _this = this;
	        _super.call(this, props);
	        this.periodClick = function (e) {
	            var _a = e.target.dataset, time = _a.time, unit = _a.unit;
	            if (time && unit) {
	                _this.props.showPeriod(moment(time - 0), unit);
	            }
	        };
	        this.touchStart = function (e) {
	            if (e.touches.length === 1) {
	                _this.setState({
	                    touchTarget: e.target || e.touchTarget,
	                    touchActive: true
	                });
	            }
	        };
	        this.touchEnd = function (e) {
	            if (!_this.state.touchActive) {
	                return _this.resetTouchState();
	            }
	            var changedTouches = e.changedTouches[0];
	            if (changedTouches) {
	                var elem = document.elementFromPoint(changedTouches.pageX, changedTouches.pageY);
	                if (elem !== _this.state.touchTarget) {
	                    return _this.resetTouchState();
	                }
	            }
	            _this.resetTouchState();
	            _this.periodClick(e);
	        };
	        this.state = {
	            scrollTop: 0,
	            componentTop: 0,
	            touchTarget: null,
	            touchActive: false
	        };
	    }
	    Header.prototype.scroll = function (e) {
	        if (this.props.fixedHeader === 'absolute' && window && window.document) {
	            var scroll_1 = window.document.body.scrollTop;
	            this.setState({
	                scrollTop: scroll_1
	            });
	        }
	    };
	    Header.prototype.setComponentTop = function () {
	        var viewportOffset = this.refs.header.getBoundingClientRect();
	        this.setState({
	            componentTop: viewportOffset.top
	        });
	    };
	    Header.prototype.componentDidMount = function () {
	        var _this = this;
	        this.setComponentTop();
	        this.scroll();
	        this.scrollEventListener = {
	            handleEvent: function (event) {
	                _this.scroll();
	            }
	        };
	        window.addEventListener('scroll', this.scrollEventListener);
	    };
	    Header.prototype.componentWillUnmount = function () {
	        window.removeEventListener('scroll', this.scrollEventListener);
	    };
	    Header.prototype.componentWillReceiveProps = function () {
	        this.setComponentTop();
	    };
	    Header.prototype.headerLabel = function (time, unit, width) {
	        if (unit === 'year') {
	            return time.format(width < 46 ? 'YY' : 'YYYY');
	        }
	        else if (unit === 'month') {
	            return time.format(width < 65 ? 'MM/YY' : width < 75 ? 'MM/YYYY' : width < 120 ? 'MMM YYYY' : 'MMMM YYYY');
	        }
	        else if (unit === 'day') {
	            return time.format(width < 150 ? 'L' : 'dddd, LL');
	        }
	        else if (unit === 'hour') {
	            return time.format(width < 50 ? 'HH' : width < 130 ? 'HH:00' : width < 150 ? 'L, HH:00' : 'dddd, LL, HH:00');
	        }
	        else {
	            return time.format('LLL');
	        }
	    };
	    Header.prototype.subHeaderLabel = function (time, unit, width) {
	        if (unit === 'year') {
	            return time.format(width < 46 ? 'YY' : 'YYYY');
	        }
	        else if (unit === 'month') {
	            return time.format(width < 37 ? 'MM' : width < 85 ? 'MMM' : 'MMMM');
	        }
	        else if (unit === 'day') {
	            return time.format(width < 47 ? 'D' : width < 80 ? 'dd D' : width < 120 ? 'ddd, Do' : 'dddd, Do');
	        }
	        else if (unit === 'hour') {
	            return time.format(width < 50 ? 'HH' : 'HH:00');
	        }
	        else if (unit === 'minute') {
	            return time.format(width < 60 ? 'mm' : 'HH:mm');
	        }
	        else {
	            return time.get(unit);
	        }
	    };
	    Header.prototype.resetTouchState = function () {
	        this.setState({
	            touchTarget: null,
	            touchActive: false
	        });
	    };
	    Header.prototype.render = function () {
	        var _this = this;
	        var timeLabels = [];
	        var _a = this.props, canvasTimeStart = _a.canvasTimeStart, canvasTimeEnd = _a.canvasTimeEnd, canvasWidth = _a.canvasWidth, lineHeight = _a.lineHeight, visibleTimeStart = _a.visibleTimeStart, visibleTimeEnd = _a.visibleTimeEnd, minUnit = _a.minUnit, timeSteps = _a.timeSteps, fixedHeader = _a.fixedHeader, headerLabelGroupHeight = _a.headerLabelGroupHeight, headerLabelHeight = _a.headerLabelHeight;
	        var scrollTop = this.state.scrollTop;
	        var ratio = canvasWidth / (canvasTimeEnd - canvasTimeStart);
	        var twoHeaders = minUnit !== 'year';
	        // add the top header
	        if (twoHeaders) {
	            var nextUnit_1 = utils_1.getNextUnit(minUnit);
	            utils_1.iterateTimes(visibleTimeStart, visibleTimeEnd, nextUnit_1, timeSteps, function (time, nextTime) {
	                var startTime = Math.max(visibleTimeStart, time.valueOf());
	                var endTime = Math.min(visibleTimeEnd, nextTime.valueOf());
	                var left = Math.round((startTime.valueOf() - canvasTimeStart) * ratio, -2);
	                var right = Math.round((endTime.valueOf() - canvasTimeStart) * ratio, -2);
	                var labelWidth = right - left;
	                var leftCorrect = fixedHeader === 'fixed' ? Math.round((canvasTimeStart - visibleTimeStart) * ratio) - 1 : 0;
	                timeLabels.push(React.createElement("div", {key: "top-label-" + time.valueOf(), href: '#', className: 'rct-label-group', "data-time": time, "data-unit": nextUnit_1, style: {
	                    left: (left + leftCorrect) + "px",
	                    width: labelWidth + "px",
	                    height: headerLabelGroupHeight + "px",
	                    lineHeight: headerLabelGroupHeight + "px",
	                    cursor: 'pointer'
	                }}, _this.headerLabel(time, nextUnit_1, labelWidth)));
	            });
	        }
	        utils_1.iterateTimes(canvasTimeStart, canvasTimeEnd, minUnit, timeSteps, function (time, nextTime) {
	            var left = Math.round((time.valueOf() - canvasTimeStart) * ratio, -2);
	            var minUnitValue = time.get(minUnit === 'day' ? 'date' : minUnit);
	            var firstOfType = minUnitValue === (minUnit === 'day' ? 1 : 0);
	            var labelWidth = Math.round((nextTime.valueOf() - time.valueOf()) * ratio, -2);
	            var borderWidth = firstOfType ? 2 : 1;
	            var leftCorrect = fixedHeader === 'fixed' ? Math.round((canvasTimeStart - visibleTimeStart) * ratio) - borderWidth + 1 : 0;
	            timeLabels.push(React.createElement("div", {key: "label-" + time.valueOf(), href: '#', className: "rct-label " + (twoHeaders ? '' : 'rct-label-only') + " " + (firstOfType ? 'rct-first-of-type' : '') + " ", "data-time": time, "data-unit": minUnit, style: {
	                top: (minUnit === 'year' ? 0 : headerLabelGroupHeight) + "px",
	                left: (left + leftCorrect) + "px",
	                width: labelWidth + "px",
	                height: (minUnit === 'year' ? headerLabelGroupHeight + headerLabelHeight : headerLabelHeight) + "px",
	                lineHeight: (minUnit === 'year' ? headerLabelGroupHeight + headerLabelHeight : headerLabelHeight) + "px",
	                fontSize: (labelWidth > 30 ? '14' : labelWidth > 20 ? '12' : '10') + "px",
	                cursor: 'pointer'
	            }}, _this.subHeaderLabel(time, minUnit, labelWidth)));
	        });
	        var zIndex = this.props.zIndex;
	        var headerStyle = {
	            height: (headerLabelGroupHeight + headerLabelHeight) + "px",
	            lineHeight: lineHeight + "px"
	        };
	        if (fixedHeader === 'fixed') {
	            headerStyle['position'] = 'fixed';
	            headerStyle['width'] = '100%';
	            headerStyle['zIndex'] = zIndex;
	        }
	        else if (fixedHeader === 'absolute') {
	            var componentTop = this.state.componentTop;
	            if (scrollTop >= componentTop) {
	                headerStyle['position'] = 'absolute';
	                headerStyle['top'] = (scrollTop - componentTop) + "px";
	                headerStyle['width'] = canvasWidth + "px";
	                headerStyle['left'] = '0';
	            }
	        }
	        return (React.createElement("div", {ref: 'header', key: 'header', className: 'rct-header', onTouchStart: this.touchStart, onTouchEnd: this.touchEnd, onClick: this.periodClick, style: headerStyle}, timeLabels));
	    };
	    Header.defaultProps = {
	        fixedHeader: 'none',
	        zIndex: 11
	    };
	    return Header;
	}(React.Component));
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.default = Header;
	//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVhZGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiaGVhZGVyLnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQSxJQUFZLEtBQUssV0FBTSxPQUN2QixDQUFDLENBRDZCO0FBQzlCLElBQVksTUFBTSxXQUFNLFFBRXhCLENBQUMsQ0FGK0I7QUFFaEMsc0JBQXdDLFVBR3hDLENBQUMsQ0FIaUQ7QUF3QmxEO0lBQW9DLDBCQUEyQjtJQVk3RCxnQkFBYSxLQUFLO1FBWnBCLGlCQW9PQztRQXZORyxrQkFBTSxLQUFLLENBQUMsQ0FBQztRQTRFZixnQkFBVyxHQUFHLFVBQUMsQ0FBQztZQUNkLElBQUEscUJBQXFDLEVBQTlCLGNBQUksRUFBRSxjQUFJLENBQW9CO1lBQ3JDLEVBQUUsQ0FBQyxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUNqQixLQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFBO1lBQy9DLENBQUM7UUFDSCxDQUFDLENBQUE7UUFFRCxlQUFVLEdBQUcsVUFBQyxDQUFDO1lBQ2IsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDM0IsS0FBSSxDQUFDLFFBQVEsQ0FBQztvQkFDWixXQUFXLEVBQUUsQ0FBQyxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsV0FBVztvQkFDdEMsV0FBVyxFQUFFLElBQUk7aUJBQ2xCLENBQUMsQ0FBQTtZQUNKLENBQUM7UUFDSCxDQUFDLENBQUE7UUFFRCxhQUFRLEdBQUcsVUFBQyxDQUFDO1lBQ1gsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7Z0JBQzVCLE1BQU0sQ0FBQyxLQUFJLENBQUMsZUFBZSxFQUFFLENBQUE7WUFDL0IsQ0FBQztZQUVELElBQUksY0FBYyxHQUFHLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUE7WUFDeEMsRUFBRSxDQUFDLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztnQkFDbkIsSUFBSSxJQUFJLEdBQUcsUUFBUSxDQUFDLGdCQUFnQixDQUFDLGNBQWMsQ0FBQyxLQUFLLEVBQUUsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFBO2dCQUNoRixFQUFFLENBQUMsQ0FBQyxJQUFJLEtBQUssS0FBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO29CQUNwQyxNQUFNLENBQUMsS0FBSSxDQUFDLGVBQWUsRUFBRSxDQUFBO2dCQUMvQixDQUFDO1lBQ0gsQ0FBQztZQUVELEtBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQTtZQUN0QixLQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFBO1FBQ3JCLENBQUMsQ0FBQTtRQTFHQyxJQUFJLENBQUMsS0FBSyxHQUFHO1lBQ1gsU0FBUyxFQUFFLENBQUM7WUFDWixZQUFZLEVBQUUsQ0FBQztZQUNmLFdBQVcsRUFBRSxJQUFJO1lBQ2pCLFdBQVcsRUFBRSxLQUFLO1NBQ25CLENBQUE7SUFDSCxDQUFDO0lBRUQsdUJBQU0sR0FBTixVQUFRLENBQUU7UUFDUixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsS0FBSyxVQUFVLElBQUksTUFBTSxJQUFJLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ3ZFLElBQU0sUUFBTSxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztZQUM5QyxJQUFJLENBQUMsUUFBUSxDQUFDO2dCQUNaLFNBQVMsRUFBRSxRQUFNO2FBQ2xCLENBQUMsQ0FBQTtRQUNKLENBQUM7SUFDSCxDQUFDO0lBRUQsZ0NBQWUsR0FBZjtRQUNFLElBQU0sY0FBYyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLHFCQUFxQixFQUFFLENBQUM7UUFDaEUsSUFBSSxDQUFDLFFBQVEsQ0FBQztZQUNaLFlBQVksRUFBRSxjQUFjLENBQUMsR0FBRztTQUNqQyxDQUFDLENBQUE7SUFDSixDQUFDO0lBRUQsa0NBQWlCLEdBQWpCO1FBQUEsaUJBV0M7UUFWQyxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDdkIsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBRWQsSUFBSSxDQUFDLG1CQUFtQixHQUFHO1lBQ3pCLFdBQVcsRUFBRSxVQUFDLEtBQUs7Z0JBQ2pCLEtBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQTtZQUNmLENBQUM7U0FDRixDQUFDO1FBRUYsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQTtJQUM3RCxDQUFDO0lBRUQscUNBQW9CLEdBQXBCO1FBQ0UsTUFBTSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQTtJQUNoRSxDQUFDO0lBRUQsMENBQXlCLEdBQXpCO1FBQ0UsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFBO0lBQ3hCLENBQUM7SUFFRCw0QkFBVyxHQUFYLFVBQWEsSUFBSSxFQUFFLElBQUksRUFBRSxLQUFLO1FBQzVCLEVBQUUsQ0FBQyxDQUFDLElBQUksS0FBSyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ3BCLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxFQUFFLEdBQUcsSUFBSSxHQUFHLE1BQU0sQ0FBQyxDQUFBO1FBQ2hELENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxLQUFLLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDNUIsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLEVBQUUsR0FBRyxPQUFPLEdBQUcsS0FBSyxHQUFHLEVBQUUsR0FBRyxTQUFTLEdBQUcsS0FBSyxHQUFHLEdBQUcsR0FBRyxVQUFVLEdBQUcsV0FBVyxDQUFDLENBQUE7UUFDNUcsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLEtBQUssS0FBSyxDQUFDLENBQUMsQ0FBQztZQUMxQixNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsR0FBRyxHQUFHLEdBQUcsR0FBRyxVQUFVLENBQUMsQ0FBQTtRQUNwRCxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksS0FBSyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQzNCLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxFQUFFLEdBQUcsSUFBSSxHQUFHLEtBQUssR0FBRyxHQUFHLEdBQUcsT0FBTyxHQUFHLEtBQUssR0FBRyxHQUFHLEdBQUcsVUFBVSxHQUFHLGlCQUFpQixDQUFDLENBQUE7UUFDOUcsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUE7UUFDM0IsQ0FBQztJQUNILENBQUM7SUFFRCwrQkFBYyxHQUFkLFVBQWdCLElBQUksRUFBRSxJQUFJLEVBQUUsS0FBSztRQUMvQixFQUFFLENBQUMsQ0FBQyxJQUFJLEtBQUssTUFBTSxDQUFDLENBQUMsQ0FBQztZQUNwQixNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsRUFBRSxHQUFHLElBQUksR0FBRyxNQUFNLENBQUMsQ0FBQTtRQUNoRCxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksS0FBSyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQzVCLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxFQUFFLEdBQUcsSUFBSSxHQUFHLEtBQUssR0FBRyxFQUFFLEdBQUcsS0FBSyxHQUFHLE1BQU0sQ0FBQyxDQUFBO1FBQ3JFLENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxLQUFLLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDMUIsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLEVBQUUsR0FBRyxHQUFHLEdBQUcsS0FBSyxHQUFHLEVBQUUsR0FBRyxNQUFNLEdBQUcsS0FBSyxHQUFHLEdBQUcsR0FBRyxTQUFTLEdBQUcsVUFBVSxDQUFDLENBQUE7UUFDbkcsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLEtBQUssTUFBTSxDQUFDLENBQUMsQ0FBQztZQUMzQixNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsRUFBRSxHQUFHLElBQUksR0FBRyxPQUFPLENBQUMsQ0FBQTtRQUNqRCxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQzdCLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxFQUFFLEdBQUcsSUFBSSxHQUFHLE9BQU8sQ0FBQyxDQUFBO1FBQ2pELENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFBO1FBQ3ZCLENBQUM7SUFDSCxDQUFDO0lBbUNELGdDQUFlLEdBQWY7UUFDRSxJQUFJLENBQUMsUUFBUSxDQUFDO1lBQ1osV0FBVyxFQUFFLElBQUk7WUFDakIsV0FBVyxFQUFFLEtBQUs7U0FDbkIsQ0FBQyxDQUFBO0lBQ0osQ0FBQztJQUVELHVCQUFNLEdBQU47UUFBQSxpQkFrR0M7UUFqR0MsSUFBSSxVQUFVLEdBQUcsRUFBRSxDQUFBO1FBQ25CLElBQUEsZUFJYyxFQUhaLG9DQUFlLEVBQUUsZ0NBQWEsRUFBRSw0QkFBVyxFQUFFLDBCQUFVLEVBQ3ZELHNDQUFnQixFQUFFLGtDQUFjLEVBQUUsb0JBQU8sRUFBRSx3QkFBUyxFQUFFLDRCQUFXLEVBQ2pFLGtEQUFzQixFQUFFLHdDQUFpQixDQUM3QjtRQUVaLG9DQUFTLENBQ0c7UUFDZCxJQUFNLEtBQUssR0FBRyxXQUFXLEdBQUcsQ0FBQyxhQUFhLEdBQUcsZUFBZSxDQUFDLENBQUE7UUFDN0QsSUFBTSxVQUFVLEdBQUcsT0FBTyxLQUFLLE1BQU0sQ0FBQTtRQUVyQyxxQkFBcUI7UUFDckIsRUFBRSxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztZQUNmLElBQU0sVUFBUSxHQUFHLG1CQUFXLENBQUMsT0FBTyxDQUFDLENBQUE7WUFFckMsb0JBQVksQ0FBQyxnQkFBZ0IsRUFBRSxjQUFjLEVBQUUsVUFBUSxFQUFFLFNBQVMsRUFBRSxVQUFDLElBQUksRUFBRSxRQUFRO2dCQUNqRixJQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFBO2dCQUM1RCxJQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLGNBQWMsRUFBRSxRQUFRLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQTtnQkFDNUQsSUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLFNBQVMsQ0FBQyxPQUFPLEVBQUUsR0FBRyxlQUFlLENBQUMsR0FBRyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQTtnQkFDNUUsSUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsR0FBRyxlQUFlLENBQUMsR0FBRyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQTtnQkFDM0UsSUFBTSxVQUFVLEdBQUcsS0FBSyxHQUFHLElBQUksQ0FBQTtnQkFDL0IsSUFBTSxXQUFXLEdBQUcsV0FBVyxLQUFLLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsZUFBZSxHQUFHLGdCQUFnQixDQUFDLEdBQUcsS0FBSyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQTtnQkFFOUcsVUFBVSxDQUFDLElBQUksQ0FDYixxQkFBQyxHQUFHLElBQUMsR0FBRyxFQUFFLGVBQWEsSUFBSSxDQUFDLE9BQU8sRUFBSyxFQUNuQyxJQUFJLEVBQUMsR0FBRyxFQUNSLFNBQVMsRUFBQyxpQkFBaUIsR0FDM0IsU0FBUyxHQUFFLElBQUssR0FDaEIsU0FBUyxHQUFFLFVBQVMsRUFDcEIsS0FBSyxFQUFFO29CQUNMLElBQUksRUFBRSxDQUFHLElBQUksR0FBRyxXQUFXLFFBQUk7b0JBQy9CLEtBQUssRUFBSyxVQUFVLE9BQUk7b0JBQ3hCLE1BQU0sRUFBSyxzQkFBc0IsT0FBSTtvQkFDckMsVUFBVSxFQUFLLHNCQUFzQixPQUFJO29CQUN6QyxNQUFNLEVBQUUsU0FBUztpQkFDakIsR0FDSixLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxVQUFRLEVBQUUsVUFBVSxDQUFFLENBQzFDLENBQ1AsQ0FBQTtZQUNILENBQUMsQ0FBQyxDQUFBO1FBQ0osQ0FBQztRQUVELG9CQUFZLENBQUMsZUFBZSxFQUFFLGFBQWEsRUFBRSxPQUFPLEVBQUUsU0FBUyxFQUFFLFVBQUMsSUFBSSxFQUFFLFFBQVE7WUFDOUUsSUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsR0FBRyxlQUFlLENBQUMsR0FBRyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQTtZQUN2RSxJQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sS0FBSyxLQUFLLEdBQUcsTUFBTSxHQUFHLE9BQU8sQ0FBQyxDQUFBO1lBQ25FLElBQU0sV0FBVyxHQUFHLFlBQVksS0FBSyxDQUFDLE9BQU8sS0FBSyxLQUFLLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFBO1lBQ2hFLElBQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLEdBQUcsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLEdBQUcsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUE7WUFDaEYsSUFBTSxXQUFXLEdBQUcsV0FBVyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUE7WUFDdkMsSUFBTSxXQUFXLEdBQUcsV0FBVyxLQUFLLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsZUFBZSxHQUFHLGdCQUFnQixDQUFDLEdBQUcsS0FBSyxDQUFDLEdBQUcsV0FBVyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUE7WUFFNUgsVUFBVSxDQUFDLElBQUksQ0FDYixxQkFBQyxHQUFHLElBQUMsR0FBRyxFQUFFLFdBQVMsSUFBSSxDQUFDLE9BQU8sRUFBSyxFQUMvQixJQUFJLEVBQUMsR0FBRyxFQUNSLFNBQVMsRUFBRSxnQkFBYSxVQUFVLEdBQUcsRUFBRSxHQUFHLGdCQUFnQixXQUFJLFdBQVcsR0FBRyxtQkFBbUIsR0FBRyxFQUFFLE9BQUksR0FDeEcsU0FBUyxHQUFFLElBQUssR0FDaEIsU0FBUyxHQUFFLE9BQVEsRUFDbkIsS0FBSyxFQUFFO2dCQUNMLEdBQUcsRUFBRSxDQUFHLE9BQU8sS0FBSyxNQUFNLEdBQUcsQ0FBQyxHQUFHLHNCQUFzQixRQUFJO2dCQUMzRCxJQUFJLEVBQUUsQ0FBRyxJQUFJLEdBQUcsV0FBVyxRQUFJO2dCQUMvQixLQUFLLEVBQUssVUFBVSxPQUFJO2dCQUN4QixNQUFNLEVBQUssQ0FBQyxPQUFPLEtBQUssTUFBTSxHQUFHLHNCQUFzQixHQUFHLGlCQUFpQixHQUFHLGlCQUFpQixDQUFDLE9BQUk7Z0JBQ3BHLFVBQVUsRUFBSyxDQUFDLE9BQU8sS0FBSyxNQUFNLEdBQUcsc0JBQXNCLEdBQUcsaUJBQWlCLEdBQUcsaUJBQWlCLENBQUMsT0FBSTtnQkFDeEcsUUFBUSxFQUFLLENBQUMsVUFBVSxHQUFHLEVBQUUsR0FBRyxJQUFJLEdBQUcsVUFBVSxHQUFHLEVBQUUsR0FBRyxJQUFJLEdBQUcsSUFBSSxDQUFDLE9BQUk7Z0JBQ3pFLE1BQU0sRUFBRSxTQUFTO2FBQ2pCLEdBQ0osS0FBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsT0FBTyxFQUFFLFVBQVUsQ0FBRSxDQUM1QyxDQUNQLENBQUE7UUFDSCxDQUFDLENBQUMsQ0FBQTtRQUVNLDhCQUFNLENBQWU7UUFFN0IsSUFBSSxXQUFXLEdBQUc7WUFDaEIsTUFBTSxFQUFFLENBQUcsc0JBQXNCLEdBQUcsaUJBQWlCLFFBQUk7WUFDekQsVUFBVSxFQUFLLFVBQVUsT0FBSTtTQUM5QixDQUFDO1FBRUYsRUFBRSxDQUFDLENBQUMsV0FBVyxLQUFLLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDNUIsV0FBVyxDQUFDLFVBQVUsQ0FBQyxHQUFHLE9BQU8sQ0FBQztZQUNsQyxXQUFXLENBQUMsT0FBTyxDQUFDLEdBQUcsTUFBTSxDQUFDO1lBQzlCLFdBQVcsQ0FBQyxRQUFRLENBQUMsR0FBRyxNQUFNLENBQUE7UUFDaEMsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxXQUFXLEtBQUssVUFBVSxDQUFDLENBQUMsQ0FBQztZQUN0QyxJQUFJLFlBQVksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQztZQUMzQyxFQUFFLENBQUMsQ0FBQyxTQUFTLElBQUksWUFBWSxDQUFDLENBQUMsQ0FBQztnQkFDOUIsV0FBVyxDQUFDLFVBQVUsQ0FBQyxHQUFHLFVBQVUsQ0FBQztnQkFDckMsV0FBVyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUcsU0FBUyxHQUFHLFlBQVksUUFBSSxDQUFDO2dCQUNyRCxXQUFXLENBQUMsT0FBTyxDQUFDLEdBQU0sV0FBVyxPQUFJLENBQUM7Z0JBQzFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsR0FBRyxHQUFHLENBQUE7WUFDM0IsQ0FBQztRQUNILENBQUM7UUFFRCxNQUFNLENBQUMsQ0FDTCxxQkFBQyxHQUFHLElBQUMsR0FBRyxFQUFDLFFBQVEsRUFBQyxHQUFHLEVBQUMsUUFBUSxFQUFDLFNBQVMsRUFBQyxZQUFZLEVBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxVQUFXLEVBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxRQUFTLEVBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxXQUFZLEVBQUMsS0FBSyxFQUFFLFdBQVksR0FDM0osVUFBVyxDQUNSLENBQ1AsQ0FBQTtJQUNILENBQUM7SUFsT1EsbUJBQVksR0FBRztRQUNsQixXQUFXLEVBQUUsTUFBTTtRQUNuQixNQUFNLEVBQUUsRUFBRTtLQUNiLENBQUM7SUFnT04sYUFBQztBQUFELENBQUMsQUFwT0QsQ0FBb0MsS0FBSyxDQUFDLFNBQVMsR0FvT2xEO0FBcE9EO3dCQW9PQyxDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnXG5pbXBvcnQgKiBhcyBtb21lbnQgZnJvbSAnbW9tZW50J1xuXG5pbXBvcnQge2l0ZXJhdGVUaW1lcywgZ2V0TmV4dFVuaXR9IGZyb20gJy4uL3V0aWxzJ1xuXG5cbnR5cGUgSGVhZGVyUG9zaXRpb24gPSAnZml4ZWQnIHwgJ2Fic29sdXRlJyB8ICdub25lJztcblxuXG5pbnRlcmZhY2UgUHJvcHMge1xuICAgIHRpbWVTdGVwcz9cbiAgICBoZWFkZXJMYWJlbEdyb3VwSGVpZ2h0P1xuICAgIGhlYWRlckxhYmVsSGVpZ2h0P1xuICAgIHNob3dQZXJpb2Q6IChtb21lbnQsIHVuaXQpID0+IHZvaWRcbiAgICBjYW52YXNUaW1lU3RhcnQ6IG51bWJlclxuICAgIGNhbnZhc1RpbWVFbmQ6IG51bWJlclxuICAgIGNhbnZhc1dpZHRoOiBudW1iZXJcbiAgICBsaW5lSGVpZ2h0OiBudW1iZXJcbiAgICB2aXNpYmxlVGltZVN0YXJ0OiBudW1iZXJcbiAgICB2aXNpYmxlVGltZUVuZDogbnVtYmVyXG4gICAgbWluVW5pdDogc3RyaW5nXG4gICAgd2lkdGg6IG51bWJlclxuICAgIGZpeGVkSGVhZGVyOiBIZWFkZXJQb3NpdGlvblxuICAgIHpJbmRleD86IG51bWJlclxuICAgIHpvb20/XG59XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEhlYWRlciBleHRlbmRzIFJlYWN0LkNvbXBvbmVudDxQcm9wcywgYW55PiB7XG4gICAgc3RhdGljIGRlZmF1bHRQcm9wcyA9IHtcbiAgICAgICAgZml4ZWRIZWFkZXI6ICdub25lJyxcbiAgICAgICAgekluZGV4OiAxMVxuICAgIH07XG5cbiAgcmVmczoge1xuICAgIFtrZXk6IHN0cmluZ106IEVsZW1lbnRcbiAgICBoZWFkZXI6IEVsZW1lbnRcbiAgfTtcbiAgc2Nyb2xsRXZlbnRMaXN0ZW5lcjtcblxuICBjb25zdHJ1Y3RvciAocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgIHNjcm9sbFRvcDogMCxcbiAgICAgIGNvbXBvbmVudFRvcDogMCxcbiAgICAgIHRvdWNoVGFyZ2V0OiBudWxsLFxuICAgICAgdG91Y2hBY3RpdmU6IGZhbHNlXG4gICAgfVxuICB9XG5cbiAgc2Nyb2xsIChlPykge1xuICAgIGlmICh0aGlzLnByb3BzLmZpeGVkSGVhZGVyID09PSAnYWJzb2x1dGUnICYmIHdpbmRvdyAmJiB3aW5kb3cuZG9jdW1lbnQpIHtcbiAgICAgIGNvbnN0IHNjcm9sbCA9IHdpbmRvdy5kb2N1bWVudC5ib2R5LnNjcm9sbFRvcDtcbiAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICBzY3JvbGxUb3A6IHNjcm9sbFxuICAgICAgfSlcbiAgICB9XG4gIH1cblxuICBzZXRDb21wb25lbnRUb3AgKCkge1xuICAgIGNvbnN0IHZpZXdwb3J0T2Zmc2V0ID0gdGhpcy5yZWZzLmhlYWRlci5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIGNvbXBvbmVudFRvcDogdmlld3BvcnRPZmZzZXQudG9wXG4gICAgfSlcbiAgfVxuXG4gIGNvbXBvbmVudERpZE1vdW50ICgpIHtcbiAgICB0aGlzLnNldENvbXBvbmVudFRvcCgpO1xuICAgIHRoaXMuc2Nyb2xsKCk7XG5cbiAgICB0aGlzLnNjcm9sbEV2ZW50TGlzdGVuZXIgPSB7XG4gICAgICBoYW5kbGVFdmVudDogKGV2ZW50KSA9PiB7XG4gICAgICAgIHRoaXMuc2Nyb2xsKClcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3Njcm9sbCcsIHRoaXMuc2Nyb2xsRXZlbnRMaXN0ZW5lcilcbiAgfVxuXG4gIGNvbXBvbmVudFdpbGxVbm1vdW50ICgpIHtcbiAgICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcignc2Nyb2xsJywgdGhpcy5zY3JvbGxFdmVudExpc3RlbmVyKVxuICB9XG5cbiAgY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wcyAoKSB7XG4gICAgdGhpcy5zZXRDb21wb25lbnRUb3AoKVxuICB9XG5cbiAgaGVhZGVyTGFiZWwgKHRpbWUsIHVuaXQsIHdpZHRoKSB7XG4gICAgaWYgKHVuaXQgPT09ICd5ZWFyJykge1xuICAgICAgcmV0dXJuIHRpbWUuZm9ybWF0KHdpZHRoIDwgNDYgPyAnWVknIDogJ1lZWVknKVxuICAgIH0gZWxzZSBpZiAodW5pdCA9PT0gJ21vbnRoJykge1xuICAgICAgcmV0dXJuIHRpbWUuZm9ybWF0KHdpZHRoIDwgNjUgPyAnTU0vWVknIDogd2lkdGggPCA3NSA/ICdNTS9ZWVlZJyA6IHdpZHRoIDwgMTIwID8gJ01NTSBZWVlZJyA6ICdNTU1NIFlZWVknKVxuICAgIH0gZWxzZSBpZiAodW5pdCA9PT0gJ2RheScpIHtcbiAgICAgIHJldHVybiB0aW1lLmZvcm1hdCh3aWR0aCA8IDE1MCA/ICdMJyA6ICdkZGRkLCBMTCcpXG4gICAgfSBlbHNlIGlmICh1bml0ID09PSAnaG91cicpIHtcbiAgICAgIHJldHVybiB0aW1lLmZvcm1hdCh3aWR0aCA8IDUwID8gJ0hIJyA6IHdpZHRoIDwgMTMwID8gJ0hIOjAwJyA6IHdpZHRoIDwgMTUwID8gJ0wsIEhIOjAwJyA6ICdkZGRkLCBMTCwgSEg6MDAnKVxuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gdGltZS5mb3JtYXQoJ0xMTCcpXG4gICAgfVxuICB9XG5cbiAgc3ViSGVhZGVyTGFiZWwgKHRpbWUsIHVuaXQsIHdpZHRoKSB7XG4gICAgaWYgKHVuaXQgPT09ICd5ZWFyJykge1xuICAgICAgcmV0dXJuIHRpbWUuZm9ybWF0KHdpZHRoIDwgNDYgPyAnWVknIDogJ1lZWVknKVxuICAgIH0gZWxzZSBpZiAodW5pdCA9PT0gJ21vbnRoJykge1xuICAgICAgcmV0dXJuIHRpbWUuZm9ybWF0KHdpZHRoIDwgMzcgPyAnTU0nIDogd2lkdGggPCA4NSA/ICdNTU0nIDogJ01NTU0nKVxuICAgIH0gZWxzZSBpZiAodW5pdCA9PT0gJ2RheScpIHtcbiAgICAgIHJldHVybiB0aW1lLmZvcm1hdCh3aWR0aCA8IDQ3ID8gJ0QnIDogd2lkdGggPCA4MCA/ICdkZCBEJyA6IHdpZHRoIDwgMTIwID8gJ2RkZCwgRG8nIDogJ2RkZGQsIERvJylcbiAgICB9IGVsc2UgaWYgKHVuaXQgPT09ICdob3VyJykge1xuICAgICAgcmV0dXJuIHRpbWUuZm9ybWF0KHdpZHRoIDwgNTAgPyAnSEgnIDogJ0hIOjAwJylcbiAgICB9IGVsc2UgaWYgKHVuaXQgPT09ICdtaW51dGUnKSB7XG4gICAgICByZXR1cm4gdGltZS5mb3JtYXQod2lkdGggPCA2MCA/ICdtbScgOiAnSEg6bW0nKVxuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gdGltZS5nZXQodW5pdClcbiAgICB9XG4gIH1cblxuICBwZXJpb2RDbGljayA9IChlKSA9PiB7XG4gICAgY29uc3Qge3RpbWUsIHVuaXR9ID0gZS50YXJnZXQuZGF0YXNldFxuICAgIGlmICh0aW1lICYmIHVuaXQpIHtcbiAgICAgIHRoaXMucHJvcHMuc2hvd1BlcmlvZChtb21lbnQodGltZSAtIDApLCB1bml0KVxuICAgIH1cbiAgfVxuXG4gIHRvdWNoU3RhcnQgPSAoZSkgPT4ge1xuICAgIGlmIChlLnRvdWNoZXMubGVuZ3RoID09PSAxKSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgdG91Y2hUYXJnZXQ6IGUudGFyZ2V0IHx8IGUudG91Y2hUYXJnZXQsXG4gICAgICAgIHRvdWNoQWN0aXZlOiB0cnVlXG4gICAgICB9KVxuICAgIH1cbiAgfVxuXG4gIHRvdWNoRW5kID0gKGUpID0+IHtcbiAgICBpZiAoIXRoaXMuc3RhdGUudG91Y2hBY3RpdmUpIHtcbiAgICAgIHJldHVybiB0aGlzLnJlc2V0VG91Y2hTdGF0ZSgpXG4gICAgfVxuXG4gICAgdmFyIGNoYW5nZWRUb3VjaGVzID0gZS5jaGFuZ2VkVG91Y2hlc1swXVxuICAgIGlmIChjaGFuZ2VkVG91Y2hlcykge1xuICAgICAgdmFyIGVsZW0gPSBkb2N1bWVudC5lbGVtZW50RnJvbVBvaW50KGNoYW5nZWRUb3VjaGVzLnBhZ2VYLCBjaGFuZ2VkVG91Y2hlcy5wYWdlWSlcbiAgICAgIGlmIChlbGVtICE9PSB0aGlzLnN0YXRlLnRvdWNoVGFyZ2V0KSB7XG4gICAgICAgIHJldHVybiB0aGlzLnJlc2V0VG91Y2hTdGF0ZSgpXG4gICAgICB9XG4gICAgfVxuXG4gICAgdGhpcy5yZXNldFRvdWNoU3RhdGUoKVxuICAgIHRoaXMucGVyaW9kQ2xpY2soZSlcbiAgfVxuXG4gIHJlc2V0VG91Y2hTdGF0ZSAoKSB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICB0b3VjaFRhcmdldDogbnVsbCxcbiAgICAgIHRvdWNoQWN0aXZlOiBmYWxzZVxuICAgIH0pXG4gIH1cblxuICByZW5kZXIgKCkge1xuICAgIGxldCB0aW1lTGFiZWxzID0gW11cbiAgICBjb25zdCB7XG4gICAgICBjYW52YXNUaW1lU3RhcnQsIGNhbnZhc1RpbWVFbmQsIGNhbnZhc1dpZHRoLCBsaW5lSGVpZ2h0LFxuICAgICAgdmlzaWJsZVRpbWVTdGFydCwgdmlzaWJsZVRpbWVFbmQsIG1pblVuaXQsIHRpbWVTdGVwcywgZml4ZWRIZWFkZXIsXG4gICAgICBoZWFkZXJMYWJlbEdyb3VwSGVpZ2h0LCBoZWFkZXJMYWJlbEhlaWdodFxuICAgIH0gPSB0aGlzLnByb3BzXG4gICAgY29uc3Qge1xuICAgICAgc2Nyb2xsVG9wXG4gICAgfSA9IHRoaXMuc3RhdGVcbiAgICBjb25zdCByYXRpbyA9IGNhbnZhc1dpZHRoIC8gKGNhbnZhc1RpbWVFbmQgLSBjYW52YXNUaW1lU3RhcnQpXG4gICAgY29uc3QgdHdvSGVhZGVycyA9IG1pblVuaXQgIT09ICd5ZWFyJ1xuXG4gICAgLy8gYWRkIHRoZSB0b3AgaGVhZGVyXG4gICAgaWYgKHR3b0hlYWRlcnMpIHtcbiAgICAgIGNvbnN0IG5leHRVbml0ID0gZ2V0TmV4dFVuaXQobWluVW5pdClcblxuICAgICAgaXRlcmF0ZVRpbWVzKHZpc2libGVUaW1lU3RhcnQsIHZpc2libGVUaW1lRW5kLCBuZXh0VW5pdCwgdGltZVN0ZXBzLCAodGltZSwgbmV4dFRpbWUpID0+IHtcbiAgICAgICAgY29uc3Qgc3RhcnRUaW1lID0gTWF0aC5tYXgodmlzaWJsZVRpbWVTdGFydCwgdGltZS52YWx1ZU9mKCkpXG4gICAgICAgIGNvbnN0IGVuZFRpbWUgPSBNYXRoLm1pbih2aXNpYmxlVGltZUVuZCwgbmV4dFRpbWUudmFsdWVPZigpKVxuICAgICAgICBjb25zdCBsZWZ0ID0gTWF0aC5yb3VuZCgoc3RhcnRUaW1lLnZhbHVlT2YoKSAtIGNhbnZhc1RpbWVTdGFydCkgKiByYXRpbywgLTIpXG4gICAgICAgIGNvbnN0IHJpZ2h0ID0gTWF0aC5yb3VuZCgoZW5kVGltZS52YWx1ZU9mKCkgLSBjYW52YXNUaW1lU3RhcnQpICogcmF0aW8sIC0yKVxuICAgICAgICBjb25zdCBsYWJlbFdpZHRoID0gcmlnaHQgLSBsZWZ0XG4gICAgICAgIGNvbnN0IGxlZnRDb3JyZWN0ID0gZml4ZWRIZWFkZXIgPT09ICdmaXhlZCcgPyBNYXRoLnJvdW5kKChjYW52YXNUaW1lU3RhcnQgLSB2aXNpYmxlVGltZVN0YXJ0KSAqIHJhdGlvKSAtIDEgOiAwXG5cbiAgICAgICAgdGltZUxhYmVscy5wdXNoKFxuICAgICAgICAgIDxkaXYga2V5PXtgdG9wLWxhYmVsLSR7dGltZS52YWx1ZU9mKCl9YH1cbiAgICAgICAgICAgICAgIGhyZWY9JyMnXG4gICAgICAgICAgICAgICBjbGFzc05hbWU9J3JjdC1sYWJlbC1ncm91cCdcbiAgICAgICAgICAgICAgIGRhdGEtdGltZT17dGltZX1cbiAgICAgICAgICAgICAgIGRhdGEtdW5pdD17bmV4dFVuaXR9XG4gICAgICAgICAgICAgICBzdHlsZT17e1xuICAgICAgICAgICAgICAgICBsZWZ0OiBgJHtsZWZ0ICsgbGVmdENvcnJlY3R9cHhgLFxuICAgICAgICAgICAgICAgICB3aWR0aDogYCR7bGFiZWxXaWR0aH1weGAsXG4gICAgICAgICAgICAgICAgIGhlaWdodDogYCR7aGVhZGVyTGFiZWxHcm91cEhlaWdodH1weGAsXG4gICAgICAgICAgICAgICAgIGxpbmVIZWlnaHQ6IGAke2hlYWRlckxhYmVsR3JvdXBIZWlnaHR9cHhgLFxuICAgICAgICAgICAgICAgICBjdXJzb3I6ICdwb2ludGVyJ1xuICAgICAgICAgICAgICAgfX0+XG4gICAgICAgICAgICB7dGhpcy5oZWFkZXJMYWJlbCh0aW1lLCBuZXh0VW5pdCwgbGFiZWxXaWR0aCl9XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIClcbiAgICAgIH0pXG4gICAgfVxuXG4gICAgaXRlcmF0ZVRpbWVzKGNhbnZhc1RpbWVTdGFydCwgY2FudmFzVGltZUVuZCwgbWluVW5pdCwgdGltZVN0ZXBzLCAodGltZSwgbmV4dFRpbWUpID0+IHtcbiAgICAgIGNvbnN0IGxlZnQgPSBNYXRoLnJvdW5kKCh0aW1lLnZhbHVlT2YoKSAtIGNhbnZhc1RpbWVTdGFydCkgKiByYXRpbywgLTIpXG4gICAgICBjb25zdCBtaW5Vbml0VmFsdWUgPSB0aW1lLmdldChtaW5Vbml0ID09PSAnZGF5JyA/ICdkYXRlJyA6IG1pblVuaXQpXG4gICAgICBjb25zdCBmaXJzdE9mVHlwZSA9IG1pblVuaXRWYWx1ZSA9PT0gKG1pblVuaXQgPT09ICdkYXknID8gMSA6IDApXG4gICAgICBjb25zdCBsYWJlbFdpZHRoID0gTWF0aC5yb3VuZCgobmV4dFRpbWUudmFsdWVPZigpIC0gdGltZS52YWx1ZU9mKCkpICogcmF0aW8sIC0yKVxuICAgICAgY29uc3QgYm9yZGVyV2lkdGggPSBmaXJzdE9mVHlwZSA/IDIgOiAxXG4gICAgICBjb25zdCBsZWZ0Q29ycmVjdCA9IGZpeGVkSGVhZGVyID09PSAnZml4ZWQnID8gTWF0aC5yb3VuZCgoY2FudmFzVGltZVN0YXJ0IC0gdmlzaWJsZVRpbWVTdGFydCkgKiByYXRpbykgLSBib3JkZXJXaWR0aCArIDEgOiAwXG5cbiAgICAgIHRpbWVMYWJlbHMucHVzaChcbiAgICAgICAgPGRpdiBrZXk9e2BsYWJlbC0ke3RpbWUudmFsdWVPZigpfWB9XG4gICAgICAgICAgICAgaHJlZj0nIydcbiAgICAgICAgICAgICBjbGFzc05hbWU9e2ByY3QtbGFiZWwgJHt0d29IZWFkZXJzID8gJycgOiAncmN0LWxhYmVsLW9ubHknfSAke2ZpcnN0T2ZUeXBlID8gJ3JjdC1maXJzdC1vZi10eXBlJyA6ICcnfSBgfVxuICAgICAgICAgICAgIGRhdGEtdGltZT17dGltZX1cbiAgICAgICAgICAgICBkYXRhLXVuaXQ9e21pblVuaXR9XG4gICAgICAgICAgICAgc3R5bGU9e3tcbiAgICAgICAgICAgICAgIHRvcDogYCR7bWluVW5pdCA9PT0gJ3llYXInID8gMCA6IGhlYWRlckxhYmVsR3JvdXBIZWlnaHR9cHhgLFxuICAgICAgICAgICAgICAgbGVmdDogYCR7bGVmdCArIGxlZnRDb3JyZWN0fXB4YCxcbiAgICAgICAgICAgICAgIHdpZHRoOiBgJHtsYWJlbFdpZHRofXB4YCxcbiAgICAgICAgICAgICAgIGhlaWdodDogYCR7KG1pblVuaXQgPT09ICd5ZWFyJyA/IGhlYWRlckxhYmVsR3JvdXBIZWlnaHQgKyBoZWFkZXJMYWJlbEhlaWdodCA6IGhlYWRlckxhYmVsSGVpZ2h0KX1weGAsXG4gICAgICAgICAgICAgICBsaW5lSGVpZ2h0OiBgJHsobWluVW5pdCA9PT0gJ3llYXInID8gaGVhZGVyTGFiZWxHcm91cEhlaWdodCArIGhlYWRlckxhYmVsSGVpZ2h0IDogaGVhZGVyTGFiZWxIZWlnaHQpfXB4YCxcbiAgICAgICAgICAgICAgIGZvbnRTaXplOiBgJHsobGFiZWxXaWR0aCA+IDMwID8gJzE0JyA6IGxhYmVsV2lkdGggPiAyMCA/ICcxMicgOiAnMTAnKX1weGAsXG4gICAgICAgICAgICAgICBjdXJzb3I6ICdwb2ludGVyJ1xuICAgICAgICAgICAgIH19PlxuICAgICAgICAgIHt0aGlzLnN1YkhlYWRlckxhYmVsKHRpbWUsIG1pblVuaXQsIGxhYmVsV2lkdGgpfVxuICAgICAgICA8L2Rpdj5cbiAgICAgIClcbiAgICB9KVxuXG4gICAgY29uc3QgeyB6SW5kZXggfSA9IHRoaXMucHJvcHNcblxuICAgIGxldCBoZWFkZXJTdHlsZSA9IHtcbiAgICAgIGhlaWdodDogYCR7aGVhZGVyTGFiZWxHcm91cEhlaWdodCArIGhlYWRlckxhYmVsSGVpZ2h0fXB4YCxcbiAgICAgIGxpbmVIZWlnaHQ6IGAke2xpbmVIZWlnaHR9cHhgXG4gICAgfTtcblxuICAgIGlmIChmaXhlZEhlYWRlciA9PT0gJ2ZpeGVkJykge1xuICAgICAgaGVhZGVyU3R5bGVbJ3Bvc2l0aW9uJ10gPSAnZml4ZWQnO1xuICAgICAgaGVhZGVyU3R5bGVbJ3dpZHRoJ10gPSAnMTAwJSc7XG4gICAgICBoZWFkZXJTdHlsZVsnekluZGV4J10gPSB6SW5kZXhcbiAgICB9IGVsc2UgaWYgKGZpeGVkSGVhZGVyID09PSAnYWJzb2x1dGUnKSB7XG4gICAgICBsZXQgY29tcG9uZW50VG9wID0gdGhpcy5zdGF0ZS5jb21wb25lbnRUb3A7XG4gICAgICBpZiAoc2Nyb2xsVG9wID49IGNvbXBvbmVudFRvcCkge1xuICAgICAgICBoZWFkZXJTdHlsZVsncG9zaXRpb24nXSA9ICdhYnNvbHV0ZSc7XG4gICAgICAgIGhlYWRlclN0eWxlWyd0b3AnXSA9IGAke3Njcm9sbFRvcCAtIGNvbXBvbmVudFRvcH1weGA7XG4gICAgICAgIGhlYWRlclN0eWxlWyd3aWR0aCddID0gYCR7Y2FudmFzV2lkdGh9cHhgO1xuICAgICAgICBoZWFkZXJTdHlsZVsnbGVmdCddID0gJzAnXG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgcmVmPSdoZWFkZXInIGtleT0naGVhZGVyJyBjbGFzc05hbWU9J3JjdC1oZWFkZXInIG9uVG91Y2hTdGFydD17dGhpcy50b3VjaFN0YXJ0fSBvblRvdWNoRW5kPXt0aGlzLnRvdWNoRW5kfSBvbkNsaWNrPXt0aGlzLnBlcmlvZENsaWNrfSBzdHlsZT17aGVhZGVyU3R5bGV9PlxuICAgICAgICB7dGltZUxhYmVsc31cbiAgICAgIDwvZGl2PlxuICAgIClcbiAgfVxufVxuIl19

/***/ },
/* 15 */
/*!******************************************!*\
  !*** ./src/lib/lines/vertical-lines.tsx ***!
  \******************************************/
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var React = __webpack_require__(/*! react */ 2);
	var utils_1 = __webpack_require__(/*! ../utils */ 11);
	var VerticalLines = (function (_super) {
	    __extends(VerticalLines, _super);
	    function VerticalLines() {
	        _super.apply(this, arguments);
	    }
	    VerticalLines.prototype.shouldComponentUpdate = function (nextProps, nextState) {
	        return !(nextProps.canvasTimeStart === this.props.canvasTimeStart &&
	            nextProps.canvasTimeEnd === this.props.canvasTimeEnd &&
	            nextProps.canvasWidth === this.props.canvasWidth &&
	            nextProps.lineHeight === this.props.lineHeight &&
	            nextProps.lineCount === this.props.lineCount &&
	            nextProps.minUnit === this.props.minUnit &&
	            nextProps.timeSteps === this.props.timeSteps &&
	            nextProps.fixedHeader === this.props.fixedHeader &&
	            nextProps.height === this.props.height &&
	            nextProps.headerHeight === this.props.headerHeight);
	    };
	    VerticalLines.prototype.render = function () {
	        var _this = this;
	        var _a = this.props, canvasTimeStart = _a.canvasTimeStart, canvasTimeEnd = _a.canvasTimeEnd, canvasWidth = _a.canvasWidth, minUnit = _a.minUnit, timeSteps = _a.timeSteps, height = _a.height, headerHeight = _a.headerHeight;
	        var ratio = canvasWidth / (canvasTimeEnd - canvasTimeStart);
	        var lines = [];
	        utils_1.iterateTimes(canvasTimeStart, canvasTimeEnd, minUnit, timeSteps, function (time, nextTime) {
	            var left = Math.round((time.valueOf() - canvasTimeStart) * ratio, -2);
	            var minUnitValue = time.get(minUnit === 'day' ? 'date' : minUnit);
	            var firstOfType = minUnitValue === (minUnit === 'day' ? 1 : 0);
	            var lineWidth = firstOfType ? 2 : 1;
	            var labelWidth = Math.ceil((nextTime.valueOf() - time.valueOf()) * ratio) - lineWidth;
	            var leftPush = _this.props.fixedHeader === 'fixed' && firstOfType ? -1 : 0;
	            var classNames = 'rct-vl' +
	                (firstOfType ? ' rct-vl-first' : '') +
	                (minUnit === 'day' || minUnit === 'hour' || minUnit === 'minute' ? " rct-day-" + time.day() : '');
	            lines.push(React.createElement("div", {key: "line-" + time.valueOf(), className: classNames, style: {
	                top: headerHeight + "px",
	                left: (left + leftPush) + "px",
	                width: labelWidth + "px",
	                height: (height - headerHeight) + "px"
	            }}));
	        });
	        return (React.createElement("div", {className: 'rct-vertical-lines'}, lines));
	    };
	    VerticalLines.defaultProps = {
	        fixedHeader: 'none',
	        dayBackground: null
	    };
	    return VerticalLines;
	}(React.Component));
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.default = VerticalLines;
	//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmVydGljYWwtbGluZXMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ2ZXJ0aWNhbC1saW5lcy50c3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsSUFBWSxLQUFLLFdBQU0sT0FFdkIsQ0FBQyxDQUY2QjtBQUU5QixzQkFBMkIsVUFHM0IsQ0FBQyxDQUhvQztBQWlCckM7SUFBMkMsaUNBQTJCO0lBQXRFO1FBQTJDLDhCQUEyQjtJQXVEdEUsQ0FBQztJQWpEQyw2Q0FBcUIsR0FBckIsVUFBdUIsU0FBUyxFQUFFLFNBQVM7UUFDekMsTUFBTSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsZUFBZSxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZTtZQUN4RCxTQUFTLENBQUMsYUFBYSxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsYUFBYTtZQUNwRCxTQUFTLENBQUMsV0FBVyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVztZQUNoRCxTQUFTLENBQUMsVUFBVSxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVTtZQUM5QyxTQUFTLENBQUMsU0FBUyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUztZQUM1QyxTQUFTLENBQUMsT0FBTyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTztZQUN4QyxTQUFTLENBQUMsU0FBUyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUztZQUM1QyxTQUFTLENBQUMsV0FBVyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVztZQUNoRCxTQUFTLENBQUMsTUFBTSxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTTtZQUN0QyxTQUFTLENBQUMsWUFBWSxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUMxRCxDQUFBO0lBQ0gsQ0FBQztJQUVELDhCQUFNLEdBQU47UUFBQSxpQkFrQ0M7UUFqQ0MsSUFBQSxlQUE0RyxFQUFwRyxvQ0FBZSxFQUFFLGdDQUFhLEVBQUUsNEJBQVcsRUFBRSxvQkFBTyxFQUFFLHdCQUFTLEVBQUUsa0JBQU0sRUFBRSw4QkFBWSxDQUFnQjtRQUM3RyxJQUFNLEtBQUssR0FBRyxXQUFXLEdBQUcsQ0FBQyxhQUFhLEdBQUcsZUFBZSxDQUFDLENBQUM7UUFFOUQsSUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFDO1FBRWYsb0JBQVksQ0FBQyxlQUFlLEVBQUUsYUFBYSxFQUFFLE9BQU8sRUFBRSxTQUFTLEVBQUUsVUFBQyxJQUFJLEVBQUUsUUFBUTtZQUM5RSxJQUFNLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxHQUFHLGVBQWUsQ0FBQyxHQUFHLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3hFLElBQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxLQUFLLEtBQUssR0FBRyxNQUFNLEdBQUcsT0FBTyxDQUFDLENBQUM7WUFDcEUsSUFBTSxXQUFXLEdBQUcsWUFBWSxLQUFLLENBQUMsT0FBTyxLQUFLLEtBQUssR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDakUsSUFBTSxTQUFTLEdBQUcsV0FBVyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDdEMsSUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsR0FBRyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsR0FBRyxTQUFTLENBQUM7WUFDeEYsSUFBTSxRQUFRLEdBQUcsS0FBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLEtBQUssT0FBTyxJQUFJLFdBQVcsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7WUFFNUUsSUFBTSxVQUFVLEdBQUcsUUFBUTtnQkFDUixDQUFDLFdBQVcsR0FBRyxlQUFlLEdBQUcsRUFBRSxDQUFDO2dCQUNwQyxDQUFDLE9BQU8sS0FBSyxLQUFLLElBQUksT0FBTyxLQUFLLE1BQU0sSUFBSSxPQUFPLEtBQUssUUFBUSxHQUFHLGNBQVksSUFBSSxDQUFDLEdBQUcsRUFBSSxHQUFHLEVBQUUsQ0FBQyxDQUFDO1lBRXJILEtBQUssQ0FBQyxJQUFJLENBQ1IscUJBQUMsR0FBRyxJQUFDLEdBQUcsRUFBRSxVQUFRLElBQUksQ0FBQyxPQUFPLEVBQUssRUFDOUIsU0FBUyxFQUFFLFVBQVcsRUFDdEIsS0FBSyxFQUFFO2dCQUNMLEdBQUcsRUFBSyxZQUFZLE9BQUk7Z0JBQ3hCLElBQUksRUFBRSxDQUFHLElBQUksR0FBRyxRQUFRLFFBQUk7Z0JBQzVCLEtBQUssRUFBSyxVQUFVLE9BQUk7Z0JBQ3hCLE1BQU0sRUFBRSxDQUFHLE1BQU0sR0FBRyxZQUFZLFFBQUk7YUFDcEMsRUFBRyxDQUFDLENBQUE7UUFDZixDQUFDLENBQUMsQ0FBQztRQUVILE1BQU0sQ0FBQyxDQUNMLHFCQUFDLEdBQUcsSUFBQyxTQUFTLEVBQUMsb0JBQW9CLEdBQ2hDLEtBQU0sQ0FDSCxDQUNQLENBQUE7SUFDSCxDQUFDO0lBckRRLDBCQUFZLEdBQUc7UUFDbEIsV0FBVyxFQUFFLE1BQU07UUFDbkIsYUFBYSxFQUFFLElBQUk7S0FDdEIsQ0FBQztJQW1ETixvQkFBQztBQUFELENBQUMsQUF2REQsQ0FBMkMsS0FBSyxDQUFDLFNBQVMsR0F1RHpEO0FBdkREOytCQXVEQyxDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnXG5cbmltcG9ydCB7aXRlcmF0ZVRpbWVzfSBmcm9tICcuLi91dGlscydcblxuXG5pbnRlcmZhY2UgUHJvcHMge1xuICAgIGNhbnZhc1RpbWVTdGFydDogbnVtYmVyXG4gICAgY2FudmFzVGltZUVuZDogbnVtYmVyXG4gICAgY2FudmFzV2lkdGg6IG51bWJlclxuICAgIGxpbmVIZWlnaHQ6IG51bWJlclxuICAgIGxpbmVDb3VudDogbnVtYmVyXG4gICAgbWluVW5pdDogc3RyaW5nXG4gICAgdGltZVN0ZXBzOiB7W3Byb3A6IHN0cmluZ106IHN0cmluZ31cbiAgICBmaXhlZEhlYWRlcjogc3RyaW5nXG4gICAgaGVpZ2h0P1xuICAgIGhlYWRlckhlaWdodD9cbn1cblxuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBWZXJ0aWNhbExpbmVzIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50PFByb3BzLCBhbnk+IHtcbiAgICBzdGF0aWMgZGVmYXVsdFByb3BzID0ge1xuICAgICAgICBmaXhlZEhlYWRlcjogJ25vbmUnLFxuICAgICAgICBkYXlCYWNrZ3JvdW5kOiBudWxsXG4gICAgfTtcblxuICBzaG91bGRDb21wb25lbnRVcGRhdGUgKG5leHRQcm9wcywgbmV4dFN0YXRlKSB7XG4gICAgcmV0dXJuICEobmV4dFByb3BzLmNhbnZhc1RpbWVTdGFydCA9PT0gdGhpcy5wcm9wcy5jYW52YXNUaW1lU3RhcnQgJiZcbiAgICAgICAgICAgICBuZXh0UHJvcHMuY2FudmFzVGltZUVuZCA9PT0gdGhpcy5wcm9wcy5jYW52YXNUaW1lRW5kICYmXG4gICAgICAgICAgICAgbmV4dFByb3BzLmNhbnZhc1dpZHRoID09PSB0aGlzLnByb3BzLmNhbnZhc1dpZHRoICYmXG4gICAgICAgICAgICAgbmV4dFByb3BzLmxpbmVIZWlnaHQgPT09IHRoaXMucHJvcHMubGluZUhlaWdodCAmJlxuICAgICAgICAgICAgIG5leHRQcm9wcy5saW5lQ291bnQgPT09IHRoaXMucHJvcHMubGluZUNvdW50ICYmXG4gICAgICAgICAgICAgbmV4dFByb3BzLm1pblVuaXQgPT09IHRoaXMucHJvcHMubWluVW5pdCAmJlxuICAgICAgICAgICAgIG5leHRQcm9wcy50aW1lU3RlcHMgPT09IHRoaXMucHJvcHMudGltZVN0ZXBzICYmXG4gICAgICAgICAgICAgbmV4dFByb3BzLmZpeGVkSGVhZGVyID09PSB0aGlzLnByb3BzLmZpeGVkSGVhZGVyICYmXG4gICAgICAgICAgICAgbmV4dFByb3BzLmhlaWdodCA9PT0gdGhpcy5wcm9wcy5oZWlnaHQgJiZcbiAgICAgICAgICAgICBuZXh0UHJvcHMuaGVhZGVySGVpZ2h0ID09PSB0aGlzLnByb3BzLmhlYWRlckhlaWdodFxuICAgIClcbiAgfVxuXG4gIHJlbmRlciAoKSB7XG4gICAgY29uc3QgeyBjYW52YXNUaW1lU3RhcnQsIGNhbnZhc1RpbWVFbmQsIGNhbnZhc1dpZHRoLCBtaW5Vbml0LCB0aW1lU3RlcHMsIGhlaWdodCwgaGVhZGVySGVpZ2h0IH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IHJhdGlvID0gY2FudmFzV2lkdGggLyAoY2FudmFzVGltZUVuZCAtIGNhbnZhc1RpbWVTdGFydCk7XG5cbiAgICBsZXQgbGluZXMgPSBbXTtcblxuICAgIGl0ZXJhdGVUaW1lcyhjYW52YXNUaW1lU3RhcnQsIGNhbnZhc1RpbWVFbmQsIG1pblVuaXQsIHRpbWVTdGVwcywgKHRpbWUsIG5leHRUaW1lKSA9PiB7XG4gICAgICBjb25zdCBsZWZ0ID0gTWF0aC5yb3VuZCgodGltZS52YWx1ZU9mKCkgLSBjYW52YXNUaW1lU3RhcnQpICogcmF0aW8sIC0yKTtcbiAgICAgIGNvbnN0IG1pblVuaXRWYWx1ZSA9IHRpbWUuZ2V0KG1pblVuaXQgPT09ICdkYXknID8gJ2RhdGUnIDogbWluVW5pdCk7XG4gICAgICBjb25zdCBmaXJzdE9mVHlwZSA9IG1pblVuaXRWYWx1ZSA9PT0gKG1pblVuaXQgPT09ICdkYXknID8gMSA6IDApO1xuICAgICAgY29uc3QgbGluZVdpZHRoID0gZmlyc3RPZlR5cGUgPyAyIDogMTtcbiAgICAgIGNvbnN0IGxhYmVsV2lkdGggPSBNYXRoLmNlaWwoKG5leHRUaW1lLnZhbHVlT2YoKSAtIHRpbWUudmFsdWVPZigpKSAqIHJhdGlvKSAtIGxpbmVXaWR0aDtcbiAgICAgIGNvbnN0IGxlZnRQdXNoID0gdGhpcy5wcm9wcy5maXhlZEhlYWRlciA9PT0gJ2ZpeGVkJyAmJiBmaXJzdE9mVHlwZSA/IC0xIDogMDtcblxuICAgICAgY29uc3QgY2xhc3NOYW1lcyA9ICdyY3QtdmwnICtcbiAgICAgICAgICAgICAgICAgICAgICAgICAoZmlyc3RPZlR5cGUgPyAnIHJjdC12bC1maXJzdCcgOiAnJykgK1xuICAgICAgICAgICAgICAgICAgICAgICAgIChtaW5Vbml0ID09PSAnZGF5JyB8fCBtaW5Vbml0ID09PSAnaG91cicgfHwgbWluVW5pdCA9PT0gJ21pbnV0ZScgPyBgIHJjdC1kYXktJHt0aW1lLmRheSgpfWAgOiAnJyk7XG5cbiAgICAgIGxpbmVzLnB1c2goXG4gICAgICAgIDxkaXYga2V5PXtgbGluZS0ke3RpbWUudmFsdWVPZigpfWB9XG4gICAgICAgICAgICAgY2xhc3NOYW1lPXtjbGFzc05hbWVzfVxuICAgICAgICAgICAgIHN0eWxlPXt7XG4gICAgICAgICAgICAgICB0b3A6IGAke2hlYWRlckhlaWdodH1weGAsXG4gICAgICAgICAgICAgICBsZWZ0OiBgJHtsZWZ0ICsgbGVmdFB1c2h9cHhgLFxuICAgICAgICAgICAgICAgd2lkdGg6IGAke2xhYmVsV2lkdGh9cHhgLFxuICAgICAgICAgICAgICAgaGVpZ2h0OiBgJHtoZWlnaHQgLSBoZWFkZXJIZWlnaHR9cHhgXG4gICAgICAgICAgICAgfX0gLz4pXG4gICAgfSk7XG5cbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9J3JjdC12ZXJ0aWNhbC1saW5lcyc+XG4gICAgICAgIHtsaW5lc31cbiAgICAgIDwvZGl2PlxuICAgIClcbiAgfVxufVxuIl19

/***/ },
/* 16 */
/*!********************************************!*\
  !*** ./src/lib/lines/horizontal-lines.tsx ***!
  \********************************************/
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var React = __webpack_require__(/*! react */ 2);
	var HorizontalLines = (function (_super) {
	    __extends(HorizontalLines, _super);
	    function HorizontalLines() {
	        _super.apply(this, arguments);
	    }
	    HorizontalLines.prototype.render = function () {
	        var _a = this.props, lineCount = _a.lineCount, canvasWidth = _a.canvasWidth, groupHeights = _a.groupHeights, headerHeight = _a.headerHeight;
	        var lines = [];
	        var totalHeight = headerHeight;
	        for (var i = 0; i < lineCount; i++) {
	            lines.push(React.createElement("div", {key: "horizontal-line-" + i, className: i % 2 === 0 ? 'rct-hl-even' : 'rct-hl-odd', style: {
	                top: totalHeight + "px",
	                left: '0px',
	                width: canvasWidth + "px",
	                height: (groupHeights[i] - 1) + "px"
	            }}));
	            totalHeight += groupHeights[i];
	        }
	        return (React.createElement("div", {className: 'rct-horizontal-lines'}, lines));
	    };
	    HorizontalLines.prototype.shouldComponentUpdate = function (nextProps, nextState) {
	        return !(nextProps.canvasWidth === this.props.canvasWidth &&
	            nextProps.lineHeight === this.props.lineHeight &&
	            nextProps.lineCount === this.props.lineCount &&
	            nextProps.groupHeights === this.props.groupHeights);
	    };
	    HorizontalLines.defaultProps = {
	        borderWidth: 1
	    };
	    return HorizontalLines;
	}(React.Component));
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.default = HorizontalLines;
	//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9yaXpvbnRhbC1saW5lcy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImhvcml6b250YWwtbGluZXMudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLElBQVksS0FBSyxXQUFNLE9BR3ZCLENBQUMsQ0FINkI7QUFhOUI7SUFBOEIsbUNBQTJCO0lBQXpEO1FBQThCLDhCQUEyQjtJQXVDekQsQ0FBQztJQWpDRyxnQ0FBTSxHQUFOO1FBQ0ksSUFBQSxlQUF1RSxFQUFoRSx3QkFBUyxFQUFFLDRCQUFXLEVBQUUsOEJBQVksRUFBRSw4QkFBWSxDQUFlO1FBQ3hFLElBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQztRQUVmLElBQUksV0FBVyxHQUFHLFlBQVksQ0FBQztRQUMvQixHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFNBQVMsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBQ3JDLEtBQUssQ0FBQyxJQUFJLENBQ04scUJBQUMsR0FBRyxJQUNBLEdBQUcsRUFBRSxxQkFBbUIsQ0FBSSxFQUM1QixTQUFTLEVBQUUsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEdBQUcsYUFBYSxHQUFHLFlBQWEsRUFDdEQsS0FBSyxFQUFFO2dCQUNILEdBQUcsRUFBSyxXQUFXLE9BQUk7Z0JBQ3ZCLElBQUksRUFBRSxLQUFLO2dCQUNYLEtBQUssRUFBSyxXQUFXLE9BQUk7Z0JBQ3pCLE1BQU0sRUFBRSxDQUFHLFlBQVksQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLFFBQUk7YUFDcEMsRUFDSixDQUFDLENBQUM7WUFDUixXQUFXLElBQUksWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFBO1FBQ2xDLENBQUM7UUFFRCxNQUFNLENBQUMsQ0FDTCxxQkFBQyxHQUFHLElBQUMsU0FBUyxFQUFDLHNCQUFzQixHQUNsQyxLQUFNLENBQ0gsQ0FDUCxDQUFBO0lBQ0gsQ0FBQztJQUVDLCtDQUFxQixHQUFyQixVQUF1QixTQUFTLEVBQUUsU0FBUztRQUN2QyxNQUFNLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxXQUFXLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXO1lBQ2hELFNBQVMsQ0FBQyxVQUFVLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVO1lBQzlDLFNBQVMsQ0FBQyxTQUFTLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTO1lBQzVDLFNBQVMsQ0FBQyxZQUFZLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsQ0FBQTtJQUNoRSxDQUFDO0lBcENNLDRCQUFZLEdBQUc7UUFDbEIsV0FBVyxFQUFFLENBQUM7S0FDakIsQ0FBQztJQW1DTixzQkFBQztBQUFELENBQUMsQUF2Q0QsQ0FBOEIsS0FBSyxDQUFDLFNBQVMsR0F1QzVDO0FBRUQ7a0JBQWUsZUFBZSxDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnXG5cblxuaW50ZXJmYWNlIFByb3BzIHtcbiAgICBncm91cHNcbiAgICBjYW52YXNXaWR0aFxuICAgIGxpbmVIZWlnaHRcbiAgICBsaW5lQ291bnRcbiAgICBncm91cEhlaWdodHM/XG4gICAgaGVhZGVySGVpZ2h0P1xufVxuXG5cbmNsYXNzIEhvcml6b250YWxMaW5lcyBleHRlbmRzIFJlYWN0LkNvbXBvbmVudDxQcm9wcywgYW55PiB7XG5cbiAgICBzdGF0aWMgZGVmYXVsdFByb3BzID0ge1xuICAgICAgICBib3JkZXJXaWR0aDogMVxuICAgIH07XG5cbiAgICByZW5kZXIgKCkge1xuICAgICAgICBjb25zdCB7bGluZUNvdW50LCBjYW52YXNXaWR0aCwgZ3JvdXBIZWlnaHRzLCBoZWFkZXJIZWlnaHR9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgbGV0IGxpbmVzID0gW107XG5cbiAgICAgICAgdmFyIHRvdGFsSGVpZ2h0ID0gaGVhZGVySGVpZ2h0O1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGxpbmVDb3VudDsgaSsrKSB7XG4gICAgICAgIGxpbmVzLnB1c2goXG4gICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAga2V5PXtgaG9yaXpvbnRhbC1saW5lLSR7aX1gfVxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17aSAlIDIgPT09IDAgPyAncmN0LWhsLWV2ZW4nIDogJ3JjdC1obC1vZGQnfVxuICAgICAgICAgICAgICAgIHN0eWxlPXt7XG4gICAgICAgICAgICAgICAgICAgIHRvcDogYCR7dG90YWxIZWlnaHR9cHhgLFxuICAgICAgICAgICAgICAgICAgICBsZWZ0OiAnMHB4JyxcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IGAke2NhbnZhc1dpZHRofXB4YCxcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiBgJHtncm91cEhlaWdodHNbaV0gLSAxfXB4YFxuICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAvPik7XG4gICAgICAgIHRvdGFsSGVpZ2h0ICs9IGdyb3VwSGVpZ2h0c1tpXVxuICAgIH1cblxuICAgIHJldHVybiAoXG4gICAgICA8ZGl2IGNsYXNzTmFtZT0ncmN0LWhvcml6b250YWwtbGluZXMnPlxuICAgICAgICB7bGluZXN9XG4gICAgICA8L2Rpdj5cbiAgICApXG4gIH1cblxuICAgIHNob3VsZENvbXBvbmVudFVwZGF0ZSAobmV4dFByb3BzLCBuZXh0U3RhdGUpIHtcbiAgICAgICAgcmV0dXJuICEobmV4dFByb3BzLmNhbnZhc1dpZHRoID09PSB0aGlzLnByb3BzLmNhbnZhc1dpZHRoICYmXG4gICAgICAgICAgICAgICAgIG5leHRQcm9wcy5saW5lSGVpZ2h0ID09PSB0aGlzLnByb3BzLmxpbmVIZWlnaHQgJiZcbiAgICAgICAgICAgICAgICAgbmV4dFByb3BzLmxpbmVDb3VudCA9PT0gdGhpcy5wcm9wcy5saW5lQ291bnQgJiZcbiAgICAgICAgICAgICAgICAgbmV4dFByb3BzLmdyb3VwSGVpZ2h0cyA9PT0gdGhpcy5wcm9wcy5ncm91cEhlaWdodHMpXG4gICAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBIb3Jpem9udGFsTGluZXNcbiJdfQ==

/***/ },
/* 17 */
/*!**************************************!*\
  !*** ./src/lib/lines/today-line.tsx ***!
  \**************************************/
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var React = __webpack_require__(/*! react */ 2);
	var TodayLine = (function (_super) {
	    __extends(TodayLine, _super);
	    function TodayLine() {
	        _super.apply(this, arguments);
	    }
	    // TODO: should currentTime come from a prop? probably...?
	    TodayLine.prototype.render = function () {
	        var currentTime = new Date().getTime();
	        if (currentTime > this.props.canvasTimeStart && currentTime < this.props.canvasTimeEnd) {
	            var ratio = this.props.canvasWidth / (this.props.canvasTimeEnd - this.props.canvasTimeStart);
	            var left = Math.round((currentTime - this.props.canvasTimeStart) * ratio);
	            var top_1 = this.props.headerHeight;
	            var height = this.props.height - this.props.headerHeight;
	            var styles = {
	                top: top_1 + "px",
	                left: left + "px",
	                height: height + "px"
	            };
	            return React.createElement("div", {className: 'rct-today', style: styles});
	        }
	        else {
	            return React.createElement("div", null);
	        }
	    };
	    return TodayLine;
	}(React.Component));
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.default = TodayLine;
	//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9kYXktbGluZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInRvZGF5LWxpbmUudHN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLElBQWEsS0FBSyxXQUFNLE9BR3hCLENBQUMsQ0FIOEI7QUFjL0I7SUFBd0IsNkJBQTJCO0lBQW5EO1FBQXdCLDhCQUEyQjtJQXFCbkQsQ0FBQztJQXBCRywwREFBMEQ7SUFDMUQsMEJBQU0sR0FBTjtRQUNJLElBQUksV0FBVyxHQUFHLElBQUksSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFLENBQUM7UUFFdkMsRUFBRSxDQUFDLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7WUFDckYsSUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQy9GLElBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsR0FBRyxLQUFLLENBQUMsQ0FBQztZQUM1RSxJQUFNLEtBQUcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQztZQUNwQyxJQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQztZQUMzRCxJQUFNLE1BQU0sR0FBRztnQkFDWCxHQUFHLEVBQUssS0FBRyxPQUFJO2dCQUNmLElBQUksRUFBSyxJQUFJLE9BQUk7Z0JBQ2pCLE1BQU0sRUFBSyxNQUFNLE9BQUk7YUFDeEIsQ0FBQztZQUVGLE1BQU0sQ0FBQyxxQkFBQyxHQUFHLElBQUMsU0FBUyxFQUFDLFdBQVcsRUFBQyxLQUFLLEVBQUUsTUFBTyxFQUFHLENBQUE7UUFDdkQsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osTUFBTSxDQUFDLHFCQUFDLEdBQUcsUUFBRyxDQUFBO1FBQ2xCLENBQUM7SUFDTCxDQUFDO0lBQ0wsZ0JBQUM7QUFBRCxDQUFDLEFBckJELENBQXdCLEtBQUssQ0FBQyxTQUFTLEdBcUJ0QztBQUVEO2tCQUFlLFNBQVMsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzICBSZWFjdCBmcm9tICdyZWFjdCdcblxuXG5pbnRlcmZhY2UgUHJvcHMge1xuICAgIGNhbnZhc1RpbWVTdGFydDogbnVtYmVyXG4gICAgY2FudmFzVGltZUVuZDogbnVtYmVyXG4gICAgY2FudmFzV2lkdGg6IG51bWJlclxuICAgIGxpbmVIZWlnaHQ6IG51bWJlclxuICAgIGxpbmVDb3VudDogbnVtYmVyXG4gICAgaGVhZGVySGVpZ2h0OiBudW1iZXJcbiAgICBoZWlnaHQ6IG51bWJlclxufVxuXG5cbmNsYXNzIFRvZGF5TGluZSBleHRlbmRzIFJlYWN0LkNvbXBvbmVudDxQcm9wcywgYW55PiB7XG4gICAgLy8gVE9ETzogc2hvdWxkIGN1cnJlbnRUaW1lIGNvbWUgZnJvbSBhIHByb3A/IHByb2JhYmx5Li4uP1xuICAgIHJlbmRlciAoKSB7XG4gICAgICAgIGxldCBjdXJyZW50VGltZSA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpO1xuXG4gICAgICAgIGlmIChjdXJyZW50VGltZSA+IHRoaXMucHJvcHMuY2FudmFzVGltZVN0YXJ0ICYmIGN1cnJlbnRUaW1lIDwgdGhpcy5wcm9wcy5jYW52YXNUaW1lRW5kKSB7XG4gICAgICAgICAgICBjb25zdCByYXRpbyA9IHRoaXMucHJvcHMuY2FudmFzV2lkdGggLyAodGhpcy5wcm9wcy5jYW52YXNUaW1lRW5kIC0gdGhpcy5wcm9wcy5jYW52YXNUaW1lU3RhcnQpO1xuICAgICAgICAgICAgY29uc3QgbGVmdCA9IE1hdGgucm91bmQoKGN1cnJlbnRUaW1lIC0gdGhpcy5wcm9wcy5jYW52YXNUaW1lU3RhcnQpICogcmF0aW8pO1xuICAgICAgICAgICAgY29uc3QgdG9wID0gdGhpcy5wcm9wcy5oZWFkZXJIZWlnaHQ7XG4gICAgICAgICAgICBjb25zdCBoZWlnaHQgPSB0aGlzLnByb3BzLmhlaWdodCAtIHRoaXMucHJvcHMuaGVhZGVySGVpZ2h0O1xuICAgICAgICAgICAgY29uc3Qgc3R5bGVzID0ge1xuICAgICAgICAgICAgICAgIHRvcDogYCR7dG9wfXB4YCxcbiAgICAgICAgICAgICAgICBsZWZ0OiBgJHtsZWZ0fXB4YCxcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IGAke2hlaWdodH1weGBcbiAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgIHJldHVybiA8ZGl2IGNsYXNzTmFtZT0ncmN0LXRvZGF5JyBzdHlsZT17c3R5bGVzfSAvPlxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIDxkaXYgLz5cbiAgICAgICAgfVxuICAgIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgVG9kYXlMaW5lXG4iXX0=

/***/ }
/******/ ])
});
;
//# sourceMappingURL=react-calendar-timeline.js.map