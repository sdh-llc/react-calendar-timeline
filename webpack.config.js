/*jslint browser: true, indent: 4, plusplus: true, vars: true*/
/*global module, __dirname, require*/
/*global alert: false, console: false, WSH: false*/

var webpack = require('webpack');
var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    cache: true,
    context: __dirname,
    entry: {
        'react-calendar-timeline': './src/index.ts'
    },
    output: {
        path: path.join(__dirname, 'build/dist/'),
        filename: '[name].js',
        publicPath: 'build/dist/',
        library: 'ReactCalendarTimeline',
        libraryTarget: 'umd',
        umdNamedDefine: true
    },
    module: {
        loaders: [
            {
                test: /\.tsx?$/,
                loader: 'ts-loader'
            },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract('style-loader', 'css-loader')
            },
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract('style-loader', 'css-loader!sass-loader')
            }
        ]
    },
    plugins: [
        new webpack.NoErrorsPlugin(),
        new ExtractTextPlugin('[name].css')
    ],
    externals: {
        'react': {
            root: 'React',
            commonjs: 'react',
            commonjs2: 'react',
            amd: 'react'
        },
        'react-dom': {
            root: 'ReactDOM',
            commonjs: 'react-dom',
            commonjs2: 'react-dom',
            amd: 'react-dom'
        },
        'moment': 'moment',
        'interact.js': {
            root: 'interact',
            commonjs: 'interact.js',
            commonjs2: 'interact.js',
            amd: 'interact.js'
        }
    },
    resolve: {
        extensions: ['', '.js', '.jsx', '.ts', '.tsx', '.scss', '.md']
    }
};
