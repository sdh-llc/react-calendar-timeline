import * as React from 'react'
import * as interact from 'interact.js'
import * as moment from 'moment'

import {_get} from '../utils'


interface Props {
    groupHeights?
    groupTops?
    keys?
    selected?
    item?
    canvasTimeStart?
    canvasTimeEnd?
    canvasWidth?
    lineHeight?
    order?
    dragSnap?
    minResizeWidth?
    canChangeGroup?
    topOffset?
    canMove?
    canResize?
    dimensions?
    useResizeHandle?
    moveResizeValidator?: (eventType, item, time) => void
    onDrag?: (id, time, order) => void
    onDrop?: (id, time, order) => void
    onResizing?: (id, newResizeEnd) => void
    onResized?: (itemId, newResizeEnd) => void
    onItemDoubleClick?: (itemId, e) => void
    onContextMenu?: (itemId, e) => void
    onSelect?: (itemId, clickType, e) => void

    hideClippedTitle?: boolean
}


export default class Item extends React.Component<Props, any> {
    static defaultProps = {
        selected: false,
        hideClippedTitle: true,
    };

    itemId;
    itemTitle;
    itemTitleHTML;
    itemDivTitle;
    itemTimeStart;
    itemTimeEnd;
    startedClicking;
    startedTouching;

    refs: {
        [key: string]: any
        dragRight
        item
    };

  constructor (props) {
    super(props);

    this.cacheDataFromProps(props);

    this.state = {
      interactMounted: false,

      dragging: null,
      dragStart: null,
      preDragPosition: null,
      dragTime: null,
      dragGroupDelta: null,

      resizing: null,
      resizeStart: null,
      resizeTime: null,

      tooltip: false,
      contentIsHidden: false,
    }
  }

  shouldComponentUpdate (nextProps, nextState) {
    var shouldUpdate = nextState.dragging !== this.state.dragging ||
                       nextState.dragTime !== this.state.dragTime ||
                       nextState.dragGroupDelta !== this.state.dragGroupDelta ||
                       nextState.resizing !== this.state.resizing ||
                       nextState.resizeTime !== this.state.resizeTime ||
                       nextProps.keys !== this.props.keys ||
                       nextProps.selected !== this.props.selected ||
                       nextProps.item !== this.props.item ||
                       nextProps.canvasTimeStart !== this.props.canvasTimeStart ||
                       nextProps.canvasTimeEnd !== this.props.canvasTimeEnd ||
                       nextProps.canvasWidth !== this.props.canvasWidth ||
                       nextProps.lineHeight !== this.props.lineHeight ||
                       nextProps.order !== this.props.order ||
                       nextProps.dragSnap !== this.props.dragSnap ||
                       nextProps.minResizeWidth !== this.props.minResizeWidth ||
                       nextProps.selected !== this.props.selected ||
                       nextProps.canChangeGroup !== this.props.canChangeGroup ||
                       nextProps.topOffset !== this.props.topOffset ||
                       nextProps.canMove !== this.props.canMove ||
                       nextProps.canResize !== this.props.canResize ||
                       nextProps.dimensions !== this.props.dimensions ||
                       nextState.tooltip !== this.state.tooltip ||
                       nextState.contentIsHidden !== this.state.contentIsHidden;
    return shouldUpdate
  }

  cacheDataFromProps (props) {
    this.itemId = _get(props.item, props.keys.itemIdKey);
    this.itemTitle = _get(props.item, props.keys.itemTitleKey);
    this.itemTitleHTML = _get(props.item, props.keys.itemTitleHTMLKey);
    this.itemDivTitle = props.keys.itemDivTitleKey ? _get(props.item, props.keys.itemDivTitleKey) : this.itemTitle;
    this.itemTimeStart = _get(props.item, props.keys.itemTimeStartKey);
    this.itemTimeEnd = _get(props.item, props.keys.itemTimeEndKey)
  }

  coordinateToTimeRatio (props = this.props) {
    return (props.canvasTimeEnd - props.canvasTimeStart) / props.canvasWidth
  }

  dragTimeSnap (dragTime, considerOffset?) {
    const { dragSnap } = this.props
    if (dragSnap) {
      const offset = considerOffset ? moment().utcOffset() * 60 * 1000 : 0
      return Math.round(dragTime / dragSnap) * dragSnap - offset % dragSnap
    } else {
      return dragTime
    }
  }

  resizeTimeSnap (dragTime) {
    const { dragSnap } = this.props
    if (dragSnap) {
      const endTime = this.itemTimeEnd % dragSnap
      return Math.round((dragTime - endTime) / dragSnap) * dragSnap + endTime
    } else {
      return dragTime
    }
  }

  dragTime (e) {
    const startTime = this.itemTimeStart

    if (this.state.dragging) {
      const deltaX = e.pageX - this.state.dragStart.x
      const timeDelta = deltaX * this.coordinateToTimeRatio()

      return this.dragTimeSnap(startTime + timeDelta, true)
    } else {
      return startTime
    }
  }

  dragGroupDelta (e) {
    const {groupTops, order, topOffset} = this.props
    if (this.state.dragging) {
      if (!this.props.canChangeGroup) {
        return 0
      }
      let groupDelta = 0

      for (var key of Object.keys(groupTops)) {
        var item = groupTops[key]
        if (e.pageY - topOffset > item) {
          groupDelta = parseInt(key, 10) - order
        } else {
          break
        }
      }

      if (this.props.order + groupDelta < 0) {
        return 0 - this.props.order
      } else {
        return groupDelta
      }
    } else {
      return 0
    }
  }

  resizeTimeDelta (e) {
    const length = this.itemTimeEnd - this.itemTimeStart
    const timeDelta = this.dragTimeSnap((e.pageX - this.state.resizeStart) * this.coordinateToTimeRatio())

    if (length + timeDelta < (this.props.dragSnap || 1000)) {
      return (this.props.dragSnap || 1000) - length
    } else {
      return timeDelta
    }
  }

  componentDidMount () {
      this.checkContentSize();
  }

  mountInteract () {
    const rightResize = this.props.useResizeHandle ? this.refs.dragRight : true
    interact(this.refs.item)
      .resizable({
        edges: {left: false, right: rightResize, top: false, bottom: false},
        enabled: this.props.selected && this.canResize()
      })
      .draggable({
        enabled: this.props.selected
      })
      .on('dragstart', (e) => {
        if (this.props.selected) {
          this.setState({
            dragging: true,
            dragStart: {x: e.pageX, y: e.pageY},
            preDragPosition: {x: e.target.offsetLeft, y: e.target.offsetTop},
            dragTime: this.itemTimeStart,
            dragGroupDelta: 0
          })
        } else {
          return false
        }
      })
      .on('dragmove', (e) => {
        if (this.state.dragging) {
          let dragTime = this.dragTime(e)
          let dragGroupDelta = this.dragGroupDelta(e)

          if (this.props.moveResizeValidator) {
            dragTime = this.props.moveResizeValidator('move', this.props.item, dragTime)
          }

          if (this.props.onDrag) {
            this.props.onDrag(this.itemId, dragTime, this.props.order + dragGroupDelta)
          }

          this.setState({
            dragTime: dragTime,
            dragGroupDelta: dragGroupDelta
          })
        }
      })
      .on('dragend', (e) => {
        if (this.state.dragging) {
          if (this.props.onDrop) {
            let dragTime = this.dragTime(e)

            if (this.props.moveResizeValidator) {
              dragTime = this.props.moveResizeValidator('move', this.props.item, dragTime)
            }

            this.props.onDrop(this.itemId, dragTime, this.props.order + this.dragGroupDelta(e))
          }

          this.setState({
            dragging: false,
            dragStart: null,
            preDragPosition: null,
            dragTime: null,
            dragGroupDelta: null
          })
        }
      })
      .on('resizestart', (e) => {
        if (this.props.selected) {
          this.setState({
            resizing: true,
            resizeStart: e.pageX,
            newResizeEnd: 0
          })
        } else {
          return false
        }
      })
      .on('resizemove', (e) => {
        if (this.state.resizing) {
          let newResizeEnd = this.resizeTimeSnap(this.itemTimeEnd + this.resizeTimeDelta(e))

          if (this.props.moveResizeValidator) {
            newResizeEnd = this.props.moveResizeValidator('resize', this.props.item, newResizeEnd)
          }

          if (this.props.onResizing) {
            this.props.onResizing(this.itemId, newResizeEnd)
          }

          this.setState({
            newResizeEnd: newResizeEnd
          })
        }
      })
      .on('resizeend', (e) => {
        if (this.state.resizing) {
          let newResizeEnd = this.resizeTimeSnap(this.itemTimeEnd + this.resizeTimeDelta(e))

          if (this.props.moveResizeValidator) {
            newResizeEnd = this.props.moveResizeValidator('resize', this.props.item, newResizeEnd)
          }

          if (this.props.onResized && this.resizeTimeDelta(e) !== 0) {
            this.props.onResized(this.itemId, newResizeEnd)
          }
          this.setState({
            resizing: null,
            resizeStart: null,
            newResizeEnd: null
          })
        }
      })
      .on('tap', (e) => {
        this.actualClick(e, e.pointerType === 'mouse' ? 'click' : 'touch')
      })

    this.setState({
      interactMounted: true
    })
  }

  canResize (props = this.props) {
    if (!props.canResize) {
      return false
    }
    let width = parseInt(this.props.dimensions.width, 10)
    return width >= props.minResizeWidth
  }

  canMove (props = this.props) {
    return !!props.canMove
  }

  componentWillReceiveProps (nextProps) {
    this.cacheDataFromProps(nextProps)

    let { interactMounted } = this.state
    const couldDrag = this.props.selected && this.canMove(this.props)
    const couldResize = this.props.selected && this.canResize(this.props)
    const willBeAbleToDrag = nextProps.selected && this.canMove(nextProps)
    const willBeAbleToResize = nextProps.selected && this.canResize(nextProps)

    if (nextProps.selected && !interactMounted) {
      this.mountInteract()
      interactMounted = true
    }

    if (interactMounted && couldResize !== willBeAbleToResize) {
      interact(this.refs.item)
        .resizable({enabled: willBeAbleToResize})
    }
    if (interactMounted && couldDrag !== willBeAbleToDrag) {
      interact(this.refs.item)
        .draggable({enabled: willBeAbleToDrag})
    }

    if (!nextProps.selected && this.props.selected) {
        this.setState({tooltip: false});
        this.checkContentSize();
    }
  }

  onMouseDown = (e) => {
    if (!this.state.interactMounted) {
      e.preventDefault()
      this.startedClicking = true
    }
  };

  onMouseUp = (e) => {
    if (!this.state.interactMounted && this.startedClicking) {
      this.startedClicking = false
      this.actualClick(e, 'click')
    }
  };

  onTouchStart = (e) => {
    if (!this.state.interactMounted) {
      e.preventDefault()
      this.startedTouching = true
    }
  };

  onTouchEnd = (e) => {
    if (!this.state.interactMounted && this.startedTouching) {
      this.startedTouching = false
      this.actualClick(e, 'touch')
    }
  };

  handleDoubleClick = (e) => {
    e.preventDefault()
    e.stopPropagation()
    if (this.props.onItemDoubleClick) {
      this.props.onItemDoubleClick(this.itemId, e)
    }
  };

  handleContextMenu = (e) => {
    if (this.props.onContextMenu) {
      e.preventDefault()
      e.stopPropagation()
      this.props.onContextMenu(this.itemId, e)
    }
  };

  actualClick (e, clickType) {
    if (this.props.onSelect) {
      this.props.onSelect(this.itemId, clickType, e)
    }
  }

  handleClick = (ev) => {
    ev.preventDefault();
    ev.stopPropagation();
    this.setState({tooltip: !this.state.tooltip});
  };

  determineTooltipPosition = () => {
      if (!this.refs['item']) {
          return 'top';
      }
      return this.refs['item'].offsetTop > 100 ? 'top' : 'bottom';
  };

  checkContentSize = () => {
      const el = this.refs['content'];
      if (!el) {
          return false;
      }
      if (this.props.hideClippedTitle) {
          const textIsClipped = el.scrollWidth > el.clientWidth;
          if (textIsClipped && !this.state.contentIsHidden) {
              this.setState({contentIsHidden: true});
          } else if (!textIsClipped && this.state.contentIsHidden) {
              this.setState({contentIsHidden: false});
          }
      }
  };

  render () {
    const dimensions = this.props.dimensions;
    if (typeof this.props.order === 'undefined' || this.props.order === null) {
      return null
    }

    const classNames = 'rct-item' +
                       (this.props.selected ? ' selected' : '') +
                       (this.canMove(this.props) ? ' can-move' : '') +
                       (this.canResize(this.props) ? ' can-resize' : '') +
                       (this.props.item.className ? ` ${this.props.item.className}` : '');

    const style = {
      left: `${dimensions.left}px`,
      top: `${dimensions.top}px`,
      width: `${dimensions.width}px`,
      height: `${dimensions.height}px`,
      // lineHeight: `${dimensions.height}px`
    };
    if (this.props.item.color) {
        style['backgroundColor'] = this.props.item.color;
    }

    return (
      <div key={this.itemId}
           ref='item'
           className={classNames}
           title={this.itemDivTitle}
           onMouseDown={this.onMouseDown}
           onMouseUp={this.onMouseUp}
           onTouchStart={this.onTouchStart}
           onTouchEnd={this.onTouchEnd}
           onDoubleClick={this.handleDoubleClick}
           onContextMenu={this.handleContextMenu}
           style={style}>
        <div
            title={this.itemDivTitle}
            className={`
                rct-item-tooltip
                ${this.state.tooltip ? 'active' : ''}
                ${this.determineTooltipPosition()}
            `}
            style={{
                height: `${dimensions.height}px`,
            }}
            onClick={this.handleClick}
        >^</div>
        <style dangerouslySetInnerHTML={{
            __html: `
                .rct-item-tooltip.top:before {
                    bottom: ${dimensions.height - 1}px;
                }
                .rct-item-tooltip.top:after {
                    bottom: ${dimensions.height + 5}px;
                }

                .rct-item-tooltip.bottom:after {
                    top: ${dimensions.height + 5}px;
                }
            `
        }} />
        <div className='rct-item-overflow'>
          <div ref='content' className='rct-item-content' style={{
            visibility: this.state.contentIsHidden ? 'hidden' : 'visible'
          }}>
            {this.itemTitleHTML ? (
                <div dangerouslySetInnerHTML={{__html: this.itemTitleHTML}} />
            ) : (
                this.itemTitle
            )}
          </div>
        </div>
        { this.props.useResizeHandle ? <div ref='dragRight' className='rct-drag-right'></div> : '' }
      </div>
    )
  }
}
