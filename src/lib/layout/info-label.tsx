import * as React from 'react'
// import shouldPureComponentUpdate from 'react-pure-render/function'


interface Props {
  label: string
}


export default class InfoLabel extends React.Component<Props, any> {
  // shouldComponentUpdate = shouldPureComponentUpdate;

    static defaultProps = {
        label: ''
    };

  render () {
    return (
      <div className='rct-infolabel'>
        {this.props.label}
      </div>
    )
  }
}
